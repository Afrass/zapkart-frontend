export const paginationController = (
  currentPage,
  limit,
  products,
  showPagination,
  pagination
) => {
  // const startIndex = currentPage * limit - limit
  // const endIndex = startIndex + limit

  // console.log(startIndex, endIndex)

  // const getPaginatedProducts = products.slice(startIndex, endIndex)

  let start = Math.floor((currentPage - 1) / showPagination) * showPagination
  let end = start + showPagination
  const getPaginationGroup = pagination.slice(start, end)
  console.log(getPaginationGroup, 'showwwswq')

  return {
    // getPaginatedProducts,
    getPaginationGroup,
  }
}

export const cratePagination = (
  totalProductTemplateCount,
  limit,
  setPagination,
  setPages
) => {
  // set pagination
  let arr = new Array(Math.ceil(totalProductTemplateCount / limit))
    .fill()
    .map((_, idx) => idx + 1)

  setPagination(arr)
  setPages(Math.ceil(totalProductTemplateCount / limit))
}
