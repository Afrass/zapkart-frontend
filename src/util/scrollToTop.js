const scrollToTop = () => {
  window.scrollTo({ top: 0, behavior: 'smooth' })
}

//   const scrollToBottom = () => {
//     window.scrollTo(
//       200,
//       document.body.scrollHeight || document.documentElement.scrollHeight
//     )
//   }
export default scrollToTop
