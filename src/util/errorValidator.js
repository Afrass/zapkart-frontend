/* eslint-disable no-unused-vars */
import { toast } from 'react-toastify'

export const errorValidator = (res) => {
  console.log('my-res', res)
  if (res) {
    if (res?.errors) {
      for (const [key, value] of Object.entries(res?.errors)) {
        value.forEach((cur) => {
          toast.error(`${key}: ${cur}`)
        })
      }
    } else {
      if (res.status !== 401) {
        toast.error(res.title)
      }
    }
  }
}
