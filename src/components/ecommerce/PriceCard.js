/* eslint-disable react/prop-types */
import React from 'react'
import { useSelector } from 'react-redux'

const PriceCard = ({
  addToCart,
  addToWishlist,
  quantityCounter,
  setQuantityCounter,
  type,
  extraClass = '',
}) => {
  const { product, productTemplate } = useSelector(
    (state) => state.productTemplate
  )
  return (
    <div
      className={`pricing-info mt-10 ${extraClass}`}
      style={{ padding: (!product?.price || product?.qty === 0) && 20 }}
    >
      <>
        {product?.price && product?.qty > 0 ? (
          <>
            <div className="d-flex align-items-center flex-wrap detail-info">
              <h2 className="title-detail mr-5">₹{product?.price}</h2>
              <span className="price-old mr-5">₹{product?.mrpPrice}</span>
              <span className="offer-percentage">
                {' '}
                {Math.floor(
                  ((product?.mrpPrice - product?.price) / product?.mrpPrice) *
                    100
                )}{' '}
                % Off
              </span>
            </div>
            <p
              style={{
                color: 'black',
                fontSize: '13px',
                marginBottom: 0,
              }}
            >
              Inclusive of all taxes
            </p>

            <p style={{ color: 'black' }}>
              {productTemplate?.medicinePackaging}
            </p>

            {/* <div className="bt-1 border-color-1 mt-30 mb-30"></div> */}
            {product?.price && (
              <div className="detail-extralink mt-10">
                <div className="detail-qty border radius">
                  <a
                    href="#/"
                    onClick={() => {
                      // product?.minOrderQty
                      quantityCounter > 1 &&
                        setQuantityCounter(quantityCounter - 1)
                    }}
                    className="qty-down"
                  >
                    <i className="fi-rs-angle-small-down"></i>
                  </a>
                  <span className="qty-val">{quantityCounter}</span>
                  <a
                    href="#/"
                    onClick={() => {
                      product.allowedQuantityPerOrder > quantityCounter &&
                        setQuantityCounter(quantityCounter + 1)
                    }}
                    className="qty-up"
                  >
                    <i className="fi-rs-angle-small-up"></i>
                  </a>
                </div>

                <div className="product-extra-link2">
                  <button
                    onClick={() => addToCart(product)}
                    className="button button-add-to-cart"
                  >
                    Add to cart
                  </button>
                  {type !== 'medicine' && (
                    <a
                      href="#/"
                      aria-label="Add To Wishlist"
                      className="action-btn hover-up"
                      // onClick={(e) => handleWishlist(product)}
                      onClick={() => addToWishlist(product)}
                    >
                      <i className="fi-rs-heart"></i>
                    </a>
                  )}
                </div>
              </div>
            )}
          </>
        ) : (
          <h4>Out Of Stock</h4>
        )}
      </>
    </div>
  )
}

export default PriceCard
