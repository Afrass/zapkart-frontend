/* eslint-disable react/prop-types */
import React from 'react'

const MedicineProductMainDetails = ({ productTemplate }) => {
  console.log(productTemplate, 'plsjlb')
  return (
    <div className="short-desc mb-20" style={{ marginTop: '-15px' }}>
      <div className="mt-4">
        {productTemplate?.manufacturer?.name && (
          <>
            <h5>Manufacturer</h5>
            <p>{productTemplate?.manufacturer?.name}</p>
          </>
        )}

        {productTemplate?.composition?.length > 0 && (
          <>
            <h5>Composition</h5>
            <p style={{ fontSize: 13 }}>
              {productTemplate.composition?.length > 0 &&
                productTemplate.composition?.map((cur, index) =>
                  index === productTemplate.composition?.length - 1 ? (
                    <span key={cur.id}>{`${cur?.name}(${cur?.qty})`}</span>
                  ) : (
                    <span key={cur.id}>{`${cur?.name}(${cur?.qty})+ `}</span>
                  )
                )}
            </p>
          </>
        )}
        {productTemplate?.storageTemperature && (
          <>
            <h5>Storage</h5>
            <p>{productTemplate?.storageTemperature}</p>
          </>
        )}

        {productTemplate?.substitutes?.length > 0 && (
          <a href="#substitutes" className="link">
            View Substitutes
          </a>
        )}
      </div>
    </div>
  )
}

export default MedicineProductMainDetails
