/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import React from 'react'
import { ListGroup } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import { toast } from 'react-toastify'
import { addToCartSlice } from 'redux/slices/cartSlice'

const OtherSellers = ({ otherClass = '' }) => {
  const { authorized } = useSelector((state) => state.auth)
  const dispatch = useDispatch()
  const navigate = useNavigate()

  const { otherProductsFromVariant } = useSelector(
    (state) => state.productTemplate
  )

  const addToCart = async (product) => {
    if (authorized) {
      dispatch(addToCartSlice(product.id, 1))
    } else {
      toast.warning('You have to login before adding to cart')
      navigate('/Login')
    }
  }

  return (
    <div className={otherClass}>
      {otherProductsFromVariant?.length > 0 && (
        <div
          className="detail-info"
          style={{
            padding: '20px',
            border: '1px solid #ddd',
            borderBottomStyle: 'none',
          }}
        >
          <h2 className="title-detail text-center" style={{ fontSize: '22px' }}>
            Other Sellers
          </h2>
        </div>
      )}
      <ListGroup as="ol" numbered>
        {otherProductsFromVariant?.map((product, i) => (
          <ListGroup.Item
            key={i}
            as="li"
            className="d-flex justify-content-between align-items-center p-20"
          >
            <div className="ms-2 me-auto">
              <div className="d-flex align-items-center detail-info flex-wrap">
                <h2 className="title-detail mr-5">₹{product?.price}</h2>
                <span className="price-old mr-5">₹{product?.mrpPrice}</span>
                <span className="offer-percentage">
                  {' '}
                  {Math.floor(
                    ((product?.mrpPrice - product?.price) / product?.mrpPrice) *
                      100
                  )}{' '}
                  % Off
                </span>
              </div>
              Seller: {product.username}
            </div>
            <button
              onClick={() => addToCart(product)}
              className="button button-add-to-cart"
            >
              Add to cart
            </button>
          </ListGroup.Item>
        ))}
      </ListGroup>
    </div>
  )
}

export default OtherSellers
