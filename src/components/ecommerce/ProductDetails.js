/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import React, { useState } from 'react'
import { toast } from 'react-toastify'
// import {
//   addToCart,
//   decreaseQuantity,
//   increaseQuantity,
// } from '../../redux/action/cart'
// import { addToCompare } from '../../redux/action/compareAction'
// import { addToWishlist } from '../../redux/action/wishlistAction'
import ProductTab from '../common/ProductTab'
import RelatedSlider from '../sliders/RelatedSlider'
import ThumbSlider from '../sliders/ThumbSlider'
import { useNavigate } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { useEffect } from 'react'
import cartService from 'service/cart'
import { onProductVariantChange } from 'redux/slices/productTemplateSlice'
import { Badge, Tag } from 'antd'
import { ListGroup } from 'react-bootstrap'
import { addToCartSlice } from 'redux/slices/cartSlice'
import { addToWishlistSlice } from 'redux/slices/wishlistSlice'
import orderService from 'service/order'
import NoImage from 'assets/imgs/shop/no-image.svg'
import prescriptionIcon from 'assets/imgs/shop/prescription.png'
import PriceCard from './PriceCard'
import OtherSellers from './OtherSellers'
import CheckIfDeliverable from 'components/common/CheckIfDeliverable'
import MedicineProductMainDetails from './MedicineProductMainDetails'

const ProductDetails = ({ product, productTemplate, type = 'nonMedicine' }) => {
  const [quantityCounter, setQuantityCounter] = useState(1)
  const [selectedVariantId, setSelectedVariantId] = useState(null)
  const [selectedBulkPriceQty, setSelectedBulkPriceQty] = useState(null)

  const navigate = useNavigate()
  const { authorized } = useSelector((state) => state.auth)
  const dispatch = useDispatch()

  useEffect(() => {
    if (product?.variant) {
      setSelectedVariantId(product?.variant.id)
    }
  }, [product?.variant])

  const addToCart = async (product) => {
    if (authorized) {
      dispatch(addToCartSlice(product.id, quantityCounter))
    } else {
      toast.warning('You have to login before adding to cart')
      navigate('/Login')
    }
  }

  const handleCart = (product) => {
    // addToCart(product)
    toast('Product added to Cart !')
  }

  const handleCompare = (product) => {
    // addToCompare(product)
    toast('Added to Compare list !')
  }

  const handleWishlist = (product) => {
    // addToWishlist(product)
    toast('Added to Wishlist !')
  }

  const inCart = true
  //   cartItems.find((cartItem) => cartItem.id === product.id)

  const addToWishlist = (product) => {
    if (authorized) {
      dispatch(addToWishlistSlice(product.id))
    } else {
      toast.warning('You have to login before adding to Wishlist')
      navigate('/Login')
    }
  }

  return (
    <>
      <section className="mt-50 product-details-section">
        <div className="container">
          <div className="row flex-row-reverse">
            <div className="col-xl-11 col-lg-12 m-auto">
              <div className="product-detail accordion-detail">
                <div className="row mb-20  mt-30">
                  <div className="col-lg-4 col-md-12 col-sm-12 col-xs-12 mb-md-0 mb-sm-5">
                    <div className="detail-gallery">
                      {/* <span className="zoom-icon">
                        <i className="fi-rs-search"></i>
                      </span> */}

                      <div
                        className="product-image-slider"
                        // style={{
                        //   border:
                        //     type === 'medicine' ? 'none' : '1px solid #ececec',
                        // }}
                      >
                        <ThumbSlider
                          images={
                            product?.variant?.images?.length > 0
                              ? product.variant.images
                              : product?.images?.length > 0
                              ? product?.images
                              : productTemplate?.images?.length > 0
                              ? productTemplate?.images
                              : [NoImage]
                          }
                          isImgAvailable={
                            product?.variant?.images?.length > 0
                              ? true
                              : product?.images?.length > 0
                              ? true
                              : productTemplate?.images?.length > 0
                              ? true
                              : false
                          }
                          type={type}
                        />
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                    <div className="detail-info  main-detailed-info-cont">
                      {/* out-stock */}
                      {/* <span className="stock-status">
            
                          {product?.qty > 0 ? 'InStock' : 'OutOfStock'}
                        </span> */}
                      {/* <div className="d-flex justify-content-end align-items-center mt-20">
                 
                        <form
                          onSubmit={checkIfDeliverableHandler}
                          className="d-flex align-items-center mb-20"
                        >
                          <div
                            style={{
                              transform: 'translateY(-15px)',
                              position: 'relative',
                            }}
                          >
                            <label>Check Deliverable?</label>
                            <input
                              placeholder="Enter Zipcode"
                              className="form-control mr-10"
                              onChange={(e) => setZipCode(e.target.value)}
                              value={zipCode}
                            />
                            <div className="deliverable-container">
                              {deliverable ? (
                                <span className="deliverable">Deliverable</span>
                              ) : deliverable === false ? (
                                <span className="not-deliverable">
                                  Not Deliverable
                                </span>
                              ) : null}
                            </div>
                          </div>

                          <button
                            type="submit"
                            className="button button-add-to-cart ml-5"
                          >
                            Check
                          </button>
                        </form>
                      </div> */}

                      {product?.variant?.name ? (
                        <h2 className="title-detail">
                          {product?.variant?.name}
                        </h2>
                      ) : product?.name ? (
                        <h2 className="title-detail">{product?.name}</h2>
                      ) : (
                        <h2 className="title-detail">
                          {productTemplate?.name}
                        </h2>
                      )}

                      {(product.prescriptionRequired ||
                        productTemplate?.prescriptionRequired) && (
                        <div className="d-flex align-items-center mt-5">
                          <img
                            src={prescriptionIcon}
                            alt="prescription"
                            className="mr-5"
                          />
                          <span>Prescription Required</span>
                        </div>
                      )}

                      {/* Price card only show in mobile */}
                      <PriceCard
                        addToCart={addToCart}
                        addToWishlist={addToWishlist}
                        quantityCounter={quantityCounter}
                        setQuantityCounter={setQuantityCounter}
                        type={type}
                        extraClass="show-only-mobile"
                      />

                      {/* {product?.brand?.name && (
                        <div className="mt-3">
                          <h5>Brand</h5>
                          <p>{productTemplate?.brand?.name}</p>
                        </div>
                      )} */}
                      {productTemplate?.productType !== 'Medicine' &&
                        productTemplate?.manufacturer?.name && (
                          <div className="mt-10">
                            {/* <h5>Category</h5> */}
                            <p>{productTemplate?.manufacturer?.name}</p>
                          </div>
                        )}
                      {productTemplate?.productType !== 'Medicine' &&
                        productTemplate?.category?.name && (
                          <div className="mt-10 mb-10">
                            {/* <h5>Category</h5> */}
                            <p>{productTemplate?.category?.name}</p>
                          </div>
                        )}

                      {/* {product?.variant?.attributes ? (
                        <h2 className="title-detail">
                          {product?.variant?.name}
                        </h2>
                      ) : (
                        <h2 className="title-detail">{product?.name}</h2>
                      )} */}

                      {/* <div className="product-detail-rating">
                        <div className="product-rate-cover text-end">
                          <div className="product-rate d-inline-block">
                            <div
                              className="product-rating"
                              style={{
                                width: `${100 / 50}%`,
                              }}
                            ></div>
                          </div>
                          <span className="font-small ml-5 text-muted">
                            {' '}
                            ({product?.total_rating_count} Ratings)
                          </span>
                        </div>
                      </div> */}

                      {/* Pricinggg */}

                      {/* {product?.price ? (
                        <div className="clearfix product-price-cover">
                          <div className="product-price primary-color float-left">
                            <span className="current-price  text-brand">
                              ₹{product?.price}
                            </span>
                            <span>
                              <span className="save-price font-md color3 ml-15">
                                {Math.floor(
                                  ((product?.mrpPrice - product?.price) /
                                    product?.mrpPrice) *
                                    100
                                )}{' '}
                                % Off
                              </span>
                              <span className="old-price font-md ml-15">
                                ₹{product?.mrpPrice}
                              </span>
                            </span>
                          </div>
                        </div>
                      ) : (
                  
                        <Tag
                          color="red"
                          style={{ padding: '10px', marginBottom: '20px' }}
                        >
                          Out Of Stock
                        </Tag>
                      )} */}

                      {/* <span className="stock-status">
                        OutOfStock
                       </span> */}

                      {/* <div className="short-desc mb-30">
                        <p className="font-lg">
           
                          <div
                            dangerouslySetInnerHTML={{
                              __html: product?.description,
                            }}
                          ></div>
                        </p>
                      </div> */}
                      {type === 'medicine' && (
                        <MedicineProductMainDetails
                          productTemplate={productTemplate}
                        />
                      )}

                      {/* <div className="attr-detail attr-color mb-15"> */}
                      {/* <strong className="mr-10">Color</strong> */}
                      {/* <ul className="list-filter color-filter">
                          {product?.variations?.map((clr, i) => (
                            <li key={i}>
                              <a href="#/">
                                <span className={`product-color-${clr}`}></span>
                              </a>
                            </li>
                          ))}
                        </ul> */}
                      {/* </div> */}

                      <div className="attr-detail attr-size">
                        {/* {productTemplate?.variants?.length > 0 && (
                          <strong className="mr-10">Variants</strong>
                        )} */}
                        <ul className="list-filter size-filter font-small">
                          {productTemplate?.variants?.map((variant, i) => (
                            <li
                              key={variant.id}
                              className={
                                variant.id === selectedVariantId && 'active'
                              }
                              onClick={() => {
                                dispatch(onProductVariantChange(variant.id))
                                setSelectedVariantId(variant.id)
                              }}
                            >
                              <a>
                                {variant?.displayName}
                                {/* <br />
                              ₹{product?.price} */}
                              </a>
                            </li>
                          ))}
                        </ul>
                      </div>

                      {productTemplate?.productType !== 'Medicine' && (
                        <div
                          className={
                            product?.bulkPrice?.length > 0 ? 'mt-20' : ''
                          }
                        >
                          {product?.bulkPrice?.length > 0 && (
                            <h6 className="mb-10">Bulk Price</h6>
                          )}
                          <ul className="list-filter size-filter font-small">
                            {product?.bulkPrice?.map((item, i) => (
                              <li
                                style={{ marginBottom: '10px' }}
                                key={item.id}
                                className={
                                  item.qty === selectedBulkPriceQty && 'active'
                                }
                                onClick={() => {
                                  setSelectedBulkPriceQty(item.qty)
                                  setQuantityCounter(item.qty)
                                }}
                              >
                                <a>
                                  <span className="mr-20">Qty{item?.qty}.</span>{' '}
                                  <span>₹{item?.price}</span>
                                  {/* <br />
                                ₹{product?.price} */}
                                </a>
                              </li>
                            ))}
                          </ul>
                        </div>
                      )}

                      {/* {product?.bulkPrice?.length > 0 && (
                        <div className='mt-20'>
                        <h6 className='mb-10'>Bulk Price</h6>
                        
                          <div className="static-box-details">  
                          {
                            product.bulkPrice.map((item, i) => (
                              <div key={i} className='box-details'>
                                <span className='mr-20'>Qty{item?.qty}.</span> <span>₹{item?.price}</span>
                            </div>
                            ))
                          }                       
                            
                          </div>
                        </div>
                      )} */}

                      {product?.variant?.attributes?.length > 0 && (
                        <div className="mt-10 mb-10">
                          <h6 className="mb-10">Attributes</h6>
                          <div className="static-box-details">
                            {product?.variant?.attributes.map((item, i) => (
                              <div key={i} className="box-details">
                                <span className="mr-20">{item?.name}</span>{' '}
                                <span>{item?.value?.value}</span>
                              </div>
                            ))}
                          </div>
                        </div>
                      )}

                      {/* <ul className="product-meta font-xs color-grey mt-50">
                        <li className="mb-5">
                          SKU:
                          <a href="#/">FWM15VKT</a>
                        </li>
                        {productTemplate?.tags?.length > 0 && (
                          <li className="mb-5">
                            Tags:
                            {productTemplate?.tags?.map((tag, i) => (
                              <a key={i} href="#/" rel="tag" className="me-1">
                                {tag},
                              </a>
                            ))}
                          </li>
                        )}

                        {product?.price && (
                          <li>
                            Availability:
                            <span className="in-stock text-success ml-5">
                              {product.qty} Items In Stock
                            </span>
                          </li>
                        )}
                      </ul> */}
                    </div>
                  </div>
                  <div className="col-lg-4 col-md-12 col-sm-12 col-xs-12">
                    <CheckIfDeliverable productId={product.id} />

                    {/* Price card only show in Desktop */}
                    <PriceCard
                      addToCart={addToCart}
                      addToWishlist={addToWishlist}
                      quantityCounter={quantityCounter}
                      setQuantityCounter={setQuantityCounter}
                      type={type}
                      extraClass="show-only-desktop"
                    />
                  </div>
                </div>
                <OtherSellers otherClass="other-sellers-mob-view" />

                {type !== 'medicine' && (
                  <div className="row">
                    <div className="col-md-8">
                      <ProductTab
                        product={product}
                        productTemplate={productTemplate}
                      />
                    </div>
                    <div className="col-md-4">
                      {/* Sellers */}
                      <OtherSellers otherClass="other-sellers-desk-view" />
                    </div>

                    {/* <div className="row mt-60">
                      <div className="col-12">
                        <h3 className="section-title style-1 mb-30">
                          Related products
                        </h3>
                      </div>
                      <div className="col-12">
                        <div className="row related-products position-relative">
                          <RelatedSlider />
                        </div>
                      </div>
                    </div> */}
                  </div>
                )}
                {/* 
                <div className="row mt-60">
                  <div className="col-12">
                    <h3 className="section-title style-1 mb-30">
                      Related products
                    </h3>
                  </div>
                  <div className="col-12">
                    <div className="row related-products position-relative">
                      <RelatedSlider />
                    </div>
                  </div>
                </div> */}

                {/* {quickView ? null : (
                  <>
                    <ProductTab />
                    <div className="row mt-60">
                      <div className="col-12">
                        <h3 className="section-title style-1 mb-30">
                          Related products
                        </h3>
                      </div>
                      <div className="col-12">
                        <div className="row related-products position-relative">
                          <RelatedSlider />
                        </div>
                      </div>
                    </div>
                  </>
                )} */}
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  )
}

export default ProductDetails
