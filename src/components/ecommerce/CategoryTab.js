/* eslint-disable react/prop-types */
import React, { useEffect, useState } from 'react'
// import { server } from "../../config/index";
// import Cat1Tab from '../elements/FeaturedTab';
// import Cat2Tab from '../elements/NewArrivalTab';
// import Cat3Tab from '../elements/TrendingTab';
// import productData from 'data/product'
import ProductCards from 'components/common/productCard/ProductCards'

function CategoryTab() {
  const [active, setActive] = useState('1')
  // const [catAll, setCatAll] = useState([]);
  // const [cat1, setCat1] = useState([]);
  // const [cat2, setCat2] = useState([]);
  // const [cat3, setCat3] = useState([]);
  const [products, setProducts] = useState([])

  useEffect(() => {
    fetchAllProducts()
  }, [])

  const fetchAllProducts = () => {
    // Products
    fetch(
      `${process.env.REACT_APP_BASE_URL}/api/catalog/v1/products?page=1&limit=10`
    )
      .then((resp) => resp.json())
      .then((data) => {
        setProducts(data.data)
        setActive('1')
      })
  }

  const fetchFeaturedProducts = () => {
    fetch(
      `${process.env.REACT_APP_BASE_URL}/api/catalog/v1/products?isBase=true&adminapprovestatus=approve&page=1&limit=10&featured=true`
    )
      .then((resp) => resp.json())
      .then(({ data }) => {
        setProducts(data)
        setActive('2')
      })
  }

  // const catPAll = async () => {
  //   // const request = await fetch(`${server}/static/product.json`);
  //   // const allProducts = await request.json();
  //   const allProduct = productData.filter((item) => item.category)
  //   // const catAllItem = allProducts.filter((item) => item.category);
  //   setProducts(allProduct)
  //   setActive('1')
  // }
  // const catP1 = async () => {
  //   // const request = await fetch(`${server}/static/product.json`);
  //   // const allProducts = await request.json();
  //   const cat1Item = productData.filter((item) => item.category === 'jeans')
  //   setProducts(cat1Item)
  //   setActive('2')
  // }

  // const catP2 = async () => {
  //   // const request = await fetch(`${server}/static/product.json`);
  //   // const allProducts = await request.json();
  //   const cat2Item = productData.filter((item) => item.category === 'shoe')
  //   setProducts(cat2Item)
  //   setActive('3')
  // }
  // const catP3 = async () => {
  //   // const request = await fetch(`${server}/static/product.json`);
  //   // const allProducts = await request.json();
  //   const cat3Item = productData.filter((item) => item.category === 'jacket')
  //   setProducts(cat3Item)
  //   setActive('4')
  // }

  // useEffect(() => {
  //   catPAll()
  // }, [])

  return (
    <>
      <div className="section-title style-2 wow animate__animated animate__fadeIn">
        <h3>Popular Products</h3>
        <ul className="nav nav-tabs links" id="myTab" role="tablist">
          <li className="nav-item" role="presentation">
            <button
              className={active === '1' ? 'nav-link active' : 'nav-link'}
              onClick={fetchAllProducts}
            >
              All
            </button>
          </li>
          <li className="nav-item" role="presentation">
            <button
              className={active === '2' ? 'nav-link active' : 'nav-link'}
              onClick={fetchFeaturedProducts}
            >
              Featured
            </button>
          </li>
          {/* <li className="nav-item" role="presentation">
            <button
              className={active === '3' ? 'nav-link active' : 'nav-link'}
              onClick={catP2}
            >
              Popular
            </button>
          </li>
          <li className="nav-item" role="presentation">
            <button
              className={active === '4' ? 'nav-link active' : 'nav-link'}
              onClick={catP3}
            >
              New added
            </button>
          </li> */}
        </ul>
      </div>

      <div className="tab-content wow fadeIn animated">
        <div
          // className={
          //     active === "1"
          //         ? "tab-pane fade show active"
          //         : "tab-pane fade"
          // }
          className="tab-pane show active"
        >
          <div className="product-grid-4 row">
            <ProductCards products={products} slice={10} />
          </div>
        </div>

        {/* <div
                    className={
                        active === "2"
                            ? "tab-pane fade show active"
                            : "tab-pane fade"
                    }
                >
                    <div className="product-grid-4 row">
                        <Cat1Tab products={products} />
                    </div>
                </div>

                <div
                    className={
                        active === "3"
                            ? "tab-pane fade show active"
                            : "tab-pane fade"
                    }
                >
                    <div className="product-grid-4 row">
                        <Cat3Tab products={products} />
                    </div>
                </div>
                <div
                    className={
                        active === "4"
                            ? "tab-pane fade show active"
                            : "tab-pane fade"
                    }
                >
                    <div className="product-grid-4 row">
                        <Cat2Tab products={products} />
                    </div>
                </div> */}
      </div>
    </>
  )
}
export default CategoryTab
