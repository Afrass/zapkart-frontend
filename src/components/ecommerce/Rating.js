/* eslint-disable react/prop-types */
import { Avatar, Progress } from "antd";
import moment from "moment";
import React, { useState } from "react";
import StarRatings from "react-star-ratings";

const Rating = ({ reviews }) => {
  const [isSeeAllClicked, setIsSeeAllClicked] = useState(false);
  return (
    <div className="rating-section">
      {reviews?.data?.length > 0 && (
        <>
          <h4>Customer reviews</h4>

          <div className="row border-bottom align-items-center">
            <div className="col-md-7">
              <div>
                <div className="d-flex align-items-center mt-20 mb-20">
                  <span className="mr-2 ant-progress-text">5</span>
                  <Progress
                    percent={reviews?.fiveStarPercent}
                    strokeColor={
                      reviews?.fiveStarPercent === 100 ? "##3BB77E" : "#FF0000"
                    }
                    format={(percent) => `${percent}%`}
                  />
                </div>
                <div className="d-flex align-items-center mt-20 mb-20">
                  <span className="mr-2 ant-progress-text">4</span>
                  <Progress
                    percent={reviews?.fourStarPercent}
                    strokeColor={
                      reviews?.fourStarPercent === 100 ? "#3BB77E" : "#FF0000"
                    }
                    format={(percent) => `${percent}%`}
                  />
                </div>
                <div className="d-flex align-items-center mt-20 mb-20">
                  <span className="mr-2 ant-progress-text">3</span>
                  <Progress
                    percent={reviews?.threeStarPercent}
                    strokeColor={
                      reviews?.threeStarPercent === 100 ? "#3BB77E" : "#FF0000"
                    }
                    format={(percent) => `${percent}%`}
                  />
                </div>
                <div className="d-flex align-items-center mt-20 mb-20">
                  <span className="mr-2 ant-progress-text">2</span>
                  <Progress
                    percent={reviews?.twoStarPercent}
                    strokeColor={
                      reviews?.twoStarPercent === 100 ? "#3BB77E" : "#FF0000"
                    }
                    format={(percent) => `${percent}%`}
                  />
                </div>
                <div className="d-flex align-items-center mt-20 mb-20">
                  <span className="mr-2 ant-progress-text">1</span>
                  <Progress
                    percent={reviews?.oneStarPercent}
                    strokeColor={
                      reviews?.oneStarPercent === 100 ? "#3BB77E" : "#FF0000"
                    }
                    format={(percent) => `${percent}%`}
                  />
                </div>

                {/* {reviews?.length > 0 ? (
                  reviews?.map((review) => (
                    <div
                      key={review.id}
                      className="d-flex align-items-center mt-20 mb-20"
                    >
                      <span className="mr-2 ant-progress-text">5</span>
                      <Progress percent={30} />
                    </div>
                  ))
                ) : (
                  <p>No Reviews Found </p>
                )} */}
              </div>
            </div>
            <div className="col-md-5 pl-15 mb-10">
              <div className="rating-more-info">
                <div className="pl-20">
                  <div className="d-flex align-items-center mb-10">
                    <p className="product-overall-rating">
                      {reviews?.overalRating}
                    </p>
                    <p className="product-overall-per-rating">/5</p>
                  </div>

                  <StarRatings
                    rating={reviews?.overalRating}
                    starRatedColor="#3BB77E"
                    numberOfStars={5}
                    name="rating"
                    svgIconPath="M20.213 23.4449C20.2091 24.1522 19.6172 24.4596 18.9608 24.1049C17.0118 23.0543 15.0522 22.0226 13.1321 20.9228C12.5334 20.5795 12.0964 20.5899 11.5045 20.9275C9.63828 21.9923 7.7297 22.9872 5.84325 24.0189C5.47847 24.2184 5.13198 24.4009 4.73352 24.1182C4.33505 23.8354 4.40627 23.4392 4.47269 23.0448C4.87308 20.6589 5.27058 18.273 5.67578 15.889C5.72101 15.6251 5.6161 15.453 5.43612 15.2715C3.7643 13.5957 2.10307 11.9106 0.433181 10.2329C0.155026 9.95303 -0.110616 9.67689 0.0472293 9.24661C0.201225 8.82674 0.569853 8.74636 0.968317 8.68867C3.30809 8.34729 5.64594 7.99171 7.98668 7.65127C8.2504 7.61345 8.30622 7.43188 8.39477 7.24747C9.44098 5.0592 10.4881 2.87093 11.5382 0.684546C11.8029 0.133222 12.2774 -0.0474 12.7355 0.209821C12.9627 0.337486 13.0483 0.559718 13.1494 0.772493C14.1754 2.91821 15.2052 5.06204 16.2206 7.21154C16.3544 7.49524 16.5103 7.6229 16.8491 7.67019C19.1754 7.99455 21.4969 8.3539 23.8194 8.70286C24.1697 8.75581 24.469 8.89199 24.5942 9.2504C24.7203 9.61164 24.5566 9.88305 24.3045 10.1355C22.6124 11.8358 20.932 13.5475 19.2313 15.2393C18.9858 15.4833 18.9348 15.7112 18.9897 16.0327C19.3997 18.4167 19.7953 20.8027 20.1947 23.1886C20.2101 23.2822 20.2082 23.3777 20.213 23.4449Z"
                    svgIconViewBox="0 0 25 25"
                  />

                  <p className="rating-text mt-10">{reviews?.total} Reviews</p>
                </div>
              </div>
            </div>
          </div>
        </>
      )}

      <div className="custome-review-section pt-20">
        <h4>Reviews</h4>

        {reviews?.data?.length > 0 ? (
          <>
            <>
              {reviews?.data
                ?.slice(...(isSeeAllClicked ? [0, 2] : [0, 1]))
                ?.map((review) => (
                  <div
                    key={review?.id}
                    className="customer-review-block mt-15 mb-30"
                  >
                    <div className="customer-info mb-5">
                      <Avatar
                        size={64}
                        src={review?.userImage}
                        className="mr-10"
                      >
                        {review?.userName}
                      </Avatar>
                      <div className="customer-info-details">
                        <p className="customer-name">{review?.userName}</p>
                        <span className="customer-date">
                          {moment(new Date(review?.createdAt * 1000)).format(
                            "MMMM, YYYY"
                          )}
                        </span>
                      </div>
                    </div>
                    <StarRatings
                      rating={2}
                      starRatedColor="#3BB77E"
                      numberOfStars={5}
                      starSpacing={"3.5px"}
                      name="rating"
                      svgIconPath="M20.213 23.4449C20.2091 24.1522 19.6172 24.4596 18.9608 24.1049C17.0118 23.0543 15.0522 22.0226 13.1321 20.9228C12.5334 20.5795 12.0964 20.5899 11.5045 20.9275C9.63828 21.9923 7.7297 22.9872 5.84325 24.0189C5.47847 24.2184 5.13198 24.4009 4.73352 24.1182C4.33505 23.8354 4.40627 23.4392 4.47269 23.0448C4.87308 20.6589 5.27058 18.273 5.67578 15.889C5.72101 15.6251 5.6161 15.453 5.43612 15.2715C3.7643 13.5957 2.10307 11.9106 0.433181 10.2329C0.155026 9.95303 -0.110616 9.67689 0.0472293 9.24661C0.201225 8.82674 0.569853 8.74636 0.968317 8.68867C3.30809 8.34729 5.64594 7.99171 7.98668 7.65127C8.2504 7.61345 8.30622 7.43188 8.39477 7.24747C9.44098 5.0592 10.4881 2.87093 11.5382 0.684546C11.8029 0.133222 12.2774 -0.0474 12.7355 0.209821C12.9627 0.337486 13.0483 0.559718 13.1494 0.772493C14.1754 2.91821 15.2052 5.06204 16.2206 7.21154C16.3544 7.49524 16.5103 7.6229 16.8491 7.67019C19.1754 7.99455 21.4969 8.3539 23.8194 8.70286C24.1697 8.75581 24.469 8.89199 24.5942 9.2504C24.7203 9.61164 24.5566 9.88305 24.3045 10.1355C22.6124 11.8358 20.932 13.5475 19.2313 15.2393C18.9858 15.4833 18.9348 15.7112 18.9897 16.0327C19.3997 18.4167 19.7953 20.8027 20.1947 23.1886C20.2101 23.2822 20.2082 23.3777 20.213 23.4449Z"
                      svgIconViewBox="0 0 25 25"
                    />
                    <p className="customer-review-content mt-10 mb-10">
                      {review?.title}
                    </p>
                    <p className="customer-review-content">{review?.message}</p>
                  </div>
                ))}
              {!isSeeAllClicked && (
                <div className="d-flex justify-content-center mr-25 ml-25">
                  <button
                    type="submit"
                    className="button button-add-to-cart w-100"
                    onClick={() => setIsSeeAllClicked(true)}
                  >
                    See All
                  </button>
                </div>
              )}
            </>
          </>
        ) : (
          <p className="mt-10">No Customer Reviews Found</p>
        )}
      </div>
    </div>
    // M20.213 23.4449C20.2091 24.1522 19.6172 24.4596 18.9608 24.1049C17.0118 23.0543 15.0522 22.0226 13.1321 20.9228C12.5334 20.5795 12.0964 20.5899 11.5045 20.9275C9.63828 21.9923 7.7297 22.9872 5.84325 24.0189C5.47847 24.2184 5.13198 24.4009 4.73352 24.1182C4.33505 23.8354 4.40627 23.4392 4.47269 23.0448C4.87308 20.6589 5.27058 18.273 5.67578 15.889C5.72101 15.6251 5.6161 15.453 5.43612 15.2715C3.7643 13.5957 2.10307 11.9106 0.433181 10.2329C0.155026 9.95303 -0.110616 9.67689 0.0472293 9.24661C0.201225 8.82674 0.569853 8.74636 0.968317 8.68867C3.30809 8.34729 5.64594 7.99171 7.98668 7.65127C8.2504 7.61345 8.30622 7.43188 8.39477 7.24747C9.44098 5.0592 10.4881 2.87093 11.5382 0.684546C11.8029 0.133222 12.2774 -0.0474 12.7355 0.209821C12.9627 0.337486 13.0483 0.559718 13.1494 0.772493C14.1754 2.91821 15.2052 5.06204 16.2206 7.21154C16.3544 7.49524 16.5103 7.6229 16.8491 7.67019C19.1754 7.99455 21.4969 8.3539 23.8194 8.70286C24.1697 8.75581 24.469 8.89199 24.5942 9.2504C24.7203 9.61164 24.5566 9.88305 24.3045 10.1355C22.6124 11.8358 20.932 13.5475 19.2313 15.2393C18.9858 15.4833 18.9348 15.7112 18.9897 16.0327C19.3997 18.4167 19.7953 20.8027 20.1947 23.1886C20.2101 23.2822 20.2082 23.3777 20.213 23.4449Z
  );
};

export default Rating;
