/* eslint-disable react/prop-types */
import React, { useState } from "react";
import SwiperCore, { Navigation, Thumbs, Pagination } from "swiper";
import "swiper/css/thumbs";
import "swiper/css/pagination";
import { Swiper, SwiperSlide } from "swiper/react";
// import Zoom from 'react-img-zoom'

SwiperCore.use([Navigation, Thumbs]);

const ThumbSlider = ({ images, type, isImgAvailable }) => {
  const [thumbsSwiper, setThumbsSwiper] = useState(null);

  return (
    <div>
      <Swiper
        style={{
          "--swiper-pagination-bullet": "#fff",
          // '--swiper-pagination-color': '#fff',
        }}
        modules={[Pagination]}
        loop={true}
        spaceBetween={10}
        pagination={true}
        thumbs={{ swiper: thumbsSwiper }}
        className="mySwiper2"
      >
        {/* {product.gallery.map((item, i) => (
          <SwiperSlide key={i}>
            <img src={item.thumb} alt="" />
            <Zoom
              img={item.thumb}
              zoomScale={5}
              width={500}
              height={500}
              ransitionTime={0.5}
            />
          </SwiperSlide>
        ))} */}
        {images?.map((curImg, i) => (
          <SwiperSlide key={i}>
            <img
              // src={`${process.env.REACT_APP_BASE_URL}/${curImg.url}`}
              src={curImg}
              style={{
                height: type === "medicine" ? "180px" : "400px",
                width: type === "medicine" ? "100%" : "100%",
                objectFit: isImgAvailable ? "cover" : "contain",
              }}
              alt=""
            />
            {/* <Zoom
              img={item.thumb}
              zoomScale={5}
              width={500}
              height={500}
              ransitionTime={0.5}
            /> */}
          </SwiperSlide>
        ))}
      </Swiper>
      {type !== "medicine" && (
        <Swiper
          onSwiper={setThumbsSwiper}
          loop={true}
          spaceBetween={10}
          slidesPerView={4}
          freeMode={true}
          watchSlidesProgress={true}
          className="mySwiper mt-15"
        >
          {images?.map((curImg, i) => (
            <SwiperSlide key={i}>
              <img
                // src={`${process.env.REACT_APP_BASE_URL}/${curImg.thumbnail}`}
                src={curImg}
                alt=""
              />
              {/* <Zoom
              img={item.thumb}
              zoomScale={5}
              width={500}
              height={500}
              ransitionTime={0.5}
            /> */}
            </SwiperSlide>
          ))}
          {/* {product.gallery.map((item, i) => (
          <SwiperSlide key={i}>
            <img src={item.thumb} alt="" />
          </SwiperSlide>
        ))} */}
        </Swiper>
      )}
    </div>
  );
};

export default ThumbSlider;
