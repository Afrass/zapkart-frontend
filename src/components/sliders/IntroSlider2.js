/* eslint-disable react/prop-types */
import React from 'react'
// import { useNavigate } from 'react-router-dom'
import SwiperCore, { EffectFade, Navigation, Pagination } from 'swiper'
import 'swiper/css/effect-fade'
import 'swiper/css/pagination'
import { Swiper, SwiperSlide } from 'swiper/react'

// import slider4Img from 'assets/imgs/slider/slider-4.png'

// import slider3Img from 'assets/imgs/slider/slider-3.png'
// import { useState } from 'react'
// import { useEffect } from 'react'

SwiperCore.use([Navigation, Pagination, EffectFade])

const IntroSlider2 = ({ homeHeroBanner, isMobile }) => {
  // const navigate = useNavigate()

  return (
    <>
      <Swiper
        slidesPerView={1}
        spaceBetween={0}
        effect={'fade'}
        fadeEffect={{
          crossFade: true,
        }}
        loop={true}
        pagination={{
          clickable: true,
        }}
        navigation={{
          prevEl: '.custom_prev_i1',
          nextEl: '.custom_next_i1',
        }}
        className="hero-slider-1 style-4 dot-style-1 dot-style-1-position-1"
      >
        {homeHeroBanner?.length > 0 &&
          homeHeroBanner?.map((cur, i) => (
            <SwiperSlide
              key={i}
              onClick={() => (window.location.href = cur?.forwardUrl)}
            >
              {cur?.image && (
                <div
                  className="single-hero-slider single-animation-wrap"
                  style={{
                    backgroundImage: `url(${
                      isMobile ? cur?.mobileImage : cur?.image
                    })`,
                    backgroundPosition: 'center',
                    backgroundSize: 'cover',
                    backgroundRepeat: 'no-repeat',
                  }}
                >
                  <div className="slider-content mr-100">
                    {/* <h1 className="display-2 mb-40">
          
                      {cur.name}
                    </h1> */}
                    {/* <Link
                      to={cur.forwardUrl}
                      className="btn w-100 hover-up  mt-230"
                    >
                      BOOK NOW
                    </Link> */}
                    {/* <p className="mb-65">Save up to 50% off on your first order</p> */}
                    {/* <form className="form-subcriber d-flex">
                       <input type="email" placeholder="Your emaill address" />
                       <button className="btn" type="submit">
                         Subscribe
                       </button>
                     </form> */}
                  </div>
                </div>
              )}
            </SwiperSlide>
          ))}
      </Swiper>

      <div className="slider-arrow hero-slider-1-arrow">
        <span className="slider-btn slider-prev slick-arrow custom_prev_i1">
          <i className="fi-rs-angle-left"></i>
        </span>
        <span className="slider-btn slider-next slick-arrow custom_next_i1">
          <i className="fi-rs-angle-right"></i>
        </span>
      </div>
    </>
  )
}

export default IntroSlider2
