/* eslint-disable react/prop-types */
import React from 'react'
import { useNavigate } from 'react-router-dom'
// import { useNavigate } from 'react-router-dom'
import SwiperCore, { Autoplay, Navigation } from 'swiper'
import { Swiper, SwiperSlide } from 'swiper/react'
import NoImage from 'assets/imgs/shop/no-image.svg'
// import { useRouter } from 'next/router'
// import { connect } from 'react-redux'
// import { updateProductCategory } from './../../redux/action/productFiltersAction'

SwiperCore.use([Navigation, Autoplay])

const BrandSlider = ({ brands }) => {
  const navigate = useNavigate()
  // var data = [
  //   {
  //     id: 1,
  //     title: 'Cake & Milk',
  //     slug: 'jeans',
  //     img: 'cat-13.png',
  //     bg: 'bg-9',
  //   },
  //   {
  //     id: 2,
  //     title: 'Oganic Kiwi',
  //     slug: 'shoe',
  //     img: 'cat-12.png',
  //     bg: 'bg-10',
  //   },
  //   {
  //     id: 3,
  //     title: 'Peach',
  //     slug: 'jacket',
  //     img: 'cat-11.png',
  //     bg: 'bg-11',
  //   },
  //   {
  //     id: 4,
  //     title: 'Red Apple',
  //     img: 'cat-9.png',
  //     bg: 'bg-12',
  //   },
  //   {
  //     id: 5,
  //     title: 'Snack',
  //     img: 'cat-3.png',
  //     bg: 'bg-13',
  //   },
  //   {
  //     id: 6,
  //     title: 'Vegetables',
  //     img: 'cat-1.png',
  //     bg: 'bg-14',
  //   },
  //   {
  //     id: 7,
  //     title: 'Strawberry',
  //     img: 'cat-2.png',
  //     bg: 'bg-15',
  //   },

  //   // {
  //   //   id: 8,
  //   //   title: 'Black plum',
  //   //   img: 'cat-4.png',
  //   //   bg: 'bg-12',
  //   // },
  //   // {
  //   //   id: 9,
  //   //   title: 'Custard apple',
  //   //   img: 'cat-5.png',
  //   //   bg: 'bg-10',
  //   // },
  //   // {
  //   //   id: 10,
  //   //   title: 'Coffe & Tea',
  //   //   img: 'cat-14.png',
  //   //   bg: 'bg-12',
  //   // },
  //   // {
  //   //   id: 11,
  //   //   title: 'Headphone',
  //   //   img: 'cat-15.png',
  //   //   bg: 'bg-11',
  //   // },
  // ]

  //   const router = useRouter()

  // eslint-disable-next-line no-unused-vars
  const selectCategory = (e, category) => {
    e.preventDefault()
    // removeSearchTerm();
    // updateProductCategory(category)
    // router.push({
    //   pathname: '/products',
    //   query: {
    //     cat: category, //
    //   },
    // })
  }

  return (
    <>
      <Swiper
        autoplay={true}
        loop={true}
        navigation={{
          prevEl: '.custom_prev_f_brand',
          nextEl: '.custom_next_f_brand',
        }}
        // navigation={{
        //   prevEl: '.custom_prev_ct1brand',
        //   nextEl: '.custom_next_ct1brand',
        // }}
        className="custom-class"
        slidesPerView={3}
        breakpoints={{
          480: {
            slidesPerView: brands?.length >= 2 ? 2 : brands?.length,
          },
          640: {
            slidesPerView: brands?.length >= 3 ? 3 : brands?.length,
          },
          768: {
            slidesPerView: brands?.length >= 5 ? 5 : brands?.length,
          },
          1024: {
            slidesPerView: brands?.length >= 8 ? 8 : brands?.length,
          },
          1200: {
            slidesPerView: brands?.length >= 10 ? 10 : brands?.length,
          },
        }}
      >
        {/* {data.map((item, i) => (
          <SwiperSlide key={i}>
            <div
              className={`card-2 bg${item.bg} wow animate__animated animate__fadeInUp`}
              onClick={(e) => selectCategory(e, item.slug)}
            >
              <figure className=" img-hover-scale overflow-hidden">
                <a href="#/">
                  <img
                    src={require(`assets/imgs/shop/${item.img}`)}
                    alt={item.title}
                  />
                </a>
              </figure>
              <h6>
                <a href="#/">{item.title}</a>
              </h6>
              <span>26 items</span>
            </div>
          </SwiperSlide>
        ))} */}

        {brands?.map((item, i) => (
          <SwiperSlide key={i}>
            <div
              className={`card-2 bg${i} wow animate__animated animate__fadeInUp`}
              onClick={() =>
                navigate(
                  `/products?brandId=${item.id}&selected=brand-${item?.name}`
                )
              }
            >
              <figure className=" img-hover-scale overflow-hidden">
                <a href="#/">
                  <img
                    // src={`${process.env.REACT_APP_BASE_URL}/${item.images[0].url}`}
                    src={item.image || NoImage}
                    alt={item.name}
                    height={80}
                    width={80}
                    style={{ objectFit: item.image ? 'cover' : 'contain' }}
                  />
                </a>
              </figure>
              <h6>
                <a href="#/">{item.name}</a>
              </h6>
              {/* <span>26 items</span> */}
            </div>
          </SwiperSlide>
        ))}
      </Swiper>

      {/* <div
        id="carausel-10-columns-arrows"
      >
        <span className="slider-btn slider-prev slick-arrow custom_prev_ct1brand">
          <i className="fi-rs-arrow-small-left"></i>
        </span>
        <span className="slider-btn slider-next slick-arrow custom_next_ct1brand">
          <i className="fi-rs-arrow-small-right"></i>
        </span>
      </div> */}
      <div
        className="slider-arrow slider-arrow-2 slider-arrow-brand carausel-4-columns-arrow"
        style={{ top: '32%' }}
      >
        <span className="slider-btn slider-prev slick-arrow custom_prev_f_brand">
          <i className="fi-rs-arrow-small-left"></i>
        </span>
        <span className="slider-btn slider-next slick-arrow custom_next_f_brand">
          <i className="fi-rs-arrow-small-right"></i>
        </span>
      </div>
    </>
  )
}

export default BrandSlider
