/* eslint-disable react/prop-types */
import React, { useRef } from 'react'
import SwiperCore, { Navigation } from 'swiper'
import { Swiper, SwiperSlide } from 'swiper/react'
import SingleProduct2 from 'components/common/productCard/ProductCard2'

SwiperCore.use([Navigation])

const ProductSlider = ({ products }) => {
  const div = useRef(null)

  return (
    <div ref={div}>
      <Swiper
        spaceBetween={24}
        // grid={{
        //   rows: 2,
        // }}
        loop={true}
        navigation={{
          prevEl: '.custom_prev_f_product',
          nextEl: '.custom_next_f_product',
        }}
        slidesPerView={3}
        className="custom-class"
        breakpoints={{
          480: {
            slidesPerView: 3,
          },
          482: {
            slidesPerView: 2,
          },
          640: {
            slidesPerView: 2,
          },
          768: {
            slidesPerView: 2,
          },
          1024: {
            slidesPerView: 4,
          },
        }}
      >
        {products.map((product, i) => (
          <SwiperSlide key={i}>
            <SingleProduct2 product={product} />
          </SwiperSlide>
        ))}
      </Swiper>

      <div className="slider-arrow slider-arrow-2 slider-arrow-product carausel-4-columns-arrow">
        <span className="slider-btn slider-prev slick-arrow custom_prev_f_product">
          <i className="fi-rs-arrow-small-left"></i>
        </span>
        <span className="slider-btn slider-next slick-arrow custom_next_f_product">
          <i className="fi-rs-arrow-small-right"></i>
        </span>
      </div>
    </div>
  )
}

export default ProductSlider
