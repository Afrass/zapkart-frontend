/* eslint-disable react/prop-types */
import React from 'react'

import { Link } from 'react-router-dom'

const Breadcrumb = ({ parent, sub, subChild, subSubChild, noBreadcrumb }) => {
  return (
    <>
      <div
        className={`page-header breadcrumb-wrap ${noBreadcrumb}`}
        style={{
          pointerEvents: window.location.pathname.includes(
            '/user-authdetails-update'
          )
            ? 'none'
            : 'all',
        }}
      >
        <div className="container">
          <div className="breadcrumb">
            <Link to="/">{parent}</Link>
            <span></span> {sub}
            {subChild && (
              <>
                <span></span> {subChild}
              </>
            )}
            {subSubChild && (
              <>
                <span></span> {subSubChild}
              </>
            )}
          </div>
        </div>
      </div>
    </>
  )
}

export default Breadcrumb
