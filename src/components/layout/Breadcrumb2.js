/* eslint-disable react/prop-types */
import React from 'react'
// import { useRouter } from 'next/router'
import { Link } from 'react-router-dom'
// import Tags from 'components/filters/Tags'

const Breadcrumb2 = ({ title, subParent, subChild }) => {
  // const router = useRouter()

  // const titlex = router.query.cat
  // console.log(titlex);
  return (
    <>
      <div className="page-header mt-30 mb-50">
        <div className="container">
          <div className="archive-header">
            <div className="row align-items-center">
              <div>
                {/* className="col-xl-3" */}
                <h3 className="mb-15 text-capitalize">
                  {title === 'Medicines' ? title : 'Products'}
                </h3>
                <div className="breadcrumb">
                  <Link to="/">
                    <a href="#/" rel="nofollow">
                      <i className="fi-rs-home mr-5"></i>Home
                    </a>
                  </Link>
                  <span></span>
                  {subParent}
                  <span></span>
                  {subChild}
                </div>
              </div>
              {/* <div className="col-xl-9 text-end d-none d-xl-block">
                <Tags />
              </div> */}
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default Breadcrumb2
