/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import { Link, NavLink, useNavigate } from 'react-router-dom'
import React, { useEffect, useState } from 'react'
import CategoryProduct2 from '../filters/CategoryProduct2'
// import CategoryProduct3 from '../ecommerce/Filter/CategoryProduct3'
import Search from '../common/Search'

// Icons
import logoIcon from 'assets/imgs/theme/logo.png'
// import compareIcon from 'assets/imgs/theme/icons/icon-compare.svg'
import heartIcon from 'assets/imgs/theme/icons/icon-heart.svg'
import cartIcon from 'assets/imgs/theme/icons/icon-cart.svg'
import userIcon from 'assets/imgs/theme/icons/icon-user.svg'
import icon1 from 'assets/imgs/theme/icons/icon-1.svg'
import icon2 from 'assets/imgs/theme/icons/icon-2.svg'
import icon3 from 'assets/imgs/theme/icons/icon-3.svg'
import icon4 from 'assets/imgs/theme/icons/icon-4.svg'
import { useDispatch, useSelector } from 'react-redux'
import { setLogout } from 'redux/slices/authSlice'
import { getCategories } from 'redux/slices/categorySlice'
import { getCartSlice } from 'redux/slices/cartSlice'
import { getWishlistSlice } from 'redux/slices/wishlistSlice'
import OutsideClickHandler from 'react-outside-click-handler'
import {
  getProductsTemplateBySearch,
  setFilters,
} from 'redux/slices/productTemplateSlice'
import SearchSuggestion from 'components/common/SearchSuggestion'

// import hotIcon from 'assets/imgs/theme/icons/icon-hot.svg'
// import headphoneIcon from 'assets/imgs/theme/icons/icon-headphone.svg'

// TODO Replace <Link> to <NavLink> if that is real navigation Link

const Header = ({ toggleClick }) => {
  // const totalCartItems = 3
  // const totalCompareItems = 2
  // const totalWishlistItems = 2
  const [isToggled, setToggled] = useState(false)
  const [scroll, setScroll] = useState(0)
  const [initialCategories, setInitialCategories] = useState([])

  const [searchTerm, setSearchTerm] = useState('')
  const [isSuggestionShown, setIsSuggestionShown] = useState(false)

  const { authorized, user } = useSelector((state) => state.auth)
  const { categories } = useSelector((state) => state.category)
  const { totalCartItems } = useSelector((state) => state.cart)
  const { totalWishlistItems } = useSelector((state) => state.wishlist)

  const { filters, searchSuggestions } = useSelector(
    (state) => state.productTemplate
  )

  const navigate = useNavigate()

  const dispatch = useDispatch()

  useEffect(() => {
    document.addEventListener('scroll', () => {
      const scrollCheck = window.scrollY >= 100
      if (scrollCheck !== scroll) {
        setScroll(scrollCheck)
      }
    })
  })

  const handleToggle = () => setToggled(!isToggled)

  useEffect(() => {
    if (authorized) {
      dispatch(getCartSlice())

      dispatch(getWishlistSlice())
    }
  }, [authorized])

  useEffect(() => {
    dispatch(getCategories())
  }, [])

  const createCategoryList = (categories, parentId = null) => {
    const categoryList = []
    let category
    if (parentId == null) {
      category = categories.filter((cat) => !cat?.parentId)
    } else {
      category = categories.filter((cat) => cat?.parentId === parentId)
    }
    // eslint-disable-next-line prefer-const
    for (let cate of category) {
      categoryList.push({
        id: cate.id,
        title: cate.name,
        value: cate.id,
        key: cate.id,
        children: createCategoryList(categories, cate.id),
      })
    }

    return categoryList
  }

  useEffect(() => {
    if (categories) {
      const tree = createCategoryList(categories)

      const sortByInnerChild = tree.map((first) => {
        if (first.children?.length === 0) {
          return { ...first, nestedChildrenLvl: 0 }
        } else if (first.children?.length > 0) {
          let findInner
          first.children.map((second) => {
            if (second.children?.length === 0) {
              findInner = 1
            } else if (second.children?.length > 0) {
              findInner = 2
            }
          })
          return { ...first, nestedChildrenLvl: findInner }
        }
      })

      setInitialCategories(sortByInnerChild.slice(0, 6))
    }
  }, [categories])

  const addTofilter = (catId, catName) => {
    dispatch(
      setFilters([
        { filterName: 'categoryId', value: catId },
        { filterName: 'selected', value: `category-${catName}` },
      ])
    )
  }

  const onSearchSubmit = (text) => {
    setSearchTerm(text)
    setIsSuggestionShown(false)

    // const addSearchIfNotThereOrUpdateIfItsThere = (arr, newObj) => [
    //   ...arr.filter((o) => o.filterName !== newObj.filterName),
    //   { ...newObj },
    // ]

    // const res = addSearchIfNotThereOrUpdateIfItsThere(filters, {
    //   filterName: 'search',
    //   value: text || searchTerm,
    // })

    // dispatch(setFilters(res))

    // navigate(`/products?search=${text || searchTerm}`)

    dispatch(
      setFilters([
        { filterName: 'search', value: text || searchTerm },
        { filterName: 'selected', value: 'search' },
      ])
    )

    navigate(`/products?search=${text || searchTerm}&selected=search`)
  }

  const onSearchChange = (e) => {
    setSearchTerm(e.target.value)
    setIsSuggestionShown(true)

    dispatch(getProductsTemplateBySearch(`&search=${e.target.value}`))
  }

  const handleInput = (e) => {
    if (e.key === 'Enter') {
      e.preventDefault()
      onSearchSubmit()
    }
  }

  return (
    <>
      <header
        className="header-area header-style-1 header-height-2"
        style={{
          pointerEvents: window.location.pathname.includes(
            '/user-authdetails-update'
          )
            ? 'none'
            : 'all',
        }}
      >
        <div className="mobile-promotion">
          <span>
            Grand opening, <strong>up to 15%</strong> off all items. Only{' '}
            <strong>3 days</strong> left
          </span>
        </div>
        <div className="header-top header-top-ptb-1 d-none d-lg-block">
          <div className="container">
            <div className="row align-items-center">
              <div className="col-xl-3 col-lg-4">
                <div className="header-info">
                  <ul>
                    {/* <li>
                      <Link to="/about">About Us</Link>
                    </li> */}
                    <li>
                      <Link to="/account#dashboard">My Account</Link>
                    </li>
                    <li>
                      <Link to="/wishlist">Wishlist</Link>
                    </li>
                    <li>
                      <Link to="/account#orders">Order Tracking</Link>
                    </li>
                  </ul>
                </div>
              </div>
              <div className="col-xl-6 col-lg-4">
                <div className="text-center">
                  <div id="news-flash" className="d-inline-block">
                    <ul>
                      <li>
                        Get Medicines up to 40% off
                        {/* <Link to="/shop-grid-right">View details</Link> */}
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              {/* <div className="col-xl-3 col-lg-4">
                <div className="header-info header-info-right">
                  <ul>
                    <li>
                      Need help? Call Us:{' '}
                      <strong className="text-brand"> + 1800 900</strong>
                    </li>
                  </ul>
                </div>
              </div> */}
            </div>
          </div>
        </div>
        <div className="header-middle header-middle-ptb-1 d-none d-lg-block">
          <div className="container">
            <div className="header-wrap">
              <div className="logo logo-width-1">
                <Link to="/">
                  <img src={logoIcon} alt="logo" />
                </Link>
              </div>
              <div className="header-right">
                <div className="search-style-2">
                  <Search />
                </div>
                <div className="header-action-right">
                  <div className="header-action-2">
                    {/* <div className="search-location">
                      <form action="#">
                        <select className="select-active">
                          <option>Your Location</option>
                          <option>Alabama</option>
                          <option>Alaska</option>
                          <option>Arizona</option>
                          <option>Delaware</option>
                          <option>Florida</option>
                          <option>Georgia</option>
                          <option>Hawaii</option>
                          <option>Indiana</option>
                          <option>Maryland</option>
                          <option>Nevada</option>
                          <option>New Jersey</option>
                          <option>New Mexico</option>
                          <option>New York</option>
                        </select>
                      </form>
                    </div> */}
                    {/* <div className="header-action-icon-2">
                      <Link to="/shop-compare">
                        <img
                          className="svgInject"
                          alt="Evara"
                          src={compareIcon}
                        />
                        <span className="pro-count blue">
                          {totalCompareItems}
                        </span>
                      </Link>
                      <Link to="/shop-compare">
                        <span className="lable ml-0">Compare</span>
                      </Link>
                    </div> */}
                    <div className="header-action-icon-2">
                      <Link to="/wishlist">
                        <img
                          className="svgInject"
                          alt="Evara"
                          src={heartIcon}
                        />
                        {totalWishlistItems > 0 && (
                          <span className="pro-count blue">
                            {totalWishlistItems}
                          </span>
                        )}
                        {/* <span className="pro-count blue">
                          {totalWishlistItems}
                        </span> */}
                      </Link>
                      <Link to="/wishlist">
                        <span className="lable">Wishlist</span>
                      </Link>
                    </div>
                    <div className="header-action-icon-2">
                      <Link to="/cart" className="mini-cart-icon">
                        <img alt="Evara" src={cartIcon} />
                        {totalCartItems > 0 && (
                          <span className="pro-count blue">
                            {totalCartItems}
                          </span>
                        )}
                      </Link>
                      <Link to="/cart">
                        <span className="lable">Cart</span>
                      </Link>
                    </div>

                    <div className="header-action-icon-2">
                      <Link to="/account#dashboard">
                        <img
                          className="svgInject"
                          alt="Zapkart"
                          src={userIcon}
                        />
                      </Link>
                      {authorized ? (
                        <>
                          <Link to="/account#dashboard">
                            <span className="lable ml-0">
                              {user?.firstName
                                ? `${user?.firstName}`
                                : 'Account'}
                            </span>
                          </Link>
                          <div className="cart-dropdown-wrap cart-dropdown-hm2 account-dropdown">
                            <ul>
                              <li>
                                <Link to="/account#dashboard">
                                  <i className="fi fi-rs-user mr-10"></i>
                                  My Account
                                </Link>
                              </li>
                              {/* <li>
                                <Link to="/account">
                                  <i className="fi fi-rs-location-alt mr-10"></i>
                                  Order Tracking
                                </Link>
                              </li> */}
                              {/* <li>
                                <Link to="/account">
                                  <i className="fi fi-rs-label mr-10"></i>
                                  My Voucher
                                </Link>
                              </li> */}
                              <li>
                                <Link to="/wishlist">
                                  <i className="fi fi-rs-heart mr-10"></i>
                                  My Wishlist
                                </Link>
                              </li>
                              {/* <li>
                                <Link to="/account">
                                  <i className="fi fi-rs-settings-sliders mr-10"></i>
                                  Setting
                                </Link>
                              </li> */}
                              <li onClick={() => dispatch(setLogout())}>
                                <Link to="#/">
                                  <i className="fi fi-rs-sign-out mr-10"></i>
                                  Sign out
                                </Link>
                              </li>
                            </ul>
                          </div>
                        </>
                      ) : (
                        <Link to="/login">
                          <span className="lable ml-0">Login</span>
                        </Link>
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div
          className={
            scroll
              ? 'header-bottom header-bottom-bg-color sticky-bar stick'
              : 'header-bottom header-bottom-bg-color sticky-bar'
          }
        >
          <div className="container">
            <div className="header-wrap header-space-between position-relative">
              <div className="logo logo-width-1  d-lg-none">
                <Link to="/">
                  <img src={logoIcon} alt="logo" />
                </Link>
              </div>
              <div className="header-nav d-none d-lg-flex">
                <OutsideClickHandler onOutsideClick={() => setToggled(false)}>
                  <div className="main-categori-wrap d-none d-lg-block">
                    <a
                      href="#/"
                      className="categories-button-active"
                      onClick={handleToggle}
                    >
                      <span className="fi-rs-apps"></span>
                      <span className="et">Browse</span> All Categories
                      <i className="fi-rs-angle-down"></i>
                    </a>

                    <div
                      className={
                        isToggled
                          ? 'categories-dropdown-wrap categories-dropdown-active-large font-heading open'
                          : 'categories-dropdown-wrap categories-dropdown-active-large font-heading'
                      }
                    >
                      <div className="d-flex categori-dropdown-inner">
                        <CategoryProduct2 categories={categories} />
                        {/* <CategoryProduct3 /> */}
                      </div>
                      {/* <div
                      className="more_slide_open"
                      style={{ display: 'none' }}
                    >
                      <div className="d-flex categori-dropdown-inner">
                        <ul>
                          <li>
                            <Link to="/products">
                              <img src={icon1} alt="" />
                              Milks and Dairies
                            </Link>
                          </li>
                          <li>
                            <Link to="/products">
                              <img src={icon2} alt="" />
                              Clothing & beauty
                            </Link>
                          </li>
                        </ul>
                        <ul className="end">
                          <li>
                            <Link to="/products">
                              <img src={icon3} alt="" />
                              Wines & Drinks
                            </Link>
                          </li>
                          <li>
                            <Link to="/products">
                              <img src={icon4} alt="" />
                              Fresh Seafood
                            </Link>
                          </li>
                        </ul>
                      </div>
                    </div> */}
                      {/* <div className="more_categories">
                      <span className="icon"></span>{' '}
                      <span className="heading-sm-1">Show more...</span>
                    </div> */}
                    </div>
                  </div>
                </OutsideClickHandler>

                <div className="main-menu main-menu-padding-1 main-menu-lh-2 d-none d-lg-block  font-heading">
                  <nav>
                    <ul>
                      {/* <li className="hot-deals">
                        <img src={hotIcon} alt="hot deals" />
                        <Link to="/products">Hot Deals</Link>
                      </li> */}
                      <li>
                        {/* <NavLink to="/">
                          Home
                          <i className="fi-rs-angle-down"></i>
                        </NavLink> */}
                        {/* <ul className="sub-menu">
                          <li>
                            <Link to="/">Home 1</Link>
                          </li>
                          <li>
                            <Link to="/index-2">Home 2</Link>
                          </li>
                          <li>
                            <Link to="/index-3">Home 3</Link>
                          </li>
                          <li>
                            <Link to="/index-4">Home 4</Link>
                          </li>
                        </ul> */}
                      </li>
                      <li>
                        <Link
                          // className="cat-listing"
                          // to={`/products/category/${cur.id}?name=${cur.name}`}
                          to={`/products/medicine`}
                          // style={({ isActive }) =>
                          //   isActive ? { color: '#E89F72' } : undefined
                          // }
                        >
                          Medicine
                        </Link>
                      </li>

                      {/* {initialCategory?.map((cur, i) => (
                        <>
                          {!cur?.children ? (
                            <li>
                              <NavLink
                                // className="cat-listing"
                                to={`/products/category/${cur.id}?name=${cur.title}`}
                                style={({ isActive }) =>
                                  isActive ? { color: '#E89F72' } : undefined
                                }
                              >
                                {cur.title}
                              </NavLink>
                            </li>
                          ) : (
                            cur?.children?.length > 0 && (
                              <li>
                                <NavLink
                                  to={`/products/category/${cur.id}?name=${cur.title}`}
                                >
                                  {cur.title}
                                  <i className="fi-rs-angle-down"></i>
                                </NavLink>

                                <ul key={i} className="sub-menu">
                                  {cur.children.map((c1sub) => (
                                    <li>
                                      <Link to="/shop-grid-right">
                                        {c1sub.title}
                                      </Link>
                                    </li>
                                  ))}
                                </ul>
                              </li>
                            )
                            cur?.children?.map((child1) => (
                              <>
                                {!child1?.children ? (
                                  <li>
                                    <Link
                                      to={`/products/category/${cur.id}?name=${cur.title}`}
                                    >
                                      {cur.title}
                                      <i className="fi-rs-angle-down"></i>
                                    </Link>
                                    {cur.children.map((c1sub, i) => (
                                      <ul key={i} className="sub-menu">
                                        <li>
                                          <Link to="/shop-grid-right">
                                            {c1sub.title}
                                          </Link>
                                        </li>
                                      </ul>
                                    ))}
                                  </li>
                                ) : null}
                              </>
                            ))
                          )}
                        </>
                      ))} */}

                      {/* <li>
                        <NavLink activeClassName="active" to="/about">
                          About
                        </NavLink>
                      </li> */}
                      {/* <li>
                        <NavLink activeClassName="active" to="/about">
                          Health Conditions
                        </NavLink>
                      </li>
                      <li>
                        <NavLink activeClassName="active" to="/about">
                          Maternity Solutions
                        </NavLink>
                      </li>
                      <li>
                        <Link to="/shop-grid-right">
                          Covid Core Solutions
                          <i className="fi-rs-angle-down"></i>
                        </Link>
                        <ul className="sub-menu">
                          <li>
                            <Link to="/shop-grid-right">
                              Shop Grid – Right Sidebar
                            </Link>
                          </li>
                          <li>
                            <Link to="/products">Shop Grid – Left Sidebar</Link>
                          </li>
                          <li>
                            <Link to="/shop-list-right">
                              Shop List – Right Sidebar
                            </Link>
                          </li>
                          <li>
                            <Link to="/shop-list-left">
                              Shop List – Left Sidebar
                            </Link>
                          </li>
                          <li>
                            <Link to="/shop-fullwidth">Shop - Wide</Link>
                          </li>
                          <li>
                            <Link to="/shop-filter">Shop - Filter</Link>
                          </li>
                        </ul>
                      </li>

                      <li>
                        <a href="#/">
                          Diabetes <i className="fi-rs-angle-down"></i>
                        </a>
                        <ul className="sub-menu">
                          <li>
                            <Link to="/vendors">Vendors Grid</Link>
                          </li>
                          <li>
                            <Link to="/vendors-list">Vendors List</Link>
                          </li>
                          <li>
                            <Link to="/vendor-dashboard">Vendor Dashboard</Link>
                          </li>
                          <li>
                            <Link to="/vendor-guide">Vendor Guide</Link>
                          </li>
                        </ul>
                      </li> */}
                      {initialCategories?.map((cur) => {
                        return cur.nestedChildrenLvl === 0 ? (
                          <li key={cur.id}>
                            <Link
                              // className="cat-listing"
                              // to={`/products/category/${cur.id}?name=${cur.name}`}
                              to={`/products?categoryId=${cur.id}&selected=category-${cur?.title}`}
                              onClick={() => addTofilter(cur.id, cur?.title)}
                              // style={({ isActive }) =>
                              //   isActive ? { color: '#E89F72' } : undefined
                              // }
                            >
                              {cur.title}
                            </Link>
                          </li>
                        ) : cur.nestedChildrenLvl === 1 ? (
                          <li key={cur.id}>
                            <Link
                              to={`/products?categoryId=${cur.id}&selected=category-${cur?.title}`}
                              onClick={() => addTofilter(cur.id, cur?.title)}
                            >
                              {cur.title}
                              <i className="fi-rs-angle-down"></i>
                            </Link>
                            <ul className="sub-menu">
                              {cur.children?.map((child1) => (
                                <li key={child1.id}>
                                  <Link
                                    to={`/products?categoryId=${child1.id}&selected=category-${child1?.title}`}
                                    onClick={() =>
                                      addTofilter(child1.id, child1?.title)
                                    }
                                  >
                                    {child1.title}
                                  </Link>
                                </li>
                              ))}
                            </ul>
                          </li>
                        ) : cur.nestedChildrenLvl === 2 ? (
                          <li className="position-static" key={cur.id}>
                            <Link
                              to={`/products?categoryId=${cur.id}&selected=category-${cur?.title}`}
                              onClick={() => addTofilter(cur.id, cur?.title)}
                            >
                              {cur.title}
                              <i className="fi-rs-angle-down"></i>
                            </Link>

                            <ul className="mega-menu">
                              {cur.children?.map((child1) => (
                                <li
                                  key={child1.id}
                                  className="sub-mega-menu sub-mega-menu-width-22"
                                >
                                  <Link
                                    className="menu-title"
                                    to={`/products?categoryId=${child1.id}&selected=category-${child1?.title}`}
                                    onClick={() =>
                                      addTofilter(child1.id, child1?.title)
                                    }
                                  >
                                    {child1.title}
                                  </Link>
                                  <ul>
                                    {child1.children?.map((child2) => (
                                      <li key={child2.id}>
                                        <Link
                                          to={`/products?categoryId=${child2.id}&selected=category-${child2?.title}`}
                                          onClick={() =>
                                            addTofilter(
                                              child2.id,
                                              child2?.title
                                            )
                                          }
                                        >
                                          {child2.title}
                                        </Link>
                                      </li>
                                    ))}
                                  </ul>
                                </li>
                              ))}
                              {/* <li className="sub-mega-menu sub-mega-menu-width-22">
                                <a className="menu-title" href="#/">
                                  Fruit & Vegetables
                                </a>
                                <ul>
                                  <li>
                                    <a href="#/">Meat & Poultry</a>
                                  </li>
                                  <li>
                                    <a href="#/">Fresh Vegetables</a>
                                  </li>
                                  <li>
                                    <a href="#/">Herbs & Seasonings</a>
                                  </li>
                                  <li>
                                    <a href="#/">Cuts & Sprouts</a>
                                  </li>
                                  <li>
                                    <a href="#/">Exotic Fruits & Veggies</a>
                                  </li>
                                  <li>
                                    <a href="#/">Packaged Produce</a>
                                  </li>
                                </ul>
                              </li> */}
                            </ul>
                          </li>
                        ) : null
                      })}
                      {/* <li className="position-static">
                        <Link to="/#">
                          Sexual Wellness
                          <i className="fi-rs-angle-down"></i>
                        </Link>
                        <ul className="mega-menu">
                          <li className="sub-mega-menu sub-mega-menu-width-22">
                            <a className="menu-title" href="#/">
                              Fruit & Vegetables
                            </a>
                            <ul>
                              <li>
                                <a href="#/">Meat & Poultry</a>
                              </li>
                              <li>
                                <a href="#/">Fresh Vegetables</a>
                              </li>
                              <li>
                                <a href="#/">Herbs & Seasonings</a>
                              </li>
                              <li>
                                <a href="#/">Cuts & Sprouts</a>
                              </li>
                              <li>
                                <a href="#/">Exotic Fruits & Veggies</a>
                              </li>
                              <li>
                                <a href="#/">Packaged Produce</a>
                              </li>
                            </ul>
                          </li>
                          <li className="sub-mega-menu sub-mega-menu-width-22">
                            <a className="menu-title" href="#/">
                              Breakfast & Dairy
                            </a>
                            <ul>
                              <li>
                                <a href="#/">Milk & Flavoured Milk</a>
                              </li>
                              <li>
                                <a href="#/">Butter and Margarine</a>
                              </li>
                              <li>
                                <a href="#/">Eggs Substitutes</a>
                              </li>
                              <li>
                                <a href="#/">Marmalades</a>
                              </li>
                              <li>
                                <a href="#/">Sour Cream</a>
                              </li>
                              <li>
                                <a href="#/">Cheese</a>
                              </li>
                            </ul>
                          </li>
                          <li className="sub-mega-menu sub-mega-menu-width-22">
                            <a className="menu-title" href="#/">
                              Meat & Seafood
                            </a>
                            <ul>
                              <li>
                                <a href="#/">Breakfast Sausage</a>
                              </li>
                              <li>
                                <a href="#/">Dinner Sausage</a>
                              </li>
                              <li>
                                <a href="#/">Chicken</a>
                              </li>
                              <li>
                                <a href="#/">Sliced Deli Meat</a>
                              </li>
                              <li>
                                <a href="#/">Wild Caught Fillets</a>
                              </li>
                              <li>
                                <a href="#/">Crab and Shellfish</a>
                              </li>
                            </ul>
                          </li>
                          <li className="sub-mega-menu sub-mega-menu-width-34">
                            <div className="menu-banner-wrap">
                              <a href="#/">
                                <img
                                  src={require('assets/imgs/banner/banner-menu.png')}
                                  alt="Nest"
                                />
                              </a>
                              <div className="menu-banner-content">
                                <h4>Hot deals</h4>
                                <h3>
                                  Do not miss
                                  <br />
                                  Trending
                                </h3>
                                <div className="menu-banner-price">
                                  <span className="new-price text-success">
                                    Save to 50%
                                  </span>
                                </div>
                                <div className="menu-banner-btn">
                                  <a href="#/">Shop now</a>
                                </div>
                              </div>
                              <div className="menu-banner-discount">
                                <h3>
                                  <span>25%</span>
                                  off
                                </h3>
                              </div>
                            </div>
                          </li>
                        </ul>
                      </li>
                      <li>
                        <Link to="/blog-category-grid">
                          Homeopathy
                          <i className="fi-rs-angle-down"></i>
                        </Link>
                        <ul className="sub-menu">
                          <li>
                            <Link to="/blog-category-grid">
                              Blog Category Grid
                            </Link>
                          </li>
                          <li>
                            <Link to="/blog-category-list">
                              Blog Category List
                            </Link>
                          </li>
                          <li>
                            <Link to="/blog-category-big">
                              Blog Category Big
                            </Link>
                          </li>
                          <li>
                            <Link to="/blog-category-fullwidth">
                              Blog Category Wide
                            </Link>
                          </li>
                          <li>
                            <Link to="/#">
                              Single Post
                              <i className="fi-rs-angle-right"></i>
                            </Link>
                            <ul className="level-menu level-menu-modify">
                              <li>
                                <Link to="/blog-post-left">Left Sidebar</Link>
                              </li>
                              <li>
                                <Link to="/blog-post-right">Right Sidebar</Link>
                              </li>
                              <li>
                                <Link to="/blog-post-fullwidth">
                                  No Sidebar
                                </Link>
                              </li>
                            </ul>
                          </li>
                        </ul>
                      </li> */}
                      {/* <li>
                        <Link to="/#">
                          Pages
                          <i className="fi-rs-angle-down"></i>
                        </Link>
                        <ul className="sub-menu">
                          <li>
                            <Link to="/about">About Us</Link>
                          </li>
                          <li>
                            <Link to="/contact">Contact</Link>
                          </li>
                          <li>
                            <Link to="/account">My Account</Link>
                          </li>
                          <li>
                            <Link to="/login">login/register</Link>
                          </li>
                          <li>
                            <Link to="/page-purchase-guide">
                              Purchase Guide
                            </Link>
                          </li>
                          <li>
                            <Link to="/page-privacy-policy">
                              Privacy Policy
                            </Link>
                          </li>
                          <li>
                            <Link to="/page-terms">Terms of Service</Link>
                          </li>
                          <li>
                            <Link to="/page-404">404 Page</Link>
                          </li>
                        </ul>
                      </li> */}
                      {/* <li>
                        <Link to="/contact">Contact</Link>
                      </li> */}
                    </ul>
                  </nav>
                </div>
              </div>
              {/* <div className="hotline d-none d-lg-flex">
                <img src={headphoneIcon} alt="hotline" />

                <p>
                  1900 - 888<span>24/7 Support Center</span>
                </p>
              </div> */}

              <div className="header-action-icon-2 d-block d-lg-none">
                {/* <div className="burger-icon burger-icon-white">
                  <span className="burger-icon-top"></span>
                  <span className="burger-icon-mid"></span>
                  <span className="burger-icon-bottom"></span>
                </div> */}
              </div>

              <div className="header-action-right d-block d-lg-none">
                <div className="header-action-2">
                  {/* <div className="header-action-icon-2">
                    <Link to="/wishlist">
                      <img alt="Evara" src={compareIcon} />
                      <span className="pro-count white">
                        {totalCompareItems}
                      </span>
                    </Link>
                  </div> */}

                  <div className="header-action-icon-2">
                    <Link to="/account#dashboard">
                      <img className="svgInject" alt="Nest" src={userIcon} />
                    </Link>
                    {authorized ? (
                      <>
                        <Link to="/account#dashboard">
                          <span className="lable ml-0">
                            {user?.firstName ? `${user?.firstName}` : 'Account'}
                          </span>
                        </Link>
                        <div className="cart-dropdown-wrap cart-dropdown-hm2 account-dropdown">
                          <ul>
                            <li>
                              <Link to="/account#dashboard">
                                <i className="fi fi-rs-user mr-10"></i>
                                My Account
                              </Link>
                            </li>
                            {/* <li>
                              <Link to="/account">
                                <i className="fi fi-rs-location-alt mr-10"></i>
                                Order Tracking
                              </Link>
                            </li> */}
                            {/* <li>
                              <Link to="/account">
                                <i className="fi fi-rs-label mr-10"></i>
                                My Voucher
                              </Link>
                            </li> */}
                            <li>
                              <Link to="/wishlist">
                                <i className="fi fi-rs-heart mr-10"></i>
                                My Wishlist
                              </Link>
                            </li>
                            {/* <li>
                              <Link to="/account">
                                <i className="fi fi-rs-settings-sliders mr-10"></i>
                                Setting
                              </Link>
                            </li> */}
                            <li onClick={() => dispatch(setLogout())}>
                              <Link to="#/">
                                <i className="fi fi-rs-sign-out mr-10"></i>
                                Sign out
                              </Link>
                            </li>
                          </ul>
                        </div>
                      </>
                    ) : (
                      <Link to="/login">
                        <span className="lable ml-0">Login</span>
                      </Link>
                    )}
                  </div>

                  <div className="header-action-icon-2">
                    <Link to="/wishlist">
                      <img alt="Evara" src={heartIcon} />
                      {totalWishlistItems > 0 && (
                        <span className="pro-count white">
                          {totalWishlistItems}
                        </span>
                      )}
                      {/* <span className="pro-count white">
                        {totalWishlistItems}
                      </span> */}
                    </Link>
                  </div>
                  <div className="header-action-icon-2">
                    <Link to="/cart" className="mini-cart-icon">
                      <img alt="Evara" src={cartIcon} />
                      {totalCartItems > 0 && (
                        <span className="pro-count blue">{totalCartItems}</span>
                      )}
                    </Link>
                    {/* <div className="cart-dropdown-wrap cart-dropdown-hm2">
                      <ul>
                        <li>
                          <div className="shopping-cart-img">
                            <Link to="/shop-grid-right">
                              <img
                                alt="Evara"
                                src={require('assets/imgs/shop/thumbnail-3.jpg')}
                              />
                            </Link>
                          </div>
                          <div className="shopping-cart-title">
                            <h4>
                              <Link to="/shop-grid-right">
                                Plain Striola Shirts
                              </Link>
                            </h4>
                            <h3>
                              <span>1 × </span>
                              $800.00
                            </h3>
                          </div>
                          <div className="shopping-cart-delete">
                            <Link to="/#">
                              <i className="fi-rs-cross-small"></i>
                            </Link>
                          </div>
                        </li>
                        <li>
                          <div className="shopping-cart-img">
                            <Link to="/shop-grid-right">
                              <img
                                alt="Evara"
                                src={require('assets/imgs/shop/thumbnail-4.jpg')}
                              />
                            </Link>
                          </div>
                          <div className="shopping-cart-title">
                            <h4>
                              <Link to="/shop-grid-right">
                                Macbook Pro 2022
                              </Link>
                            </h4>
                            <h3>
                              <span>1 × </span>
                              $3500.00
                            </h3>
                          </div>
                          <div className="shopping-cart-delete">
                            <Link to="/#">
                              <i className="fi-rs-cross-small"></i>
                            </Link>
                          </div>
                        </li>
                      </ul>
                      <div className="shopping-cart-footer">
                        <div className="shopping-cart-total">
                          <h4>
                            Total
                            <span>$383.00</span>
                          </h4>
                        </div>
                        <div className="shopping-cart-button">
                          <Link to="/cart">View cart</Link>
                          <Link to="/shop-checkout">Checkout</Link>
                        </div>
                      </div>
                    </div> */}
                  </div>
                  <div className="header-action-icon-2 d-block d-lg-none">
                    <div
                      className="burger-icon burger-icon-white"
                      onClick={toggleClick}
                    >
                      <span className="burger-icon-top"></span>
                      <span className="burger-icon-mid"></span>
                      <span className="burger-icon-bottom"></span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="mobile-search search-style-3 mobile-header-border mt-3 mx-2">
            <form>
              <input
                type="text"
                autoComplete="off"
                name="searchQuery"
                placeholder="Search for items…"
                onKeyDown={handleInput}
                value={searchTerm}
                onChange={onSearchChange}
              />
              <button type="submit">
                <i className="fi-rs-search"></i>
              </button>
              <OutsideClickHandler
                onOutsideClick={() => setIsSuggestionShown(false)}
              >
                <SearchSuggestion
                  searchSuggestions={searchSuggestions}
                  onSelect={onSearchSubmit}
                  searchTerm={searchTerm}
                  setIsSuggestionShown={setIsSuggestionShown}
                  isSuggestionShown={isSuggestionShown}
                />
              </OutsideClickHandler>
            </form>
          </div>
        </div>
      </header>
    </>
  )
}

export default Header
