/* eslint-disable react/no-unescaped-entities */
import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import InstagramWhiteIcon from 'assets/imgs/theme/icons/icon-instagram-white.svg'
// import Icon1 from 'assets/imgs/theme/icons/icon-1.svg'
// import Icon2 from 'assets/imgs/theme/icons/icon-2.svg'
// import Icon3 from 'assets/imgs/theme/icons/icon-3.svg'
// import Icon4 from 'assets/imgs/theme/icons/icon-4.svg'
// import Icon5 from 'assets/imgs/theme/icons/icon-5.svg'
// import Icon6 from 'assets/imgs/theme/icons/icon-6.svg'
import LogoIcon from 'assets/imgs/theme/logo.png'
import LocationIcon from 'assets/imgs/theme/icons/icon-location.svg'
import ContactIcon from 'assets/imgs/theme/icons/icon-contact.svg'
import EmailIcon from 'assets/imgs/theme/icons/icon-email-2.svg'
// import ClockIcon from 'assets/imgs/theme/icons/icon-clock.svg'
// import PhoneCallIcon from 'assets/imgs/theme/icons/phone-call.svg'
import FacebookWhiteIcon from 'assets/imgs/theme/icons/icon-facebook-white.svg'
import TwitterWhiteIcon from 'assets/imgs/theme/icons/icon-twitter-white.svg'
// import PinterestWhiteIcon from 'assets/imgs/theme/icons/icon-pinterest-white.svg'
// import YoutubeWhiteIcon from 'assets/imgs/theme/icons/icon-youtube-white.svg'
import { useDispatch, useSelector } from 'react-redux'
import { getInformations } from 'redux/slices/informationSlice'
import { getSettings } from 'redux/slices/settingSlice'

const Footer = () => {
  const [width, setWidth] = useState(window.innerWidth)

  const dispatch = useDispatch()

  const { informations } = useSelector((state) => state.information)
  const { settings } = useSelector((state) => state.setting)
  const { authorized } = useSelector((state) => state.auth)

  useEffect(() => {
    dispatch(getInformations())
    dispatch(getSettings())
  }, [])

  console.log(settings, 'settingsss')

  const handleWindowSizeChange = () => {
    setWidth(window.innerWidth)
  }

  useEffect(() => {
    window.addEventListener('resize', handleWindowSizeChange)
    return () => {
      window.removeEventListener('resize', handleWindowSizeChange)
    }
  }, [])

  const isMobile = width <= 768

  return (
    <>
      <br></br>

      <footer
        className="main"
        style={{
          pointerEvents: window.location.pathname.includes(
            '/user-authdetails-update'
          )
            ? 'none'
            : 'all',
        }}
      >
        <section className="newsletter mb-15  wow animate__animated animate__fadeIn">
          <div className="container">
            <div className="row">
              <div className="col-lg-12 overflow-hidden">
                <div className="position-relative newsletter-inner">
                  <div className="newsletter-content">
                    <h4 className="mb-20">
                      Stay home & get your medecines from <br />
                      Zapkart
                    </h4>
                    {/* <p className="mb-45">
                      Start Your Daily Shopping with{' '}
                      <span className="text-brand">Nest Mart</span>
                    </p> */}
                    <form className="form-subcriber d-flex">
                      <input
                        type="email"
                        placeholder={
                          !isMobile ? 'Your emaill address' : 'Email'
                        }
                      />
                      <button className="btn" type="submit">
                        Subscribe
                      </button>
                    </form>
                  </div>
                  <img
                    src={require('assets/imgs/banner/banner-9.png')}
                    alt="newsletter"
                  />
                </div>
              </div>
            </div>
          </div>
        </section>
        {/* <section className="featured  section-padding">
          <div className="container">
            <div className="row">
              <div className="col-lg-1-5 col-md-4 col-12 col-sm-6 mb-md-4 mb-xl-0">
                <div
                  className="banner-left-icon d-flex align-items-center  wow animate__animated animate__fadeInUp"
                  data-wow-delay="0"
                >
                  <div className="banner-icon">
                    <img src={Icon1} alt="" />
                  </div>
                  <div className="banner-text">
                    <h3 className="icon-box-title">Best prices & offers</h3>
                    <p>Orders $50 or more</p>
                  </div>
                </div>
              </div>
              <div className="col-lg-1-5 col-md-4 col-12 col-sm-6">
                <div
                  className="banner-left-icon d-flex align-items-center  wow animate__animated animate__fadeInUp"
                  data-wow-delay=".1s"
                >
                  <div className="banner-icon">
                    <img src={Icon2} alt="" />
                  </div>
                  <div className="banner-text">
                    <h3 className="icon-box-title">Free delivery</h3>
                    <p>24/7 amazing services</p>
                  </div>
                </div>
              </div>
              <div className="col-lg-1-5 col-md-4 col-12 col-sm-6">
                <div
                  className="banner-left-icon d-flex align-items-center  wow animate__animated animate__fadeInUp"
                  data-wow-delay=".2s"
                >
                  <div className="banner-icon">
                    <img src={Icon3} alt="" />
                  </div>
                  <div className="banner-text">
                    <h3 className="icon-box-title">Great daily deal</h3>
                    <p>When you sign up</p>
                  </div>
                </div>
              </div>
              <div className="col-lg-1-5 col-md-4 col-12 col-sm-6">
                <div
                  className="banner-left-icon d-flex align-items-center  wow animate__animated animate__fadeInUp"
                  data-wow-delay=".3s"
                >
                  <div className="banner-icon">
                    <img src={Icon4} alt="" />
                  </div>
                  <div className="banner-text">
                    <h3 className="icon-box-title">Wide assortment</h3>
                    <p>Mega Discounts</p>
                  </div>
                </div>
              </div>
              <div className="col-lg-1-5 col-md-4 col-12 col-sm-6">
                <div
                  className="banner-left-icon d-flex align-items-center  wow animate__animated animate__fadeInUp"
                  data-wow-delay=".4s"
                >
                  <div className="banner-icon">
                    <img src={Icon5} alt="" />
                  </div>
                  <div className="banner-text">
                    <h3 className="icon-box-title">Easy returns</h3>
                    <p>Within 30 days</p>
                  </div>
                </div>
              </div>
              <div className="col-lg-1-5 col-md-4 col-12 col-sm-6 d-xl-none">
                <div
                  className="banner-left-icon d-flex align-items-center  wow animate__animated animate__fadeInUp"
                  data-wow-delay=".5s"
                >
                  <div className="banner-icon">
                    <img src={Icon6} alt="" />
                  </div>
                  <div className="banner-text">
                    <h3 className="icon-box-title">Safe delivery</h3>
                    <p>Within 30 days</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section> */}
        <section className="footer-note">
          <div className="container">
            <div className="footer-note-content">
              <h5>We Covered Every Corner of India!</h5>
              <p>
                We deliver in 27000+ pin codes all across India. So we cover all
                corners of the country. The major cities in which we deliver
                include Mumbai, Kolkata, Delhi, Bengaluru, Ahmedabad, Hyderabad,
                Kochi, Chennai, Thane, Howrah, Pune, Gurgaon, Navi Mumbai,
                Jaipur, Noida, Lucknow, Ghaziabad & Vadodara.
              </p>
            </div>
            <div className="footer-note-content">
              <h5>Zapkart will Bring an End to All Your Healthcare Needs!</h5>
              <p>
                Zapkart is a new platform to order medicine online in India. We
                are a company that begins with customer experience. Zapkart
                allows you to order medicines and healthcare products online by
                redirecting the customer to registered retail pharmacies and
                getting the products delivered to the customer's doorstep.
                Zapkart is an online medical store, selling products with a
                customer-centric vision.
              </p>
            </div>
            <div className="footer-note-content">
              <h5>
                How Can We Simplify The Customer Experience at Our Online
                Pharmacy Store?
              </h5>
              <p>
                Our doorstep delivery service is available in PAN-India across
                top cities like Bangalore, Mumbai, Delhi, Kochi, Hyderabad,
                Kolkata, Gurgaon, Pune, Noida, etc. Our online medical store
                enables you to choose from Medicines, Healthcare products, OTC
                products, Ayurvedic products, and medical equipment.
              </p>
            </div>
            <div className="footer-note-content">
              <h5>Why Should You Choose Zapkart The Online Pharmacy?</h5>
              <p>
                Enjoy exciting offers on products on our platform. Zapkart
                enables you to make trustworthy payments online. You can use
                various wallets or apply promo codes to avail yourself of
                maximum discounts on products from Zapkart. You can choose Cash
                on Delivery to pay cash at the time of delivery as we deliver
                your products at your door. We are more inclined to customer
                experience. Ordering medicines online from Zapkart will be a
                hassle-free experience for everyone. Our qualified pharmacists
                verify your order and fulfill the order via registered retail
                pharmacies. Our vision is to make medicines available to
                everyone with affordability.
              </p>
            </div>
            <div className="footer-note-content">
              <h5>Technology Extends a Hand to Help with Zapkart!</h5>
              <p>
                We are introducing medicine reminder functionality on Zapkart.
                Now you can schedule the refill medicine by yourself via zapkart
                to end recurring orders. You will get a reminder periodically
                and your order will be delivered to your door.
              </p>
            </div>
          </div>
        </section>
        <section className="section-padding footer-mid">
          <div className="container pt-15 pb-20">
            <div className="row">
              <div className="col">
                <div
                  className="widget-about font-md mb-md-3 mb-lg-3 mb-xl-0  wow animate__animated animate__fadeInUp"
                  data-wow-delay="0"
                >
                  <div className="logo  mb-30">
                    <Link to="/" className="mb-15">
                      <img src={LogoIcon} alt="logo" />
                    </Link>
                    {/* <p className="font-lg text-heading">
                      Awesome grocery store website template
                    </p> */}
                  </div>
                  <ul className="contact-infor">
                    <li>
                      <img src={LocationIcon} alt="" />
                      <strong>Address: </strong>{' '}
                      <span>
                        {settings?.address}
                        {/* Pezhakkappilly P.O Muvattupuzha, Kerala, India 686673 */}
                      </span>
                    </li>
                    <li>
                      <img src={ContactIcon} alt="" />
                      <strong>Call Us:</strong>
                      <span>{settings?.phone}</span>
                    </li>
                    <li>
                      <img src={EmailIcon} alt="" />
                      <strong>Email:</strong>
                      <span>{settings?.email}</span>
                    </li>
                    {/* <li>
                      <img src={ClockIcon} alt="" />
                      <strong>Hours:</strong>
                      <span>10:00 - 18:00, Mon - Sat</span>
                    </li> */}
                  </ul>
                </div>
              </div>
              <div
                className="footer-link-widget col  wow animate__animated animate__fadeInUp"
                data-wow-delay=".1s"
              >
                <h4 className="widget-title">Information</h4>
                <ul className="footer-list  mb-sm-5 mb-md-0">
                  {informations?.map((info) => (
                    <li key={info.id}>
                      <Link to={`/information/${info.id}`}>{info.name}</Link>
                    </li>
                  ))}
                </ul>
              </div>
              <div
                className="footer-link-widget col  wow animate__animated animate__fadeInUp"
                data-wow-delay=".2s"
              >
                <h4 className="widget-title ">Account</h4>
                <ul className="footer-list  mb-sm-5 mb-md-0">
                  <li>
                    {authorized ? (
                      <Link to="/account#dashboard">Account</Link>
                    ) : (
                      <Link to="/login">Login</Link>
                    )}
                  </li>
                  <li>
                    <a
                      href="#/"
                      onClick={() =>
                        (window.location.href =
                          process.env.REACT_APP_VENDOR_SITE_LINK)
                      }
                      target="_blank"
                    >
                      Become Vendor
                    </a>
                    {/* <Link to="/vendor-login"></Link> */}
                  </li>
                  <li>
                    <Link to="/cart">View Cart</Link>
                  </li>
                  <li>
                    <Link to="/wishlist">My Wishlist</Link>
                  </li>
                  <li>
                    <Link to="/account#orders">Track My orders</Link>
                  </li>
                </ul>
              </div>
              <div
                className="footer-link-widget col  wow animate__animated animate__fadeInUp"
                data-wow-delay=".3s"
              >
                <h4 className="widget-title ">Category</h4>
                <ul className="footer-list  mb-sm-5 mb-md-0">
                  <li>
                    <Link to="/categories">View All Categories</Link>
                  </li>
                  {/* <li>
                    <a href="#/">Maternity</a>
                  </li>
                  <li>
                    <a href="#/">Diabetes</a>
                  </li>
                  <li>
                    <a href="#/">Sexual Wellness</a>
                  </li>
                  <li>
                    <a href="#/">Fitness & Suppliments</a>
                  </li>
                  <li>
                    <a href="#/">Medical Devices</a>
                  </li>
                  <li>
                    <a href="#/">Baby Products</a>
                  </li>
                  <li>
                    <a href="#/">Fitness</a>
                  </li> */}
                </ul>
              </div>
              {/* <div
                className="footer-link-widget col  wow animate__animated animate__fadeInUp"
                data-wow-delay=".4s"
              >
                <h4 className="widget-title ">Popular</h4>
                <ul className="footer-list  mb-sm-5 mb-md-0">
                  <li>
                    <a href="#/">Capsules</a>
                  </li>
                  <li>
                    <a href="#/">T Capsules</a>
                  </li>
                  <li>
                    <a href="#/">Syrup</a>
                  </li>
                  <li>
                    <a href="#/">Dolo 650</a>
                  </li>
                  <li>
                    <a href="#/">Cough Syrup</a>
                  </li>
                  <li>
                    <a href="#/">Medicines</a>
                  </li>
                  <li>
                    <a href="#/">M Capsules</a>
                  </li>
                </ul>
              </div> */}
              {/* <div
                className="footer-link-widget widget-install-app col  wow animate__animated animate__fadeInUp"
                data-wow-delay=".5s"
              >
                <h4 className="widget-title ">Install App</h4>
                <p className="">From App Store or Google Play</p>
                <div className="download-app ">
                  <a href="#" className="hover-up mb-sm-2 mb-lg-0">
                    <img
                      className="active"
                      src={require('assets/imgs/theme/app-store.jpg')}
                      alt=""
                    />
                  </a>
                  <a href="#" className="hover-up mb-sm-2">
                    <img
                      src={require('assets/imgs/theme/google-play.jpg')}
                      alt=""
                    />
                  </a>
                </div>
                <p className="mb-20 ">Secured Payment Gateways</p>
                <img
                  className=""
                  src={require('assets/imgs/theme/payment-method.png')}
                  alt=""
                />
              </div> */}
            </div>
          </div>
        </section>
        <div
          className="container pb-30  wow animate__animated animate__fadeInUp"
          data-wow-delay="0"
        >
          <div className="row align-items-center">
            <div className="col-12 mb-30">
              <div className="footer-bottom"></div>
            </div>
            <div className="col-xl-4 col-lg-6 col-md-6">
              <p className="font-sm mb-0">
                &copy; 2021, <strong className="text-brand">Zapkart</strong> -
                India's Pharmacy <br />
                All rights reserved
              </p>
            </div>
            <div className="col-xl-4 col-lg-6 text-center d-none d-xl-block">
              {/* <div className="hotline d-lg-inline-flex mr-30">
                <img src={PhoneCallIcon} alt="hotline" />
                <p>
                  1900 - 6666<span>Working 8:00 - 22:00</span>
                </p>
              </div>
              <div className="hotline d-lg-inline-flex">
                <img src={PhoneCallIcon} alt="hotline" />
                <p>
                  1900 - 8888<span>24/7 Support Center</span>
                </p>
              </div> */}
            </div>
            <div className="col-xl-4 col-lg-6 col-md-6 text-end d-none d-md-block">
              <div className="mobile-social-icon">
                <h6>Follow Us</h6>
                <a
                  href="#/"
                  onClick={() => (window.location.href = settings.facebookUrl)}
                >
                  <img src={FacebookWhiteIcon} alt="" />
                </a>
                <a
                  href="#/"
                  onClick={() => (window.location.href = settings.twitterUrl)}
                >
                  <img src={TwitterWhiteIcon} alt="" />
                </a>
                <a
                  href="#/"
                  onClick={() => (window.location.href = settings.instagramUrl)}
                >
                  <img src={InstagramWhiteIcon} alt="" />
                </a>
                {/* <a href="#/">
                  <img src={PinterestWhiteIcon} alt="" />
                </a>
                <a href="#/">
                  <img src={YoutubeWhiteIcon} alt="" />
                </a> */}
              </div>
              <p className="font-sm">
                Up to 15% discount on your first subscribe
              </p>
            </div>
          </div>
        </div>
      </footer>
    </>
  )
}

export default Footer
