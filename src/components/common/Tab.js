/* eslint-disable react/prop-types */
import React, { useState, useEffect } from 'react'

const Tab = ({ children, active = 0 }) => {
  const [activeTab, setActiveTab] = useState(active)
  const [tabsData, setTabsData] = useState([])

  useEffect(() => {
    let data = []

    React.Children.forEach((child, element) => {
      if (!React.isValidElement(element)) return

      const {
        props: { tab, children },
      } = element
      data.push({ tab, children })
    })

    setTabsData(data)
  }, [children])

  return (
    <>
      <ul className="nav nav-tabs links" id="myTab" role="tablist">
        {tabsData.map(({ tab }, idx) => (
          <li key={idx} className="nav-item" role="presentation">
            <button
              className={idx === activeTab ? 'nav-link active' : 'nav-link'}
              onClick={() => setActiveTab(idx)}
            >
              {tab}
            </button>
          </li>
        ))}
      </ul>
      <div className="tab-content wow fadeIn animated">
        <div className="tab-pane show active">
          <div className="product-grid-4 row">
            {tabsData[activeTab] && tabsData[activeTab].children}
          </div>
        </div>
      </div>
    </>
  )
}

const TabPanel = ({ children }) => {
  return <div>{children}</div>
}

Tab.TabPanel = TabPanel

export default Tab
