/* eslint-disable react/prop-types */
import { Link } from 'react-router-dom'
import React from 'react'
import { toast } from 'react-toastify'
// import { addToCart } from "../../redux/action/cart";
import Timer from './Timer'

// eslint-disable-next-line no-unused-vars
const DealCard = ({ product, addToCart }) => {
  const handleCart = () => {
    // addToCart(product);
    toast('Product added to Cart !')
  }
  return (
    <>
      <div
        className="product-cart-wrap style-2 wow animate__animated animate__fadeInUp"
        data-wow-delay="0"
      >
        <div className="product-img-action-wrap">
          <div className="product-img">
            <Link to="/products">
              <img src={require('assets/imgs/shop/product-9-1.jpg')} alt="" />
            </Link>
          </div>
        </div>
        <div className="product-content-wrap">
          <div className="deals-countdown-wrap">
            <Timer endDateTime="2022-11-27 00:00:00" />
          </div>
          <div className="deals-content">
            <h2>
              <Link to={`/products/${product.slug}`}>{product.title}</Link>
            </h2>
            <div className="product-rate-cover">
              <div className="product-rate d-inline-block">
                <div className="product-rating" style={{ width: '90%' }}></div>
              </div>
              <span className="font-small ml-5 text-muted"> (4.0)</span>
            </div>
            <div>
              <span className="font-small text-muted">
                By <Link to="/vendor/1">NestFood</Link>
              </span>
            </div>
            <div className="product-card-bottom">
              <div className="product-price">
                <span>₹{product.price}</span>
                <span className="old-price">
                  {product.oldPrice && `₹ ${product.oldPrice}`}
                </span>
              </div>
              <div className="add-cart">
                <a
                  href="#/"
                  className="add"
                  onClick={() => handleCart(product)}
                >
                  <i className="fi-rs-shopping-cart mr-5"></i>Add{' '}
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

// const mapDispatchToProps = {
//     addToCart
// };

export default DealCard
