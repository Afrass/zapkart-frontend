/* eslint-disable react/prop-types */
import React from 'react'
import ProductCard from './ProductCard'
import ProductCard2 from './ProductCard2'

const ProductCards = ({ products, slice, productCardType = 1 }) => {
  return (
    <>
      {slice ? (
        <>
          {products?.slice(0, slice).map((product, i) => (
            <div className="col-lg-1-5 col-md-4 col-12 col-sm-6" key={i}>
              {productCardType === 1 ? (
                <ProductCard product={product} />
              ) : (
                <ProductCard2 product={product} />
              )}
            </div>
          ))}
        </>
      ) : (
        <>
          {products.map((product, i) => (
            <div className="col-lg-1-5 col-md-4 col-12 col-sm-6" key={i}>
              {productCardType === 1 ? (
                <ProductCard product={product} />
              ) : (
                <ProductCard2 product={product} />
              )}
            </div>
          ))}
        </>
      )}
    </>
  )
}

export default ProductCards
