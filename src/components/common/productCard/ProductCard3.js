/* eslint-disable react/prop-types */
// import productData from 'data/product'
import { Link, useNavigate } from 'react-router-dom'
import React from 'react'
import SwiperCore, { Navigation } from 'swiper'
import { useDispatch, useSelector } from 'react-redux'
import { toast } from 'react-toastify'
import { addToCartSlice } from 'redux/slices/cartSlice'
import { Tag } from 'antd'
import NoImage from 'assets/imgs/shop/no-image.svg'
import prescriptionIcon from 'assets/imgs/shop/prescription.png'

// import { server } from "../../config/index";

SwiperCore.use([Navigation])

const ProductCard3 = ({ product }) => {
  const { authorized } = useSelector((state) => state.auth)
  const dispatch = useDispatch()

  const navigate = useNavigate()

  const addToCart = async (product) => {
    if (authorized) {
      dispatch(addToCartSlice(product.id))
    } else {
      toast.warning('You have to login before adding to cart')
      navigate('/Login')
    }
  }

  console.log(product, 'ewdjkl')

  return (
    <div className="row">
      {product.map((product, i) => (
        <article className="col-md-4 mb-15 hover-up" key={i}>
          <div
            className="d-flex card-prod p-10 pl-15 pb-15"
            style={{ border: '1px solid #ececec' }}
          >
            <figure className="mr-15 mb-0">
              <Link
                to={
                  product?.price
                    ? `/product/${product.productTemplateId}/${product.id}/medicine`
                    : `/product/${product.id}/out-of-stock/medicine`

                  // product?.price
                  //   ? `/product/${product.productTemplateId}/${product.id}/medicine`
                  //   : '#/'
                }
              >
                <img
                  src={
                    product?.variant?.images?.length > 0
                      ? product?.variant?.images[0]
                      : product?.images?.length > 0
                      ? product?.images[0]
                      : NoImage
                  }
                  alt=""
                  height={60}
                  width={60}
                />
              </Link>
            </figure>
            <div className="d-flex flex-column mb-0">
              <h6>
                <Link
                  to={
                    product?.price
                      ? `/product/${product.productTemplateId}/${product.id}/medicine`
                      : `/product/${product.id}/out-of-stock/medicine`
                  }
                >
                  {product.name}
                </Link>
              </h6>
              {/* <div className="product-rate-cover">
              <div className="product-rate d-inline-block">
                <div className="product-rating" style={{ width: '90%' }}></div>
              </div>
              <span className="font-small ml-5 text-muted"> (4.0)</span>
            </div> */}
              {product.prescriptionRequired && (
                // // <p className="mb-0" style={{ fontSize: 13 }}>
                // //   Prescription Required
                // // </p>
                // <Tag
                //   color="orange"
                //   style={{
                //     margin: '7px 0',
                //     // padding: '10px',
                //     borderRadius: 10,
                //     width: 'fit-content',
                //   }}
                // >
                //   Prescription Required
                // </Tag>
                <div className="d-flex align-items-center">
                  <img
                    src={prescriptionIcon}
                    alt="prescription"
                    className="mr-5"
                  />
                  <span>Prescription Required</span>
                </div>
              )}
              {product.medicinePackaging && (
                <p className="mb-0" style={{ fontSize: 13 }}>
                  {product.medicinePackaging}
                </p>
              )}
              {product.manufacturer && (
                <p style={{ fontSize: 13 }}>{product.manufacturer?.name}</p>
              )}
              <p style={{ fontSize: 13 }}>
                {product.composition?.length > 0 &&
                  product.composition?.map((cur, index) =>
                    index === product.composition?.length - 1 ? (
                      <span key={cur.id}>{`${cur?.name}(${cur?.qty}) `}</span>
                    ) : (
                      <span key={cur.id}>{`${cur?.name}(${cur?.qty})+ `}</span>
                    )
                  )}
              </p>

              {/* {product?.price ? (
                <>
                  <div className="product-price">
                    <span>₹{product.price} </span>
                    <span className="old-price">
                      {product.mrpPrice && `₹ ${product.mrpPrice}`}
                    </span>
                  </div>

                  <div className="add-cart mt-3">
                    <a
                      href="#/"
                      className="add"
                      onClick={() => addToCart(product)}
                    >
                      <i className="fi-rs-shopping-cart mr-5"></i> Add
                    </a>
                  </div>
                </>
              ) : (
                <Tag
                  color="red"
                  style={{
                    padding: '10px',
                    marginBottom: '20px',
                    marginTop: '10px',
                  }}
                >
                  Out Of Stock
                </Tag>
              )} */}
              {product.price ? (
                <div className="add-cart" style={{ marginTop: 'auto' }}>
                  <a
                    href="#/"
                    className="add"
                    onClick={() => addToCart(product)}
                  >
                    <i className="fi-rs-shopping-cart mr-5"></i> Add
                  </a>
                </div>
              ) : (
                <Tag
                  color="red"
                  style={{
                    // padding: '10px',
                    borderRadius: 10,
                    marginTop: 'auto',
                    width: 'fit-content',
                  }}
                >
                  Out Of Stock
                </Tag>
              )}
            </div>
            <>
              <div
                className="product-price"
                style={{
                  marginLeft: 'auto',
                  width: '55px',
                  display: 'flex',
                  justifyContent: 'flex-end',
                }}
              >
                <span>{product.price && `₹ ${product.price}`}</span>
                {/* <span className="old-price">
                  {product.mrpPrice && `₹ ${product.mrpPrice}`}
                </span> */}
              </div>
            </>
          </div>
        </article>
      ))}
    </div>
  )
}

export default ProductCard3
