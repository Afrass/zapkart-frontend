/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import { Link, useNavigate } from 'react-router-dom'
import React from 'react'
import { connect, useDispatch, useSelector } from 'react-redux'
import { toast } from 'react-toastify'
import { addToCartApi } from 'api/products'
import cartService from 'service/cart'
import { Tag } from 'antd'
import { addToCartSlice } from 'redux/slices/cartSlice'
import { addToWishlistSlice } from 'redux/slices/wishlistSlice'
import { Badge } from 'react-bootstrap'
import NoImage from 'assets/imgs/shop/no-image.svg'
import TextTruncate from 'react-text-truncate'

const ProductCard = ({
  product,
  // addToCart,
  // addToCompare,
  // addToWishlist,
  // openQuickView,
}) => {
  console.log(product, 'plsisghgshj')

  const navigate = useNavigate()
  const dispatch = useDispatch()

  const { authorized } = useSelector((state) => state.auth)

  console.log(product, 'pwkl;jwdhd')

  const addToCart = async (product) => {
    if (authorized) {
      // const resp = await cartService.addCartItem({ productId: product.id })

      dispatch(addToCartSlice(product.id))
      // if (resp) {
      //   toast.success(`Added ${product.name} To Cart`)
      // }
      // try {
      //   const sendingData = {
      //     productId: product._id,
      //     quantity: product.minOrderQty,
      //   }
      //   const { data } = await addToCartApi(sendingData)
      //   data && toast.success('add To Cart')
      // } catch (err) {
      //   toast.error(err.response.data.error)
      // }
    } else {
      toast.warning('You have to login before adding to cart')
      navigate('/Login')
    }
    // const getCartItem = window.localStorage.getItem('cartItems')
    // if (getCartItem) {
    //   const parsedItems = JSON.parse(getCartItem)
    //   const storeItem = [...parsedItems, product]

    //   window.localStorage.setItem('cartItems', JSON.stringify(storeItem))
    //   toast.success('add To Cart')
    // } else {
    //   window.localStorage.setItem('cartItems', JSON.stringify([product]))
    //   toast.success('add To Cart')
    // }
  }

  const addToWishlist = (product) => {
    if (authorized) {
      dispatch(addToWishlistSlice(product.id))
      // const wishListItem = window.localStorage.getItem('wishlistItems')
      // if (wishListItem) {
      //   const parsedItems = JSON.parse(wishListItem)
      //   const storeItem = [...parsedItems, product]

      //   window.localStorage.setItem('wishlistItems', JSON.stringify(storeItem))
      //   toast.success(`Added ${product.name} To Wishlist`)
      // } else {
      //   window.localStorage.setItem('wishlistItems', JSON.stringify([product]))
      //   toast.success(`Added ${product.name} To Wishlist`)
      // }
    } else {
      toast.warning('You have to login before adding to Wishlist')
      navigate('/Login')
    }
  }

  return (
    <>
      <div className="product-cart-wrap mb-30 card-prod">
        <div
          className="product-img-action-wrap w-100"
          onClick={
            () =>
              product?.price
                ? navigate(
                    product?.productType === 'NonMedicine'
                      ? `/product/${product.productTemplateId}/${product.id}`
                      : `/product/${product.productTemplateId}/${product.id}/medicine`
                  )
                : navigate(
                    product?.productType === 'NonMedicine'
                      ? `/product/${product.id}/out-of-stock`
                      : `/product/${product.id}/out-of-stock/medicine`
                  )

            // to={
            //   product?.price
            //     ? `/product/${product.productTemplateId}/${product.id}`
            //     : '/'
            // }
          }
        >
          <div className="product-img product-img-zoom">
            <div className="d-flex justify-content-center">
              <img
                className="default-img"
                // src={`${process.env.REACT_APP_BASE_URL}/${product.images[0].url}`}
                src={
                  product?.variant?.images?.length > 0
                    ? product?.variant?.images[0]
                    : product?.images?.length > 0
                    ? product?.images[0]
                    : NoImage
                }
                alt=""
                style={{
                  // padding:20,
                  // height: 220,
                  objectFit:
                    product?.variant?.images?.length > 0
                      ? 'cover'
                      : product?.images?.length > 0
                      ? 'cover'
                      : 'contain',
                }}
              />
              {/* <img
                className="hover-img"
                src={`${process.env.REACT_APP_BASE_URL}/${product.images[0].url}`}
                alt=""
                style={{ height: 220, objectFit: 'cover' }}
              /> */}
            </div>
          </div>
          <div className="product-action-1">
            {/* <a
              href="#/"
              aria-label="Quick view"
              className="action-btn hover-up"
              data-bs-toggle="modal"
              //   onClick={(e) => openQuickView(product)}
            >
              <i className="fi-rs-eye"></i>
            </a> */}
            {/* <a
              href="#/"
              aria-label="Add To Cart"
              className="action-btn hover-up"
              data-bs-toggle="modal"
              //   onClick={(e) => openQuickView(product)}
            >
              <i className="fi-rs-eye"></i>
            </a> */}
            {product?.price && product?.qty > 0 && (
              <a
                href="#/"
                aria-label="Add To Wishlist"
                className="action-btn hover-up"
                // onClick={(e) => handleWishlist(product)}
                onClick={() => addToWishlist(product)}
              >
                <i className="fi-rs-heart"></i>
              </a>
            )}

            {/* <a
              href="#/"
              aria-label="Compare"
              className="action-btn hover-up"
              onClick={(e) => handleCompare(product)}
            >
              <i className="fi-rs-shuffle"></i>
            </a> */}
          </div>
          {/* 
          <div className="product-badges product-badges-position product-badges-mrg">
            {product.trending && <span className="hot">Hot</span>}
            {product.created && <span className="new">New</span>}
            {product.totalSell > 100 && <span className="best">Best Sell</span>}
            {product.discount.isActive && <span className="sale">Sale</span>}
            {product.discount.percentage >= 5 && (
              <span className="hot">{product.discount.percentage}%</span>
            )}
          </div> */}
        </div>
        <div className="product-content-wrap">
          <h2>
            <a
              href="#/"
              onClick={() =>
                product?.price
                  ? navigate(
                      product?.productType === 'NonMedicine'
                        ? `/product/${product.productTemplateId}/${product.id}`
                        : `/product/${product.productTemplateId}/${product.id}/medicine`
                    )
                  : navigate(
                      product?.productType === 'NonMedicine'
                        ? `/product/${product.id}/out-of-stock`
                        : `/product/${product.id}/out-of-stock/medicine`
                    )
              }
              // to={`/product/${product.productTemplateId}/${product.id}`}
            >
              {product?.variant?.name ? (
                <TextTruncate
                  line={2}
                  element="span"
                  truncateText="…"
                  text={product.variant.name}
                />
              ) : (
                <TextTruncate
                  line={2}
                  element="span"
                  truncateText="…"
                  text={product?.name}
                />
              )}
            </a>
          </h2>
          {/* <div className="product-category"> */}
          {/* <Link to="/products">{product.brand}</Link> */}
          {/* <Link to="/products">{product?.category[0]?.name}</Link> */}
          {/* <span>{product?.category?.name}</span> <br /> */}
          {/* <span>{product?.brand?.name}</span>
          </div> */}

          <div className="product-category">
            <span>{product?.brand?.name}</span>
            <span>{product?.manufacture?.name}</span>
            <span>{product?.productType}</span>
          </div>

          {/* <div className="product-rate-cover">
            <div className="product-rate d-inline-block">
              <div className="product-rating" style={{ width: '90%' }}></div>
            </div>
            <span className="font-small ml-5 text-muted">
              ({product.ratingScore})
              {product.total_rating}
            </span>
          </div> */}

          {/* <div>
            <span className="font-small text-muted">
              By <Link to="/vendor/1">NestFood</Link>
            </span>
          </div> */}

          <div className="product-card-bottom">
            {product?.price && (
              <div className="product-price">
                <span>₹{product?.price}</span>
                <span className="old-price">
                  {/* {product.oldPrice && `$ ${product.oldPrice}`} */}₹
                  {product?.mrpPrice}
                </span>
              </div>
            )}
            {/* 
            {product?.productType === 'Medicine' && (
              <Badge bg="danger">{product?.productType}</Badge>
            )} */}

            {/* {product?.price && product.qty > 0 ? (
              <div className="add-cart mt-10">
                <a
                  href="#/"
                  className="add"
                  onClick={(e) => addToCart(product)}
                >
                  <i className="fi-rs-shopping-cart mr-5"></i> Add
                </a>
              </div>
            ) : (
              <Tag color="red">Out Of Stock</Tag>
            )} */}
          </div>
        </div>
        <div
          className="product-content-wrap"
          style={{ marginTop: 'auto', marginBottom: 10 }}
        >
          {product?.price && product.qty > 0 ? (
            <div className="add-cart">
              <a href="#/" className="add" onClick={(e) => addToCart(product)}>
                <i className="fi-rs-shopping-cart mr-5"></i> Add
              </a>
            </div>
          ) : (
            <Tag color="red">Out Of Stock</Tag>
          )}
        </div>
      </div>
    </>
  )
}

// const mapDispatchToProps = {
//   addToCart,
//   addToCompare,
//   addToWishlist,
//   openQuickView,
// }

export default ProductCard
