/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import React, { useEffect, useState } from 'react'
import { Link } from 'react-router-dom'
import { toast } from 'react-toastify'
import { useDispatch, useSelector } from 'react-redux'
import { addToCartSlice } from 'redux/slices/cartSlice'
import { addToWishlistSlice } from 'redux/slices/wishlistSlice'
import { useNavigate } from 'react-router-dom'
import { Tag } from 'antd'
import NoImage from 'assets/imgs/shop/no-image.svg'
import TextTruncate from 'react-text-truncate'
// import { openQuickView } from '../../redux/action/quickViewAction'

const ProductCard2 = ({
  product,
  // addToCart,
  // addToCompare,
  // addToWishlist,
  // openQuickView,
}) => {
  console.log('plsskjsgj')
  const navigate = useNavigate()
  const dispatch = useDispatch()

  const [width, setWidth] = useState(window.innerWidth)
  const handleWindowSizeChange = () => {
    setWidth(window.innerWidth)
  }

  useEffect(() => {
    window.addEventListener('resize', handleWindowSizeChange)
    return () => {
      window.removeEventListener('resize', handleWindowSizeChange)
    }
  }, [])

  const isMobile = width <= 478

  const { authorized } = useSelector((state) => state.auth)

  const addToCart = async (product) => {
    if (authorized) {
      dispatch(addToCartSlice(product.id))
    } else {
      toast.warning('You have to login before adding to cart')
      navigate('/Login')
    }
  }

  const addToWishlist = (product) => {
    if (authorized) {
      dispatch(addToWishlistSlice(product.id))
    } else {
      toast.warning('You have to login before adding to Wishlist')
      navigate('/Login')
    }
  }
  return (
    <>
      <div
        className="product-cart-wrap mb-30 slider-product-card"
        style={{ height: '250px' }}
      >
        <div
          className="product-img-action-wrap mob-p-0 product-cart-wrap-mob"
          onClick={() =>
            product?.price &&
            navigate(`/product/${product.productTemplateId}/${product.id}`)
          }
        >
          <div className="product-img product-img-zoom">
            <Link to={`/product/${product.productTemplateId}/${product.id}`}>
              <img
                className="default-img"
                style={{
                  objectFit:
                    product?.variant?.images?.length > 0
                      ? 'cover'
                      : product?.images?.length > 0
                      ? 'cover'
                      : 'contain',
                }}
                src={
                  product?.variant?.images?.length > 0
                    ? product?.variant?.images[0]
                    : product?.images?.length > 0
                    ? product?.images[0]
                    : NoImage
                }
                alt=""
              />
              {/* <img
                className="hover-img"
                src={require('assets/imgs/shop/product-9-1.png')}
                alt=""
              /> */}
            </Link>
          </div>
          <div className="product-action-1">
            {/* <a
              href="#/"
              aria-label="Quick view"
              className="action-btn hover-up"
              data-bs-toggle="modal"
              // data-bs-target="#quickViewModal"
              //   onClick={(e) => openQuickView(product)}
            >
              <i className="fi-rs-eye"></i>
            </a> */}
            {product?.price && product?.qty > 0 && (
              <a
                href="#/"
                aria-label="Add To Wishlist"
                className="action-btn hover-up"
                onClick={(e) => addToWishlist(product)}
              >
                <i className="fi-rs-heart"></i>
              </a>
            )}

            {/* <a
              href="#/"
              aria-label="Compare"
              className="action-btn hover-up"
              onClick={(e) => handleCompare(product)}
            >
              <i className="fi-rs-shuffle"></i>
            </a> */}
          </div>

          {/* <div className="product-badges product-badges-position product-badges-mrg">
            {product.trending && <span className="hot">Hot</span>}
            {product.created && <span className="new">New</span>}
            {product.totalSell > 100 && <span className="best">Best Sell</span>}
            {product.discount.isActive && <span className="sale">Sale</span>}
            {product.discount.percentage >= 5 && (
              <span className="hot">{product.discount.percentage}%</span>
            )}
          </div> */}
        </div>
        <div className="product-content-wrap mob-p-5">
          <div className="product-category">
            {/* <Link to="/products">{product.brand}</Link> */}
            {isMobile ? (
              <TextTruncate
                line={1}
                element="span"
                truncateText="…"
                text={product?.category?.name}
              />
            ) : (
              <span>{product?.category?.name}</span>
            )}
          </div>
          <h2 className="product-heading">
            {/* <Link to={`/products/${product.slug}`}>{product.title}</Link> */}
            <TextTruncate
              line={2}
              element="a"
              truncateText="…"
              text={
                product?.variant?.name
                  ? product.variant.name
                  : product.name
                  ? product.name
                  : product.name
              }
            />
            {/* <a
              href="#/"
              onClick={() =>
                product?.price &&
                navigate(`/product/${product.productTemplateId}/${product.id}`)
              }
              className="text-truncate"
            >
              {product?.variant?.name
                ? product.variant.name
                : product.name
                ? product.name
                : product.name}
            </a> */}
          </h2>

          {/* <div className="product-rate d-inline-block">
            <div className="product-rating" style={{ width: '90%' }}></div>
          </div> */}

          <div className="product-price mt-10 mb-15">
            {product?.price && (
              <>
                <span>₹{product?.price}</span>
                <span className="old-price">
                  {/* {product.oldPrice && `$ ${product.oldPrice}`} */}₹
                  {product?.mrpPrice}
                </span>
              </>
            )}
          </div>

          {/* <div className="sold mt-15 mb-15">
            <div className="progress mb-5">
              <div
                className="progress-bar"
                role="progressbar"
                style={{ width: '50%' }}
              ></div>
            </div>
            <span className="font-xs text-heading"> Sold: 90/120</span>
          </div> */}
          {product?.price && product.qty > 0 ? (
            <>
              <a
                href="#/"
                className="btn w-100 hover-up desk-cart-show-btn-slider"
                onClick={(e) => addToCart(product)}
              >
                <i className="fi-rs-shopping-cart mr-5"></i>{' '}
                <span>Add To Cart</span>
              </a>

              {/* <a
                href="#/"
                className="btn w-100 hover-up mob-cart-show-btn-slider"
                onClick={(e) => addToCart(product)}
              >
                <i className="fi-rs-shopping-cart mr-5"></i>
              </a> */}
            </>
          ) : (
            <Tag color="red" className="mt-1">
              Out Of Stock
            </Tag>
          )}
        </div>
      </div>
    </>
  )
}

// const mapDispatchToProps = {
//   addToCart,
//   addToCompare,
//   addToWishlist,
//   openQuickView,
// }

export default ProductCard2
