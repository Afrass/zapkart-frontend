/* eslint-disable react/prop-types */
import React from "react";
import { Link } from "react-router-dom";

const CustomActionComp = ({ iconImg, heading, content }) => {
  return (
    <main className="main page-404">
      <div className="page-content pt-50 pb-50">
        <div className="container">
          <div className="row">
            <div className="col-xl-8 col-lg-10 col-md-12 m-auto text-center">
              <p className="mb-20">
                <img src={iconImg} alt="" className="hover-up" />
              </p>
              <h1 className="display-2 mb-30">{heading}</h1>
              <p className="font-lg text-grey-700 mb-30">{content}</p>
              {/* <div className="search-form">
                    <form action="#">
                      <input type="text" placeholder="Search…" />
                      <button type="submit">
                        <i className="fi-rs-search"></i>
                      </button>
                    </form>
                  </div> */}
              <Link
                to="/"
                className="btn btn-default submit-auto-width font-xs hover-up mt-30"
              >
                <i className="fi-rs-home mr-5"></i> Back To Home Page
              </Link>
            </div>
          </div>
        </div>
      </div>
    </main>
  );
};

export default CustomActionComp;
