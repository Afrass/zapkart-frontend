/* eslint-disable react/prop-types */
import React from 'react'

const RadioButton = ({ filters, handleRadioButton }) => {
  return (
    <>
      {filters.map((item) => (
        <div key={item.value} className="d-flex align-items-center mt-4">
          <input
            type="radio"
            // className="form-check-input"
            name={item.name}
            value={item.value}
            checked={item.checked}
            onChange={(e) => handleRadioButton(e)}
            id={item.value}
            defaultChecked={item.defaultChecked}
          />
          <label
            htmlFor={item.label}
            className="mx-2"
            style={{
              textTransform: 'capitalize',
              transform: 'translateY(5px)',
            }}
          >
            <h6>{item.label}</h6>
          </label>
        </div>
      ))}
      {/* <input type="radio" id="html" name="fav_language" value="HTML" /> 
      <label htmlFor="html">HTML</label> */}
    </>
  )
}

export default RadioButton
