/* eslint-disable react/prop-types */
import moment from 'moment'
import React, { useEffect, useState } from 'react'
import reviewService from 'service/review'

const ProductTab = ({ product, productTemplate }) => {
  const [activeIndex, setActiveIndex] = useState(1)
  const [reviews, setReviews] = useState([])

  const getProductReviews = async (itemId) => {
    const reviewsData = await reviewService.getProductReview(itemId)

    if(reviewsData) {
      setReviews(reviewsData)
    }
  }

  useEffect(() => {
    if(product?.id) {
      getProductReviews(product?.id)
    }
  }, [product?.id])

  const handleOnClick = (index) => {
    setActiveIndex(index)
  }

  return (
    <div className="product-info mb-20">
      <div className="tab-style3">
        <ul className="nav nav-tabs text-uppercase">
          <li className="nav-item">
            <a
              className={activeIndex === 1 ? 'nav-link active' : 'nav-link'}
              id="Description-tab"
              data-bs-toggle="tab"
              onClick={() => handleOnClick(1)}
            >
              Description
            </a>
          </li>
          <li className="nav-item">
            <a
              className={activeIndex === 2 ? 'nav-link active' : 'nav-link'}
              id="Additional-info-tab"
              data-bs-toggle="tab"
              onClick={() => handleOnClick(2)}
            >
              Additional info
            </a>
          </li>
          {/* <li className="nav-item">
            <a
              className={activeIndex === 3 ? 'nav-link active' : 'nav-link'}
              id="Reviews-tab"
              data-bs-toggle="tab"
              onClick={() => handleOnClick(3)}
            >
              Vendor
            </a>
          </li> */}
          <li className="nav-item">
            <a
              href="#/"
              className={activeIndex === 4 ? 'nav-link active' : 'nav-link'}
              id="Reviews-tab"
              data-bs-toggle="tab"
              onClick={() => handleOnClick(4)}
            >
              Reviews {reviews?.length > 0 ? `(${reviews.length})`: ''}
            </a>
          </li>
        </ul>
        <div className="tab-content shop_info_tab entry-main-content">
          <div
            className={
              activeIndex === 1 ? 'tab-pane fade show active' : 'tab-pane fade'
            }
            id="Description"
          >
            <div
              dangerouslySetInnerHTML={{
                __html: product?.variant?.description
                  ? product?.variant?.description
                  : product?.description
                  ? product?.description
                  : productTemplate?.description,
              }}
            />
            {/* <div className="">
              <p>
                Uninhibited carnally hired played in whimpered dear gorilla
                koala depending and much yikes off far quetzal goodness and from
                for grimaced goodness unaccountably and meadowlark near
                unblushingly crucial scallop tightly neurotic hungrily some and
                dear furiously this apart.
              </p>
              <p>
                Spluttered narrowly yikes left moth in yikes bowed this that
                grizzly much hello on spoon-fed that alas rethought much
                decently richly and wow against the frequent fluidly at
                formidable acceptably flapped besides and much circa far over
                the bucolically hey precarious goldfinch mastodon goodness
                gnashed a jellyfish and one however because.
              </p>
              <ul className="product-more-infor mt-30">
                <li>
                  <span>Type Of Packing</span> Bottle
                </li>
                <li>
                  <span>Color</span> Green, Pink, Powder Blue, Purple
                </li>
                <li>
                  <span>Quantity Per Case</span> 100ml
                </li>
                <li>
                  <span>Ethyl Alcohol</span> 70%
                </li>
                <li>
                  <span>Piece In One</span> Carton
                </li>
              </ul>
              <hr className="wp-block-separator is-style-dots" />
              <p>
                Laconic overheard dear woodchuck wow this outrageously taut
                beaver hey hello far meadowlark imitatively egregiously hugged
                that yikes minimally unanimous pouted flirtatiously as beaver
                beheld above forward energetic across this jeepers beneficently
                cockily less a the raucously that magic upheld far so the this
                where crud then below after jeez enchanting drunkenly more much
                wow callously irrespective limpet.
              </p>
              <h4 className="mt-30">Packaging & Delivery</h4>
              <hr className="wp-block-separator is-style-wide" />
              <p>
                Less lion goodness that euphemistically robin expeditiously
                bluebird smugly scratched far while thus cackled sheepishly
                rigid after due one assenting regarding censorious while
                occasional or this more crane went more as this less much amid
                overhung anathematic because much held one exuberantly sheep
                goodness so where rat wry well concomitantly.
              </p>
              <p>
                Scallop or far crud plain remarkably far by thus far iguana lewd
                precociously and and less rattlesnake contrary caustic wow this
                near alas and next and pled the yikes articulate about as less
                cackled dalmatian in much less well jeering for the thanks
                blindly sentimental whimpered less across objectively fanciful
                grimaced wildly some wow and rose jeepers outgrew lugubrious
                luridly irrationally attractively dachshund.
              </p>
            </div> */}
          </div>
          <div
            className={
              activeIndex === 2 ? 'tab-pane fade show active' : 'tab-pane fade'
            }
            id="Additional-info"
          >
            <table className="font-md">
              <tbody>
                {/* {product?.productCode && (
                  <tr>
                    <th>Product Code</th>
                    <td>
                      <p>{product?.productCode}</p>
                    </td>
                  </tr>
                )} */}

                {product?.brand?.name && (
                  <tr>
                    <th>Brand</th>
                    <td>
                      <p>{product?.brand?.name}</p>
                    </td>
                  </tr>
                )}
                {/* {product?.category?.name && (
                  <tr>
                    <th>Category</th>
                    <td>
                      <p>{product?.category?.name}</p>
                    </td>
                  </tr>
                )} */}

                {/* {product?.qty && (
                  <tr>
                    <th>Overall Quantity</th>
                    <td>
                      <p>{product?.qty}</p>
                    </td>
                  </tr>
                )} */}
                {/* {product?.allowedQuantityPerOrder && (
                  <tr>
                    <th>Allowed Quantity Per Order</th>
                    <td>
                      <p>{product?.allowedQuantityPerOrder}</p>
                    </td>
                  </tr>
                )} */}

                {/* <tr>
                  <th>Prescription Required</th>
                  <td>
                    <p>{product?.prescriptionRequired ? 'Yes' : 'No'}</p>
                  </td>
                </tr> */}

                <tr>
                  <th>Returnable</th>
                  <td>
                    <p>{product?.returnable ? 'Yes' : 'No'}</p>
                  </td>
                </tr>

                <tr>
                  <th>Height</th>
                  <td>
                    <p>
                      {product?.shippingDetail?.height}{' '}
                      {product?.shippingDetail?.lengthClass}
                    </p>
                  </td>
                </tr>
                <tr>
                  <th>Width</th>
                  <td>
                    <p>
                      {product?.shippingDetail?.height}{' '}
                      {product?.shippingDetail?.lengthClass}
                    </p>
                  </td>
                </tr>
                <tr>
                  <th>Length</th>
                  <td>
                    <p>
                      {product?.shippingDetail?.length}{' '}
                      {product?.shippingDetail?.lengthClass}
                    </p>
                  </td>
                </tr>
                <tr>
                  <th>Weight</th>
                  <td>
                    <p>
                      {product?.shippingDetail?.weight}{' '}
                      {product?.shippingDetail?.weightClass}
                    </p>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          {/* <div
            className={
              activeIndex === 3 ? 'tab-pane fade show active' : 'tab-pane fade'
            }
            id="Reviews"
          >
            <div className="vendor-logo d-flex mb-30">
              <img src="/assets/imgs/vendor/vendor-18.svg" alt="" />
              <div className="vendor-name ml-15">
                <h6>
                  <a href="vendor-details-2.html">Noodles Co.</a>
                </h6>
                <div className="product-rate-cover text-end">
                  <div className="product-rate d-inline-block">
                    <div
                      className="product-rating"
                      style={{ width: '90%' }}
                    ></div>
                  </div>
                  <span className="font-small ml-5 text-muted">
                    {' '}
                    (32 reviews)
                  </span>
                </div>
              </div>
            </div>
            <ul className="contact-infor mb-50">
              <li>
                <img src="/assets/imgs/theme/icons/icon-location.svg" alt="" />
                <strong>Address: </strong>{' '}
                <span>
                  5171 W Campbell Ave undefined Kent, Utah 53127 United States
                </span>
              </li>
              <li>
                <img src="/assets/imgs/theme/icons/icon-contact.svg" alt="" />
                <strong>Contact Seller:</strong>
                <span>(+91) - 540-025-553</span>
              </li>
            </ul>
            <div className="d-flex mb-55">
              <div className="mr-30">
                <p className="text-brand font-xs">Rating</p>
                <h4 className="mb-0">92%</h4>
              </div>
              <div className="mr-30">
                <p className="text-brand font-xs">Ship on time</p>
                <h4 className="mb-0">100%</h4>
              </div>
              <div>
                <p className="text-brand font-xs">Chat response</p>
                <h4 className="mb-0">89%</h4>
              </div>
            </div>
            <p>
              Noodles & Company is an American fast-casual restaurant that
              offers international and American noodle dishes and pasta in
              addition to soups and salads. Noodles & Company was founded in
              1995 by Aaron Kennedy and is headquartered in Broomfield,
              Colorado. The company went public in 2013 and recorded a $457
              million revenue in 2017.In late 2018, there were 460 Noodles &
              Company locations across 29 states and Washington, D.C.
            </p>
          </div> */}
          <div
            className={
              activeIndex === 4 ? 'tab-pane fade show active' : 'tab-pane fade'
            }
            id="Reviews"
          >
            <div className="comments-area">
              <div className="row">
                <div className="col-lg-12">
                  <h4 className="mb-30">Customer Reviews</h4>
                  <div className="comment-list">
                    {
                      reviews?.length > 0 ? (
                        reviews?.map(review => (
                          <div className="single-comment justify-content-between d-flex" key={review.id}>
                            <div className="user justify-content-between align-items-center d-flex">
                              <div className="thumb text-center">
                                <img src={review?.userImage} alt="" />
                                <h6>
                                  <a href="#/">{review?.userName}</a>
                                </h6>
                                {/* <p className="font-xxs">Since 2012</p> */}
                              </div>
                              <div className="desc">
                                {/* <div className="product-rate d-inline-block">
                                  <div
                                    className="product-rating"
                                    style={{
                                      width: '90%',
                                    }}
                                  ></div>
                                </div> */}
                                <p>
                                  {review?.title}
                                </p>
                                <div className="d-flex justify-content-between">
                                  <div className="d-flex justify-content-center flex-column">
                                    <p className="font-xs mr-30">
                                      {review?.message}
                                    </p>
                                    <p className="font-xs mr-30">
                                      {moment(new Date(review?.createdAt * 1000)).format('MMMM DD YYYY, hh:mm a')}
                                      {/* December 4, 2020 at 3:12 pm */}
                                    </p>
                                    {/* <a href="#" className="text-brand btn-reply">
                                      Reply
                                      <i className="fi-rs-arrow-right"></i>
                                    </a> */}
                                  </div>
                                </div>
                              </div>
                            </div>
                        </div>
                        ))
                      ): <p>No Reviews Found</p>
                    }
                 

                    {/* <div className="single-comment justify-content-between d-flex">
                      <div className="user justify-content-between d-flex">
                        <div className="thumb text-center">
                          <img src="/assets/imgs/blog/author-3.png" alt="" />
                          <h6>
                            <a href="#">Ana Rosie</a>
                          </h6>
                          <p className="font-xxs">Since 2008</p>
                        </div>
                        <div className="desc">
                          <div className="product-rate d-inline-block">
                            <div
                              className="product-rating"
                              style={{
                                width: '90%',
                              }}
                            ></div>
                          </div>
                          <p>Great low price and works well.</p>
                          <div className="d-flex justify-content-between">
                            <div className="d-flex align-items-center">
                              <p className="font-xs mr-30">
                                December 4, 2020 at 3:12 pm
                              </p>
                              <a href="#" className="text-brand btn-reply">
                                Reply
                                <i className="fi-rs-arrow-right"></i>
                              </a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div className="single-comment justify-content-between d-flex">
                      <div className="user justify-content-between d-flex">
                        <div className="thumb text-center">
                          <img src="/assets/imgs/blog/author-4.png" alt="" />
                          <h6>
                            <a href="#">Steven Keny</a>
                          </h6>
                          <p className="font-xxs">Since 2010</p>
                        </div>
                        <div className="desc">
                          <div className="product-rate d-inline-block">
                            <div
                              className="product-rating"
                              style={{
                                width: '90%',
                              }}
                            ></div>
                          </div>
                          <p>
                            Authentic and Beautiful, Love these way more than
                            ever expected They are Great earphones
                          </p>
                          <div className="d-flex justify-content-between">
                            <div className="d-flex align-items-center">
                              <p className="font-xs mr-30">
                                December 4, 2020 at 3:12 pm
                              </p>
                              <a href="#" className="text-brand btn-reply">
                                Reply
                                <i className="fi-rs-arrow-right"></i>
                              </a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div> */}
                  </div>
                </div>
                {/* <div className="col-lg-4">
                  <h4 className="mb-30">Customer reviews</h4>
                  <div className="d-flex mb-30">
                    <div className="product-rate d-inline-block mr-15">
                      <div
                        className="product-rating"
                        style={{
                          width: '90%',
                        }}
                      ></div>
                    </div>
                    <h6>4.8 out of 5</h6>
                  </div>
                  <div className="progress">
                    <span>5 star</span>
                    <div
                      className="progress-bar"
                      role="progressbar"
                      style={{
                        width: ' 50%',
                      }}
                      aria-valuenow="50"
                      aria-valuemin="0"
                      aria-valuemax="100"
                    >
                      50%
                    </div>
                  </div>
                  <div className="progress">
                    <span>4 star</span>
                    <div
                      className="progress-bar"
                      role="progressbar"
                      style={{
                        width: ' 25%',
                      }}
                      aria-valuenow="25"
                      aria-valuemin="0"
                      aria-valuemax="100"
                    >
                      25%
                    </div>
                  </div>
                  <div className="progress">
                    <span>3 star</span>
                    <div
                      className="progress-bar"
                      role="progressbar"
                      style={{
                        width: ' 45%',
                      }}
                      aria-valuenow="45"
                      aria-valuemin="0"
                      aria-valuemax="100"
                    >
                      45%
                    </div>
                  </div>
                  <div className="progress">
                    <span>2 star</span>
                    <div
                      className="progress-bar"
                      role="progressbar"
                      style={{
                        width: ' 65%',
                      }}
                      aria-valuenow="65"
                      aria-valuemin="0"
                      aria-valuemax="100"
                    >
                      65%
                    </div>
                  </div>
                  <div className="progress mb-30">
                    <span>1 star</span>
                    <div
                      className="progress-bar"
                      role="progressbar"
                      style={{
                        width: ' 85%',
                      }}
                      aria-valuenow="85"
                      aria-valuemin="0"
                      aria-valuemax="100"
                    >
                      85%
                    </div>
                  </div>
                  <a href="#" className="font-xs text-muted">
                    How are ratings calculated?
                  </a>
                </div> */}
              </div>
            </div>

            {/* <div className="comment-form">
              <h4 className="mb-15">Add a review</h4>
              <div className="product-rate d-inline-block mb-30"></div>
              <div className="row">
                <div className="col-lg-8 col-md-12">
                  <form
                    className="form-contact comment_form"
                    action="#"
                    id="commentForm"
                  >
                    <div className="row">
                      <div className="col-12">
                        <div className="form-group">
                          <textarea
                            className="form-control w-100"
                            name="comment"
                            id="comment"
                            cols="30"
                            rows="9"
                            placeholder="Write Comment"
                          ></textarea>
                        </div>
                      </div>
                      <div className="col-sm-6">
                        <div className="form-group">
                          <input
                            className="form-control"
                            name="name"
                            id="name"
                            type="text"
                            placeholder="Name"
                          />
                        </div>
                      </div>
                      <div className="col-sm-6">
                        <div className="form-group">
                          <input
                            className="form-control"
                            name="email"
                            id="email"
                            type="email"
                            placeholder="Email"
                          />
                        </div>
                      </div>
                      <div className="col-12">
                        <div className="form-group">
                          <input
                            className="form-control"
                            name="website"
                            id="website"
                            type="text"
                            placeholder="Website"
                          />
                        </div>
                      </div>
                    </div>
                    <div className="form-group">
                      <button
                        type="submit"
                        className="button button-contactForm"
                      >
                        Submit Review
                      </button>
                    </div>
                  </form>
                </div>
              </div>
            </div> */}
          </div>
        </div>
      </div>
    </div>
  )
}

export default ProductTab
