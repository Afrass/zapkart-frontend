/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import React from 'react'
// import OutsideClickHandler from 'react-outside-click-handler'

const SearchSuggestion = ({
  searchSuggestions,
  onSelect,
  searchTerm = '',
  setIsSuggestionShown,
  isSuggestionShown,
}) => {
  const availableSearchsuggestions = searchSuggestions?.filter((cur) =>
    cur.text.toLowerCase().includes(searchTerm.toLowerCase())
  )

  console.log(
    availableSearchsuggestions,
    searchTerm,
    searchSuggestions,
    'shoeeee'
  )
  return (
    <>
      {searchTerm?.trim().length > 0 &&
        isSuggestionShown &&
        availableSearchsuggestions?.length > 0 && (
          <div className="search-suggestions">
            {availableSearchsuggestions?.map((cur) => (
              <div
                key={cur.text}
                className="search-item d-flex align-items-center p-2"
                onClick={() => onSelect(cur.text)}
              >
                {cur?.image && (
                  <img src={cur.image} alt="product" height={32} width={32} />
                )}
                <div className="ml-10">
                  <p>{cur.text}</p>
                  {cur.category}
                </div>
              </div>
            ))}
          </div>
        )}
      {/* </OutsideClickHandler> */}
    </>
  )
}

export default SearchSuggestion
