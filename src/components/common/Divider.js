import React from 'react'

const Divider = () => {
  return (
    <hr
      style={{
        background: '#c2b7b7',
        margin: '20px 0',
      }}
    />
  )
}

export default Divider
