/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import React, { useState } from 'react'
import { toast } from 'react-toastify'
// import {
//   addToCart,
//   decreaseQuantity,
//   increaseQuantity,
// } from '../../redux/action/cart'
// import { addToCompare } from '../../redux/action/compareAction'
// import { addToWishlist } from '../../redux/action/wishlistAction'
import ProductTab from './ProductTab'
import RelatedSlider from '../sliders/RelatedSlider'
import ThumbSlider from '../sliders/ThumbSlider'
import { useNavigate } from 'react-router-dom'
import { useSelector } from 'react-redux'
import { useEffect } from 'react'
import { addToCartApi } from 'api/products'

const ProductDetailsPresc = (
  //   {
  //   product,
  //   cartItems,
  //   addToCompare,
  //   addToCart,
  //   addToWishlist,
  //   increaseQuantity,
  //   decreaseQuantity,
  //   quickView,
  // }
  { product }
) => {
  // const [quantity, setQuantity] = useState(1)
  const [quantityCounter, setQuantityCounter] = useState(1)
  const navigate = useNavigate()
  const { authorized } = useSelector((state) => state.auth)

  useEffect(() => {
    if (product) {
      setQuantityCounter(product.minOrderQty)
    }
  }, [product])

  const addToCart = async (product) => {
    // const getCartItem = window.localStorage.getItem('cartItems')
    // if (getCartItem) {
    //   const parsedItems = JSON.parse(getCartItem)
    //   const storeItem = [...parsedItems, product]

    //   window.localStorage.setItem('cartItems', JSON.stringify(storeItem))
    //   toast.success('add To Cart')
    // } else {
    //   window.localStorage.setItem('cartItems', JSON.stringify([product]))
    //   toast.success('add To Cart')
    // }
    if (authorized) {
      try {
        const sendingData = {
          productId: product._id,
          // TODO: CHECK LATER
          quantity: quantityCounter,
        }
        const { data } = await addToCartApi(sendingData)
        data && toast.success(`Added ${product.name} To Cart`)
      } catch (err) {
        toast.error(err.response.data.error)
      }
    } else {
      toast.warning('You have to login before adding to cart')
      navigate('/Login')
    }
  }

  const handleCart = (product) => {
    // addToCart(product)
    toast('Product added to Cart !')
  }

  const handleCompare = (product) => {
    // addToCompare(product)
    toast('Added to Compare list !')
  }

  const handleWishlist = (product) => {
    // addToWishlist(product)
    toast('Added to Wishlist !')
  }

  const inCart = true
  //   cartItems.find((cartItem) => cartItem.id === product.id)

  const addToWishlist = (product) => {
    if (authorized) {
      const wishListItem = window.localStorage.getItem('wishlistItems')
      if (wishListItem) {
        const parsedItems = JSON.parse(wishListItem)
        const storeItem = [...parsedItems, product]

        window.localStorage.setItem('wishlistItems', JSON.stringify(storeItem))
        toast.success(toast.success(`Added ${product.name} To Wishlist`))
      } else {
        window.localStorage.setItem('wishlistItems', JSON.stringify([product]))
        toast.success(toast.success(`Added ${product.name} To Wishlist`))
      }
    } else {
      toast.warning('You have to login before adding to Wishlist')
      navigate('/Login')
    }
  }

  return (
    <>
      <section>
        {/* <div className="container"> */}
        <div className="row flex-row-reverse">
          {/* col-xl-10  */}
          <div className="col-lg-12 m-auto">
            <div className="product-detail accordion-detail">
              {/* mt-30 */}
              <div className="row">
                <div className="col-lg-6 col-md-12 col-sm-12 col-xs-12 mb-md-0 mb-sm-5">
                  <div className="detail-gallery">
                    <span className="zoom-icon">
                      <i className="fi-rs-search"></i>
                    </span>

                    <div className="product-image-slider">
                      <ThumbSlider
                        images={
                          product?.images?.length > 0 ? product.images : []
                        }
                      />
                    </div>
                  </div>
                </div>
                <div className="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                  <div className="detail-info  pr-30 pl-30">
                    {/* out-stock */}
                    <span className="stock-status">
                      {' '}
                      {product?.availability}{' '}
                    </span>
                    <h2 className="title-detail">{product?.name}</h2>
                    <div className="product-detail-rating">
                      <div className="product-rate-cover text-end">
                        <div className="product-rate d-inline-block">
                          <div
                            className="product-rating"
                            style={{
                              width: `${100 / product?.total_rating}%`,
                            }}
                          ></div>
                        </div>
                        <span className="font-small ml-5 text-muted">
                          {' '}
                          ({product?.total_rating_count} Ratings)
                        </span>
                      </div>
                    </div>
                    <div className="clearfix product-price-cover">
                      <div className="product-price primary-color float-left">
                        <span className="current-price  text-brand">
                          ₹{product?.productPricing?.salePrice}
                        </span>
                        <span>
                          {/* <span className="save-price font-md color3 ml-15">
                              {product?.discount?.percentage}% Off
                            </span> */}
                          <span className="old-price font-md ml-15">
                            {/* {product.oldPrice
                                ? `$ ${product?.oldPrice}`
                                : null} */}
                            ₹{product?.productPricing?.listPrice}
                          </span>
                        </span>
                      </div>
                    </div>

                    <div className="short-desc mb-30">
                      <p className="font-lg">
                        {product?.textDescription
                          ? product?.textDescription
                          : 'Text Description needs to add...'}
                      </p>
                      <div className="mt-4">
                        <h5>Manufacture</h5>
                        <p>Glaxo SmithKline Pharmaceuticals Ltd</p>
                        <h5>Salt Composition</h5>
                        <p>Amoxycillin (500mg) + Clavulanic Acid (125mg)</p>
                        <h5>Storage</h5>
                        <p>Store below 30</p>
                      </div>
                    </div>
                    <div className="attr-detail attr-color mb-15">
                      {/* <strong className="mr-10">Color</strong> */}
                      {/* <ul className="list-filter color-filter">
                          {product?.variations?.map((clr, i) => (
                            <li key={i}>
                              <a href="#/">
                                <span className={`product-color-${clr}`}></span>
                              </a>
                            </li>
                          ))}
                        </ul> */}
                    </div>
                    {/* <div className="attr-detail attr-size">
                        <strong className="mr-10">Size</strong>
                        <ul className="list-filter size-filter font-small">
                          <li className="active">
                            <a>M</a>
                          </li>
                          <li>
                            <a>L</a>
                          </li>
                          <li>
                            <a>XL</a>
                          </li>
                          <li>
                            <a>XXL</a>
                          </li>
                        </ul>
                      </div> */}
                    {/* <div className="bt-1 border-color-1 mt-30 mb-30"></div> */}
                    <div className="detail-extralink">
                      <div className="detail-qty border radius">
                        <a
                          href="#/"
                          onClick={() => {
                            quantityCounter > product.minOrderQty &&
                              setQuantityCounter(quantityCounter - 1)
                          }}
                          // onClick={(e) =>
                          //   !inCart
                          //     ? setQuantity(quantity > 1 ? quantity - 1 : 1)
                          //     : decreaseQuantity(product?.id)
                          // }
                          className="qty-down"
                        >
                          <i className="fi-rs-angle-small-down"></i>
                        </a>
                        <span className="qty-val">{quantityCounter}</span>
                        <a
                          href="#/"
                          // onClick={() =>
                          //   !inCart
                          //     ? setQuantity(quantity + 1)
                          //     : increaseQuantity(product.id)
                          // }
                          onClick={() => {
                            product.maxOrderQty > quantityCounter &&
                              setQuantityCounter(quantityCounter + 1)
                          }}
                          className="qty-up"
                        >
                          <i className="fi-rs-angle-small-up"></i>
                        </a>
                      </div>
                      <div className="product-extra-link2">
                        <button
                          onClick={() => addToCart(product)}
                          className="button button-add-to-cart"
                        >
                          Add to cart
                        </button>
                        <a
                          href="#/"
                          aria-label="Add To Wishlist"
                          className="action-btn hover-up"
                          // onClick={(e) => handleWishlist(product)}
                          onClick={() => addToWishlist(product)}
                        >
                          <i className="fi-rs-heart"></i>
                        </a>
                        {/* <a
                            href="#/"
                            aria-label="Compare"
                            className="action-btn hover-up"
                            onClick={(e) => handleCompare(product)}
                          >
                            <i className="fi-rs-shuffle"></i>
                          </a> */}
                      </div>
                    </div>
                    <ul className="product-meta font-xs color-grey mt-50">
                      {/* <li className="mb-5">
                          SKU:
                          <a href="#/">FWM15VKT</a>
                        </li> */}
                      {/* <li className="mb-5">
                        Tags:
                        {product?.tags?.map((tag, i) => (
                          <a key={i} href="#/" rel="tag" className="me-1">
                            {tag},
                          </a>
                        ))}
                      </li>
                      <li>
                        Availability:
                        <span className="in-stock text-success ml-5">
                          {product?.availability}
                        </span>
                      </li> */}
                    </ul>
                  </div>
                </div>
              </div>

              {/* {quickView ? null : (
                  <>
                    <ProductTab />
                    <div className="row mt-60">
                      <div className="col-12">
                        <h3 className="section-title style-1 mb-30">
                          Related products
                        </h3>
                      </div>
                      <div className="col-12">
                        <div className="row related-products position-relative">
                          <RelatedSlider />
                        </div>
                      </div>
                    </div>
                  </>
                )} */}
            </div>
          </div>
        </div>
        {/* </div> */}
      </section>
    </>
  )
}

// const mapStateToProps = (state) => ({
//   cartItems: state.cart,
// })

// const mapDispatchToProps = {
//   addToCompare,
//   addToWishlist,
//   addToCart,
//   increaseQuantity,
//   decreaseQuantity,
// }

export default ProductDetailsPresc
