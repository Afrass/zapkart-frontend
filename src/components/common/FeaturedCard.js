/* eslint-disable react/prop-types */
import React from 'react'

const FeaturedCard = ({ icon, heading, content }) => {
  return (
    <div className="featured-card">
      <img src={icon} alt="icon" />
      <h4>{heading}</h4>
      <p>{content}</p>
      <a href="#/">Read more</a>
    </div>
  )
}

export default FeaturedCard
