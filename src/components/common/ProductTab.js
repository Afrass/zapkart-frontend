/* eslint-disable react/prop-types */
import Rating from "components/ecommerce/Rating";
// import moment from "moment";
import React, { useEffect, useState } from "react";
import reviewService from "service/review";

const ProductTab = ({ product, productTemplate }) => {
  const [reviews, setReviews] = useState([]);

  const getProductReviews = async (itemId) => {
    const reviewsData = await reviewService.getProductReview(itemId);

    if (reviewsData) {
      setReviews(reviewsData);
    }
  };

  useEffect(() => {
    if (product?.id) {
      getProductReviews(product?.id);
    }
  }, [product?.id]);

  return (
    <>
      <div className="mb-20">
        <div className="tab-style3">
          <div className="tab-content shop_info_tab entry-main-content">
            <div className="product-info mb-20">
              <div className={"tab-pane fade show active"} id="Description">
                <h4 className="mb-10">Description</h4>
                <div
                  dangerouslySetInnerHTML={{
                    __html: product?.variant?.description
                      ? product?.variant?.description
                      : product?.description
                      ? product?.description
                      : productTemplate?.description,
                  }}
                />
              </div>
            </div>
            {/* <div className="product-info mb-20">
              <div className={"tab-pane fade show active"} id="Additional-info">
                <h4 className="mb-10">Additional information</h4>
                <table className="font-md">
                  <tbody>
                    {product?.brand?.name && (
                      <tr>
                        <th>Brand</th>
                        <td>
                          <p>{product?.brand?.name}</p>
                        </td>
                      </tr>
                    )}

                    <tr>
                      <th>Returnable</th>
                      <td>
                        <p>{product?.returnable ? "Yes" : "No"}</p>
                      </td>
                    </tr>

                    <tr>
                      <th>Height</th>
                      <td>
                        <p>
                          {product?.shippingDetail?.height}{" "}
                          {product?.shippingDetail?.lengthClass}
                        </p>
                      </td>
                    </tr>
                    <tr>
                      <th>Width</th>
                      <td>
                        <p>
                          {product?.shippingDetail?.height}{" "}
                          {product?.shippingDetail?.lengthClass}
                        </p>
                      </td>
                    </tr>
                    <tr>
                      <th>Length</th>
                      <td>
                        <p>
                          {product?.shippingDetail?.length}{" "}
                          {product?.shippingDetail?.lengthClass}
                        </p>
                      </td>
                    </tr>
                    <tr>
                      <th>Weight</th>
                      <td>
                        <p>
                          {product?.shippingDetail?.weight}{" "}
                          {product?.shippingDetail?.weightClass}
                        </p>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div> */}

            {/* <div
            className={
              'tab-pane fade show active'
            }
            id="Reviews"
          >
            <div className="vendor-logo d-flex mb-30">
              <img src="/assets/imgs/vendor/vendor-18.svg" alt="" />
              <div className="vendor-name ml-15">
                <h6>
                  <a href="vendor-details-2.html">Noodles Co.</a>
                </h6>
                <div className="product-rate-cover text-end">
                  <div className="product-rate d-inline-block">
                    <div
                      className="product-rating"
                      style={{ width: '90%' }}
                    ></div>
                  </div>
                  <span className="font-small ml-5 text-muted">
                    {' '}
                    (32 reviews)
                  </span>
                </div>
              </div>
            </div>
            <ul className="contact-infor mb-50">
              <li>
                <img src="/assets/imgs/theme/icons/icon-location.svg" alt="" />
                <strong>Address: </strong>{' '}
                <span>
                  5171 W Campbell Ave undefined Kent, Utah 53127 United States
                </span>
              </li>
              <li>
                <img src="/assets/imgs/theme/icons/icon-contact.svg" alt="" />
                <strong>Contact Seller:</strong>
                <span>(+91) - 540-025-553</span>
              </li>
            </ul>
            <div className="d-flex mb-55">
              <div className="mr-30">
                <p className="text-brand font-xs">Rating</p>
                <h4 className="mb-0">92%</h4>
              </div>
              <div className="mr-30">
                <p className="text-brand font-xs">Ship on time</p>
                <h4 className="mb-0">100%</h4>
              </div>
              <div>
                <p className="text-brand font-xs">Chat response</p>
                <h4 className="mb-0">89%</h4>
              </div>
            </div>
            <p>
              Noodles & Company is an American fast-casual restaurant that
              offers international and American noodle dishes and pasta in
              addition to soups and salads. Noodles & Company was founded in
              1995 by Aaron Kennedy and is headquartered in Broomfield,
              Colorado. The company went public in 2013 and recorded a $457
              million revenue in 2017.In late 2018, there were 460 Noodles &
              Company locations across 29 states and Washington, D.C.
            </p>
          </div> */}
            <div className="product-info">
              <div className={"tab-pane fade show active"} id="Reviews">
                <Rating reviews={reviews} />

                {/* <div className="comments-area">
              <div className="row">
                <div className="col-lg-12">
                  <h4 className="mb-10">Customer Reviews</h4>
                  <div className="comment-list">
                    {reviews?.data?.length > 0 ? (
                      reviews?.data.map((review) => (
                        <div
                          className="single-comment justify-content-between d-flex"
                          key={review.id}
                        >
                          <div className="user justify-content-between align-items-center d-flex">
                            <div className="thumb text-center">
                              <img src={review?.userImage} alt="" />
                              <h6>
                                <a href="#/">{review?.userName}</a>
                              </h6>
                              <p className="font-xxs">Since 2012</p>
                            </div>
                            <div className="desc">
                              <div className="product-rate d-inline-block">
                                  <div
                                    className="product-rating"
                                    style={{
                                      width: '90%',
                                    }}
                                  ></div>
                                </div>
                              <p>{review?.title}</p>
                              <div className="d-flex justify-content-between">
                                <div className="d-flex justify-content-center flex-column">
                                  <p className="font-xs mr-30">
                                    {review?.message}
                                  </p>
                                  <p className="font-xs mr-30">
                                    {moment(
                                      new Date(review?.createdAt * 1000)
                                    ).format("MMMM DD YYYY, hh:mm a")}
                                    December 4, 2020 at 3:12 pm
                                  </p>
                                  <a href="#" className="text-brand btn-reply">
                                      Reply
                                      <i className="fi-rs-arrow-right"></i>
                                    </a>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      ))
                    ) : (
                      <p>No Reviews Found</p>
                    )}

                  </div>
                </div>
                <div className="col-lg-4">
                  <h4 className="mb-30">Customer reviews</h4>
                  <div className="d-flex mb-30">
                    <div className="product-rate d-inline-block mr-15">
                      <div
                        className="product-rating"
                        style={{
                          width: '90%',
                        }}
                      ></div>
                    </div>
                    <h6>4.8 out of 5</h6>
                  </div>
                  <div className="progress">
                    <span>5 star</span>
                    <div
                      className="progress-bar"
                      role="progressbar"
                      style={{
                        width: ' 50%',
                      }}
                      aria-valuenow="50"
                      aria-valuemin="0"
                      aria-valuemax="100"
                    >
                      50%
                    </div>
                  </div>
                  <div className="progress">
                    <span>4 star</span>
                    <div
                      className="progress-bar"
                      role="progressbar"
                      style={{
                        width: ' 25%',
                      }}
                      aria-valuenow="25"
                      aria-valuemin="0"
                      aria-valuemax="100"
                    >
                      25%
                    </div>
                  </div>
                  <div className="progress">
                    <span>3 star</span>
                    <div
                      className="progress-bar"
                      role="progressbar"
                      style={{
                        width: ' 45%',
                      }}
                      aria-valuenow="45"
                      aria-valuemin="0"
                      aria-valuemax="100"
                    >
                      45%
                    </div>
                  </div>
                  <div className="progress">
                    <span>2 star</span>
                    <div
                      className="progress-bar"
                      role="progressbar"
                      style={{
                        width: ' 65%',
                      }}
                      aria-valuenow="65"
                      aria-valuemin="0"
                      aria-valuemax="100"
                    >
                      65%
                    </div>
                  </div>
                  <div className="progress mb-30">
                    <span>1 star</span>
                    <div
                      className="progress-bar"
                      role="progressbar"
                      style={{
                        width: ' 85%',
                      }}
                      aria-valuenow="85"
                      aria-valuemin="0"
                      aria-valuemax="100"
                    >
                      85%
                    </div>
                  </div>
                  <a href="#" className="font-xs text-muted">
                    How are ratings calculated?
                  </a>
                </div>
              </div>
            </div> */}

                {/* <div className="comment-form">
              <h4 className="mb-15">Add a review</h4>
              <div className="product-rate d-inline-block mb-30"></div>
              <div className="row">
                <div className="col-lg-8 col-md-12">
                  <form
                    className="form-contact comment_form"
                    action="#"
                    id="commentForm"
                  >
                    <div className="row">
                      <div className="col-12">
                        <div className="form-group">
                          <textarea
                            className="form-control w-100"
                            name="comment"
                            id="comment"
                            cols="30"
                            rows="9"
                            placeholder="Write Comment"
                          ></textarea>
                        </div>
                      </div>
                      <div className="col-sm-6">
                        <div className="form-group">
                          <input
                            className="form-control"
                            name="name"
                            id="name"
                            type="text"
                            placeholder="Name"
                          />
                        </div>
                      </div>
                      <div className="col-sm-6">
                        <div className="form-group">
                          <input
                            className="form-control"
                            name="email"
                            id="email"
                            type="email"
                            placeholder="Email"
                          />
                        </div>
                      </div>
                      <div className="col-12">
                        <div className="form-group">
                          <input
                            className="form-control"
                            name="website"
                            id="website"
                            type="text"
                            placeholder="Website"
                          />
                        </div>
                      </div>
                    </div>
                    <div className="form-group">
                      <button
                        type="submit"
                        className="button button-contactForm"
                      >
                        Submit Review
                      </button>
                    </div>
                  </form>
                </div>
              </div>
            </div> */}
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default ProductTab;
