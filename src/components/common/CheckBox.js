/* eslint-disable react/prop-types */

import React from 'react'

const CheckBox = ({ filters, handleCheckBox }) => {
  return (
    <div>
      {filters.map((item) => (
        <div key={item.id}>
          <input
            type="checkbox"
            className="form-check-input"
            name={item.id}
            value={item.id}
            checked={item.defaultChecked}
            onChange={(e) => handleCheckBox(e)}
            id={item.id}
          />
          {item.name && (
            <label
              className="form-check-label"
              htmlFor={item.id}
              style={{ textTransform: 'capitalize' }}
            >
              {' '}
              {item.name}
            </label>
          )}

          <br />
        </div>
      ))}
    </div>
  )
}

export default CheckBox
