/* eslint-disable react/prop-types */
import React, { useEffect, useState } from "react";
import orderService from "service/order";

const CheckIfDeliverable = ({ productId }) => {
  const [zipCode, setZipCode] = useState("");
  const [deliverable, setIsDeliverable] = useState(null);

  useEffect(() => {
    if (zipCode?.trim().length === 0) {
      setIsDeliverable(null);
    }
  }, [zipCode]);

  const checkIfDeliverableHandler = async (e) => {
    e.preventDefault();
    if (zipCode?.trim()?.length > 0) {
      const checkDeliverable = await orderService.checkIfDeliverable(
        productId,
        zipCode
      );
      if (checkDeliverable?.isDeliver) {
        setIsDeliverable(true);
        // toast.success('Product Is Deliverable in Your Zipcode')
      } else {
        setIsDeliverable(false);

        // toast.error('Product Is Not Deliverable in Your Zipcode')
      }
    }
  };
  return (
    <div className="d-flex align-items-center mt-20">
      <form
        onSubmit={checkIfDeliverableHandler}
        className="d-flex align-items-center"
      >
        <div
          style={{
            transform: "translateY(-15px)",
            position: "relative",
          }}
        >
          <label>Check Deliverable?</label>
          <input
            placeholder="Enter Zipcode"
            className="form-control mr-10"
            onChange={(e) => setZipCode(e.target.value)}
            value={zipCode}
          />
          <div className="deliverable-container">
            {deliverable ? (
              <span className="deliverable">Deliverable</span>
            ) : deliverable === false ? (
              <span className="not-deliverable">Not Deliverable</span>
            ) : null}
          </div>
        </div>

        <button type="submit" className="button button-add-to-cart ml-5">
          Check
        </button>
      </form>
    </div>
  );
};

export default CheckIfDeliverable;
