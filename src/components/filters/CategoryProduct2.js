/* eslint-disable react/prop-types */
// import { useRouter } from "next/router";
// import { connect } from "react-redux";
// import { updateProductCategory } from "../../../redux/action/productFiltersAction";
import React from 'react'
import { useDispatch } from 'react-redux'
// Svg Icons
// import category1 from 'assets/imgs/theme/icons/category-1.svg'
// import category2 from 'assets/imgs/theme/icons/category-2.svg'
// import category3 from 'assets/imgs/theme/icons/category-3.svg'
// import category4 from 'assets/imgs/theme/icons/category-4.svg'
// import category5 from 'assets/imgs/theme/icons/category-5.svg'
import { Link, useNavigate } from 'react-router-dom'
import { setFilters } from 'redux/slices/productTemplateSlice'

const CategoryProduct2 = ({ categories }) => {
  const navigate = useNavigate()
  const dispatch = useDispatch()
  // const router = useRouter();

  // const removeSearchTerm = () => {
  //     router.push({
  //         pathname: "/products",
  //     });
  // };

  // const selectCategory = (e, category) => {
  //   // e.preventDefault()
  //   // console.log(category)
  //   // removeSearchTerm();
  //   // updateProductCategory(category);
  //   // router.push({
  //   //     pathname: "/products",
  //   //     query: {
  //   //         cat: category, //
  //   //     },
  //   // });
  // }

  const onCategoryClick = (id, name) => {
    dispatch(
      setFilters([
        { filterName: 'categoryId', value: id },
        { filterName: 'selected', value: `category-${name}` },
      ])
    )
    navigate(`/products?categoryId=${id}&selected=category-${name}`)
  }

  return (
    <>
      <ul>
        {categories?.map((cur, i) => (
          <li key={i} onClick={() => onCategoryClick(cur.id, cur?.name)}>
            <Link to={`/products?categoryId=${cur?.id}`}>
              <img src={cur?.image} alt="" />
              {cur.name}
            </Link>
          </li>
        ))}
      </ul>
    </>
  )
}

export default CategoryProduct2
