/* eslint-disable react/prop-types */
import React from 'react'

// Import Svgs
// import cat1Icon from 'assets/imgs/theme/icons/category-1.svg'
// import cat2Icon from 'assets/imgs/theme/icons/category-2.svg'
// import cat3Icon from 'assets/imgs/theme/icons/category-3.svg'
import { Link, useNavigate } from 'react-router-dom'

const CategoryProduct = ({ categories }) => {
  const navigate = useNavigate()
  // eslint-disable-next-line no-unused-vars
  const selectCategory = (e, category) => {
    e.preventDefault()
    // removeSearchTerm();
    // updateProductCategory(category);
    // router.push({
    //     pathname: "/products",
    //     query: {
    //         cat: category, //
    //     },
    // });
  }
  return (
    <>
      <ul>
        <li onClick={() => navigate(`/products`)}>
          <Link to={`/products`}>All</Link>
        </li>
        {categories?.map((cat) => (
          <li
            key={cat.id}
            onClick={() => navigate(`/products?categoryId=${cat.id}`)}
          >
            <Link to={`/products?categoryId=${cat.id}`}>
              {/* <img src={cat1Icon} alt="" /> */}
              {cat.name}
            </Link>
            {/* <span className="count">30</span> */}
          </li>
        ))}

        {/* <li onClick={(e) => selectCategory(e, 'shoe')}>
          <a href="#/">
            <img src={cat2Icon} alt="" />
            Clothing
          </a>
          <span className="count">35</span>
        </li>
        <li onClick={(e) => selectCategory(e, 'jacket')}>
          <a href="#/">
            <img src={cat3Icon} alt="" />
            Pet Foods{' '}
          </a>
          <span className="count">42</span>
        </li> */}
      </ul>
    </>
  )
}

export default CategoryProduct
