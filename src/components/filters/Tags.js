/* eslint-disable react/prop-types */

import React, { useState } from 'react'
// import { updateProductFilters } from "../../../redux/action/productFiltersAction";

const Tags = () => {
  const tags = [
    { value: '' },
    { value: 'snack' },
    { value: 'milk' },
    { value: 'fruit' },
    { value: 'broccoli' },
    { value: 'salad' },
    { value: 'appetizer' },
  ]
  // const [selectedTags, setTags] = useState([]);
  const [active, setActive] = useState(0)
  // useEffect(() => {
  //     const filters = {
  //         tags: selectedTags, //
  //     };

  //     updateProductFilters(filters);
  // }, [selectedTags]);

  // eslint-disable-next-line no-unused-vars
  const handleClick = (i, target) => {
    // setTags(target);
    setActive(active === i ? 0 : i)
  }
  return (
    <>
      <ul className="tags-list">
        {tags.map((tag, i) => (
          <li
            key={i}
            className="hover-up"
            onClick={() => handleClick(i, tag.value)}
          >
            <a
              href="#/"
              className={
                active === i ? 'cat-item text-brand-2' : 'cat-item text-brand'
              }
            >
              <i className="fi-rs-cross mr-10"></i>
              {i === 0 ? 'All' : `${tag.value}`}
            </a>
          </li>
        ))}
      </ul>
    </>
  )
}

// const mapDidpatchToProps = {
//   updateProductFilters,
// }

export default Tags
