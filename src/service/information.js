import instance from 'api'
const informationService = {}

informationService.getInformations = async function () {
  try {
    const {
      data: { data },
    } = await instance.get('/api/v1/information/public')
    const resData = data.filter((cur) => cur.status === 'Active')
    return resData
  } catch (err) {
    console.log(err, 'show-err')
  }
}

informationService.getInformationById = async function (id) {
  try {
    const {
      data: { data },
    } = await instance.get(`/api/v1/information/${id}/public`)
    return data
  } catch (err) {
    console.log(err, 'show-err')
  }
}

export default informationService
