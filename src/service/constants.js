import instance from 'api'
const constantsService = {}

constantsService.getConstants = async function () {
  try {
    const { data } = await instance.get('/api/v1/constants')

    return data
  } catch (err) {
    console.log(err, 'show-err')
  }
}

export default constantsService
