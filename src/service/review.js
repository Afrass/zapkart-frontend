import instance from "api";
const reviewService = {};

reviewService.getCustomerReviewByProduct = async function (
  userId,
  orderId,
  itemId
) {
  try {
    const {
      data: { data },
    } = await instance.get(
      `/api/v1/reviews?userId=${userId}&orderId=${orderId}&itemId=${itemId}`
    );
    // const resData = data.filter((cur) => cur.status === 'Active')
    return data;
  } catch (err) {
    console.log(err.response.data, "show-err");
  }
};

reviewService.getProductReview = async function (itemId) {
  try {
    const { data } = await instance.get(`/api/v1/reviews?itemId=${itemId}`);
    return data;
  } catch (err) {
    console.log(err.response.data, "show-err");
  }
};

reviewService.createProductReview = async function (data) {
  try {
    const res = await instance.post(`/api/v1/reviews`, data);
    return res;
  } catch (err) {
    console.log(err);
  }
};

reviewService.editProductReview = async function (reviewId, data) {
  try {
    const res = await instance.put(`/api/v1/reviews/${reviewId}`, data);
    return res;
  } catch (err) {
    console.log(err);
  }
};

reviewService.deleteProductReview = async function (reviewId) {
  try {
    const res = await instance.delete(`/api/v1/reviews/${reviewId}`);
    return res;
  } catch (err) {
    console.log(err);
  }
};

export default reviewService;
