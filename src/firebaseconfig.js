// Import the functions you need from the SDKs you need
import { initializeApp } from 'firebase/app'
import { getAuth } from 'firebase/auth'
import { getMessaging, getToken } from 'firebase/messaging'
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration

//
// const firebaseConfig = {
//   apiKey: "AIzaSyBUdVQKSYiMrWXKZLhpcR4pmtIUFa-40w0",
//   authDomain: "zapkartnew2022-dc8a8.firebaseapp.com",
//   projectId: "zapkartnew2022-dc8a8",
//   storageBucket: "zapkartnew2022-dc8a8.appspot.com",
//   messagingSenderId: "812753080859",
//   appId: "1:812753080859:web:63d1a2c52ff2a2a5161ebe",
// };

// // Initialize Firebase
// const intializeFirebase = initializeApp(firebaseConfig);

// export const authentication = getAuth(intializeFirebase);

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: 'AIzaSyDE96sBpGy5JJKHNwTHP3OuUSChRCIgH_U',
  authDomain: 'ecommerce-74b71.firebaseapp.com',
  projectId: 'ecommerce-74b71',
  storageBucket: 'ecommerce-74b71.appspot.com',
  messagingSenderId: '1031062435017',
  appId: '1:1031062435017:web:d6b5ac03cd40d344480918',
}

// Initialize Firebase
const intializeFirebase = initializeApp(firebaseConfig)

export const authentication = getAuth(intializeFirebase)

const messaging = getMessaging()

const vapidKey =
  // "BCCUk3qpa0a_mI0M_AYJV6mjrkg3WfRgq2gadB5Hx6PRXNAZRyFuJRNzMPR9p-wVH4en_4z7X9Y5QRcj9BLacs0";
  'BIrtcruShLJM1CmUV4z1NPWMxQPkBKTF83rCSN1WgiK-Hii-QAzOQtkxVODBEDD7YFwg3j5guRfffW95eHSbiM8'

export const getDeviceToken = (setDeviceToken) => {
  getToken(messaging, { vapidKey })
    .then((currentToken) => {
      if (currentToken) {
        setDeviceToken(currentToken)
        // Send the token to your server and update the UI if necessary
        // ...
      } else {
        // Show permission request UI
        console.log(
          'No registration token available. Request permission to generate one.'
        )
        // ...
      }
    })
    .catch((err) => {
      setDeviceToken(null)
      console.log('An error occurred while retrieving token. ', err)
      // ...
    })
}

export default intializeFirebase
