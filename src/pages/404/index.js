import React from 'react'
import Layout from 'components/layout/Layout'
import { Link } from 'react-router-dom'
import CustomActionComp from 'components/common/CustomActionComp'

import Icon404 from 'assets/imgs/page/page-404.png'

const Custom404 = () => {
  const content = (
    <>
      The link you clicked may be broken or the page may have been removed.
      <br />
      visit the{' '}
      <Link to="/">
        <span> Homepage</span>
      </Link>
      or{' '}
      <Link to="/contact">
        <span>Contact us</span>
      </Link>
      about the problem
    </>
  )
  return (
    <>
      <Layout parent="Home" sub="Pages" subChild="404">
        <CustomActionComp
          iconImg={Icon404}
          heading="Page Not Found"
          content={content}
        />
      </Layout>
    </>
  )
}

export default Custom404
