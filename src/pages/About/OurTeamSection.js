import React from 'react'

// SVG Icon Imports
import facebookIcon from 'assets/imgs/theme/icons/icon-facebook-brand.svg'
import instagramIcon from 'assets/imgs/theme/icons/icon-instagram-brand.svg'
import youtubeIcon from 'assets/imgs/theme/icons/icon-youtube-brand.svg'
import twitterIcon from 'assets/imgs/theme/icons/icon-twitter-brand.svg'

const OurTeamSection = () => {
  return (
    <div className="container">
      <div className="row">
        <div className="col-xl-10 col-lg-12 m-auto">
          <section className="mb-50">
            <h2 className="title style-3 mb-40 text-center">Our Team</h2>
            <div className="row">
              <div className="col-lg-4 mb-lg-0 mb-md-5 mb-sm-5">
                <h6 className="mb-5 text-brand">Our Team</h6>
                <h1 className="mb-30">Meet Our Expert Team</h1>
                <p className="mb-30">
                  Proin ullamcorper pretium orci. Donec necscele risque leo. Nam
                  massa dolor imperdiet neccon sequata congue idsem. Maecenas
                  malesuada faucibus finibus.
                </p>
                <p className="mb-30">
                  Proin ullamcorper pretium orci. Donec necscele risque leo. Nam
                  massa dolor imperdiet neccon sequata congue idsem. Maecenas
                  malesuada faucibus finibus.
                </p>
                <a href="#/" className="btn">
                  View All Members
                </a>
              </div>
              <div className="col-lg-8">
                <div className="row">
                  <div className="col-lg-6 col-md-6">
                    <div className="team-card">
                      <img
                        src={require('assets/imgs/page/about-6.png')}
                        alt=""
                      />
                      <div className="content text-center">
                        <h4 className="mb-5">H. Merinda</h4>
                        <span>CEO & Co-Founder</span>
                        <div className="social-network mt-20">
                          <a href="#/">
                            <img src={facebookIcon} alt="" />
                          </a>
                          <a href="#/">
                            <img src={twitterIcon} alt="" />
                          </a>
                          <a href="#/">
                            <img src={instagramIcon} alt="" />
                          </a>
                          <a href="#/">
                            <img src={youtubeIcon} alt="" />
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-6 col-md-6">
                    <div className="team-card">
                      <img
                        src={require('assets/imgs/page/about-8.png')}
                        alt=""
                      />
                      <div className="content text-center">
                        <h4 className="mb-5">Dilan Specter</h4>
                        <span>Head Engineer</span>
                        <div className="social-network mt-20">
                          <a href="#/">
                            <img src={facebookIcon} alt="" />
                          </a>
                          <a href="#/">
                            <img src={twitterIcon} alt="" />
                          </a>
                          <a href="#/">
                            <img src={instagramIcon} alt="" />
                          </a>
                          <a href="#/">
                            <img src={youtubeIcon} alt="" />
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>
  )
}

export default OurTeamSection
