import React from 'react'
import Layout from 'components/layout/Layout'

// Sections Import
import AboutIntroSection from './AboutIntroSection'
import AboutFeatureSection from './AboutFeatureSection'

import AboutPerformanceSection from './AboutPerformanceSection'
import AboutTestimonalsSection from './AboutTestimonalsSection'
import OurTeamSection from './OurTeamSection'

const About = () => {
  return (
    <>
      <Layout parent="Home" sub="Pages" subChild="About">
        <div className="container pt-50">
          <div className="row">
            <div className="col-xl-10 col-lg-12 m-auto">
              <AboutIntroSection />
              <AboutFeatureSection />
              <AboutPerformanceSection />
            </div>
          </div>
        </div>
        <AboutTestimonalsSection />
        <OurTeamSection />
      </Layout>
    </>
  )
}

export default About
