import FeaturedCard from 'components/common/FeaturedCard'
import React from 'react'

// SVG Imports
import icon1 from 'assets/imgs/theme/icons/icon-1.svg'
import icon2 from 'assets/imgs/theme/icons/icon-2.svg'
import icon3 from 'assets/imgs/theme/icons/icon-3.svg'
import icon4 from 'assets/imgs/theme/icons/icon-4.svg'
import icon5 from 'assets/imgs/theme/icons/icon-5.svg'
import icon6 from 'assets/imgs/theme/icons/icon-6.svg'

const AboutFeatureSection = () => {
  return (
    <section className="text-center mb-50">
      <h2 className="title style-3 mb-40">What We Provide?</h2>
      <div className="row">
        <div className="col-lg-4 col-md-6 mb-24">
          <FeaturedCard
            icon={icon1}
            heading="Best Prices & Offers"
            content="There are many variations of passages of Lorem Ipsum
            available, but the majority have suffered alteration in
            some form"
          />
        </div>
        <div className="col-lg-4 col-md-6 mb-24">
          <FeaturedCard
            icon={icon2}
            heading="Wide Assortment"
            content="There are many variations of passages of Lorem Ipsum
            available, but the majority have suffered alteration in
            some form"
          />
        </div>
        <div className="col-lg-4 col-md-6 mb-24">
          <FeaturedCard
            icon={icon3}
            heading="Free Delivery"
            content="There are many variations of passages of Lorem Ipsum
            available, but the majority have suffered alteration in
            some form"
          />
        </div>
        <div className="col-lg-4 col-md-6 mb-24">
          <FeaturedCard
            icon={icon4}
            heading="Easy Returns"
            content="There are many variations of passages of Lorem Ipsum
            available, but the majority have suffered alteration in
            some form"
          />
        </div>
        <div className="col-lg-4 col-md-6 mb-24">
          <FeaturedCard
            icon={icon5}
            heading="100% Satisfaction"
            content="There are many variations of passages of Lorem Ipsum
            available, but the majority have suffered alteration in
            some form"
          />
        </div>
        <div className="col-lg-4 col-md-6 mb-24">
          <FeaturedCard
            icon={icon6}
            heading="Great Daily Deal"
            content="There are many variations of passages of Lorem Ipsum
            available, but the majority have suffered alteration in
            some form"
          />
        </div>
      </div>
    </section>
  )
}

export default AboutFeatureSection
