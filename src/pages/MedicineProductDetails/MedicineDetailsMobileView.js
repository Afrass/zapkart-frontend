/* eslint-disable react/prop-types */
import { Collapse, Drawer } from 'antd'
import React from 'react'
import { useSelector } from 'react-redux'
import MedicineSubs from './MedicineSubs'

const MedicineDetailsMobileView = ({
  setIsSubsDrawerOpen,
  isSubsDrawerOpen,
}) => {
  const { Panel } = Collapse
  const { product, productTemplate } = useSelector(
    (state) => state.productTemplate
  )
  return (
    <Collapse className="mb-3">
      {product?.description && (
        <Panel header="Description" key="1">
          <section id="description">
            <h4 className="mb-2">Description</h4>
            <div
              className="med-another-content"
              dangerouslySetInnerHTML={{
                __html: product?.description,
              }}
            />
          </section>
        </Panel>
      )}

      {productTemplate?.howToUse && (
        <Panel header="How to Use" key="2">
          <section id="howtouse">
            <h4 className="mb-2">How To Use</h4>
            <div
              className="med-another-content"
              dangerouslySetInnerHTML={{
                __html: productTemplate?.howToUse,
              }}
            />
          </section>
        </Panel>
      )}
      {productTemplate?.uses && (
        <Panel header="Uses" key="3">
          <section id="uses">
            <h4 className="mb-2">Uses</h4>
            <div
              className="med-another-content"
              dangerouslySetInnerHTML={{
                __html: productTemplate?.uses,
              }}
            />
          </section>
        </Panel>
      )}

      {productTemplate?.expertAdvice && (
        <Panel header="Expert Advice" key="4">
          <section id="expertAdvice">
            <h4 className="mb-2">Expert Advice</h4>
            <div
              className="med-another-content"
              dangerouslySetInnerHTML={{
                __html: productTemplate?.expertAdvice,
              }}
            />
          </section>
        </Panel>
      )}
      {productTemplate?.faq && (
        <Panel header="FAQ" key="5">
          <section id="faq">
            <h4 className="mb-2">FAQ</h4>
            <div
              className="med-another-content"
              dangerouslySetInnerHTML={{
                __html: productTemplate?.faq,
              }}
            />
          </section>
        </Panel>
      )}
      {productTemplate?.pregnancyInteraction && (
        <Panel header="Pregnancy Interaction" key="6">
          <section id="pregnancyInteraction">
            <h4 className="mb-2">Pregnancy Interaction</h4>
            <div
              className="med-another-content"
              dangerouslySetInnerHTML={{
                __html: productTemplate?.pregnancyInteraction,
              }}
            />
          </section>
        </Panel>
      )}
      {productTemplate?.sideEffects && (
        <Panel header="SideEffects" key="7">
          <section id="sideEffects">
            <h4 className="mb-2">SideEffects</h4>
            <div
              className="med-another-content"
              dangerouslySetInnerHTML={{
                __html: productTemplate?.sideEffects,
              }}
            />
          </section>
        </Panel>
      )}

      {productTemplate?.substitutes?.length > 0 && (
        <Panel header="Substitutes" key="8">
          <MedicineSubs
            setIsSubsDrawerOpen={setIsSubsDrawerOpen}
            showBtn={true}
          />
          <Drawer
            title={`Related Substitutes`}
            placement="right"
            onClose={() => setIsSubsDrawerOpen(false)}
            visible={isSubsDrawerOpen}
            width={'100%'}
          >
            <MedicineSubs
              setIsSubsDrawerOpen={setIsSubsDrawerOpen}
              showBtn={false}
              width="8"
            />
          </Drawer>
        </Panel>
      )}
    </Collapse>
  )
}

export default MedicineDetailsMobileView
