/* eslint-disable react/prop-types */
import React from 'react'
import { useSelector } from 'react-redux'
import { useNavigate } from 'react-router-dom'

const MedicineSubs = ({ showBtn, width = '5', setIsSubsDrawerOpen }) => {
  console.log(showBtn, setIsSubsDrawerOpen, 'SSK')
  const { productTemplate } = useSelector((state) => state.productTemplate)
  const navigate = useNavigate()
  return (
    <>
      <div className="section-scroll-offset-escape" id="substitutes"></div>
      <section>
        <h4 className="mb-2">Substitutes</h4>
        <p className="mb-10">
          For informational purposes only. Consult a doctor before taking any
          medicines.
        </p>

        <div className="row">
          <div className={`col-lg-${width} col-sm-12`}>
            {productTemplate?.substitutes?.slice(0, 5).map((cur) => (
              <div
                onClick={() => {
                  navigate(
                    `/product/${cur.productTemplateId}/${cur.productId}/medicine`
                  )
                  setIsSubsDrawerOpen(false)
                }}
                key={cur?.name}
                className="d-flex m-2 justify-content-between mb-20"
                role="button"
              >
                <div className="">
                  <h6>{cur?.name}</h6>
                  <p style={{ fontSize: 13 }}>{cur?.brand?.name}</p>
                </div>
                <div className="d-flex align-items-center">
                  <h6 className="current-price  text-brand mr-20">
                    {cur?.price && `₹${cur?.price}`}
                  </h6>

                  <span className="fi-rs-angle-right"></span>
                </div>
              </div>
            ))}
            {showBtn && (
              <div>
                <button
                  type="submit"
                  className="button button-add-to-cart ml-5"
                  onClick={() => setIsSubsDrawerOpen(true)}
                >
                  See All
                </button>
              </div>
            )}
          </div>
        </div>
      </section>
    </>
  )
}

export default MedicineSubs
