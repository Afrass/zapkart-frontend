/* eslint-disable react/prop-types */
import React, { useEffect, useState } from 'react'
// import Scrollspy from 'react-scrollspy'

// import ProductDetails from '../../components/ecommerce/ProductDetails'
import Layout from '../../components/layout/Layout'
// import productData from 'data/product'
import { useParams } from 'react-router-dom'
import ProductDetails from 'components/ecommerce/ProductDetails'
import scrollToTop from 'util/scrollToTop'
// import Producthowtouse from './Producthowtouse'
import Divider from 'components/common/Divider'
// import UsesBenefits from './UsesBenefits'
// import SideEffects from './SideEffects'
import { useDispatch, useSelector } from 'react-redux'
import {
  getProductfromProductTemplate,
  setResetProductsDetails,
} from 'redux/slices/productTemplateSlice'
import { Collapse } from 'antd'
import Preloader from 'components/common/Preloader'
import MoreDetailsDesktopView from './MoreDetailsDesktopView'
// import MedicineDetailsMobileView from './MedicineDetailsMobileView'
import OtherSellers from '../../components/ecommerce/OtherSellers'

const ProductDetailsMedicine = () => {
  // const [product, setProduct] = useState({})
  // const [activeIndex, setActiveIndex] = useState(1)
  const [width, setWidth] = useState(window.innerWidth)
  const [isSubsDrawerOpen, setIsSubsDrawerOpen] = useState(false)
  // const [quantityCounter, setQuantityCounter] = useState(1)
  // const [relatedProducts, setRelatedProducts] = useState([])
  // const [selectImage, setSelectedImage] = useState(null)
  // const navigate = useNavigate()
  // eslint-disable-next-line no-unused-vars
  const { Panel } = Collapse
  const dispatch = useDispatch()
  const {
    product,
    productsTemplate,
    productTemplate,
    otherProductsFromVariant,
    loading,
  } = useSelector((state) => state.productTemplate)

  let { productTemplateId, id } = useParams()

  useEffect(() => {
    if (id !== 'out-of-stock') {
      dispatch(getProductfromProductTemplate(productTemplateId, id))
    } else {
      dispatch(setResetProductsDetails())
      dispatch(getProductfromProductTemplate(productTemplateId))
    }
    scrollToTop()
  }, [id])

  const handleWindowSizeChange = () => {
    setWidth(window.innerWidth)
  }

  useEffect(() => {
    window.addEventListener('resize', handleWindowSizeChange)
    return () => {
      window.removeEventListener('resize', handleWindowSizeChange)
    }
  }, [])

  const isMobile = width <= 768

  // const { id } = useParams()

  // const findProductIndex = (list, slug) => {
  //   const index = list.findIndex((item) => item.slug === slug)
  //   return index
  // }

  // const index = findProductIndex(productData, id)
  // const onMenuClick = (index) => {
  //   setActiveIndex(index)
  // }

  return (
    <>
      <Layout
        parent="Home"
        sub="Category"
        subChild={productTemplate?.category?.name}
        subSubChild={
          product?.variant?.name || product?.name || productTemplate?.name
        }
      >
        <div className="container">
          {loading ? (
            <Preloader />
          ) : (
            <div className="row mt-4">
              {/* {!isMobile && (
                <div className="col-md-3">
                  <div
                    className="dashboard-menu mb-4"
                    style={{ position: 'sticky', top: '100px' }}
                  >
                    <ul className="nav flex-column" role="tablist">
                      <Scrollspy
                        className="scrollspy"
                        items={[
                          'description',
                          'howtouse',
                          'expertAdvice',
                          'faq',
                          'pregnancyInteraction',
                          'sideEffects',
                          'uses',
                          'substitutes',
                        ]}
                        offset={-80}
                        currentClassName="active"
                      >
                        {productTemplate?.description && (
                          <li className="nav-item">
                            <a
                              href="#description"
                              className={
                                activeIndex === 1 ? 'nav-link' : 'nav-link'
                              }
                              onClick={() => onMenuClick(1)}
                            >
                              Description
                            </a>
                          </li>
                        )}

                        {productTemplate?.howToUse && (
                          <li className="nav-item">
                            <a
                              href="#howtouse"
                              className={
                                activeIndex === 1 ? 'nav-link' : 'nav-link'
                              }
                              onClick={() => onMenuClick(1)}
                            >
                              How To Use
                            </a>
                          </li>
                        )}
                        {productTemplate?.uses && (
                          <li className="nav-item">
                            <a
                              href="#uses"
                              className={
                                activeIndex === 1 ? 'nav-link' : 'nav-link'
                              }
                              onClick={() => onMenuClick(1)}
                            >
                              Uses
                            </a>
                          </li>
                        )}
                        {productTemplate?.expertAdvice && (
                          <li className="nav-item">
                            <a
                              href="#expertAdvice"
                              className={
                                activeIndex === 1 ? 'nav-link' : 'nav-link'
                              }
                              onClick={() => onMenuClick(1)}
                            >
                              Expert Advice
                            </a>
                          </li>
                        )}
                        {productTemplate?.faq && (
                          <li className="nav-item">
                            <a
                              href="#faq"
                              className={
                                activeIndex === 1 ? 'nav-link' : 'nav-link'
                              }
                              onClick={() => onMenuClick(1)}
                            >
                              FAQ
                            </a>
                          </li>
                        )}
                        {productTemplate?.pregnancyInteraction && (
                          <li className="nav-item">
                            <a
                              href="#pregnancyInteraction"
                              className={
                                activeIndex === 1 ? 'nav-link' : 'nav-link'
                              }
                              onClick={() => onMenuClick(1)}
                            >
                              Pregnancy Interaction
                            </a>
                          </li>
                        )}
                        {productTemplate?.sideEffects && (
                          <li className="nav-item">
                            <a
                              href="#sideEffects"
                              className={
                                activeIndex === 1 ? 'nav-link' : 'nav-link'
                              }
                              onClick={() => onMenuClick(1)}
                            >
                              Side Effects
                            </a>
                          </li>
                        )}
                        {productTemplate?.substitutes?.length > 0 && (
                          <li className="nav-item">
                            <a
                              href="#substitutes"
                              className={
                                activeIndex === 1 ? 'nav-link' : 'nav-link'
                              }
                              onClick={() => onMenuClick(1)}
                            >
                              Substitutes
                            </a>
                          </li>
                        )}

                      </Scrollspy>
                    </ul>
                  </div>
                </div>
              )} */}

              <div className={!isMobile ? 'col-md-12 mb-4' : 'col-md-12'}>
                <ProductDetails
                  product={product}
                  productsTemplate={productsTemplate}
                  productTemplate={productTemplate}
                  otherProductsFromVariant={otherProductsFromVariant}
                  type="medicine"
                />
                <Divider />

                {/* <Panel header="Category" key="1"></Panel> */}
                <div className="col-xl-11 col-lg-12 m-auto ">
                  <div className="row">
                    <div className="col-md-8">
                      <div className="product-info">
                        <MoreDetailsDesktopView
                          isSubsDrawerOpen={isSubsDrawerOpen}
                          setIsSubsDrawerOpen={setIsSubsDrawerOpen}
                        />
                        {/* {!isMobile ? (
                          <MoreDetailsDesktopView
                            isSubsDrawerOpen={isSubsDrawerOpen}
                            setIsSubsDrawerOpen={setIsSubsDrawerOpen}
                          />
                        ) : (
                          <MedicineDetailsMobileView
                            isSubsDrawerOpen={isSubsDrawerOpen}
                            setIsSubsDrawerOpen={setIsSubsDrawerOpen}
                          />
                        )} */}
                      </div>
                    </div>
                    <div className="col-md-4">
                      <OtherSellers otherClass="other-sellers-desk-view" />
                    </div>
                  </div>
                </div>

                {/* <Producthowtouse />
                <Divider />
                <UsesBenefits />
                <Divider />
                <SideEffects /> */}
              </div>
            </div>
          )}
        </div>
      </Layout>
    </>
  )
}

export default ProductDetailsMedicine
