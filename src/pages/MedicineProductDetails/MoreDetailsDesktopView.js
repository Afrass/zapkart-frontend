/* eslint-disable react/prop-types */
import { Divider, Drawer } from 'antd'
import React, { useEffect, useState } from 'react'
import { useSelector } from 'react-redux'
import MedicineSubs from './MedicineSubs'

const MoreDetailsDesktopView = ({ isSubsDrawerOpen, setIsSubsDrawerOpen }) => {
  const { productTemplate, product } = useSelector(
    (state) => state.productTemplate
  )

  const [width, setWidth] = useState(window.innerWidth)
  const handleWindowSizeChange = () => {
    setWidth(window.innerWidth)
  }

  useEffect(() => {
    window.addEventListener('resize', handleWindowSizeChange)
    return () => {
      window.removeEventListener('resize', handleWindowSizeChange)
    }
  }, [])

  const isMobile = width <= 478

  console.log(isMobile, 'plsjkgdewuyiv')

  return (
    <>
      {(product?.description || productTemplate?.description) && (
        <>
          <section id="description">
            <h4 className="mb-2">Description</h4>
            <div
              className="med-another-content"
              dangerouslySetInnerHTML={{
                __html: product?.description || productTemplate?.description,
              }}
            />
          </section>
          <Divider />
        </>
      )}

      {productTemplate?.howToUse && (
        <>
          <section id="howtouse">
            <h4 className="mb-2">How To Use</h4>
            <div
              className="med-another-content"
              dangerouslySetInnerHTML={{
                __html: productTemplate?.howToUse,
              }}
            />
          </section>
          <Divider />
        </>
      )}

      {productTemplate?.uses && (
        <>
          <section id="uses">
            <h4 className="mb-2">Uses</h4>
            <div
              className="med-another-content"
              dangerouslySetInnerHTML={{
                __html: productTemplate?.uses,
              }}
            />
          </section>
          <Divider />
        </>
      )}
      {productTemplate?.expertAdvice && (
        <>
          <section id="expertAdvice">
            <h4 className="mb-2">Expert Advice</h4>
            <div
              className="med-another-content"
              dangerouslySetInnerHTML={{
                __html: productTemplate?.expertAdvice,
              }}
            />
          </section>
          <Divider />
        </>
      )}
      {productTemplate?.faq && (
        <>
          <section id="faq">
            <h4 className="mb-2">FAQ</h4>
            <div
              className="med-another-content"
              dangerouslySetInnerHTML={{
                __html: productTemplate?.faq,
              }}
            />
          </section>
          <Divider />
        </>
      )}
      {productTemplate?.pregnancyInteraction && (
        <>
          <section id="pregnancyInteraction">
            <h4 className="mb-2">Pregnancy Interaction</h4>
            <div
              className="med-another-content"
              dangerouslySetInnerHTML={{
                __html: productTemplate?.pregnancyInteraction,
              }}
            />
          </section>
          <Divider />
        </>
      )}

      {productTemplate?.sideEffects && (
        <>
          <section id="sideEffects">
            <h4 className="mb-2">Side Effects</h4>
            <div
              className="med-another-content"
              dangerouslySetInnerHTML={{
                __html: productTemplate?.sideEffects,
              }}
            />
          </section>

          <Divider />
        </>
      )}

      {productTemplate?.substitutes?.length > 0 && (
        <>
          <MedicineSubs
            showBtn={true}
            setIsSubsDrawerOpen={setIsSubsDrawerOpen}
          />
          <Drawer
            title={`Related Substitutes`}
            placement="right"
            onClose={() => setIsSubsDrawerOpen(false)}
            visible={isSubsDrawerOpen}
            width={isMobile ? '100%' : '50%'}
          >
            <MedicineSubs
              showBtn={false}
              setIsSubsDrawerOpen={setIsSubsDrawerOpen}
              width="8"
            />
          </Drawer>
        </>
      )}
    </>
  )
}

export default MoreDetailsDesktopView
