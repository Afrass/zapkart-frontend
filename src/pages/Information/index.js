import React, { useEffect } from "react";
import Layout from "components/layout/Layout";
import { useParams } from "react-router-dom";
import { getInformation } from "redux/slices/informationSlice";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import scrollToTop from "util/scrollToTop";

const Orders = () => {
  const { id } = useParams();
  const dispatch = useDispatch();
  const { information } = useSelector((state) => state.information);

  useEffect(() => {
    dispatch(getInformation(id));
    scrollToTop();
  }, [id]);

  return (
    <Layout parent="Home" sub="Information" subChild={information.name}>
      <section className="mt-50 mb-50">
        <div className="container">
          <h4>{information.name}</h4>
          <img src={information.image} alt="info" className="mt-30" />

          <div className="single-content mt-30">
            <div
              dangerouslySetInnerHTML={{ __html: information.description }}
            />
          </div>
        </div>
      </section>
    </Layout>
  );
};

export default Orders;
