import Layout from 'components/layout/Layout'
import { Link, useLocation } from 'react-router-dom'
import React, { useEffect, useState } from 'react'
import DashboardSection from './DashboardSection'
import OrdersSection from './OrderSection'
// import TrackOrdersSection from './TrackOrdersSection'
import AddressSection from './AddressSection'
import AccountDetailsSection from './AccountDetailsSection'
import { setLogout } from 'redux/slices/authSlice'
import { useDispatch } from 'react-redux'
import UpdatePasswordSection from './UpdatePasswordSection'
import PrescriptionSection from './PrescriptionSection'

const Account = () => {
  const [activeIndex, setActiveIndex] = useState(1)

  const handleOnClick = (index) => setActiveIndex(index)
  const dispatch = useDispatch()
  const { hash } = useLocation()

  useEffect(() => {
    if (hash) {
      getDefaultActiveSideNav(hash)
    }
  }, [hash])

  const getDefaultActiveSideNav = (hash) => {
    switch (hash) {
      case '#dashboard':
        return handleOnClick(1)
      case '#address':
        return handleOnClick(4)
      case '#accountdetails':
        return handleOnClick(5)
      case '#updatepassword':
        return handleOnClick(6)
      case '#prescription':
        return handleOnClick(7)
      case '#orders':
        return handleOnClick(8)
      default:
        return handleOnClick(1)
    }
  }

  return (
    <>
      <Layout parent="Home" sub="Account" subChild={hash?.split('#')[1]}>
        <div className="page-content pt-60 pb-100">
          <div className="container">
            <div className="row">
              <div className="col-lg-10 m-auto">
                <div className="row">
                  <div className="col-md-3">
                    <div className="dashboard-menu">
                      <ul className="nav flex-column" role="tablist">
                        <li className="nav-item">
                          <a
                            href="#dashboard"
                            className={
                              activeIndex === 1 ? 'nav-link active' : 'nav-link'
                            }
                            onClick={() => handleOnClick(1)}
                          >
                            <i className="fi-rs-settings-sliders mr-10"></i>
                            Dashboard
                          </a>
                        </li>

                        {/* <li className="nav-item">
                          <a
                            href="#/"
                            className={
                              activeIndex === 3 ? 'nav-link active' : 'nav-link'
                            }
                            onClick={() => handleOnClick(3)}
                          >
                            <i className="fi-rs-shopping-cart-check mr-10"></i>
                            Track Your Order
                          </a>
                        </li> */}
                        <li className="nav-item">
                          <a
                            href="#address"
                            className={
                              activeIndex === 4 ? 'nav-link active' : 'nav-link'
                            }
                            onClick={() => handleOnClick(4)}
                          >
                            <i className="fi-rs-marker mr-10"></i>My Address
                          </a>
                        </li>
                        <li className="nav-item">
                          <a
                            href="#accountdetails"
                            className={
                              activeIndex === 5 ? 'nav-link active' : 'nav-link'
                            }
                            onClick={() => handleOnClick(5)}
                          >
                            <i className="fi-rs-user mr-10"></i>Account details
                          </a>
                        </li>
                        <li className="nav-item">
                          <a
                            href="#updatepassword"
                            className={
                              activeIndex === 6 ? 'nav-link active' : 'nav-link'
                            }
                            onClick={() => handleOnClick(6)}
                          >
                            <i className="fi-rs-password mr-10"></i>Update
                            Password
                          </a>
                        </li>
                        <li className="nav-item">
                          <a
                            href="#prescription"
                            className={
                              activeIndex === 7 ? 'nav-link active' : 'nav-link'
                            }
                            onClick={() => handleOnClick(7)}
                          >
                            <i className="fi-rs-medicine mr-10"></i>
                            Prescriptions
                          </a>
                        </li>

                        <li className="nav-item">
                          <a
                            href="#orders"
                            className={
                              activeIndex === 8 ? 'nav-link active' : 'nav-link'
                            }
                            onClick={() => handleOnClick(8)}
                          >
                            <i className="fi-rs-shopping-bag mr-10"></i>Orders
                          </a>
                        </li>

                        <li
                          className="nav-item"
                          onClick={() => dispatch(setLogout())}
                        >
                          <Link to="#/" className="nav-link">
                            <i className="fi-rs-sign-out mr-10"></i>Logout
                          </Link>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <div className="col-md-9">
                    <div className="tab-content account dashboard-content">
                      <div
                        className={
                          activeIndex === 1
                            ? 'tab-pane fade active show'
                            : 'tab-pane fade '
                        }
                      >
                        <DashboardSection />
                      </div>

                      {/* <div
                        className={
                          activeIndex === 3
                            ? 'tab-pane fade active show'
                            : 'tab-pane fade '
                        }
                      >
                        <TrackOrdersSection />
                      </div> */}
                      <div
                        className={
                          activeIndex === 4
                            ? 'tab-pane fade active show'
                            : 'tab-pane fade '
                        }
                      >
                        <AddressSection />
                      </div>
                      <div
                        className={
                          activeIndex === 5
                            ? 'tab-pane fade active show'
                            : 'tab-pane fade '
                        }
                      >
                        <AccountDetailsSection />
                      </div>
                      <div
                        className={
                          activeIndex === 6
                            ? 'tab-pane fade active show'
                            : 'tab-pane fade '
                        }
                      >
                        <UpdatePasswordSection />
                      </div>
                      <div
                        className={
                          activeIndex === 7
                            ? 'tab-pane fade active show'
                            : 'tab-pane fade '
                        }
                      >
                        <PrescriptionSection />
                      </div>
                      <div
                        className={
                          activeIndex === 8
                            ? 'tab-pane fade active show'
                            : 'tab-pane fade '
                        }
                      >
                        <OrdersSection />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Layout>
    </>
  )
}

export default Account
