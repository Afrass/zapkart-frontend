// import { authentication } from 'firebaseconfig'
import { Card, Upload } from 'antd'
import useUpload from 'hooks/useUpload'
import React, { useEffect, useState } from 'react'
// import { toast } from 'react-toastify'
import authService from 'service/auth'
// import prescriptionIcon from 'assets/imgs/theme/icons/medical-prescription.png'
import { multipleImageUpload } from 'util/imageUploader'
import { toast } from 'react-toastify'

const PrescriptionSection = () => {
  const [prescriptionsList, setPrescriptionsList] = useState([])
  const [prescriptions, setPrescriptions] = useState([])

  // For multiple prescription Image upload
  const {
    fileList: fileListImages,
    beforeUpload: beforeUploadImages,
    onChange: onChangeImages,
    onRemove: onRemoveImages,
    setFileList: setFileListImages,
  } = useUpload(1, 'multiple')

  const getUserPrescriptions = async () => {
    const allPrescriptions = await authService.getUserPrescriptions()
    if (allPrescriptions) {
      setPrescriptionsList(allPrescriptions)
    }
  }

  useEffect(() => {
    getUserPrescriptions()
  }, [])

  useEffect(() => {
    if (prescriptionsList) {
      const prescs = prescriptionsList.map((cur, i) => {
        return {
          uid: i + Math.random() * 10,
          url: cur,
        }
      })

      setPrescriptions(prescs)

      setFileListImages(prescs)
    }
  }, [prescriptionsList])

  const propsPrescriptions = {
    multiple: true,
    beforeUpload: beforeUploadImages,
    onRemove: onRemoveImages,
    onChange: onChangeImages,
    fileList: fileListImages,
  }

  useEffect(() => {
    setPrescriptions(fileListImages)
  }, [fileListImages])

  const uploadPrescriptions = async () => {
    const sendingValues = {}
    if (prescriptions.length !== 0 && prescriptions !== null) {
      const prescriptionsValue = await multipleImageUpload(
        prescriptions,
        'prescriptions'
      )
      sendingValues.prescriptions = prescriptionsValue
    } else {
      sendingValues.prescriptions = []
    }

    const updated = await authService.updateUserPrescriptions(sendingValues)

    if (updated) {
      toast.success('Prescription Updated')
    }
  }

  return (
    <div>
      <div className="card">
        <div className="card-header">
          <h5>prescriptions</h5>
        </div>
        <div className="card-body">
          <div className="mb-4">
            <Card title="Prescriptions">
              <Upload listType="picture-card" {...propsPrescriptions}>
                {/* <CustomIcon className="display-3" svg={ImageSvg} /> */}
                {/* Add Prescription */}
                {/* <img
                  src={prescriptionIcon}
                  alt="prescription-add"
                  height={80}
                  width={80}
                /> */}
                + Prescription
              </Upload>
            </Card>
            <button
              type="submit"
              className="btn btn-fill-out submit font-weight-bold"
              name="submit"
              value="Submit"
              onClick={uploadPrescriptions}
            >
              Save Change
            </button>
          </div>
        </div>
      </div>
    </div>
  )
}

export default PrescriptionSection
