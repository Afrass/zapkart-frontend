import React from 'react'
import { useEffect } from 'react'
import { useState } from 'react'

import authService from 'service/auth'

import { ErrorMessage, Field, Form, Formik } from 'formik'
import ErrorText from 'components/common/ErrorText'
import * as Yup from 'yup'
import { toast } from 'react-toastify'
import { useRef } from 'react'
import scrollToTop from 'util/scrollToTop'
import { useDispatch, useSelector } from 'react-redux'
import { setUser } from 'redux/slices/authSlice'
import constantsService from 'service/constants'
// import { scrollToTop, scrollToBottom } from '../../utils/scroll'

const AddressSection = () => {
  const { user } = useSelector((state) => state.auth)

  const dispatch = useDispatch()

  const defaultInitialValues = {
    line1: '',
    city: '',
    state: '',
    country: '',
    phone: '',
    zipcode: '',
  }

  const [isFormOpen, setIsFormOpen] = useState(false)
  const [selectedAddress, setSelectedAddress] = useState(null)
  const [states, setStates] = useState([])
  // const [addressDetails, setAddressDetails] = useState([])
  const [initialValues, setInitialValues] = useState(defaultInitialValues)
  const formikRef = useRef()

  const validationSchema = Yup.object({
    line1: Yup.string().required(' Required'),
    city: Yup.string().required(' Required'),
    state: Yup.string().required(' Required'),
    phone: Yup.string().required(' Required'),
    zipcode: Yup.string().required(' Required'),
  })

  const getConstants = async () => {
    const consts = await constantsService.getConstants()

    setStates(Object.values(consts.GENERAL.STATES.INDIA))
  }

  const onSubmitHandler = async (
    { line1, city, state, phone, zipcode },
    { setSubmitting }
  ) => {
    let sendingValues = {
      line1,
      city,
      state,
      phone,
      zipcode,
      country: 'India',
    }
    // Create
    if (!selectedAddress?.id) {
      const res = await authService.createUserAddress(sendingValues)
      if (res) {
        toast.success('Address Added Success')
        getAddress()
        scrollToTop()
        setInitialValues(defaultInitialValues)
        setIsFormOpen(false)
      }
    } else {
      const res = await authService.updateUserAddress(
        selectedAddress.id,
        sendingValues
      )
      if (res) {
        toast.success('Edit Address Successfully')
        getAddress()
        scrollToTop()
      }
    }

    setSubmitting(false)
  }

  const deleteAddressHandler = async (id) => {
    const res = await authService.deleteUserAddress(id)
    if (res) {
      toast.success('Address Deleted Success')
      getAddress()

      setSelectedAddress(null)
      setInitialValues(defaultInitialValues)
    }
  }

  useEffect(() => {
    getAddress()
    getConstants()
  }, [])

  useEffect(() => {
    if (selectedAddress) {
      setInitialValues(selectedAddress)
    }
  }, [selectedAddress])

  const getAddress = async () => {
    const { data } = await authService.getUserProfile()
    if (data) {
      dispatch(setUser({ ...user, address: data.address }))
    }
  }

  return (
    <div className="row">
      <div className="col-lg-12">
        <div className="card mb-3 mb-lg-0">
          <div className="card-header">
            <h5 className="mb-0">Shipping Address</h5>
          </div>
          {user.address?.length > 0 ? (
            <div className="row">
              {user.address.map((address, i) => (
                <div className="card-body col-md-6" key={i}>
                  <address>
                    {address.line1}
                    <br />
                    {address.zipcode}, {address.city}, {address.state}
                    <br />
                    {address.phone}
                  </address>
                  <a
                    href="#address"
                    className="btn-small"
                    onClick={() => {
                      setIsFormOpen(true)
                      setSelectedAddress(address)
                    }}
                  >
                    Edit
                  </a>
                  <a
                    href="#address"
                    className="btn-small ml-20"
                    style={{ color: 'red' }}
                    onClick={() => deleteAddressHandler(address.id)}
                  >
                    Delete
                  </a>
                </div>
              ))}
            </div>
          ) : (
            <p>No Address Found</p>
          )}
        </div>

        <div className="add-new-address">
          <button
            className="btn btn-small"
            onClick={() => {
              setIsFormOpen(true)
              setSelectedAddress(null)
              setInitialValues(defaultInitialValues)
            }}
          >
            <span className="fi-rs-plus"></span> Add new address
          </button>
        </div>
      </div>

      {isFormOpen && (
        <div className="address-card-wrapper mt-30">
          <h3 className="inner-page-heading">Shipping Address</h3>
          <div className="card-form-wrapper mt-20">
            <Formik
              initialValues={initialValues}
              onSubmit={onSubmitHandler}
              validationSchema={validationSchema}
              enableReinitialize
              innerRef={formikRef}
            >
              {(fomrik) => {
                const { touched, errors } = fomrik

                return (
                  <Form>
                    <div className="row">
                      <div className="col-md-6">
                        <div className="form-group">
                          <label htmlFor="fullName">Line 1</label>

                          <Field
                            component="textarea"
                            cols="70"
                            rows="10"
                            type="text"
                            name="line1"
                            className="form-control"
                            placeholder="Line 1"
                            style={{
                              border: `${
                                touched.line1 && errors.line1
                                  ? '1px solid red'
                                  : ''
                              }`,
                              height: 115,
                            }}
                          />
                          <ErrorMessage name="line1" component={ErrorText} />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="form-group">
                          <label htmlFor="city">City</label>
                          <Field
                            type="text"
                            name="city"
                            className="form-control"
                            placeholder="city"
                            style={{
                              border: `${
                                touched.city && errors.city
                                  ? '1px solid red'
                                  : ''
                              }`,
                            }}
                          />
                          <ErrorMessage name="city" component={ErrorText} />
                        </div>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-md-6">
                        {/* <div className="form-group">
                          <label for="">Country*</label>
                          <select name="" id="" className="form-control">
                            <option value="">Select Country</option>
                          </select>
                        </div> */}
                        <div className="form-group">
                          <label htmlFor="state">State</label>
                          <Field
                            as="select"
                            // type="text"
                            name="state"
                            className="form-control"
                            placeholder="state"
                            style={{
                              border: `${
                                touched.state && errors.state
                                  ? '1px solid red'
                                  : ''
                              }`,
                            }}
                          >
                            {states?.map((state) => (
                              <option key={state} value={state}>
                                {state}
                              </option>
                            ))}
                          </Field>
                          <ErrorMessage name="state" component={ErrorText} />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="form-group">
                          <label htmlFor="">Phone</label>
                          <Field
                            type="text"
                            name="phone"
                            className="form-control"
                            placeholder="+919087654321"
                            style={{
                              border: `${
                                touched.phone && errors.phone
                                  ? '1px solid red'
                                  : ''
                              }`,
                            }}
                          />
                          <ErrorMessage name="phone" component={ErrorText} />
                        </div>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-md-6">
                        <div className="form-group">
                          <label htmlFor="zipcode">Zipcode</label>
                          <Field
                            type="text"
                            name="zipcode"
                            className="form-control"
                            placeholder="zipcode"
                            style={{
                              border: `${
                                touched.zipcode && errors.zipcode
                                  ? '1px solid red'
                                  : ''
                              }`,
                            }}
                          />
                          <ErrorMessage name="zipcode" component={ErrorText} />
                        </div>
                      </div>
                      <div className="col-md-6"></div>
                    </div>
                    {/*
                      <div>
                          <label htmlFor="address_type">Address Type</label>
                          <Field
                            as="select"
                            name="address_type"
                            className="form-control"
                            placeholder="Address Type"
                            style={{
                              border: `${
                                touched.address_type && errors.address_type
                                  ? '1px solid red'
                                  : ''
                              }`,
                            }}
                          >
                            <option value="home">Home</option>
                            <option value="office">Office</option>
                            <option value="other">Other</option>
                          </Field>
                    </div> */}
                    <div className="form-btns-wrapper">
                      <a
                        href="#/"
                        className="btn-small mr-20"
                        style={{ color: 'red' }}
                        onClick={() => {
                          setIsFormOpen(false)
                          setInitialValues(defaultInitialValues)
                        }}
                      >
                        Cancel
                      </a>
                      <button className="btn">Save</button>
                    </div>
                  </Form>
                )
              }}
            </Formik>
          </div>
        </div>
      )}
    </div>
  )
}

export default AddressSection

{
  /* <div className="col-lg-6">
        <div className="card">
          <div className="card-header">
            <h5 className="mb-0">Shipping Address</h5>
          </div>
          <div className="card-body">
            <address>
              4299 Express Lane
              <br />
              Sarasota, <br />
              FL 34249 USA <br />
              Phone: 1.941.227.4444
            </address>
            <p>Sarasota</p>
            <a href="#/" className="btn-small">
              Edit
            </a>
          </div>
        </div>
      </div> */
}
