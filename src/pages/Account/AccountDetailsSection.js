/* eslint-disable no-unused-vars */
import React from 'react'
import { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { toast } from 'react-toastify'
import { setUser } from 'redux/slices/authSlice'
import ImageUploader from 'components/common/ImageUpload'
import { singleImageUploader } from 'util/imageUploader'
import authService from 'service/auth'

const AccountDetailsSection = () => {
  const [submitLoading, setSubmitLoading] = useState(false)
  const [firstName, setFirstName] = useState('')
  const [lastName, setLastName] = useState('')
  const [email, setEmail] = useState('')
  const [phone, setPhone] = useState('')
  const [displayImage, setDisplayImage] = useState('')
  const dispatch = useDispatch()

  const { user } = useSelector((state) => state.auth)

  useEffect(() => {
    if (user) {
      const { firstName, lastName, phone, email, displayImage, prescriptions } =
        user
      setFirstName(firstName)
      setLastName(lastName)
      setPhone(phone)
      setEmail(email)
      setDisplayImage(displayImage)
    }
  }, [user])

  const updateProfile = async (e) => {
    e.preventDefault()
    setSubmitLoading(true)
    const sendingValues = {
      firstName,
      lastName,
    }

    if (displayImage?.length !== 0 && displayImage !== null) {
      const displayImageValue = await singleImageUploader(
        displayImage,
        'profile'
      )
      sendingValues.displayImage = displayImageValue

      sendingValues.displayImage = displayImageValue
    } else {
      sendingValues.displayImage = null
    }

    if (!displayImage) {
      sendingValues.displayImage = null
    }

    const updated = await authService.updateUser(sendingValues)
    if (updated) {
      const { data } = await authService.getUserProfile()

      const dispatchingData = {
        firstName: data.firstName,
        id: data.id,
        lastName: data.lastName,
        email: data.email,
        phone: data.phone,
        address: data.address,
        prescriptions: data.prescriptions,
        displayImage: data.displayImage,
        emailVerified: data.emailVerified,
        phoneVerified: data.phoneNumber ? true : false,
      }
      dispatch(setUser(dispatchingData))
      toast.success('Profile updated successfully')
    }

    setSubmitLoading(false)
  }

  return (
    <div className="card">
      <div className="card-header">
        <h5>Account Details</h5>
      </div>
      <div className="card-body">
        {/* <p>
          Already have an account? <Link to="/page-login">Log in instead!</Link>
        </p> */}
        <form method="post" name="enq" onSubmit={updateProfile}>
          <div className="row">
            <div className="form-group">
              <label htmlFor>First Name</label>
              <input
                type="text"
                className="form-control"
                value={firstName}
                onChange={(e) => setFirstName(e.target.value)}
                placeholder="First name"
              />
            </div>
            <div className="form-group">
              <label htmlFor>Last Name</label>
              <input
                type="text"
                value={lastName}
                onChange={(e) => setLastName(e.target.value)}
                className="form-control"
                placeholder="Last name"
              />
            </div>
            <div className="form-group">
              <label htmlFor>Profile Photo</label>
              <ImageUploader
                source={displayImage ? displayImage : null}
                setSource={setDisplayImage}
              />
            </div>
            <div className="row">
              <div className="col-md-5">
                <div className="form-group">
                  <label htmlFor>Email</label>
                  <input
                    type="email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    className="form-control"
                    placeholder="Email"
                    disabled
                  />
                </div>
              </div>
              <div className="col-md-7">
                <div className="form-group">
                  <label htmlFor>Phone</label>
                  {/* <span className="country-code">+971</span> */}
                  <input
                    type="text"
                    className="form-control"
                    value={phone}
                    disabled
                  />
                </div>
              </div>
            </div>

            <div className="col-md-12">
              <button
                type="submit"
                className="btn btn-fill-out submit font-weight-bold"
                name="submit"
                value="Submit"
                disabled={submitLoading}
              >
                Save Change
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  )
}

export default AccountDetailsSection
