import React from 'react'
import { useSelector } from 'react-redux'

const DashboardSection = () => {
  const { user } = useSelector((state) => state.auth)

  return (
    <div className="card">
      <div className="card-header">
        <h3 className="mb-0">Hello {user?.firstName}!</h3>
      </div>
      <div className="card-body">
        <p>
          From your account dashboard. you can easily check &amp; view your{' '}
          <a href="#orders">recent Orders</a>,
          <br />
          manage your <a href="#address">
            shipping and billing addresses
          </a> and <a href="#accountdetails">edit account details.</a>
        </p>
      </div>
    </div>
  )
}

export default DashboardSection
