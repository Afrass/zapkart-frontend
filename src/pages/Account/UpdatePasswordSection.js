// import { authentication } from 'firebaseconfig'
import { updatePassword } from 'firebase/auth'
import { authentication } from 'firebaseconfig'
import React, { useState } from 'react'
import { useDispatch } from 'react-redux'
import { toast } from 'react-toastify'
import { setLogout } from 'redux/slices/authSlice'
import getFBError from 'util/firebaseErrors'

const UpdatePasswordSection = () => {
  const [password, setPassword] = useState('')
  const [confirmPassword, setConfirmPassword] = useState('')
  const dispatch = useDispatch()

  const changePassword = async (e) => {
    e.preventDefault()
    if (password === confirmPassword) {
      updatePassword(authentication.currentUser, password)
        .then(() => {
          toast.success('Password updated successfully')
        })
        .catch((error) => {
          toast.error(getFBError(error.code))
          if (error.code === 'auth/requires-recent-login') {
            setTimeout(() => {
              dispatch(setLogout())
            }, 2000)
          }
        })
    } else {
      toast.error('Password does not match')
    }
  }

  return (
    <div>
      <div className="card">
        <div className="card-header">
          <h5>Account Details</h5>
        </div>
        <div className="card-body">
          {/* <p>
          Already have an account? <Link to="/page-login">Log in instead!</Link>
        </p> */}
          <form method="post" name="passwordchange" onSubmit={changePassword}>
            <div className="row">
              <div className="form-group col-md-12">
                <label>
                  New Password <span className="required">*</span>
                </label>
                <input
                  required=""
                  className="form-control"
                  name="password"
                  type="password"
                  onChange={(e) => setPassword(e.target.value)}
                />
              </div>
              <div className="form-group col-md-12">
                <label>
                  Confirm Password <span className="required">*</span>
                </label>
                <input
                  required=""
                  className="form-control"
                  name="cpassword"
                  type="password"
                  onChange={(e) => setConfirmPassword(e.target.value)}
                />
              </div>

              <div className="col-md-12">
                <button
                  type="submit"
                  className="btn btn-fill-out submit font-weight-bold"
                  name="submit"
                  value="Submit"
                >
                  Change Password
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  )
}

export default UpdatePasswordSection
