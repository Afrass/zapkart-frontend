/* eslint-disable react/prop-types */
import { Popconfirm } from "antd";
import React, { useEffect, useState } from "react";
import { Button, Modal } from "react-bootstrap";
import { useSelector } from "react-redux";
import StarRatings from "react-star-ratings";
import { toast } from "react-toastify";
import reviewService from "service/review";

const ReviewSection = ({
  openReviewModal,
  setOpenReviewModal,
  orderId,
  itemId,
}) => {
  const { user } = useSelector((state) => state.auth);
  const [reviewId, setReviewId] = useState(null);
  const [title, setTitle] = useState("");
  const [message, setMessage] = useState("");
  const [rating, setRating] = useState(1);
  const [mode, setMode] = useState("Add");

  console.log(itemId, "sdghk");

  useEffect(() => {
    if (itemId && orderId) {
      getProductReview();
    }
  }, [itemId, orderId, openReviewModal]);

  const getProductReview = async () => {
    const review = await reviewService.getCustomerReviewByProduct(
      user.id,
      orderId,
      itemId
    );
    if (review?.length > 0) {
      setMode("Edit");
      setMessage(review[0].message);
      setTitle(review[0].title);
      setReviewId(review[0].id);
      setRating(review[0].rating);
    } else {
      resetStates();
    }
  };

  const createProductReview = async (e) => {
    e.preventDefault();
    const sendingValues = {
      orderId,
      itemId,
      title,
      message,
      rating,
    };
    const createReview = await reviewService.createProductReview(sendingValues);

    if (createReview) {
      setOpenReviewModal(false);
      resetStates();
      toast.success("Review Added Successfully");
      setMode("Edit");
    }
  };

  const editProductReview = async (e, reviewId) => {
    e.preventDefault();

    const sendingValues = {
      title,
      message,
      rating,
    };
    const editReview = await reviewService.editProductReview(
      reviewId,
      sendingValues
    );

    if (editReview) {
      toast.success("Review Edited Success");
      setOpenReviewModal(false);
      resetStates();
    }
  };

  const resetStates = () => {
    setMode("Add");
    setMessage("");
    setTitle("");
    setReviewId(null);
    setRating(1);
  };

  const onCloseModal = () => {
    setOpenReviewModal(false);
    resetStates();
  };

  const deleteReview = (reviewId) => {
    const deletedReview = reviewService.deleteProductReview(reviewId);

    if (deletedReview) {
      toast.success("Deleted Review Success");
      onCloseModal();
    }
  };

  return (
    <Modal show={openReviewModal} onHide={onCloseModal}>
      <Modal.Header className="d-flex justify-content-between">
        <Modal.Title>Product Review</Modal.Title>
        <span
          className="fi-rs-cross-circle"
          style={{ fontSize: 20, cursor: "pointer" }}
          onClick={onCloseModal}
        ></span>
      </Modal.Header>
      <Modal.Body>
        <div className="comment-form">
          <div>
            <div>
              <form
                className="form-contact comment_form"
                id="commentForm"
                onSubmit={(e) =>
                  mode === "Add"
                    ? createProductReview(e)
                    : editProductReview(e, reviewId)
                }
              >
                <div className="row">
                  <div className="col-12">
                    <div className="form-group">
                      <input
                        className="form-control"
                        name="Title"
                        id="Title"
                        type="text"
                        placeholder="Title"
                        onChange={(e) => setTitle(e.target.value)}
                        value={title}
                      />
                    </div>
                  </div>
                  <div className="col-12">
                    <div className="form-group">
                      <textarea
                        className="form-control w-100"
                        name="comment"
                        id="comment"
                        cols="30"
                        rows="9"
                        placeholder="Write Comment"
                        onChange={(e) => setMessage(e.target.value)}
                        value={message}
                      ></textarea>
                    </div>
                  </div>
                  <div className="col-12">
                    <div className="form-group">
                      <StarRatings
                        rating={rating}
                        starRatedColor="#3BB77E"
                        starHoverColor="#29A56C"
                        changeRating={(newRating) => setRating(newRating)}
                        numberOfStars={5}
                        name="rating"
                      />
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Popconfirm
          title="Are you sure want to Delete this Review?"
          onConfirm={() => deleteReview(reviewId)}
          okText="Yes"
          cancelText="No"
        >
          <Button style={{ background: "red" }}>Delete Review</Button>
        </Popconfirm>

        <Button
          variant="primary"
          onClick={(e) =>
            mode === "Add"
              ? createProductReview(e)
              : editProductReview(e, reviewId)
          }
          // onClick={handleClose}
        >
          {mode} Review
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default ReviewSection;
