/* eslint-disable react/prop-types */
/* eslint-disable no-unused-vars */
import React, { useEffect, useState } from "react";
import Layout from "components/layout/Layout";
import { Link, useNavigate, useParams } from "react-router-dom";
import productImg from "assets/imgs/shop/product-1-1.png";
import { useDispatch, useSelector } from "react-redux";
import orderService from "service/order";
import { Modal } from "react-bootstrap";
import PrescriptionOrderSection from "./PrescriptionOrderSection";
import { Divider, Image, message, Popconfirm } from "antd";
import NoImage from "assets/imgs/shop/no-image.svg";

import { toast } from "react-toastify";
import ReviewSection from "./ReviewSection";
import moment from "moment";
import { addToCartSlice } from "redux/slices/cartSlice";

const OrderDetails = ({ id }) => {
  const [order, setOrder] = useState(null);
  const [openViewModal, setOpenViewModal] = useState(false);
  const [openReviewModal, setOpenReviewModal] = useState(false);
  const [itemId, setItemId] = useState(null);
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const { authorized } = useSelector((state) => state.auth);

  const getOrderById = async () => {
    const order = await orderService.getOrderById(id);

    if (order) {
      setOrder(order);
    }
  };

  useEffect(() => {
    getOrderById();
  }, [id]);

  const addToCart = async (product) => {
    if (authorized) {
      dispatch(addToCartSlice(product.id, 1));
    } else {
      toast.warning("You have to login before adding to cart");
      navigate("/Login");
    }
  };

  const onCancelConfirm = async () => {
    const cancelOrder = await orderService.cancelOrder(id);

    if (cancelOrder) {
      toast.success("Order Cancelled");
      getOrderById();
    }
  };

  const onCancelOrderItem = async (orderItemId) => {
    const cancelOrderItem = await orderService.cancelOrderItem(id, [
      orderItemId,
    ]);

    if (cancelOrderItem) {
      toast.success("Order Item Cancelled");
      getOrderById();
    }
  };

  const onReturnItem = async (orderItemId) => {
    const retuenItem = await orderService.returnOrderItem(id, [orderItemId]);

    if (retuenItem) {
      toast.success("Return Item Success");
      getOrderById();
    }
  };

  return (
    <section className="mt-50 mb-50">
      <div className="container">
        <div className="row">
          <div className="col-lg-12">
            <div className="d-flex justify-content-between align-items-center">
              <h4 className="mb-20">Order Details</h4>
              <div className="d-flex">
                {order?.status === "Prescriptions Missing" && (
                  <button
                    className="button white button-see-all ml-5"
                    onClick={() => setOpenViewModal(true)}
                  >
                    Re-Upload Prescription
                  </button>
                )}

                {order?.status !== "Cancelled" &&
                  order?.status !== "Verifying Prescription" &&
                  order?.status !== "Prescriptions Missing" &&
                  order?.status !== "Confirmed" && (
                    <Popconfirm
                      title="Are you sure to Cancel this Order?"
                      onConfirm={onCancelConfirm}
                      okText="Yes"
                      cancelText="No"
                    >
                      <button className="button white button-see-all ml-5">
                        Cancel Order
                      </button>
                    </Popconfirm>
                  )}
              </div>
            </div>

            <div className="order-details-container">
              <div className="order-main-details">
                <div className="order-main-item">
                  <h6>Order Date</h6>
                  <p className="mb-3 secondary-text-parent">
                    {moment(new Date(order?.createdAt * 1000)).format(
                      "MMMM, YYYY"
                    )}
                  </p>
                </div>
                <div className="order-main-item mb-3">
                  <h6>Order #</h6>
                  <p className="secondary-text-parent">{order?.orderNo}</p>
                </div>
                <div className="order-main-item mb-3">
                  <h6>Order Total</h6>
                  <p className="secondary-text-parent">
                    ₹{order?.totalAmount}({order?.items?.length})
                  </p>
                </div>
                <div className="order-main-item mb-3">
                  <h6>Order Status</h6>
                  <p className="secondary-text-parent">{order?.status}</p>
                </div>
                <div className="order-main-item mb-3">
                  <h6>Order Invoice No</h6>
                  <p className="secondary-text-parent">
                    {order?.invoice?.invoiceNo}
                  </p>
                </div>
              </div>

              <h4 className="mb-25 mt-25">Shipping Details</h4>
              <div className="order-details-items-container">
                <div className="order-summary-contents">
                  {order?.items?.map((item) => (
                    <div
                      className="order-summary-item justify-content-between p-25"
                      key={item.id}
                    >
                      <div className="d-flex flex-wrap">
                        <img
                          alt="order-item-pic"
                          src={
                            item?.images?.length > 0 ? item?.images[0] : NoImage
                          }
                          className="mb-10"
                        />
                        <div className="order-summary-main-details">
                          <div>
                            <h4 className="order-heading mb-1">
                              {item?.name} ({item?.quantity})
                            </h4>
                            <h4 className="show-in-mobile order-heading mb-1">
                              ₹{item?.price}
                            </h4>
                            <p>{item?.status}</p>
                          </div>
                          <div>
                            <button
                              className="button full-width-mob-btn button-see-all ml-5 mr-3"
                              onClick={() => addToCart(item)}
                            >
                              Add To Cart
                            </button>
                            <Popconfirm
                              title="Are you sure to Return this Order Item?"
                              onConfirm={() => onReturnItem(order.id, item.id)}
                              okText="Yes"
                              cancelText="No"
                            >
                              <button className="button full-width-mob-btn white button-see-all ml-5">
                                Return Item
                              </button>
                            </Popconfirm>
                          </div>
                        </div>
                      </div>

                      <div className="d-flex align-items-center hide-in-mobile">
                        <h4 className="order-heading mb-1 order-detail-price">
                          ₹{item?.price}
                        </h4>
                      </div>
                      {item.status !== "Cancelled" ||
                        item.status === "Delivered" ||
                        (item.status === "Completed" && (
                          <div
                            className={
                              "d-flex flex-column justify-content-center"
                            }
                          >
                            {item.status !== "Cancelled" && (
                              <Popconfirm
                                title="Are you sure to Return this Order Item?"
                                onConfirm={() => onCancelOrderItem(item.id)}
                                okText="Yes"
                                cancelText="No"
                              >
                                <button className="button white button-see-all ml-5 mb-10">
                                  Cancel Order Item
                                </button>
                              </Popconfirm>
                            )}

                            {item.status === "Delivered" ||
                              (item.status === "Completed" && (
                                <button
                                  className="button white button-see-all ml-5"
                                  onClick={() => {
                                    setOpenReviewModal(true);
                                    setItemId(item?.id);
                                  }}
                                >
                                  Write a Product Review
                                </button>
                              ))}
                          </div>
                        ))}
                    </div>
                  ))}
                </div>
              </div>

              {order?.prescriptions?.length > 0 && (
                <div>
                  <h4 className="mt-25">Prescriptions</h4>
                  <Image.PreviewGroup>
                    {order?.prescriptions?.map((cur, i) => (
                      <Image key={i} width={200} src={cur} />
                    ))}
                  </Image.PreviewGroup>
                </div>
              )}

              <h4 className="mb-25 mt-25">Payment Information</h4>
              <div className="order-details-payment-container">
                <div className="pt-25 pb-0 pr-25 pl-25">
                  <h6 className="mb-3">Payment Method</h6>
                  <p className="mb-3 secondary-text-parent">Debit Card</p>
                </div>
                <Divider />
                <div className="pb-0 pr-25 pl-25">
                  <h6 className="mb-3">Address</h6>
                  <p
                    className="mb-3 secondary-text-parent"
                    style={{ width: 280 }}
                  >
                    15nd Flr Mirza Street, Nr Abdul Rehaman Street, Crawford
                    Market, Mumbai-400003{" "}
                  </p>
                </div>
              </div>

              {/* <h4 className="mb-25 mt-25">Order Summary</h4>

              <div className="order-details-payment-container">
                <div
                  style={{ width: "250px" }}
                  className="d-flex justify-content-between"
                >
                  <p className="mb-3 secondary-text-parent">Items: </p>
                  <p className="mb-3 secondary-text-parent">Items: </p>
                </div>
              </div> */}

              {/* <h6 className="mb-3">Order No: {order?.orderNo}</h6>
              <h6 className="mb-3">Order No: {order?.orderNo}</h6> */}
            </div>

            {/* {order?.invoice && (
              <h6 className="mb-3">Invoice No: {order?.invoice?.invoiceNo}</h6>
            )}
            <h6 className="mb-3">Total Amount: ₹{order?.totalAmount}</h6>
            <h6 className="mb-3">Order Status: {order?.status}</h6> */}

            {/* {order?.status !== "Cancelled" &&
              order?.status !== "Verifying Prescription" &&
              order?.status !== "Prescriptions Missing" &&
              order?.status !== "Confirmed" && (
                <Popconfirm
                  title="Are you sure to Cancel this Order?"
                  onConfirm={onCancelConfirm}
                  okText="Yes"
                  cancelText="No"
                >
                  <p
                    style={{
                      textDecoration: "underline",
                      cursor: "pointer",
                      color: "red",
                      width: "max-content",
                      marginTop: "10px",
                      marginBottom: "10px",
                    }}
                  >
                    Cancel Order
                  </p>
                </Popconfirm>
              )} */}

            {/* <PrescriptionSection
                selectedPrescriptions={selectedPrescriptions}
                setSelectedPrescriptions={setSelectedPrescriptions}
              /> */}

            {/* <h5>Upload Prescr</h5> */}
            {/* <div className="table-responsive shopping-summery">
              <table
                className={
                  order?.items?.length > 0 ? "table table-wishlist" : "d-none"
                }
              >
                <thead>
                  <tr className="main-heading">
                    <th className="custome-checkbox start pl-30" colSpan="2">
                      Product
                    </th>
                    <th scope="col">Price</th>
                    <th scope="col">Qty</th>
                    <th scope="col" colSpan="2">
                      Status
                    </th>
                    <th>Product Review</th>
                    <th scope="col">Actions</th>
                  </tr>
                </thead>
                <tbody>
                  {order?.items?.map((item) => (
                    <>
                      <tr key={item.id}>
                        <td className="image product-thumbnail">
                          <img src={item.images[0]} alt="" />
                        </td>

                        <td className="product-des product-name">
                          <h6 className="product-name">
                            <Link to="/products">{item?.name}</Link>
                          </h6>
                          <div className="product-rate-cover">
                            <span className="font-small ml-5 text-muted">
                              Pres: {item?.prescriptionRequired ? "Yes" : "No"}
                            </span>
                          </div>
                        </td>

                        <td className="price" data-title="quantity">
                          <h4 className="text-body">₹{item.price}</h4>
                        </td>

                        <td className="price" data-title="quantity">
                          <h4 className="text-body">{item.quantity}</h4>
                        </td>

                        <td
                          colSpan="2"
                          className="text-right"
                          data-title="status"
                        >
                          <h4 className="text-body" style={{ fontSize: 20 }}>
                            {item.status}
                          </h4>
                        </td>

                        <td className="text-right" data-title="review">
                          {item.status === "Delivered" ||
                          item.status === "Completed" ? (
                            <p
                              style={{
                                textDecoration: "underline",
                                cursor: "pointer",
                                width: "max-content",
                                marginLeft: "12px",
                              }}
                              onClick={() => {
                                setOpenReviewModal(true);
                                setItemId(item?.id);
                              }}
                            >
                              Add Review
                            </p>
                          ) : (
                            <p>Not Availble</p>
                          )}
                        </td>

                        <td className="text-right" data-title="status">
                          <div className="d-flex">
                            {item.status !== "Cancelled" && (
                              <Popconfirm
                                title="Are you sure to Cancel this Order Item?"
                                onConfirm={() => onCancelOrderItem(item.id)}
                                okText="Yes"
                                cancelText="No"
                              >
                                <p
                                  style={{
                                    textDecoration: "underline",
                                    cursor: "pointer",
                                    color: "red",
                                    width: "max-content",
                                  }}
                                >
                                  Cancel Order Item
                                </p>
                              </Popconfirm>
                            )}

                            <Popconfirm
                              title="Are you sure to Return this Order Item?"
                              onConfirm={() => onReturnItem(item.id)}
                              okText="Yes"
                              cancelText="No"
                            >
                              <p
                                style={{
                                  textDecoration: "underline",
                                  cursor: "pointer",
                                  width: "max-content",
                                  marginLeft: "12px",
                                }}
                              >
                                Return
                              </p>
                            </Popconfirm>
                          </div>
                        </td>
                      </tr>
                    </>
                  ))}
                </tbody>
              </table>
            </div> */}

            <Modal
              show={openViewModal}
              onHide={() => setOpenViewModal(false)}
              style={{ width: "100%" }}
            >
              {/* <Modal.Header>
                  <Modal.Title>Order Details</Modal.Title>
                </Modal.Header> */}
              <Modal.Body>
                <PrescriptionOrderSection
                  setModal={setOpenViewModal}
                  orderId={order?.id}
                  refetch={getOrderById}
                />
              </Modal.Body>
            </Modal>

            <ReviewSection
              openReviewModal={openReviewModal}
              setOpenReviewModal={setOpenReviewModal}
              orderId={id}
              itemId={itemId}
            />
            {/* <CartDetailsTableSection cartItems={cartItems} /> */}
          </div>
        </div>
      </div>
    </section>
  );
};

export default OrderDetails;
