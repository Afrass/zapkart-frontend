// /* eslint-disable react/prop-types */
// import { notification, Upload } from 'antd'
// import React, { useEffect, useState } from 'react'
// import prescriptionIcon from 'assets/imgs/theme/icons/medical-prescription.png'
// import { multipleImageUpload } from 'util/imageUploader'
// import authService from 'service/auth'
// import useUpload from 'hooks/useUpload'

// const PrescriptionOrderSection = ({ setModal, orderId, refetch }) => {
//   const [prescriptions, setPrescriptions] = useState([])

//   const getUserPrescriptions = async () => {
//     const data = await authService.getUserPrescriptions()
//     if (data) {
//       const prescs = data.map((cur, i) => {
//         return {
//           uid: i + Math.random() * 10,
//           url: cur,
//         }
//       })

//       setPrescriptions(prescs)

//       setFileListImages(prescs)
//     }
//   }

//   useEffect(() => {
//     getUserPrescriptions()
//   }, [])

//   const {
//     fileList: fileListImages,
//     beforeUpload: beforeUploadImages,
//     onChange: onChangeImages,
//     onRemove: onRemoveImages,
//     setFileList: setFileListImages,
//   } = useUpload(1, 'multiple')

//   const propsPrescriptions = {
//     multiple: true,
//     beforeUpload: beforeUploadImages,
//     onRemove: onRemoveImages,
//     onChange: onChangeImages,
//     fileList: fileListImages,
//   }

//   useEffect(() => {
//     setPrescriptions(fileListImages)
//   }, [fileListImages])

//   // For prescriptions
//   const onUpdatePrescription = async () => {
//     let sendingPresValues = []
//     if (prescriptions.length !== 0 && prescriptions !== null) {
//       const prescriptionsValue = await multipleImageUpload(
//         prescriptions,
//         'prescriptions'
//       )
//       sendingPresValues = prescriptionsValue
//     } else {
//       sendingPresValues = []
//     }

//     const resPresc = await authService.updateUserOrderPrescriptions(orderId, {
//       prescriptions: sendingPresValues,
//     })

//     if (resPresc) {
//       getUserPrescriptions()
//       refetch()

//       setModal(false)
//       notification.success({
//         message: 'Success',
//         description: 'Prescription uploaded successfully',
//       })
//     }
//   }

//   // console.log(selectedBillingAddressId, selectedShippingAddressId, 'selectedss')

//   return (
//     <div>
//       <h4 className="text-brand mb-3">{'Upload Prescriptions'}</h4>
//       <div className="d-flex flex-wrap align-items-center mb-10">
//         <>
//           <Upload listType="picture-card" {...propsPrescriptions}>
//             <img
//               src={prescriptionIcon}
//               alt="prescription-add"
//               height={80}
//               width={80}
//             />
//           </Upload>
//           <div className="d-flex">
//             <a
//               href="#/"
//               className="btn btn-sm"
//               onClick={() => {
//                 onUpdatePrescription()
//               }}
//             >
//               Update
//             </a>
//           </div>
//         </>
//       </div>
//     </div>
//   )
// }

// export default PrescriptionOrderSection

/* eslint-disable react/prop-types */
import { notification, Upload } from 'antd'
import React, { useEffect, useState } from 'react'
// import prescriptionIcon from 'assets/imgs/theme/icons/medical-prescription.png'
import { multipleImageUpload } from 'util/imageUploader'
import authService from 'service/auth'
import useUpload from 'hooks/useUpload'
import PrescriptionImageSelect from 'pages/Checkout/PrescriptionImageSelect'

const PrescriptionSection = ({ setModal, orderId, refetch }) => {
  const [prescriptions, setPrescriptions] = useState([])
  const [selectedPrescriptions, setSelectedPrescriptions] = useState([])

  // console.log(prescriptions, 'shooo')
  const [mode, setMode] = useState('select')

  const getUserPrescriptions = async () => {
    const data = await authService.getUserPrescriptions()
    if (data) {
      const prescs = data.map((cur, i) => {
        return {
          uid: i + Math.random() * 10,
          url: cur,
        }
      })

      setPrescriptions(prescs)

      setFileListImages(prescs)
    }
  }

  useEffect(() => {
    getUserPrescriptions()
  }, [])

  const {
    fileList: fileListImages,
    beforeUpload: beforeUploadImages,
    onChange: onChangeImages,
    onRemove: onRemoveImages,
    setFileList: setFileListImages,
  } = useUpload(1, 'multiple')

  const propsPrescriptions = {
    multiple: true,
    beforeUpload: beforeUploadImages,
    onRemove: onRemoveImages,
    onChange: onChangeImages,
    fileList: fileListImages,
  }

  useEffect(() => {
    setPrescriptions(fileListImages)
  }, [fileListImages])

  // For prescriptions
  const onUpdatePrescription = async () => {
    let sendingPresValues = []
    if (prescriptions.length !== 0 && prescriptions !== null) {
      const prescriptionsValue = await multipleImageUpload(
        prescriptions,
        'prescriptions'
      )
      sendingPresValues = prescriptionsValue
    } else {
      sendingPresValues = []
    }

    const resPresc = await authService.updateUserPrescriptions({
      prescriptions: sendingPresValues,
    })

    if (resPresc) {
      getUserPrescriptions()
      setMode('select')
      notification.success({
        message: 'Success',
        description: 'Prescription uploaded successfully',
      })
    }
  }

  // On Order Prescription
  const onOrderUpdatePrescription = async () => {
    // let sendingPresValues = []
    // if (selectedPrescriptions.length !== 0 && selectedPrescriptions !== null) {
    //   const prescriptionsValue = await multipleImageUpload(
    //     prescriptions,
    //     'prescriptions'
    //   )
    //   sendingPresValues = prescriptionsValue
    // } else {
    //   sendingPresValues = []
    // }

    const resPresc = await authService.updateUserOrderPrescriptions(orderId, {
      prescriptions: selectedPrescriptions,
    })

    if (resPresc) {
      getUserPrescriptions()
      refetch()

      setModal(false)
      notification.success({
        message: 'Success',
        description: 'Re-Upload Order Prescription Success',
      })
    }
  }

  // console.log(selectedBillingAddressId, selectedShippingAddressId, 'selectedss')

  return (
    <div>
      <h4 className="text-brand mb-3">
        {mode === 'select' ? 'Select Prescription' : 'Edit Prescriptions'}
      </h4>
      <div className="d-flex flex-wrap align-items-center mb-10">
        {mode === 'select' ? (
          <div>
            <div className="d-flex flex-wrap">
              {prescriptions?.map((cur, index) => (
                <PrescriptionImageSelect
                  cur={cur}
                  key={index}
                  setSelectedPrescriptions={setSelectedPrescriptions}
                  selectedPrescriptions={selectedPrescriptions}
                />
              ))}
            </div>

            <a
              href="#/"
              className="btn btn-sm mr-10"
              onClick={() => setMode('edit')}
            >
              Edit
            </a>
            <a
              href="#/"
              className="btn btn-sm"
              onClick={onOrderUpdatePrescription}
            >
              Submit
            </a>
          </div>
        ) : (
          <>
            <Upload listType="picture-card" {...propsPrescriptions}>
              {/* <img
                src={prescriptionIcon}
                alt="prescription-add"
                height={80}
                width={80}
              /> */}
              + Prescription
            </Upload>
            <div className="d-flex">
              <a
                href="#/"
                className="btn btn-sm mr-5"
                onClick={() => setMode('select')}
              >
                Cancel
              </a>

              <a
                href="#/"
                className="btn btn-sm"
                onClick={() => {
                  onUpdatePrescription()
                }}
              >
                Update
              </a>
            </div>
          </>
        )}
      </div>
    </div>
  )
}

export default PrescriptionSection
