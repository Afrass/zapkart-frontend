/* eslint-disable no-unused-vars */
import { Drawer, Popconfirm } from "antd";
import moment from "moment";
import React, { useEffect } from "react";
import { Modal } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { Link, useNavigate } from "react-router-dom";
import { getOrders } from "redux/slices/orderSlice";
import OrderDetails from "./OrderDetailsSection";
import NoImage from "assets/imgs/shop/no-image.svg";
import orderService from "service/order";
import { toast } from "react-toastify";
import { addToCartSlice } from "redux/slices/cartSlice";

const OrdersSection = () => {
  const [openViewModal, setOpenViewModal] = React.useState(false);
  const [selectedOrderView, setSelectedOrderView] = React.useState({});
  const [orderDetailsModal, setOrderDetailsModal] = React.useState(false);
  const [selectedOrderId, setSelectedOrderId] = React.useState([]);

  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { orders } = useSelector((state) => state.order);
  const { authorized } = useSelector((state) => state.auth);

  useEffect(() => {
    dispatch(getOrders());
  }, []);

  console.log(orders, "plsss=");

  const onReturnItem = async (orderId, orderItemId) => {
    const returnItem = await orderService.returnOrderItem(orderId, [
      orderItemId,
    ]);

    if (returnItem) {
      toast.success("Return Item Success");
      dispatch(getOrders());
    }
  };

  const addToCart = async (product) => {
    if (authorized) {
      dispatch(addToCartSlice(product.id, 1));
    } else {
      toast.warning("You have to login before adding to cart");
      navigate("/Login");
    }
  };

  return (
    <div className="card">
      <div className="card-header">
        <h5 className="mb-0">Your Orders</h5>
      </div>

      {orders?.length > 0 ? (
        orders?.map((order) => (
          <div className="order-container mb-40" key={order.id}>
            <div className="order-summary-container p-25">
              <div className="order-summary-heads heads-margin">
                <h4 className="order-heading">Order Placed</h4>
                <h4 className="order-heading heads-margin-top">
                  {moment(new Date(order?.createdAt * 1000)).format(
                    "DD MMMM YYYY"
                  )}
                </h4>
              </div>
              <div className="order-summary-heads">
                <h4 className="order-heading">Total</h4>
                <h4 className="order-heading heads-margin-top">
                  ₹{order?.totalAmount}
                </h4>
              </div>
              <div className="heads-margin-left-auto">
                <h4
                  className="order-heading link"
                  onClick={() => {
                    setOrderDetailsModal(true);
                    setSelectedOrderId(order.id);
                  }}
                >
                  Order #{order?.orderNo}
                </h4>
                <h4
                  className="order-heading mt-10 link"
                  onClick={() => toast.warn("No Invoice Found")}
                >
                  Download Invoice
                </h4>
              </div>
            </div>
            <div className="order-summary-contents">
              {order?.items?.map((item) => (
                <div className="order-summary-item p-25" key={item.id}>
                  <div>
                    <img
                      alt="order-item-pic"
                      src={item?.images?.length > 0 ? item?.images[0] : NoImage}
                    />
                  </div>
                  <div className="d-flex flex-column justify-content-between ml-15">
                    <div>
                      <h4 className="order-heading mb-1">
                        {item?.name} ({item?.quantity})
                      </h4>
                      <p>{item?.status}</p>
                    </div>
                    <div>
                      <button
                        className="button button-see-all ml-5 mr-3"
                        onClick={() => addToCart(item)}
                      >
                        Add To Cart
                      </button>
                      <Popconfirm
                        title="Are you sure to Return this Order Item?"
                        onConfirm={() => onReturnItem(order.id, item.id)}
                        okText="Yes"
                        cancelText="No"
                      >
                        <button className="button white button-see-all ml-5">
                          Return Item
                        </button>
                      </Popconfirm>
                    </div>
                  </div>
                  <div
                    style={{ marginLeft: "auto" }}
                    className="d-flex align-items-center"
                  >
                    <h4 className="order-heading mb-1">₹{item?.price}</h4>
                  </div>
                </div>
              ))}
            </div>
          </div>
        ))
      ) : (
        <p>No Orders Found</p>
      )}

      {/* <div className="card-body">
        <div className="table-responsive">
          <table className="table">
            <thead>
              <tr>
                <th>Order</th>
                <th>Status</th>
                <th>Total</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              {orders?.map((order) => (
                <tr key={order.id}>
                  <td>{order.orderNo}</td>
                  <td>{order?.status}</td>
                  <td>{`${order?.totalAmount} for ${order?.items?.length} item`}</td>
                  <td>
                    <a
                      href="#/"
                      className="btn-small d-block"
                      onClick={() => {
                        setOrderDetailsModal(true);
                        setSelectedOrderId(order.id);
                      }}
                    >
                      View
                    </a>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div> */}
      <Drawer
        title={`Order Details (${selectedOrderId})`}
        placement="right"
        onClose={() => setOrderDetailsModal(false)}
        visible={orderDetailsModal}
        width={"100%"}
      >
        <OrderDetails id={selectedOrderId} />
      </Drawer>
    </div>
  );
};

export default OrdersSection;

// <Modal
// show={openViewModal}
// onHide={() => setOpenViewModal(false)}
// style={{ width: '100%' }}
// >
// <Modal.Header>
//   <Modal.Title>Order Details</Modal.Title>
// </Modal.Header>
// <Modal.Body>
//   <div className="shadow-sm p-3 mb-5 bg-body rounded">
//     <div className="table-responsive table-no-border">
//       {selectedOrderView?.items?.length <= 0 && 'No Products'}
//       <table
//         className={
//           selectedOrderView?.items?.length > 0
//             ? 'table table-wishlist'
//             : 'd-none'
//         }
//       >
//         <thead>
//           <tr className="main-heading">
//             <th className="custome-checkbox start" colSpan="2">
//               Products
//             </th>
//             <th scope="col">Unit Price</th>
//             <th scope="col">Status</th>
//           </tr>
//         </thead>
//         <tbody>
//           {selectedOrderView?.items?.map((item) => (
//             <tr key={item.id}>
//               <td className="image product-thumbnail">
//                 <img src={item?.images[0]} alt="" />
//               </td>

//               <td className="product-des product-name">
//                 <h6 className="product-name">
//                   <Link
//                     to={`/product/${item?.productTemplateId}/${item?.id}`}
//                   >
//                     {item.name}
//                   </Link>
//                   <span className="font-small ml-50 text-muted">
//                     X {item.quantity}
//                   </span>
//                 </h6>
//                 <div className="product-rate-cover">
//                 <div className="product-rate d-inline-block">
//           <div
//             className="product-rating"
//             style={{
//               width: '90%',
//             }}
//           ></div>
//         </div>
//                 <span className="font-small ml-5 text-muted"> (4.0)</span>
//                 <span className="font-small ml-50 text-muted">
//         X {item.quantity}
//       </span>
//                 </div>
//               </td>
//               <td className="price" data-title="Price">
//                 <h4
//                   className="text-brand"
//                   style={{ fontSize: 20 }}
//                 >
//                   ₹{item.price}
//                 </h4>
//               </td>
//               <td className="price" data-title="status">
//                 <h4 className="text-brand">{item.status}</h4>
//               </td>
//             </tr>
//           ))}
//         </tbody>
//       </table>
//     </div>
//   </div>
// </Modal.Body>
// </Modal>
