import React from 'react'
import Layout from 'components/layout/Layout'
import ContactFormSection from './ContactFormSection'

const Contact = () => {
  return (
    <>
      <Layout parent="Home" sub="Pages" subChild="Contact">
        <div className="page-content pt-50">
          <div className="container">
            <div className="row">
              <div className="col-xl-10 col-lg-12 m-auto">
                <section className="mb-50">
                  <ContactFormSection />
                </section>
              </div>
            </div>
          </div>
        </div>
      </Layout>
    </>
  )
}

export default Contact
