import React, { useEffect, useState } from 'react'
// import { server } from "../../config/index";
import DealCard from 'components/common/DealCard'
import productData from 'data/product'

function DealsSection() {
  const [deals, setDeals] = useState([])

  const dealsProduct = async () => {
    // const request = await fetch(`${server}/static/product.json`);
    // const allProducts = await request.json();
    // Discount
    const discountProduct = productData.filter((item) => item.discount.isActive)

    setDeals(discountProduct)
  }

  useEffect(() => {
    dealsProduct()
  }, [])

  return (
    <>
      <div className="row">
        {deals.slice(0, 4).map((product, i) => (
          <div key={i} className="col-xl-3 col-lg-4 col-md-6">
            <DealCard product={product} />
          </div>
        ))}
      </div>
    </>
  )
}
export default DealsSection
