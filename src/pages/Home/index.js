/* eslint-disable no-lone-blocks */
/* eslint-disable no-unused-vars */
import React from 'react'
// import IntroPopup from 'components/common/IntroPopup'
import Layout from '../../components/layout/Layout'
// import IntroSlider1 from 'components/sliders/IntroSlider1'

import { Link } from 'react-router-dom'
import CategorySlider from 'components/sliders/CategorySlider'
import BannerSection from 'pages/Home/BannerSection'
// import CategoryTab from 'components/ecommerce/CategoryTab'
// import FeatureTabSlider from 'components/ecommerce/FeatureTabSlider'
// import DealsSection from 'pages/Home/DealsSection'
// import BestProductsSection from 'pages/Home/BestProductsSection'
import IntroSlider2 from 'components/sliders/IntroSlider2'
import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { getHomeWidget } from 'redux/slices/homeWidgetSlice'
import BrandSlider from 'components/sliders/BrandSlider'
import ProductCards from 'components/common/productCard/ProductCards'
import MainHeroBannerSection from './MainHeroBannerSection'
import CategoriesSection from './CategoriesSection'
import BrandsSection from './BrandsSection'
import ProductsTemplateSection from './ProductsTemplateSection'
import Preloader from 'components/common/Preloader'
import { getSettings } from 'redux/slices/settingSlice'
import { Helmet } from 'react-helmet'

const Home = () => {
  const dispatch = useDispatch()
  const { homeWidgets, homeWidgetDefaultProducts, loading } = useSelector(
    (state) => state.homeWidget
  )

  const { settings } = useSelector((state) => state.setting)

  useEffect(() => {
    dispatch(getHomeWidget())
    dispatch(getSettings())
  }, [])

  return (
    <>
      <Helmet>
        <title>{`${settings?.metaTitle}`}</title>
        <meta name="description" content={settings?.metaDescription} />
        <meta name="keywords" content={settings?.metaKeywords} />
      </Helmet>
      {/* <IntroPopup /> */}
      {loading ? (
        <Preloader />
      ) : (
        <Layout noBreadcrumb="d-none">
          <>
            {homeWidgets.map((widget, index) =>
              // First Hero Banner
              widget.listingType === 'MainBanner' ? (
                <MainHeroBannerSection key={index} widget={widget} />
              ) : // For Second Banner
              widget.listingType === 'Banner' ? (
                <BannerSection key={index} widget={widget} />
              ) : widget.listingType === 'Categories' ? (
                <CategoriesSection key={index} widget={widget} />
              ) : widget.listingType === 'Brands' ? (
                <BrandsSection key={index} widget={widget} />
              ) : widget.listingType === 'ProductTemplates' ? (
                <ProductsTemplateSection key={index} widget={widget} />
              ) : (
                widget.listingType === 'Static' && (
                  <div
                    key={index}
                    dangerouslySetInnerHTML={{ __html: widget.staticContent }}
                  ></div>
                )
              )
            )}
          </>
        </Layout>
      )}
    </>
  )
}

export default Home

// ------------------------------------NOTE: For Reference Remove this Later-------------------------------------------

{
  /* <ul className="list-inline nav nav-tabs links">
                    <li className="list-inline-item nav-item">
                      <Link className="nav-link" to="/products">
                        Cake & Milk
                      </Link>
                    </li>
                    <li className="list-inline-item nav-item">
                      <Link className="nav-link" to="/products">
                        Coffes & Teas
                      </Link>
                    </li>
                    <li className="list-inline-item nav-item">
                      <Link to="/products" className="nav-link active">
                        Pet Foods
                      </Link>
                    </li>
                    <li className="list-inline-item nav-item">
                      <Link className="nav-link" to="/products">
                        Vegetables
                      </Link>
                    </li>
                  </ul> */

  {
    /* <section className="product-tabs section-padding position-relative">
          <div className="container">
            <div className="col-lg-12">
              <CategoryTab />
            </div>
          </div>
        </section> */
  }
  {
    /* 
        <section className="section-padding pb-5">
          <div className="container">
            <FeatureTabSlider />
          </div>
        </section> */
  }

  {
    /* <section className="section-padding pb-5">
          <div className="container">
            <div
              className="section-title wow animate__animated animate__fadeIn"
              data-wow-delay="0"
            >
              <h3>Deals Of The Day</h3>
              <Link to="/products" className="show-all">
                All Deals
                <i className="fi-rs-angle-right"></i>
              </Link>
            </div>
            <DealsSection />
          </div>
        </section> */
  }

  {
    /* <BestProductsSection /> */
  }
}
