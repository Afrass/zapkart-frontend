/* eslint-disable react/prop-types */
import React, { useEffect, useState } from 'react'

const BannerSection = ({ widget }) => {
  const [width, setWidth] = useState(window.innerWidth)

  const handleWindowSizeChange = () => {
    setWidth(window.innerWidth)
  }

  useEffect(() => {
    window.addEventListener('resize', handleWindowSizeChange)
    return () => {
      window.removeEventListener('resize', handleWindowSizeChange)
    }
  }, [])

  const isMobile = width <= 768

  return (
    <>
      <section
        className="popular-categories section-padding section-shadow  banners"
        key={widget.id}
      >
        <div className="container">
          <div className="row">
            <div className="section-title">
              <div className="title">
                <h4>{widget.isTitleShow ? widget.tabTitle : ''}</h4>
              </div>
            </div>

            {widget?.listingItems?.map((cur) => (
              <div key={cur.id} className="col-lg-4 col-md-4 col-6">
                <div
                  onClick={() => (window.location.href = cur?.forwardUrl)}
                  className="banner-img wow animate__animated animate__fadeInUp"
                  data-wow-delay="0"
                >
                  <img src={isMobile ? cur?.mobileImage : cur?.image} alt="" />
                  {/* <div className="banner-text mt-50">
                    <h4 className="mb-2">Fast Delivery</h4>
            <p className="mb-4">Medicine Delivered in 90 Minute</p>
                    <a
                      className="btn btn-md btn-white btn-text-yellow"
                      href="#/"
                      onClick={() => (window.location.href = cur?.forwardUrl)}
                    >
                      View Now
                    </a>
                    <Link
                to="/products"
                className="btn btn-md btn-white btn-text-yellow"
              >
                View Now
              </Link>
                  </div> */}
                </div>
              </div>
            ))}
          </div>
        </div>
      </section>
    </>
  )
}

export default BannerSection
