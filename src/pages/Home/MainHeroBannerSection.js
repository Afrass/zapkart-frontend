/* eslint-disable react/prop-types */
import IntroSlider2 from 'components/sliders/IntroSlider2'
import React, { useEffect, useState } from 'react'

const MainHeroBannerSection = ({ widget, index }) => {
  const [width, setWidth] = useState(window.innerWidth)

  const handleWindowSizeChange = () => {
    setWidth(window.innerWidth)
  }

  useEffect(() => {
    window.addEventListener('resize', handleWindowSizeChange)
    return () => {
      window.removeEventListener('resize', handleWindowSizeChange)
    }
  }, [])

  const isMobile = width <= 768

  return (
    <section
      className="home-slider style-2 position-relative mb-20"
      key={index}
    >
      <div className="container">
        <div className="row">
          <div className="col-xl-12 col-lg-12">
            <div className="home-slide-cover">
              <IntroSlider2
                isMobile={isMobile}
                homeHeroBanner={widget.listingItems}
              />
            </div>
          </div>
          {/* 
          <div className="col-lg-4 d-none d-xl-block">
            <div
              className="banner-img style-3 animated animated"
              style={{ display: 'flex', alignItem: 'flex-end' }}
            >
              <div className="banner-text mt-50">
                <h2 className="mb-2">
                  Discount <br />
                  Medicines
                </h2>
                <Link to="/">
                  <a href="#/" className="btn btn-lg">
                    View Products
                  </a>
                </Link>
              </div>
            </div>
          </div> */}
        </div>
      </div>
    </section>
  )
}

export default MainHeroBannerSection
