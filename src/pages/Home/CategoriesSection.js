/* eslint-disable react/prop-types */
import CategorySlider from 'components/sliders/CategorySlider'
import React from 'react'
import { useNavigate } from 'react-router-dom'
import NoImage from 'assets/imgs/shop/no-image.svg'

const CategoriesSection = ({ widget }) => {
  const navigate = useNavigate()
  return (
    <section className="popular-categories section-padding section-shadow">
      <div className="container wow animate__fadeIn animate__animated">
        <div className="d-flex justify-content-between align-items-center mb-10">
          <div className="section-title">
            <div className="title">
              <h4>{widget.isTitleShow ? widget.tabTitle : ''}</h4>
            </div>
          </div>
          <button
            className="button button-see-all ml-5"
            onClick={() => navigate('/categories')}
            // onClick={() => setIsSubsDrawerOpen(true)}
          >
            See All
          </button>
        </div>

        <div className="carausel-10-columns-cover position-relative hide-in-mobile">
          <div className="carausel-10-columns" id="carausel-10-columns">
            <CategorySlider categories={widget.listingItems} />
          </div>
        </div>

        <div className="show-in-mobile">
          <div className="row mt-4">
            {widget.listingItems?.slice(0, 6)?.map((item) => (
              <div key={item.id} className="col-md-2 col-sm-6 col-6">
                <div
                  className={`card-2 bg${1} wow animate__animated animate__fadeInUp`}
                  onClick={() =>
                    navigate(
                      `/products?categoryId=${item.id}&selected=category-${item?.name}`
                    )
                  }
                >
                  <figure className="img-hover-scale overflow-hidden">
                    <a>
                      <img
                        src={item.image || NoImage}
                        alt={item.name}
                        height={80}
                        width={80}
                        style={{
                          objectFit: item.image ? 'cover' : 'contain',
                        }}
                      />
                    </a>
                  </figure>
                  <h6>
                    <a>{item.name}</a>
                  </h6>
                  {/* <span>{categories?.length} items</span> */}
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </section>
  )
}

export default CategoriesSection
