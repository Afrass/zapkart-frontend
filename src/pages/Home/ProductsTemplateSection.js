/* eslint-disable react/prop-types */
// import ProductCards from 'components/common/productCard/ProductCards'
// import FeatureTabSlider from 'components/ecommerce/FeatureTabSlider'

import ProductSliderContainer from 'components/ecommerce/ProductsSliderContainer'
import React from 'react'
import { useNavigate } from 'react-router-dom'

const ProductsTemplateSection = ({ widget }) => {
  const navigate = useNavigate()

  return (
    <section className="product-tabs section-padding section-shadow position-relative">
      <div className="container">
        <div className="col-lg-12">
          <div className="d-flex justify-content-between align-items-center mb-10">
            <div className="section-title">
              <div className="title">
                <h4>{widget.isTitleShow ? widget.tabTitle : ''}</h4>
              </div>
            </div>
            <button
              className="button button-see-all ml-5"
              onClick={() => navigate('/products')}
              // onClick={() => setIsSubsDrawerOpen(true)}
            >
              See All
            </button>
          </div>
          <div className="product-grid-4">
            {/* <ProductCards products={widget.defaultProducts} /> */}
            <ProductSliderContainer products={widget.defaultProducts} />
          </div>
        </div>
      </div>
    </section>
  )
}

export default ProductsTemplateSection
