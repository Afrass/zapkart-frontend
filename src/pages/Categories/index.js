import React from 'react'
import { useNavigate } from 'react-router-dom'
import Layout from 'components/layout/Layout'
// import { useState } from 'react'
import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { getCategories } from 'redux/slices/categorySlice'
import NoImage from 'assets/imgs/shop/no-image.svg'
// import {Helmet} from 'react-helmet'

const Categories = () => {
  const navigate = useNavigate()
  const dispatch = useDispatch()
  const { categories } = useSelector((state) => state.category)
  console.log(categories, 'categoriessss')

  useEffect(() => {
    dispatch(getCategories())
  }, [])

  return (
    <div>
      {/* {
        categories
      } */}
      {/* <Helmet>
        <title>{`${admin ? "Admin Title" : "Client Title"}`}</title>
        <meta name="description" content={`${admin ? "Admin Content" : "Client Content"}`} />
      </Helmet> */}
      <Layout parent="Home" sub="Categories">
        <div className="page-content pt-30 pb-30">
          <div className="container">
            <div>
              <h4>Categories</h4>
              <div className="row mt-4">
                {categories?.map((item) => (
                  <div key={item.id} className="col-md-2 col-sm-6 col-6">
                    <div
                      className={`card-2 bg${1} wow animate__animated animate__fadeInUp`}
                      onClick={() =>
                        navigate(
                          `/products?categoryId=${item.id}&selected=category-${item?.name}`
                        )
                      }
                    >
                      <figure className="img-hover-scale overflow-hidden">
                        <a>
                          <img
                            src={item.image || NoImage}
                            alt={item.name}
                            height={80}
                            width={80}
                            style={{
                              objectFit: item.image ? 'cover' : 'contain',
                            }}
                          />
                        </a>
                      </figure>
                      <h6>
                        <a>{item.name}</a>
                      </h6>
                      {/* <span>{categories?.length} items</span> */}
                    </div>
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>
      </Layout>
    </div>
  )
}

export default Categories
