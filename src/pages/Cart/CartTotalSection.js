/* eslint-disable react/prop-types */
/* eslint-disable no-unused-vars */
import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Link } from 'react-router-dom'
import { getSettings } from 'redux/slices/settingSlice'

// eslint-disable-next-line react/prop-types
const CartTotalSection = ({
  cartTotal,
  couponDiscount,
  totalMrp,
  couponCode,
}) => {
  const { settings } = useSelector((state) => state.setting)

  const dispatch = useDispatch()
  const [deliveryCharge, setDeliveryCharge] = useState(0)
  const [subTotal, setSubTotal] = useState(0)

  const getDeliveryCharge = (deliveryCharges, cartTotal) => {
    const charges = []
    deliveryCharges?.forEach((cur) => {
      if (cur.startAmount <= cartTotal) {
        charges.push(cur?.charge)
      }
    })
    if (charges?.length > 0) {
      return charges[charges?.length - 1]
    } else {
      return 0
    }
  }

  useEffect(() => {
    const deliveryCharge = getDeliveryCharge(
      settings.deliveryCharges,
      cartTotal
    )
    setDeliveryCharge(deliveryCharge)
  }, [settings, cartTotal])

  useEffect(() => {
    dispatch(getSettings())
  }, [])

  return (
    <div className="border p-md-4 p-30 border-radius cart-totals">
      <div className="heading_s1 mb-3">
        <h4>Cart Total</h4>
      </div>
      <div className="table-responsive">
        <table className="table">
          <tbody>
            {/* <tr>
              <td className="cart_total_label">Cart Subtotal</td>
              <td className="cart_total_amount">
                <span className="font-lg fw-900 text-brand">₹ {price()}</span>
              </td>
            </tr> */}
            <tr>
              <td className="cart_total_label">Sub Total</td>
              <td className="cart_total_amount">
                <strong>
                  <span className="font-xl fw-900 text-brand">
                    ₹{couponDiscount ? cartTotal + couponDiscount : cartTotal}
                  </span>
                </strong>
              </td>
            </tr>
            <tr>
              <td className="cart_total_label">Shipping</td>
              <td className="cart_total_amount">
                <strong>
                  <span className="font-xl fw-900 text-brand">
                    {deliveryCharge !== 0
                      ? `₹${deliveryCharge}`
                      : 'Free Shipping'}
                  </span>
                </strong>
              </td>
            </tr>

            {couponDiscount && (
              <tr>
                <td className="cart_total_label">Coupon Discount</td>
                <td className="cart_total_amount">
                  <strong>
                    <span className="font-xl fw-900 text-brand">
                      ₹{couponDiscount}
                    </span>
                  </strong>
                </td>
              </tr>
            )}

            <tr>
              <td className="cart_total_label">Discount</td>
              <td className="cart_total_amount">
                <strong>
                  <span className="font-xl fw-900 text-brand">
                    ₹{totalMrp - cartTotal}
                  </span>
                </strong>
              </td>
            </tr>
            <tr>
              <td className="cart_total_label">Total</td>
              <td className="cart_total_amount">
                <strong>
                  <span className="font-xl fw-900 text-brand">
                    ₹{cartTotal + deliveryCharge}
                  </span>
                </strong>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      {/* {isCtaShown && (
        <Link to="/checkout" className="btn">
          <i className="fi-rs-box-alt mr-10"></i>
          Proceed To CheckOut
        </Link>
      )} */}
    </div>
  )
}

export default CartTotalSection
