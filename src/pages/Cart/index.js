/* eslint-disable no-unused-vars */
import React, { useEffect } from 'react'
import CartDetailsTableSection from './CartDetailsTableSection'
// import productImg from 'assets/imgs/shop/product-1-1.png'
import Layout from 'components/layout/Layout'
import CartTotalSection from './CartTotalSection'
import { useDispatch, useSelector } from 'react-redux'
import { getCartSlice } from 'redux/slices/cartSlice'

const Cart = () => {
  const { cart, cartTotal, deliveryCharge, subTotal, totalMrp } = useSelector(
    (state) => state.cart
  )

  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(getCartSlice())
  }, [])
  console.log(deliveryCharge, subTotal, cartTotal, 'showeeaas')

  return (
    <>
      <Layout parent="Home" sub="Cart">
        <section className="mt-50 mb-50">
          <div className="container">
            <div className="row">
              <div className="col-lg-8 mb-40">
                <h3 className="heading-2 mb-10">Your Cart</h3>
                <div className="d-flex justify-content-between">
                  <h6 className="text-body">
                    There are{' '}
                    {/* <span className="text-brand">{cartItems?.length}</span>{' '} */}
                    <strong className="text-brand">{cart?.length}</strong>{' '}
                    products in your cart
                  </h6>
                  {/* <h6 className="text-body">
                    <a href="#/" className="text-muted">
                      <i className="fi-rs-trash mr-5"></i>
                      Clear Cart
                    </a>
                  </h6> */}
                </div>
              </div>
            </div>
            {/* cartItems */}
            {cart?.length > 0 ? (
              <div className="row">
                <div className="col-lg-8">
                  <CartDetailsTableSection cartItems={cart} />
                </div>
                <div className="col-lg-4 mt-50">
                  <CartTotalSection cartTotal={cartTotal} totalMrp={totalMrp} />
                </div>
              </div>
            ) : (
              <h4>No Cart Items Found</h4>
            )}
          </div>
        </section>
      </Layout>
    </>
  )
}

export default Cart

// ------------------------------------NOTE: For Reference Remove this Later-------------------------------------------

// const [cartTotal, setCartTotal] = useState(0)

// const navigate = useNavigate()

// const getCartItems = async () => {
//   try {
//     const { data } = await getCartItemsApi()
//     console.log(data, 'mydata')
//     setCartItems(data.cartlistDetails)
//     // setCartItemCount(data.cartlistDetails.length)
//     let sum = 0
//     data.cartlistDetails.map((item) => {
//       sum += item.quantity * item.price
//     })
//     setCartTotal(sum)
//   } catch (err) {
//     console.log(err)
//     // toast.error(err)
//   }
// }

// useEffect(() => {
//   dispatch(getCartSlice())
// const cartItems = localStorage.getItem('cartItems')
// const parsedItems = JSON.parse(cartItems)
// if (parsedItems) {
//   const uniqueAddresses = Array.from(
//     new Set(parsedItems.map((a) => a._id))
//   ).map((id) => {
//     return parsedItems.find((a) => a._id === id)
//   })
//   setCartItems(uniqueAddresses)
// }
// getCartItems()
// return () => setCartItemCount(null)
// }, [])

// const updateCartTotal = (operator, price) => {
//   if (operator === 'add') {
//     setCartTotal(cartTotal + price)
//   } else {
//     if (cartTotal >= 0) {
//       setCartTotal(cartTotal - price)
//     }
//   }
// }

// const onRemoveItem = async (product, qty, price) => {
//   try {
//     const { data } = await removeFromCartApi({ productId: product._id })

//     if (data) {
//       console.log(product, 'mydatass')
//        setCartTotal(cartTotal - qty * price)

//       // setCartItemCount(data.data.length)
//       setCartItems(data.data)
//     } else {
//       setCartItems([])
//     }

//     // if (data) {
//     //   toast.success('Remove Item Success')
//     //   const restItem = cartItems.filter((cur) => {
//     //     console.log(cur.product_.id, 'hetttt')
//     //     return cur.product_.id !== id
//     //   })
//     //   setCartItems(restItem)
//     // }
//   } catch (err) {
//     toast.error(err.response.data.error)
//   }
//   // setCartItems(restItem)

//   // localStorage.setItem('cartItems', JSON.stringify(restItem))
// }

// const mapStateToProps = (state) => ({
//   cartItems: state.cart,
//   activeCart: state.counter,
// })

// const mapDispatchToProps = {
//   closeCart,
//   increaseQuantity,
//   decreaseQuantity,
//   deleteFromCart,
//   openCart,
//   clearCart,
// }
