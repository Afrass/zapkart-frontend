/* eslint-disable react/prop-types */
import React from 'react'
import { useEffect } from 'react'
import { useState } from 'react'
import { useDispatch } from 'react-redux'
import { Link, useNavigate } from 'react-router-dom'
import { updateCartTotal, removeCartItemSlice } from 'redux/slices/cartSlice'
import cartService from 'service/cart'

const CartItems = ({ product, productQuantity }) => {
  const dispatch = useDispatch()

  const [quantityCounter, setQuantityCounter] = useState()
  const navigate = useNavigate()
  useEffect(() => {
    // When Cart quantity changes,update cart qty in api
    if (quantityCounter) {
      updateCartQty(product)
    }
  }, [quantityCounter])

  useEffect(() => {
    // updateCartTotal('add', product.productPricing.salePrice * productQuantity)
    setQuantityCounter(productQuantity)
  }, [product])

  const updateCartQty = async (product) => {
    await cartService.updateCartQty(product.id, { quantity: quantityCounter })
  }

  const decreaseQty = () => {
    // TODO: replace 1 with actual min quantity of product
    if (quantityCounter > (product?.minQty || 1)) {
      setQuantityCounter(quantityCounter - 1)
      dispatch(
        updateCartTotal({
          operator: 'minus',
          price: product.price,
          mrpPrice: product.mrpPrice,
        })
      )
    }
  }

  const increaseQty = () => {
    // TODO: replace 1 with actual Max quantity of product
    if ((product?.allowedQuantityPerOrder || 5) > quantityCounter) {
      setQuantityCounter(quantityCounter + 1)
      dispatch(
        updateCartTotal({
          operator: 'add',
          price: product.price,
          mrpPrice: product.mrpPrice,
        })
      )
    }
  }

  return (
    <tr key={product.id}>
      <td className="image product-thumbnail">
        <img
          src={product?.images[0]}
          alt=""
          onClick={() =>
            navigate(`/product/${product.productTemplate?.id}/${product.id}`)
          }
        />
      </td>

      <td className="product-des product-name">
        <h6 className="product-name">
          <Link
            to={
              product?.productType === 'Medicine'
                ? `/product/${product.productTemplate?.id}/${product.id}/medicine`
                : `/product/${product.productTemplate?.id}/${product.id}`
            }
          >
            {product.name?.length > 20
              ? product.name?.slice(0, 25) + '...'
              : product.name}
          </Link>
        </h6>
        <div className="product-rate-cover">
          {/* <div className="product-rate d-inline-block">
            <div
              className="product-rating"
              style={{
                width: '90%',
              }}
            ></div>
          </div> */}
          {/* <div className="product-rate d-inline-block">
            <div
              className="product-rating"
              style={{
                width: `${100 / product.total_rating}%`,
              }}
            ></div>
          </div> */}
          <span className="font-small ml-5 text-muted">
            {product.prescriptionRequired && '(Prescription Required)'}
          </span>
        </div>
      </td>
      <td className="price" data-title="Price">
        {/* sale price */}
        <h4 className="text-brand">₹{product?.price}</h4>
      </td>
      <td className="text-center detail-info" data-title="Stock">
        <div className="detail-extralink mr-15">
          <div className="detail-qty border radius ">
            <a href="#/" onClick={decreaseQty} className="qty-down">
              <i className="fi-rs-angle-small-down"></i>
            </a>
            <span className="qty-val">{quantityCounter}</span>
            <a href="#/" onClick={increaseQty} className="qty-up">
              <i className="fi-rs-angle-small-up"></i>
            </a>
          </div>
        </div>
      </td>
      {/* <td className="text-right" data-title="Cart">
        <h4 className="text-body">₹{product.quantity * product.price}</h4>
      </td> */}
      <td className="action" data-title="Remove">
        <a
          href="#/"
          onClick={() =>
            dispatch(
              removeCartItemSlice(
                product.id,
                quantityCounter,
                product?.price,
                product?.mrpPrice
              )
            )
          }
          className="text-muted"
        >
          <i className="fi-rs-trash"></i>
        </a>
      </td>
    </tr>
  )
}

export default CartItems

// ------------------------------------NOTE: For Reference Remove this Later-------------------------------------------

// const getCartItem = window.localStorage.getItem('cartItems')
// if (getCartItem) {
//   const parsedItems = JSON.parse(getCartItem)
//   const storeItem = [...parsedItems, product]

//   window.localStorage.setItem('cartItems', JSON.stringify(storeItem))
//   toast.success('add To Cart')
// } else {
//   window.localStorage.setItem('cartItems', JSON.stringify([product]))
//   toast.success('add To Cart')
// }
