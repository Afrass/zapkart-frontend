/* eslint-disable react/prop-types */
import React from 'react'
import { Link, useNavigate } from 'react-router-dom'
// import { Link } from 'react-router-dom'
import CartItems from './CartItems'

const CartDetailsTableSection = ({ cartItems, onRemoveItem }) => {
  const navigate = useNavigate()
  return (
    <>
      <div className="table-responsive shopping-summery">
        {cartItems.length <= 0 && 'No Products'}
        <table
          className={cartItems.length > 0 ? 'table table-wishlist' : 'd-none'}
        >
          <thead>
            <tr className="main-heading">
              <th className="custome-checkbox start pl-30" colSpan="2">
                Product
              </th>
              {/* <th scope="col">Prescription</th> */}
              <th scope="col">Unit Price</th>
              <th scope="col">Quantity</th>
              {/* <th scope="col">Subtotal</th> */}
              <th scope="col" className="end">
                Remove
              </th>
            </tr>
          </thead>
          <tbody>
            {cartItems?.length > 0 &&
              cartItems.map((item) => (
                <CartItems
                  key={item.id}
                  product={item.product}
                  onRemoveItem={onRemoveItem}
                  productQuantity={item.quantity}
                />
              ))}
            {/* <tr>
              <td colSpan="6" className="text-end">
                {cartItems.length > 0 && (
                  <a
                    href="#/"
                    // onClick={clearCart}
                    className="text-muted"
                  >
                    <i className="fi-rs-cross-small"></i>
                    Clear Cart
                  </a>
                )}
              </td>
            </tr> */}
          </tbody>
        </table>
      </div>
     
        
      <div className="cart-action text-end">
      <Link to="/checkout" className="btn">
        
          <i className="fi-rs-box-alt mr-10"></i>
          Proceed To CheckOut
         
          
        </Link>
        
        
        <a href="#/" className="btn shop-btn" onClick={() => navigate('/')}>
          
          <i className="fi-rs-shopping-bag mr-10"></i>
          {/* Continue Shopping */}
          Proceed To Shopping
        </a>
        
      </div>
    </>
  )
}

export default CartDetailsTableSection
