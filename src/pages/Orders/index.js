/* eslint-disable no-unused-vars */
import React, { useEffect } from 'react'
import Layout from 'components/layout/Layout'
import { Link } from 'react-router-dom'
import productImg from 'assets/imgs/shop/product-1-1.png'
import OrderFilterSection from './OrderFilterSection'
import { useDispatch, useSelector } from 'react-redux'
import { getOrders } from 'redux/slices/orderSlice'

const Orders = () => {
  const cartItems = [
    {
      id: 1,
      image: productImg,
      title: 'mask',
      price: 35,
      stock: 1,
      quantity: 2,
      status: 'processing',
    },
  ]

  const dispatch = useDispatch()
  const { orders } = useSelector((state) => state.order)

  useEffect(() => {
    dispatch(getOrders())
  }, [])

  return (
    <Layout parent="Home" sub="Shop" subChild="Orders">
      <section className="mt-50 mb-50">
        <div className="container">
          {/* <div className="row">
            <div className="col-lg-8 mb-40">
              <h3 className="heading-3 mb-10">Your Orders</h3>
            </div>
          </div> */}
          <div className="row">
            <div className="col-lg-4 mt-50 primary-sidebar sticky-sidebar pt-30">
              <div className="sidebar-widget price_range range mb-30">
                <OrderFilterSection />
              </div>
            </div>
            <div className="col-lg-8">
              <h3 className="heading-3 mb-50">Your Orders</h3>
              <div className="table-responsive shopping-summery">
                {cartItems.length <= 0 && 'No Products'}
                <table
                  className={
                    cartItems.length > 0 ? 'table table-wishlist' : 'd-none'
                  }
                >
                  <thead>
                    <tr className="main-heading">
                      <th className="custome-checkbox start pl-30" colSpan="2">
                        Product
                      </th>
                      <th scope="col">Order No</th>
                      <th scope="col">Qty</th>
                      <th scope="col">Price</th>
                      <th scope="col" colSpan="2">
                        Status
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    {orders?.map((item) => (
                      <>
                        {item?.items?.map((cur) => (
                          <tr key={cur.id}>
                            <td className="image product-thumbnail">
                              <img src={cur.images[0]} alt="" />
                            </td>

                            <td className="product-des product-name">
                              <h6 className="product-name">
                                <Link to="/products">{cur?.name}</Link>
                              </h6>
                              <div className="product-rate-cover">
                                {/* <div className="product-rate d-inline-block">
                                  <div
                                    className="product-rating"
                                    style={{
                                      width: '90%',
                                    }}
                                  ></div>
                                </div> */}

                                <span className="font-small ml-5 text-muted">
                                  {/* {' '}
                                  (4.0) */}
                                  Pres:{' '}
                                  {cur?.prescriptionRequired ? 'Yes' : 'No'}
                                </span>
                              </div>
                            </td>
                            <td className="price" data-title="order no">
                              <h4 className="text-body">₹{item.orderNo}</h4>
                            </td>

                            <td className="price" data-title="quantity">
                              <h4 className="text-body">₹{cur.quantity}</h4>
                            </td>

                            <td className="text-right" data-title="price">
                              <h4 className="text-body">{cur.price}</h4>
                            </td>

                            <td className="text-right" data-title="status">
                              <h4
                                className="text-body"
                                style={{ fontSize: 20, width: 100 }}
                              >
                                {/* {cur.status?.length > 7
                                  ? cur?.status?.slice(0, 7)
                                  : cur?.status} */}
                                {cur.status}
                              </h4>
                            </td>
                          </tr>
                        ))}
                      </>
                    ))}
                  </tbody>
                </table>
              </div>
              {/* <CartDetailsTableSection cartItems={cartItems} /> */}
            </div>
          </div>
        </div>
      </section>
    </Layout>
  )
}

export default Orders
