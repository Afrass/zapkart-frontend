/* eslint-disable no-unused-vars */
import React, { useState } from 'react'
// import { useRouter } from 'next/router'
// import { updateProductFilters } from "../../../redux/action/productFiltersAction";
import CheckBox from 'components/common/CheckBox'

const OrderFilterSection = () => {
  const orderStatus = [
    { value: 'Delivered' },
    { value: 'Cancelled' },
    { value: 'Returned' },
  ]

  const orderTime = [
    { value: 'Last 30 Days' },
    { value: '2022' },
    { value: '2021' },
    { value: '2020' },
  ]

  //   useEffect(() => {}, [sizes, searchTerm])

  //   const handleCheckBox = (
  //     event,
  //     filters,
  //     updatefilters,
  //     selectFilter,
  //     text
  //   ) => {
  //     // const value = event.target.value
  //     // const updateSizes = filters
  //     // updateSizes.forEach((item) => {
  //     //   if (item.value === value) {
  //     //     if (item.checked) {
  //     //       item.checked = false
  //     //       const newsize = text.filter((item) => item !== value)
  //     //       selectFilter([...newsize])
  //     //     } else {
  //     //       item.checked = true
  //     //       const newsize = text.includes(value) ? text : [...text, value]
  //     //       selectFilter([...newsize])
  //     //     }
  //     //   }
  //     // })
  //     // updatefilters([...updateSizes])
  //   }

  return (
    <>
      <div className="mb-20">
        <h5 className="mb-10">ORDER STATUS</h5>
        <CheckBox
          heading="Select Size"
          filters={orderStatus}
          handleCheckBox={(e) => {
            console.log(e)
          }}
        />
      </div>

      <h5 className="mb-10">ORDER TIME</h5>
      <CheckBox
        heading="Select Size"
        filters={orderTime}
        handleCheckBox={(e) => {
          console.log(e)
        }}
      />
    </>
  )
}

// const mapStateToProps = (state) => ({
//   products: state.products.items,
// })

// const mapDidpatchToProps = {
//   updateProductFilters,
// }

export default OrderFilterSection
