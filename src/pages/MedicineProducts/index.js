/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import React from 'react'
import { useEffect, useState } from 'react'

import Breadcrumb2 from 'components/layout/Breadcrumb2'

import Pagination from 'components/common/Pagination'

import Layout from 'components/layout/Layout'
import {
  paginationController,
  cratePagination,
} from 'util/paginationController'
import queryString from 'query-string'

import { useDispatch, useSelector } from 'react-redux'
import { getProductsTemplate } from 'redux/slices/productTemplateSlice'
import ProductCard3 from 'components/common/productCard/ProductCard3'
import scrollToTop from 'util/scrollToTop'
import Preloader from 'components/common/Preloader'

let showLimit = 12
let showPagination = 4

const Products = ({ productFilters }) => {
  const [searchTerm, setSearchTerm] = useState(null)
  const { defaultProducts, totalProductTemplateCount, loading } = useSelector(
    (state) => state.productTemplate
  )
  const dispatch = useDispatch()

  let [pagination, setPagination] = useState([])
  let [limit, setLimit] = useState(showLimit)
  let [pages, setPages] = useState(null)
  let [currentPage, setCurrentPage] = useState(1)

  useEffect(() => {
    cratePagination(totalProductTemplateCount, limit, setPagination, setPages)
  }, [productFilters, limit, pages, totalProductTemplateCount])

  useEffect(() => {
    setPages(Math.ceil(totalProductTemplateCount / limit))
  }, [totalProductTemplateCount])

  const { getPaginationGroup } = paginationController(
    currentPage,
    limit,
    defaultProducts,
    showPagination,
    pagination
  )

  // const paramsCircles = queryString.stringify({ circles: circleIds }, { arrayFormat: 'bracket' })

  const next = () => {
    setCurrentPage((page) => page + 1)
  }

  const prev = () => {
    setCurrentPage((page) => page - 1)
  }

  const resetPage = () => {
    setCurrentPage(1)
  }

  const handleActive = (item) => {
    setCurrentPage(item)
  }

  // const selectChange = (e) => {
  //   setLimit(Number(e.target.value))
  //   setCurrentPage(1)
  //   setPages(Math.ceil(defaultProducts.length / Number(e.target.value)))
  // }

  const paginationObj = {
    page: currentPage,
    limit,
  }

  useEffect(() => {
    dispatch(
      getProductsTemplate(
        queryString.stringify(paginationObj),
        `&productType=Medicine&${
          searchTerm?.trim()?.length > 0 ? 'search=' + searchTerm : ''
        }`
      )
    )
    scrollToTop()
  }, [currentPage])

  const onSearchChange = (e) => {
    setSearchTerm(e.target.value)
    dispatch(
      getProductsTemplate(
        queryString.stringify(paginationObj),
        `&productType=Medicine&search=${e.target.value}`
      )
    )
    resetPage()
  }

  // useEffect(() => {
  //   dispatch(
  //     queryString.stringify(paginationObj),
  //     getProductsTemplate('productType=Medicine')
  //   )
  // }, [currentPage])

  return (
    <>
      <Layout noBreadcrumb="d-none">
        <Breadcrumb2
          title="Medicines"
          subParent={'Products'}
          subChild={'Medicines'}
        />
        <section className="mt-50 mb-50">
          <div className="container mb-30">
            {/* {loading ? (
              <Preloader />
            ) : ( */}
            <div className="row flex-row-reverse">
              <div
              //   className="col-lg-4-5"
              >
                <div className="shop-product-fillter">
                  <div className="totall-product">
                    <p>
                      We found
                      <strong className="text-brand">
                        {totalProductTemplateCount}
                      </strong>
                      items for you!
                    </p>
                  </div>
                  {/* <div className="sort-by-product-area">
                      <div className="sort-by-cover mr-10">
                        <ShowSelect
                          selectChange={selectChange}
                          showLimit={showLimit}
                        />
                      </div>
                      <div className="sort-by-cover">
                        <SortSelect />
                      </div>
                    </div> */}
                </div>
                <div className="search-medicine mb-20">
                  <input
                    type="text"
                    placeholder="Search Medicine"
                    onChange={onSearchChange}
                    style={{ width: 'fit-content', height: '50px' }}
                  />
                </div>
                {/* <div> */}
                {defaultProducts.length === 0 && <h3>No Products Found </h3>}

                {/* {defaultProducts.map((item, i) => (
                      <div
                        className="col-lg-1-5 col-md-4 col-12 col-sm-6"
                        key={i}
                      > */}
                <div className="product-list-small container">
                  <ProductCard3 product={defaultProducts} />
                </div>

                {/* <SingleProductList product={item}/> */}
                {/* </div>
                    ))} */}
                {/* </div> */}

                <div className="pagination-area mt-15 mb-sm-5 mb-lg-0">
                  <nav aria-label="Page navigation example">
                    <Pagination
                      getPaginationGroup={getPaginationGroup}
                      currentPage={currentPage}
                      pages={pages}
                      next={next}
                      prev={prev}
                      handleActive={handleActive}
                    />
                  </nav>
                </div>
              </div>
              <div
              //   className="col-lg-1-5 primary-sidebar sticky-sidebar"
              >
                {/* <div className="sidebar-widget widget-category-2 mb-30">
                    <h5 className="section-title style-1 mb-30">Category</h5>
                    <CategoryProduct categories={categories} />
                  </div> */}

                {/* <div className="sidebar-widget price_range range mb-30"> */}
                {/* <h5 className="section-title style-1 mb-30">Fill by price</h5> */}

                {/* <div className="price-filter">
                      <div className="price-filter-inner">
                        <br />
                        <PriceRangeSlider />
  
                        <br />
                      </div>
                    </div> */}

                <br />
                {/* </div> */}
              </div>
            </div>
            {/* )} */}
          </div>
        </section>
        {/* <WishlistModal /> */}
        {/* <CompareModal /> */}
        {/* <CartSidebar /> */}
        {/* <QuickView /> */}
        {/* <div className="container">
                    <div className="row">
                        <div className="col-xl-6">
                            <Search />
                        </div>
                        <div className="col-xl-6">
                            <SideBarIcons />
                        </div>
                    </div>
                    <div className="row justify-content-center text-center">
                        <div className="col-xl-6">
                            <CategoryProduct />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-xl-3">
                            
                        </div>
                        <div className="col-md-9">
                            

                            

                            
                        </div>
                    </div>
                </div> */}
      </Layout>
    </>
  )
}

// const mapStateToProps = (state) => ({
//   products: state.products,
//   productFilters: state.productFilters,
// })

// const mapDidpatchToProps = {
//   // openCart,
//   fetchProduct,
//   // fetchMoreProduct,
// }

export default Products
