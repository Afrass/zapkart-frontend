import React from 'react'
// import ProductDetails from '../../components/ecommerce/ProductDetails'
import Layout from '../../components/layout/Layout'
// import productData from 'data/product'
import { useParams } from 'react-router-dom'
import ProductDetails from 'components/ecommerce/ProductDetails'
import { useEffect } from 'react'
// import { useState } from 'react'
import scrollToTop from 'util/scrollToTop'
import { useDispatch, useSelector } from 'react-redux'
import {
  getProductfromProductTemplate,
  setResetProductsDetails,
} from 'redux/slices/productTemplateSlice'
import { Helmet } from 'react-helmet'
import Preloader from 'components/common/Preloader'

const ProductDetailsPage = () => {
  const dispatch = useDispatch()
  const {
    product,
    productsTemplate,
    productTemplate,
    otherProductsFromVariant,
    loading,
  } = useSelector((state) => state.productTemplate)

  let { productTemplateId, id } = useParams()

  useEffect(() => {
    if (id !== 'out-of-stock') {
      dispatch(getProductfromProductTemplate(productTemplateId, id))
    } else {
      dispatch(setResetProductsDetails())
      dispatch(getProductfromProductTemplate(productTemplateId))
    }

    scrollToTop()
  }, [id])

  return (
    <>
      {/* {productTemplate?.id && ( */}
      <Helmet>
        <title>{`${productTemplate?.metaTitle || 'Zapkart'}`}</title>
        <meta
          name="description"
          content={productTemplate?.metaDescription || ''}
        />
        <meta name="keywords" content={productTemplate?.metaKeywords || ''} />
        <meta name="tags" content={productTemplate?.tags || ''} />
      </Helmet>
      {/* )} */}
      {loading ? (
        <Preloader />
      ) : (
        <Layout
          parent="Home"
          sub="Category"
          subChild={productTemplate?.category?.name}
          subSubChild={
            product?.variant?.name || product?.name || productTemplate?.name
          }
        >
          <div className="container">
            {product?.id || productTemplate?.id ? (
              <ProductDetails
                product={product}
                productsTemplate={productsTemplate}
                productTemplate={productTemplate}
                otherProductsFromVariant={otherProductsFromVariant}
              />
            ) : (
              <h4>No Product Found</h4>
            )}
          </div>
        </Layout>
      )}
    </>
  )
}

export default ProductDetailsPage
