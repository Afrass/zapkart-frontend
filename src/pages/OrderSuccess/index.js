/* eslint-disable react/prop-types */
import { Result } from 'antd'
import Layout from 'components/layout/Layout'
import React from 'react'
import { useParams } from 'react-router-dom'

const OrderSuccess = () => {
  const { orderId } = useParams()
  return (
    <Layout parent="Home" sub="Order" subChild="OrderSuccess">
      <section className="mt-50 mb-50">
        <div className="container">
          <div className="row">
            <Result
              status="success"
              title="Purchase Done"
              subTitle={`Your Order ${orderId} was placed successfully. We will contact you soon.`}
            />
          </div>
        </div>
      </section>
    </Layout>
  )
}

export default OrderSuccess
