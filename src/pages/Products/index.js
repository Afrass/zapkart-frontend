/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import React from 'react'
// import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'
// import { connect } from 'react-redux'
import ShowSelect from 'components/filters/ShowSelect'
import SortSelect from 'components/filters/SortSelect'
import Breadcrumb2 from 'components/layout/Breadcrumb2'
import CategoryProduct from 'components/filters/CategoryProduct'
import ProductFilterSection from './ProductFilterSection'

import Pagination from 'components/common/Pagination'
// import QuickView from './../components/ecommerce/QuickView'
import ProductCard from 'components/common/productCard/ProductCard'

import Layout from 'components/layout/Layout'
// import productData from 'data/product'
import {
  paginationController,
  cratePagination,
} from 'util/paginationController'
import { useLocation, useNavigate, useParams } from 'react-router-dom'
import queryString from 'query-string'
import scrollToTop from 'util/scrollToTop'
import { useDispatch, useSelector } from 'react-redux'
import { getCategories } from 'redux/slices/categorySlice'
import {
  getProductsTemplate,
  setFilters,
} from 'redux/slices/productTemplateSlice'
import Preloader from 'components/common/Preloader'
// import { fetchProduct } from './../redux/action/product'

// eslint-disable-next-line no-unused-vars
const Products = ({ productFilters, fetchProduct }) => {
  const { defaultProducts, filters, totalProductTemplateCount, loading } =
    useSelector((state) => state.productTemplate)
  // const products = productData
  // const [products, setProducts] = useState([])

  //   let Router = useRouter(),
  // let searchTerm = Router.query.search

  let showLimit = 12
  let showPagination = 4

  let [pagination, setPagination] = useState([])
  let [limit, setLimit] = useState(showLimit)
  let [pages, setPages] = useState(null)
  let [currentPage, setCurrentPage] = useState(1)

  // const [filters, setFilters] = useState([])
  const [searchFilter, setSearchFilter] = useState(null)
  // const [category, setCategory] = useState([])

  useEffect(() => {
    setPages(Math.ceil(totalProductTemplateCount / limit))
  }, [totalProductTemplateCount])

  useEffect(() => {
    cratePagination(totalProductTemplateCount, limit, setPagination, setPages)
  }, [productFilters, limit, pages, totalProductTemplateCount])

  const {
    // getPaginatedProducts,
    getPaginationGroup,
  } = paginationController(
    currentPage,
    limit,
    defaultProducts,
    showPagination,
    pagination
  )

  // const paramsCircles = queryString.stringify({ circles: circleIds }, { arrayFormat: 'bracket' })

  const next = () => {
    setCurrentPage((page) => page + 1)
  }

  const prev = () => {
    setCurrentPage((page) => page - 1)
  }
  const resetPage = () => {
    setCurrentPage(1)
  }

  const handleActive = (item) => {
    setCurrentPage(item)
  }

  console.log(currentPage, limit, 'pages-linmites')
  // const selectChange = (e) => {
  //   setLimit(Number(e.target.value))
  //   setCurrentPage(1)
  //   setPages(Math.ceil(defaultProducts.length / Number(e.target.value)))
  // }

  let { id } = useParams()
  const navigate = useNavigate()
  const { search } = useLocation()

  let parsed = search.replace('?', '')
  // const { category, searchQuery, brand } = queryString.parse(parsed)
  const dispatch = useDispatch()

  const parsedQueryString = queryString.parse(parsed)

  useEffect(() => {
    const initialFilters = []

    for (const key in parsedQueryString) {
      // if (parsedQueryString[key] !== 'selected') {
      if (Array.isArray(parsedQueryString[key])) {
        parsedQueryString[key]?.forEach((val) => {
          initialFilters.push({
            filterName: key,
            value: val,
          })
        })
      } else {
        initialFilters.push({
          filterName: key,
          value: parsedQueryString[key],
        })
      }
      // }
    }

    dispatch(setFilters(initialFilters))

    console.log(initialFilters, 'shstgf')
  }, [])

  // useEffect(() => {
  //   dispatch(getProductsTemplate(queryString.stringify(pagination), qs))
  // }, [page])

  // useEffect(() => {
  //   dispatch(getCategories())

  //   if (category) {
  // dispatch(getProductsTemplate(`categoryId=${category}`))
  // //   } else if (searchQuery) {
  // dispatch(getProductsTemplate(`search=${searchQuery}`))
  // //   } else if (brand) {
  // dispatch(getProductsTemplate(`brandId=${brand}`))
  // //   } else {
  // dispatch(getProductsTemplate())
  //   }
  // }, [searchQuery, brand, category])

  useEffect(() => {
    const pagination = {
      page: currentPage,
      limit,
    }
    if (filters?.length > 0) {
      let qs = ''
      filters.forEach((cur, i) => {
        // if (i === filters.length - 1) {
        qs += `&${cur.filterName}=${cur.value}`
        // } else {
        // queryString += `${cur.filterName}=${cur.value}`
        // }
      })

      console.log(qs, 'heyujgbujk')

      dispatch(getProductsTemplate(queryString.stringify(pagination), qs))

      navigate(`/products?${qs}`, { replace: true })
    } else {
      navigate(`/products`, { replace: true })

      dispatch(
        getProductsTemplate(
          queryString.stringify(pagination),
          '&productType=NonMedicine'
        )
      )
    }
    scrollToTop()
  }, [filters, currentPage])

  useEffect(() => {
    resetPage()
  }, [filters])

  useEffect(() => {
    return () => dispatch(setFilters([]))
  }, [])

  return (
    <>
      <Layout noBreadcrumb="d-none">
        <Breadcrumb2
          subParent={
            parsedQueryString?.selected
              ? parsedQueryString?.selected === 'search'
                ? 'Search'
                : parsedQueryString?.selected?.split('-')[0]
              : 'Products'
          }
          subChild={
            parsedQueryString?.selected
              ? parsedQueryString?.selected === 'search'
                ? parsedQueryString?.search
                : parsedQueryString?.selected
                    ?.split('-')
                    ?.map((cur, i) => (i !== 0 ? cur : ''))
                    .join('')
              : 'All'
          }
        />
        <section className="mt-50 mb-50">
          <div className="container mb-30">
            <div className="row">
              <div className="col-lg-1-5 primary-sidebar sticky-sidebar">
                {/* <div className="sidebar-widget widget-category-2 mb-30">
                  <h5 className="section-title style-1 mb-30">Category</h5>
                  <CategoryProduct categories={categories} />
                </div> */}

                {/* <div className="sidebar-widget price_range range mb-30"> */}
                {/* <h5 className="section-title style-1 mb-30">Fill by price</h5> */}

                {/* <div className="price-filter">
                    <div className="price-filter-inner">
                      <br />
                      <PriceRangeSlider />

                      <br />
                    </div>
                  </div> */}

                <div className="list-group">
                  {/* <label className="fw-900">Color</label> */}
                  <ProductFilterSection
                    hideFilter={parsedQueryString?.selected}
                    // setFilters={setFilters}
                    // filters={filters}
                  />
                  {/* <label className="fw-900 mt-15">Item Condition</label> */}
                  {/* <SizeFilter /> */}
                </div>
                <br />
                {/* </div> */}
              </div>
              {loading ? (
                <Preloader isFullPage={false} />
              ) : (
                <div className="col-lg-4-5">
                  <div className="shop-product-fillter">
                    <div className="totall-product">
                      <p>
                        We found
                        <strong className="text-brand">
                          {totalProductTemplateCount}
                        </strong>
                        items for you!
                      </p>
                    </div>
                    {/* <div className="sort-by-product-area">
                          <div className="sort-by-cover mr-10">
                            <ShowSelect
                              selectChange={selectChange}
                              showLimit={showLimit}
                            />
                          </div>
                          <div className="sort-by-cover">
                            <SortSelect />
                          </div>
                        </div> */}
                  </div>
                  <div className="row product-grid">
                    {defaultProducts.length === 0 && (
                      <h3>No Products Found </h3>
                    )}

                    {defaultProducts.map((item, i) => (
                      <div
                        className="col-lg-1-5 col-md-4 col-sm-6 col-6"
                        key={i}
                      >
                        <ProductCard product={item} />

                        {/* <SingleProductList product={item}/> */}
                      </div>
                    ))}
                  </div>

                  <div className="pagination-area mt-15 mb-sm-5 mb-lg-0">
                    <nav aria-label="Page navigation example">
                      <Pagination
                        getPaginationGroup={getPaginationGroup}
                        currentPage={currentPage}
                        pages={pages}
                        next={next}
                        prev={prev}
                        handleActive={handleActive}
                      />
                    </nav>
                  </div>
                </div>
              )}
            </div>
          </div>
        </section>
        {/* <WishlistModal /> */}
        {/* <CompareModal /> */}
        {/* <CartSidebar /> */}
        {/* <QuickView /> */}
        {/* <div className="container">
                    <div className="row">
                        <div className="col-xl-6">
                            <Search />
                        </div>
                        <div className="col-xl-6">
                            <SideBarIcons />
                        </div>
                    </div>
                    <div className="row justify-content-center text-center">
                        <div className="col-xl-6">
                            <CategoryProduct />
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-xl-3">
                            
                        </div>
                        <div className="col-md-9">
                            

                            

                            
                        </div>
                    </div>
                </div> */}
      </Layout>
    </>
  )
}

// const mapStateToProps = (state) => ({
//   products: state.products,
//   productFilters: state.productFilters,
// })

// const mapDidpatchToProps = {
//   // openCart,
//   fetchProduct,
//   // fetchMoreProduct,
// }

export default Products
