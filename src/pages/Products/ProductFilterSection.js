/* eslint-disable react/prop-types */
import React, { useEffect, useState } from 'react'
// import { useRouter } from 'next/router'
// import { updateProductFilters } from "../../../redux/action/productFiltersAction";
import CheckBox from 'components/common/CheckBox'
import { useDispatch, useSelector } from 'react-redux'
import { getCategories } from 'redux/slices/categorySlice'
import { getBrands } from 'redux/slices/brandSlice'
import { getManufactures } from 'redux/slices/manufactureSlice'
import { setFilters } from 'redux/slices/productTemplateSlice'
import { Collapse } from 'antd'

const ProductFilterSection = ({ hideFilter }) => {
  const [width, setWidth] = useState(window.innerWidth)

  const [filterCategoriesItems, setFilterCategoriesItems] = useState([])
  const [filterBrandsItems, setAllFilterBrandsItems] = useState([])
  const [filterManufacturesItems, setFilterManufacturesItems] = useState([])

  const { filters } = useSelector((state) => state.productTemplate)
  const { categories } = useSelector((state) => state.category)
  const { brands } = useSelector((state) => state.brand)
  const { manufactures } = useSelector((state) => state.manufacture)

  const dispatch = useDispatch()

  // In this function auto check filter selectbox if filter available
  const makeCheckBoxDefaultCheck = (data, filterName, setData) => {
    const allFilteredDatas = filters?.filter(
      (cur) => cur.filterName === filterName
    )

    const availableData = data.map((obj) => {
      const elementInArr2 = allFilteredDatas.find((o) => o.value === obj.id)
      if (elementInArr2?.value) {
        return { ...obj, defaultChecked: true }
      } else {
        return { ...obj, defaultChecked: false }
      }
    })

    setData(availableData)
  }

  const handleCheckboxChange = (e, filterName) => {
    if (e.target.checked) {
      const cloneFilters = [...filters]

      // Remove Filters
      // const removeFromHeaderNavFilters = cloneFilters.filter(
      //   (cur) => !cur.fromHeader
      // )

      dispatch(
        setFilters([...cloneFilters, { filterName, value: e.target.name }])
      )
    } else {
      const removedFilter = filters.filter((cur) => cur.value !== e.target.name)
      dispatch(setFilters(removedFilter))
    }
  }

  useEffect(() => {
    makeCheckBoxDefaultCheck(categories, 'categoryId', setFilterCategoriesItems)
    makeCheckBoxDefaultCheck(brands, 'brandId', setAllFilterBrandsItems)
    makeCheckBoxDefaultCheck(
      manufactures,
      'manufactureId',
      setFilterManufacturesItems
    )
  }, [categories, brands, manufactures, filters])

  // useEffect(() => {
  // // Get brands from filter array
  // const allFilteredManufactures = filters?.filter(
  //   (cur) => cur.filterName === 'manufactureId'
  // )
  // // We need default checked option if from url
  // const availableManufactures = manufactures.map((obj) => {
  //   const elementInArr2 = allFilteredManufactures.find(
  //     (o) => o.value === obj.id && !o.fromHeader
  //   )
  //   if (elementInArr2?.value) {
  //     return { ...obj, defaultChecked: true }
  //   } else {
  //     return { ...obj, defaultChecked: false }
  //   }
  // })
  // setAllManufactures(availableManufactures)
  // makeCheckBoxCheck(manufactures, 'manufactureId', setFilterManufacturesItems)
  // }, [manufactures, filters])

  useEffect(() => {
    if (!hideFilter?.includes('category-')) {
      dispatch(getCategories())
    }

    if (!hideFilter?.includes('brand-')) {
      dispatch(getBrands())
    }

    dispatch(getManufactures())
    setWidth(window.innerWidth)
  }, [])

  const { Panel } = Collapse

  const isMobile = width <= 768

  return (
    <>
      <h4 className="mb-3">Filters</h4>
      <Collapse defaultActiveKey={!isMobile && ['1', '2', '3']}>
        {!hideFilter?.includes('category-') && (
          <Panel header="Category" key="1">
            <div
              className="list-group-item mb-10 mt-10"
              style={{ border: '1px solid rgba(0,0,0,.125)' }}
            >
              <h5 className="mb-10">Categories</h5>
              <div className="custome-checkbox">
                <div className="mb-20">
                  <CheckBox
                    filters={filterCategoriesItems}
                    handleCheckBox={(e) =>
                      handleCheckboxChange(e, 'categoryId')
                    }
                  />
                </div>
              </div>
            </div>
          </Panel>
        )}

        {!hideFilter?.includes('brand-') && (
          <Panel header="Brands" key="2">
            <div
              className="list-group-item mb-10 mt-10"
              style={{ border: '1px solid rgba(0,0,0,.125)' }}
            >
              <h5 className="mb-10">Brands</h5>
              <div className="custome-checkbox">
                <div className="mb-20">
                  <CheckBox
                    filters={filterBrandsItems}
                    handleCheckBox={(e) => handleCheckboxChange(e, 'brandId')}
                  />
                </div>
              </div>
            </div>
          </Panel>
        )}

        <Panel header="Manufactures" key="3">
          <div
            className="list-group-item mb-10 mt-10"
            style={{ border: '1px solid rgba(0,0,0,.125)' }}
          >
            <h5 className="mb-10">Manufactures</h5>
            <div className="custome-checkbox">
              <div className="mb-20">
                <CheckBox
                  filters={filterManufacturesItems}
                  handleCheckBox={(e) =>
                    handleCheckboxChange(e, 'manufactureId')
                  }
                />
              </div>
            </div>
          </div>
        </Panel>
      </Collapse>
    </>
  )
}

export default ProductFilterSection
