import React from 'react'
import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import Layout from 'components/layout/Layout'
import { Link, useNavigate } from 'react-router-dom'
import {
  getWishlistSlice,
  removeWishlistItemSlice,
} from 'redux/slices/wishlistSlice'
import { addToCartSlice } from 'redux/slices/cartSlice'

const Wishlist = () => {
  const navigate = useNavigate()
  const dispatch = useDispatch()
  const { wishlist } = useSelector((state) => state.wishlist)

  useEffect(() => {
    dispatch(getWishlistSlice())
  }, [])

  const addToCart = async (product) => {
    dispatch(addToCartSlice(product.id))
  }

  const removeWishList = (id) => {
    dispatch(removeWishlistItemSlice(id))
  }

  return (
    <>
      <Layout parent="Home" sub="Wishlist">
        <section className="mt-50 mb-50">
          <div className="container">
            <div className="row">
              <div className="col-xl-10 col-lg-12 m-auto">
                {wishlist.length > 0 ? (
                  <div className="table-responsive shopping-summery">
                    <table className="table table-wishlist">
                      <thead>
                        <tr className="main-heading">
                          <th
                            className="custome-checkbox start pl-30"
                            colSpan="2"
                          >
                            Product
                          </th>
                          <th scope="col">Price</th>

                          <th scope="col">Stock Status</th>
                          <th scope="col">Action</th>
                          <th scope="col" className="end">
                            Remove
                          </th>
                        </tr>
                      </thead>
                      <tbody>
                        {wishlist.map((product) => (
                          <tr className="pt-30" key={product.id}>
                            <td className="image product-thumbnail pt-40">
                              <img
                                // src={`${process.env.REACT_APP_BASE_URL}/${product.images[0].url}`}
                                src={product?.images[0]}
                                alt=""
                                className="img-fluid"
                                onClick={() =>
                                  navigate(
                                    `/product/${product?.productTemplate?.id}/${product.id}`
                                  )
                                }
                              />
                            </td>

                            <td className="product-des product-name">
                              <h6 className="product-name  mb-10">
                                <Link
                                  to={`/product/${product.productTemplate?.id}/${product.id}`}
                                >
                                  {product.name}
                                </Link>
                              </h6>
                              {/* <div className="product-rate-cover">
                                <div className="product-rate d-inline-block">
                                  <div
                                    className="product-rating"
                                    style={{
                                      width: '90%',
                                    }}
                                  ></div>
                                </div>
                                <span className="font-small ml-5 text-muted">
                                  {' '}
                                  (4.0)
                                </span>
                              </div> */}
                            </td>
                            <td className="price" data-title="Price">
                              <h3 className="text-brand">₹{product?.price}</h3>
                            </td>
                            <td
                              className="text-center detail-info"
                              data-title="Stock"
                            >
                              {product.qty === 0 ? (
                                <span className="stock-status out-stock mb-0">
                                  Out of stock
                                </span>
                              ) : (
                                <span className="stock-status in-stock mb-0">
                                  In Stock
                                </span>
                              )}
                            </td>
                            <td className="text-right" data-title="Cart">
                              {product.qty === 0 ? (
                                <button className="btn btn-sm btn-secondary">
                                  Contact Us
                                </button>
                              ) : (
                                <button
                                  className="btn btn-sm"
                                  // onClick={() => handleCart(product)}
                                  onClick={() => addToCart(product)}
                                >
                                  Add to cart
                                </button>
                              )}
                            </td>
                            <td className="action" data-title="Remove">
                              <a
                                href="#/"
                                onClick={() => removeWishList(product.id)}
                              >
                                <i className="fi-rs-trash"></i>
                              </a>
                            </td>
                          </tr>
                        ))}
                      </tbody>
                    </table>
                    {/* <div className="text-right">
                      <span className="clear-btn">Clear All</span>
                    </div> */}
                  </div>
                ) : (
                  <h4 className="mb-0">No Products</h4>
                )}
              </div>
            </div>
          </div>
        </section>
      </Layout>
    </>
  )
}

export default Wishlist

// ------------------------------------NOTE: For Reference Remove this Later-------------------------------------------

// const cartItems = localStorage.getItem('wishlistItems')
// const parsedItems = JSON.parse(cartItems)
// if (parsedItems) {
//   const sortUniqueWishlist = Array.from(
//     new Set(parsedItems.map((a) => a.id))
//   ).map((id) => {
//     return parsedItems.find((a) => a.id === id)
//   })
//   setWishlistItems(sortUniqueWishlist)
// }

// const removeWishList = (id) => {
//   dispatch(removeWishlistItemSlice(id))
//   // const restWishlist = wishlistItems.filter((cur) => cur.id !== id)
//   // setWishlistItems(restWishlist)
//   // toast.success('Remove Item Success')
//   // localStorage.setItem('wishlistItems', JSON.stringify(restWishlist))
// }
