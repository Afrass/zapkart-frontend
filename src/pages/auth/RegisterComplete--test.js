/* eslint-disable no-unused-vars */
/* eslint-disable react/no-unescaped-entities */
import Layout from "components/layout/Layout";

import { ErrorMessage, Field, Form, Formik } from "formik";
import React, { useState, useEffect } from "react";
import ErrorText from "components/common/ErrorText";
import * as Yup from "yup";
import axios from "axios";
import { toast } from "react-toastify";
import { Link, useNavigate } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { setUser, setToken } from "../../redux/slices/authSlice";
import scrollToTop from "util/scrollToTop";
import {
  sendSignInLinkToEmail,
  signInWithEmailLink,
  updatePassword,
} from "firebase/auth";
import { authentication } from "firebaseconfig";
import getFBError from "util/firebaseErrors";
// import { scrollToTop } from '../../utils/scroll'

const Register = () => {
  const [currentStage, setCurrentStage] = useState("phoneVerification");
  const [phoneNumber, setPhoneNumber] = useState("");
  const [signupState, setSignupState] = useState("email");

  const navigate = useNavigate();
  const { authorized } = useSelector((state) => state.auth);
  const dispatch = useDispatch();

  // useEffect(() => {
  //   scrollToTop()
  //   if (authorized) {
  //     navigate('/')
  //   }
  // }, [authorized, navigate])

  const initialValues1 = {
    number: "",
  };

  const initialValues2 = {
    firstName: "",
    // phone: '',
    otp: "",
    lastName: "",
    password: "",
    confirmPassword: "",
  };

  // ---------------------------------------

  // Email Reg
  const validationSchema = Yup.object({
    // email: Yup.string().email('*Invalid Email').required('* Email Required'),
    // firstName: Yup.string().required('* Password Required'),
    // number: Yup.string().required('Phone Number Required'),
    email: Yup.string().required("Email Required"),
    password: Yup.string().required("* Password Required"),

    // lastName: Yup.string().nullable(),
    // password: Yup.string().required('* Password Required'),
    // confirmPassword: Yup.string()
    //   .oneOf([Yup.ref('password'), null], '* Passwords must match')
    //   .required('* Password Confirm Required'),
  });

  // -------------------------------------------------------

  //Email Reg
  const onSubmitHandler = async (
    { firstName, number, lastName, email, password },
    { setSubmitting }
  ) => {
    if (!email || !password) {
      toast.error("Email AND PASSWORD REquired");
      return;
    }
    if (password.length < 6) {
      toast.error("Password must atleast 6 characters long");
      return;
    }
    try {
      const result = await signInWithEmailLink(
        authentication,
        email,
        window.location.href
      );
      if (result.user.emailVerified) {
        // Remove user Email from localstorage
        window.localStorage.removeItem("emailForRegistration");
        //   Get user id token
        let user = authentication.currentUser;
        await updatePassword(user, password);
        const idTokenResult = await user.getIdTokenResult(true);
        // Redux Store

        // Redirect
        navigate("/");
      }
    } catch (err) {
      toast.error(getFBError(err.code));
    }
  };
  return (
    <>
      <Layout parent="Home" sub="Pages" subChild="Login & Register">
        <div className="page-content pt-50 pb-50">
          <div className="container">
            <div className="row">
              <div className="col-xl-8 col-lg-10 col-md-12 m-auto">
                <div className="row">
                  <div className="col-lg-6 pr-30 d-none d-lg-block">
                    <img
                      className="border-radius-15"
                      src={require("assets/imgs/page/login-1.png")}
                      alt=""
                    />
                  </div>
                  <div className="col-lg-6 col-md-8">
                    <div className="login_wrap widget-taber-content background-white">
                      <div className="padding_eight_all bg-white">
                        <div className="heading_s1">
                          <h3 className="mb-5">Register Complete</h3>
                        </div>

                        <Formik
                          initialValues={{
                            email: window.localStorage.getItem(
                              "emailForRegistration"
                            ),
                            password: "",
                          }}
                          onSubmit={onSubmitHandler}
                          validationSchema={validationSchema}
                        >
                          {(fomrik) => {
                            const { touched, errors } = fomrik;

                            return (
                              <Form>
                                {/* <h4>Register with OTP</h4> */}
                                <div className="form-group">
                                  <label htmlFor="email">Email</label>
                                  <Field
                                    type="email"
                                    name="email"
                                    disabled
                                    className="form-control"
                                    style={{
                                      border: `${
                                        touched.email && errors.email
                                          ? "1px solid red"
                                          : ""
                                      }`,
                                    }}
                                  />
                                  <ErrorMessage
                                    name="email"
                                    component={ErrorText}
                                  />
                                </div>

                                <div className="form-group">
                                  <label htmlFor="password">Password</label>
                                  <Field
                                    autocomplete="new-password"
                                    type="password"
                                    name="password"
                                    className="password-login form-control f2"
                                    style={{
                                      border: `${
                                        touched.password && errors.password
                                          ? "1px solid red"
                                          : ""
                                      }`,
                                    }}
                                  />
                                  <ErrorMessage
                                    name="password"
                                    component={ErrorText}
                                  />
                                </div>

                                <button className="btn w-100 mb-3">
                                  Complete Registration
                                </button>
                              </Form>
                            );
                          }}
                        </Formik>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Layout>
    </>
  );
};

export default Register;
