/* eslint-disable no-unused-vars */
/* eslint-disable react/no-unescaped-entities */
import Layout from "components/layout/Layout";

import { ErrorMessage, Field, Form, Formik } from "formik";
import React, { useState, useEffect } from "react";
import ErrorText from "components/common/ErrorText";
import * as Yup from "yup";
import axios from "axios";
import { toast } from "react-toastify";
import { Link, useNavigate } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { setUser, setToken } from "../../redux/slices/authSlice";
import scrollToTop from "util/scrollToTop";
import {
  EmailAuthProvider,
  RecaptchaVerifier,
  sendSignInLinkToEmail,
  signInWithPhoneNumber,
} from "firebase/auth";
import { authentication } from "firebaseconfig";
import EmailRegister from "./Register/EmailRegister";
import getFBError from "util/firebaseErrors";
// import { scrollToTop } from '../../utils/scroll'

const Register = () => {
  const [currentStage, setCurrentStage] = useState("phoneVerification");
  const [phoneNumber, setPhoneNumber] = useState("");
  const [signupState, setSignupState] = useState("email");
  const [phoneRegisterState, setPhoneRegisterState] = useState("first-phase");

  const navigate = useNavigate();
  const { authorized } = useSelector((state) => state.auth);
  const dispatch = useDispatch();

  useEffect(() => {
    scrollToTop();
    if (authorized) {
      navigate("/");
    }
  }, [authorized, navigate]);

  const initialValues1 = {
    number: "",
  };

  const initialValues2 = {
    firstName: "",
    // phone: '',
    otp: "",
    lastName: "",
    password: "",
    confirmPassword: "",
  };

  // Email
  const initialValuesEmailReg = {
    firstName: "",
    // phone: '',
    email: "",
    lastName: "",
    password: "",
    confirmPassword: "",
  };

  // ---------------------------------------

  const validationSchema1 = Yup.object({
    number: Yup.string().required("Phone Number Required"),
  });

  const validationSchema2 = Yup.object({
    // email: Yup.string().email('*Invalid Email').required('* Email Required'),
    firstName: Yup.string().required("* Password Required"),
    otp: Yup.string().required("OTP Required"),
    lastName: Yup.string().nullable(),
    password: Yup.string().required("* Password Required"),
    confirmPassword: Yup.string()
      .oneOf([Yup.ref("password"), null], "* Passwords must match")
      .required("* Password Confirm Required"),
  });

  // Email Reg
  const validationSchemaEmailReg = Yup.object({
    // email: Yup.string().email('*Invalid Email').required('* Email Required'),
    // firstName: Yup.string().required('* Password Required'),
    // number: Yup.string().required('Phone Number Required'),
    email: Yup.string().required("Email Required"),
    // lastName: Yup.string().nullable(),
    // password: Yup.string().required('* Password Required'),
    // confirmPassword: Yup.string()
    //   .oneOf([Yup.ref('password'), null], '* Passwords must match')
    //   .required('* Password Confirm Required'),
  });

  // -------------------------------------------------------

  const onSubmitHandler1 = async ({ number }, { setSubmitting, resetForm }) => {
    try {
      const { data } = await axios.get(
        `${process.env.REACT_APP_BASE_URL}/api/backend/v1/user/signup/mvalidate/${number}`
      );
      if (data) {
        setPhoneNumber(number);
        // alert('OTP Send Successfully')
        toast.success("OTP Sent");
        setCurrentStage("signup");
        setSubmitting(false);
        resetForm();
        document.querySelector(".first-name-signup").value = "";
      }
    } catch (err) {
      // alert(err.response.data.error)
      toast.error(err.response.data.error);
      setSubmitting(false);
    }
  };

  const onSubmitHandler2 = async (
    { firstName, lastName, otp, password },
    { setSubmitting }
  ) => {
    const sendingValues = {
      firstName,
      lastName,
      otp: Number(otp),
      password,
    };

    sendingValues.phone = phoneNumber;

    if (!lastName) {
      delete sendingValues.lastName;
    }

    setSubmitting(false);

    try {
      const { data } = await axios.post(
        `${process.env.REACT_APP_BASE_URL}/api/backend/v1/user/signup`,
        sendingValues
      );
      if (data) {
        // toast.success('Register Success! Please Login To Continue')
        // navigate('/Login')
        toast.success("SignUp Successful");
        dispatch(setToken(data.token));
        dispatch(setUser(data.user));
        // setValue('2')
        // setPhoneNumber(number)
        // alert('Successfully')
        // setCurrentStage('signup')
        setSubmitting(false);
      }
    } catch (err) {
      toast.error(err.response.data.error);
      // alert(err.response.data.error)
      setSubmitting(false);
    }
  };

  //Email Reg
  const onSubmitHandlerEmailReg = async (
    { firstName, number, lastName, email, password },
    { setSubmitting }
  ) => {
    const config = {
      url: "https://zapkart-firebase.netlify.app/register/complete",
      handleCodeInApp: true,
    };

    // Make sure enable google, and email service in firebase console website
    await sendSignInLinkToEmail(authentication, email, config);
    toast.success(
      `Email is sent to ${email}. Click the link to complete your registration.`
    );
    // Save User Email in localStorage
    window.localStorage.setItem("emailForRegistration", email);

    // clear state
    // setEmail('')

    // const sendingValues = {
    //   firstName,
    //   lastName,
    //   email,
    //   password,
    //   phone: number,
    // }

    // // sendingValues.phone = phoneNumber

    // if (!lastName) {
    //   delete sendingValues.lastName
    // }

    // setSubmitting(false)

    // try {
    //   const { data } = await axios.post(
    //     `${process.env.REACT_APP_BASE_URL}/api/backend/v1/users/register`,
    //     sendingValues
    //   )
    //   if (data) {
    //     // toast.success('Register Success! Please Login To Continue')
    //     // setValue('2')

    //     toast.success('SignUp Successful')
    //     dispatch(setToken(data.token))
    //     dispatch(setUser(data.user))

    //     // setPhoneNumber(number)
    //     // alert('Successfully')
    //     // setCurrentStage('signup')
    //     setSubmitting(false)
    //   }
    // } catch (err) {
    //   toast.error(err.response.data.error)
    //   // alert(err.response.data.error)
    //   setSubmitting(false)
    // }
  };

  // Phone number registration
  const generateRecaptcha = () => {
    window.recaptchaVerifier = new RecaptchaVerifier(
      "recaptcha-container",
      {
        size: "invisible",
        // eslint-disable-next-line no-unused-vars
        callback: (response) => {
          // reCAPTCHA solved, allow signInWithPhoneNumber.
          // onSignInSubmit();
        },
      },
      authentication
    );
  };
  const phoneNumberSentOtp = ({ number }, { setSubmitting }) => {
    generateRecaptcha();
    let appVerifier = window.recaptchaVerifier;
    signInWithPhoneNumber(authentication, `+91${number}`, appVerifier)
      .then((confirmationResult) => {
        // setLoading(false)

        window.confirmationResult = confirmationResult;
        setPhoneNumber(number);
        toast.success("OTP Sent");

        setPhoneRegisterState("second-phase");
        setSubmitting(false);
      })
      .catch((err) => {
        toast.error(getFBError(err.code));
      });
    setSubmitting(false);
  };

  // Verify otp
  const phoneNumberVerifyOtp = ({ otp }, { setSubmitting }) => {
    if (otp.length === 6) {
      const confirmationResult = window.confirmationResult;
      confirmationResult
        .confirm(otp)
        .then(async (result) => {
          // User signed in successfully.
          const { user } = result;
          const idTokenResult = await user.getIdTokenResult(true);

          toast.success("Verified Successful");
          dispatch(setToken(idTokenResult.token));
          // dispatch(
          //   setUser({
          //     email: user.email,
          //     phone: user.phoneNumber,
          //   })
          // )
          navigate("/");
          setSubmitting(false);

          // ...
        })
        .catch((error) => {
          toast.error(error.message);
          // User couldn't sign in (bad verification code?)
          // ...
        });
    } else {
      toast.error("OTP Must be 6 characters long");
    }
  };

  return (
    <>
      <Layout parent="Home" sub="Pages" subChild="Login & Register">
        <div className="page-content pt-50 pb-50">
          <div className="container">
            <div className="row">
              <div className="col-xl-8 col-lg-10 col-md-12 m-auto">
                <div className="row">
                  <div className="col-lg-6 pr-30 d-none d-lg-block">
                    <img
                      className="border-radius-15"
                      src={require("assets/imgs/page/login-1.png")}
                      alt=""
                    />
                  </div>
                  <div className="col-lg-6 col-md-8">
                    <div className="login_wrap widget-taber-content background-white">
                      <div className="padding_eight_all bg-white">
                        <div className="heading_s1">
                          <h3 className="mb-5">Register</h3>
                          <p className="mb-30">
                            Have an account?
                            <Link to="/login">Login Here</Link>
                          </p>
                        </div>
                        {signupState === "otp" ? (
                          <>
                            {currentStage === "phoneVerification" ? (
                              <Formik
                                initialValues={initialValues1}
                                onSubmit={onSubmitHandler1}
                                validationSchema={validationSchema1}
                              >
                                {(fomrik) => {
                                  const { touched, errors } = fomrik;

                                  return (
                                    <Form>
                                      {/* <h4>Register with OTP</h4> */}
                                      <div className="form-group">
                                        <label htmlFor="number">
                                          Phone Number
                                        </label>
                                        <Field
                                          type="text"
                                          name="number"
                                          className="form-control"
                                          placeholder="Eg: 9876543210"
                                          style={{
                                            border: `${
                                              touched.number && errors.number
                                                ? "1px solid red"
                                                : ""
                                            }`,
                                          }}
                                        />
                                        <ErrorMessage
                                          name="number"
                                          component={ErrorText}
                                        />
                                      </div>
                                      <div>
                                        {/* <h6 className="signin-link">
                                          if you already have an account?{' '}
                                          <Link to="/Login">Sign in here</Link>
                                        </h6> */}

                                        <p
                                          onClick={() =>
                                            setSignupState("email")
                                          }
                                          style={{
                                            textDecoration: "underline",
                                            cursor: "pointer",
                                            marginBottom: "10px",
                                          }}
                                        >
                                          Signup with Email
                                        </p>
                                        <button className="btn w-100 mb-3">
                                          Sent OTP
                                        </button>
                                      </div>
                                    </Form>
                                  );
                                }}
                              </Formik>
                            ) : (
                              <Formik
                                initialValues={initialValues2}
                                onSubmit={onSubmitHandler2}
                                validationSchema={validationSchema2}
                              >
                                {(fomrik) => {
                                  const { touched, errors, isSubmitting } =
                                    fomrik;

                                  return (
                                    <Form>
                                      {/* <h4>Register with OTP</h4> */}
                                      <div className="form-group">
                                        <label htmlFor="firstName">
                                          First Name
                                        </label>
                                        <Field
                                          className="first-name-signup form-control"
                                          type="text"
                                          name="firstName"
                                          style={{
                                            border: `${
                                              touched.firstName &&
                                              errors.firstName
                                                ? "1px solid red"
                                                : ""
                                            }`,
                                          }}
                                        />
                                        <ErrorMessage
                                          name="firstName"
                                          component={ErrorText}
                                        />
                                      </div>

                                      <div className="form-group">
                                        <label htmlFor="lastName">
                                          Last Name
                                        </label>
                                        <Field
                                          type="text"
                                          name="lastName"
                                          className="form-control"
                                          style={{
                                            border: `${
                                              touched.lastName &&
                                              errors.lastName
                                                ? "1px solid red"
                                                : ""
                                            }`,
                                          }}
                                        />
                                        <ErrorMessage
                                          name="lastName"
                                          component={ErrorText}
                                        />
                                      </div>

                                      <div className="form-group">
                                        <label htmlFor="otp">OTP</label>
                                        <Field
                                          type="text"
                                          name="otp"
                                          className="form-control"
                                          autocomplete="new-password"
                                          style={{
                                            border: `${
                                              touched.otp && errors.otp
                                                ? "1px solid red"
                                                : ""
                                            }`,
                                          }}
                                        />
                                        <ErrorMessage
                                          name="otp"
                                          component={ErrorText}
                                        />
                                      </div>

                                      <div className="form-group">
                                        <label htmlFor="password">
                                          Password
                                        </label>
                                        <Field
                                          type="password"
                                          name="password"
                                          autocomplete="new-password"
                                          className="form-control"
                                          style={{
                                            border: `${
                                              touched.password &&
                                              errors.password
                                                ? "1px solid red"
                                                : ""
                                            }`,
                                          }}
                                        />
                                        <ErrorMessage
                                          name="password"
                                          component={ErrorText}
                                        />
                                      </div>
                                      <div className="form-group">
                                        <label htmlFor="confirmPassword">
                                          Confirm Password
                                        </label>
                                        <Field
                                          type="password"
                                          name="confirmPassword"
                                          className="form-control"
                                          style={{
                                            border: `${
                                              touched.confirmPassword &&
                                              errors.confirmPassword
                                                ? "1px solid red"
                                                : ""
                                            }`,
                                          }}
                                        />
                                        <ErrorMessage
                                          name="confirmPassword"
                                          component={ErrorText}
                                        />
                                      </div>
                                      <div>
                                        <button
                                          className="btn w-100 mb-3"
                                          disabled={isSubmitting}
                                        >
                                          Register
                                        </button>
                                      </div>
                                    </Form>
                                  );
                                }}
                              </Formik>
                            )}
                          </>
                        ) : signupState === "email" ? (
                          <EmailRegister setSignupState={setSignupState} />
                        ) : (
                          <>
                            {phoneRegisterState === "first-phase" ? (
                              <Formik
                                initialValues={{
                                  number: "",
                                }}
                                onSubmit={phoneNumberSentOtp}
                                validationSchema={Yup.object({
                                  number: Yup.string().required(
                                    "Phone Number Required"
                                  ),
                                })}
                              >
                                {(fomrik) => {
                                  const { touched, errors, isSubmitting } =
                                    fomrik;

                                  return (
                                    <Form>
                                      <div className="form-group">
                                        <label htmlFor="number">
                                          Phone Number
                                        </label>
                                        <Field
                                          type="text"
                                          name="number"
                                          className="form-control"
                                          placeholder="Eg: 9876543210"
                                          style={{
                                            border: `${
                                              touched.number && errors.number
                                                ? "1px solid red"
                                                : ""
                                            }`,
                                          }}
                                        />
                                        <ErrorMessage
                                          name="number"
                                          component={ErrorText}
                                        />
                                      </div>

                                      {/* <div className="form-group">
                                        <label htmlFor="password">
                                          Password
                                        </label>
                                        <Field
                                          type="password"
                                          name="password"
                                          autocomplete="new-password"
                                          className="form-control"
                                          style={{
                                            border: `${
                                              touched.password &&
                                              errors.password
                                                ? '1px solid red'
                                                : ''
                                            }`,
                                          }}
                                        />
                                        <ErrorMessage
                                          name="password"
                                          component={ErrorText}
                                        />
                                      </div> */}
                                      {/* <div className="form-group">
                                        <label htmlFor="confirmPassword">
                                          Confirm Password
                                        </label>
                                        <Field
                                          type="password"
                                          name="confirmPassword"
                                          className="form-control"
                                          style={{
                                            border: `${
                                              touched.confirmPassword &&
                                              errors.confirmPassword
                                                ? '1px solid red'
                                                : ''
                                            }`,
                                          }}
                                        />
                                        <ErrorMessage
                                          name="confirmPassword"
                                          component={ErrorText}
                                        />
                                      </div> */}
                                      <div>
                                        {/* <p
                                          onClick={() => setSignupState('otp')}
                                          style={{
                                            textDecoration: 'underline',
                                            cursor: 'pointer',
                                            marginBottom: '10px',
                                          }}
                                        >
                                          Signup with OTP
                                        </p> */}
                                        <button
                                          className="btn w-100 mb-3"
                                          disabled={isSubmitting}
                                        >
                                          Send Otp
                                        </button>
                                      </div>
                                    </Form>
                                  );
                                }}
                              </Formik>
                            ) : phoneRegisterState === "second-phase" ? (
                              <Formik
                                initialValues={{
                                  otp: "",
                                }}
                                onSubmit={phoneNumberVerifyOtp}
                                validationSchema={Yup.object({
                                  otp: Yup.number().required("OTP Required"),
                                })}
                              >
                                {(fomrik) => {
                                  const { touched, errors, isSubmitting } =
                                    fomrik;

                                  return (
                                    <Form>
                                      <div className="form-group">
                                        <label htmlFor="number">OTP</label>
                                        <Field
                                          type="text"
                                          name="otp"
                                          className="form-control"
                                          placeholder="Enter OTP"
                                          style={{
                                            border: `${
                                              touched.number && errors.number
                                                ? "1px solid red"
                                                : ""
                                            }`,
                                          }}
                                        />
                                        <ErrorMessage
                                          name="otp"
                                          component={ErrorText}
                                        />
                                      </div>

                                      <div>
                                        <button
                                          className="btn w-100 mb-3"
                                          disabled={isSubmitting}
                                        >
                                          Verify OTP
                                        </button>
                                      </div>
                                    </Form>
                                  );
                                }}
                              </Formik>
                            ) : null}
                          </>
                        )}

                        {/* <form method="post">
                          <div className="form-group">
                            <input
                              type="text"
                              required=""
                              name="email"
                              placeholder="Username or Email *"
                            />
                          </div>
                          <div className="form-group">
                            <input
                              required=""
                              type="password"
                              name="password"
                              placeholder="Your password *"
                            />
                          </div>
                          <div className="login_footer form-group">
                            <div className="chek-form">
                              <input
                                type="text"
                                required=""
                                name="email"
                                placeholder="Security code *"
                              />
                            </div>
                            <span className="security-code">
                              <b className="text-new">8</b>
                              <b className="text-hot">6</b>
                              <b className="text-sale">7</b>
                              <b className="text-best">5</b>
                            </span>
                          </div>
                          <div className="login_footer form-group mb-50">
                            <div className="chek-form">
                              <div className="custome-checkbox">
                                <input
                                  className="form-check-input"
                                  type="checkbox"
                                  name="checkbox"
                                  id="exampleCheckbox1"
                                  value=""
                                />
                                <label
                                  className="form-check-label"
                                  htmlFor="exampleCheckbox1"
                                >
                                  <span>Remember me</span>
                                </label>
                              </div>
                            </div>
                            <a className="text-muted" href="#/">
                              Forgot password?
                            </a>
                          </div>
                          <div className="form-group">
                            <button
                              type="submit"
                              className="btn btn-heading btn-block hover-up"
                              name="login"
                            >
                              Log in
                            </button>
                          </div>
                        </form> */}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Layout>
    </>
  );
};

export default Register;
