/* eslint-disable no-unused-vars */
/* eslint-disable react/no-unescaped-entities */
import Layout from "components/layout/Layout";

import React, { useState, useEffect } from "react";
import { Link, useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";
import scrollToTop from "util/scrollToTop";

import EmailRegister from "./EmailRegister";
import OTPRegister from "./OTPRegister";
import Google from "../GeneralAuthProvider/Google";
import Facebook from "../GeneralAuthProvider/Facebook";
// import { scrollToTop } from '../../utils/scroll'

const Register = () => {
  const [signupState, setSignupState] = useState("email");

  const navigate = useNavigate();
  const { authorized } = useSelector((state) => state.auth);

  useEffect(() => {
    scrollToTop();
    if (authorized) {
      navigate("/");
    }
  }, [authorized]);

  return (
    <>
      <Layout parent="Home" sub="Pages" subChild="Login & Register">
        <div className="page-content pt-50 pb-50">
          <div className="container">
            <div className="row">
              <div className="col-xl-8 col-lg-10 col-md-12 m-auto">
                <div className="row">
                  <div className="col-lg-6 pr-30 d-none d-lg-block">
                    <img
                      className="border-radius-15"
                      src={require("assets/imgs/page/login-1.png")}
                      alt=""
                    />
                  </div>
                  <div className="col-lg-6 col-md-8">
                    <div className="login_wrap widget-taber-content background-white">
                      <div className="padding_eight_all bg-white">
                        <div className="heading_s1">
                          <h3 className="mb-5">Register</h3>
                          <p className="mb-30" style={{ fontSize: 14 }}>
                            Have an account?
                            <Link to="/login">Login Here</Link>
                          </p>
                        </div>
                        {signupState === "otp" ? (
                          <OTPRegister setSignupState={setSignupState} />
                        ) : signupState === "email" ? (
                          <EmailRegister setSignupState={setSignupState} />
                        ) : null}
                        <Google state="Register" />
                        <Facebook state="Register" />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Layout>
    </>
  );
};

export default Register;
