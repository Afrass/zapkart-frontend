/* eslint-disable react/prop-types */
import ErrorText from 'components/common/ErrorText'
import {
  createUserWithEmailAndPassword,
  // sendSignInLinkToEmail,
} from 'firebase/auth'
import { authentication } from 'firebaseconfig'
import { ErrorMessage, Field, Form, Formik } from 'formik'
import React from 'react'
import * as Yup from 'yup'
import { toast } from 'react-toastify'
import { useDispatch } from 'react-redux'
// eslint-disable-next-line no-unused-vars
import { setToken, setUser } from 'redux/slices/authSlice'
import { useNavigate } from 'react-router-dom'

const EmailRegister = ({ setSignupState }) => {
  const dispatch = useDispatch()
  const navigate = useNavigate()

  const validationSchema = Yup.object({
    email: Yup.string().required('Email Required'),
    password: Yup.string().required('* Password Required'),
  })

  //Email Reg
  const onSubmitHandlerEmailReg = async (
    { email, password },
    { setSubmitting }
  ) => {
    // const config = {
    //   // https://zapkart-firebase.netlify.app
    //   url: 'http://localhost:3000/register/complete',
    //   handleCodeInApp: true,
    // }

    // // Make sure enable google, and email service in firebase console website
    // await sendSignInLinkToEmail(authentication, email, config)
    // toast.success(
    //   `Email is sent to ${email}. Click the link to complete your registration.`
    // )
    // setSubmitting(false)
    // // Save User Email in localStorage
    // window.localStorage.setItem('emailForRegistration', email)
    // window.localStorage.setItem('passwordForRegistration', password)

    createUserWithEmailAndPassword(authentication, email?.trim(), password)
      .then(async (userCredential) => {
        // Signed in
        const user = userCredential.user

        const idTokenResult = await user.getIdTokenResult(true)

        toast.success('Register Successful')
        dispatch(setToken(idTokenResult.token))
        // dispatch(
        //   setUser({
        //     firstName: user.displayName,
        //     displayImage: user.photoURL,
        //     lastName: null,
        //     email: user.email,
        //     phone: user.phoneNumber,
        //     emailVerified: user.emailVerified,
        //     phoneVerified: user.phoneNumber ? true : false,
        //   })
        // )
        navigate('/user-authdetails-update')
      })
      .catch((error) => {
        toast.error(error.message)
        // ..
      })
    setSubmitting(false)
  }

  return (
    <Formik
      initialValues={{ email: '', password: '' }}
      onSubmit={onSubmitHandlerEmailReg}
      validationSchema={validationSchema}
    >
      {(fomrik) => {
        const { touched, errors, isSubmitting } = fomrik

        return (
          <Form>
            {/* <h2>Register With Email</h2> */}

            <div className="form-group">
              <label htmlFor="otp">Email</label>
              <Field
                type="text"
                name="email"
                className="form-control"
                // autocomplete="new-password"
                style={{
                  border: `${
                    touched.email && errors.email ? '1px solid red' : ''
                  }`,
                }}
              />
              <ErrorMessage name="email" component={ErrorText} />
            </div>
            <div className="form-group">
              <label htmlFor="password">Password</label>
              <Field
                autocomplete="new-password"
                type="password"
                name="password"
                className="password-login form-control f2"
                style={{
                  border: `${
                    touched.password && errors.password ? '1px solid red' : ''
                  }`,
                }}
              />
              <ErrorMessage name="password" component={ErrorText} />
            </div>

            <div>
              {/* <p
                onClick={() => setSignupState('otp')}
                className="unselect-text link mb-10"
              >
               
              </p> */}
              <button
                className="btn w-100 mb-3"
                disabled={isSubmitting}
                type="submit"
              >
                Register
              </button>
              <button
                className="btn w-100 mb-3"
                disabled={isSubmitting}
                type="button"
                onClick={() => setSignupState('otp')}
                style={{ background: '#fdc040' }}
              >
                Signup with OTP
              </button>
            </div>
          </Form>
        )
      }}
    </Formik>
  )
}

export default EmailRegister
