// This Component is not used

import Layout from "components/layout/Layout";

import React, { useEffect } from "react";
import { toast } from "react-toastify";
import { useNavigate } from "react-router-dom";
import { signInWithEmailLink, updatePassword } from "firebase/auth";
import { authentication } from "firebaseconfig";
import getFBError from "util/firebaseErrors";
// import { scrollToTop } from '../../utils/scroll'

const Register = () => {
  const navigate = useNavigate();

  // useEffect(() => {
  //   scrollToTop()
  //   if (authorized) {
  //     navigate('/')
  //   } else {
  //     const email = window.localStorage.getItem('emailForRegistration')
  //     const password = window.localStorage.getItem('passwordForRegistration')
  //     onSubmitHandler(email, password)
  //   }
  // }, [authorized, navigate])

  //Email Reg
  const onSubmitHandler = async () => {
    try {
      const result = await signInWithEmailLink(
        authentication,
        window.localStorage.getItem("emailForRegistration"),
        window.location.href
      );
      if (result.user.emailVerified) {
        //   Get user id token
        let user = authentication.currentUser;

        await updatePassword(
          user,
          window.localStorage.getItem("passwordForRegistration")
        );
        // Remove user Email from localstorage
        window.localStorage.removeItem("emailForRegistration");
        window.localStorage.removeItem("passwordForRegistration");
        // Redux Store

        // Redirect
        if (user.emailVerified) {
          navigate("/user-authdetails-update");
        } else {
          toast.error("Not Verified");
          // navigate('/')
        }

        // navigate('/')
      }
    } catch (err) {
      toast.error(getFBError(err.code));
    }
  };

  useEffect(() => {
    onSubmitHandler();
  }, []);

  return (
    <>
      <Layout parent="Home" sub="Pages" subChild="Login & Register">
        <h3>Verifying...</h3>
      </Layout>
    </>
  );
};

export default Register;
