/* eslint-disable react/no-unescaped-entities */
import Layout from "components/layout/Layout";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { Link, useNavigate } from "react-router-dom";
import scrollToTop from "util/scrollToTop";
import Facebook from "../GeneralAuthProvider/Facebook";
import Google from "../GeneralAuthProvider/Google";
import EmailLogin from "./EmailLogin";
import OTPLogin from "./OTPLogin";

const Login = () => {
  const [loginState, setLoginState] = useState("email");

  const navigate = useNavigate();
  const { authorized } = useSelector((state) => state.auth);

  useEffect(() => {
    scrollToTop();
    if (authorized) {
      navigate("/");
    }
  }, [authorized]);

  return (
    <Layout parent="Home" sub="Pages" subChild="Login & Register">
      <div className="page-content pt-50 pb-50">
        <div className="container">
          <div className="row">
            <div className="col-xl-8 col-lg-10 col-md-12 m-auto">
              <div className="row">
                <div className="col-lg-6 pr-30 d-none d-lg-block">
                  <img
                    className="border-radius-15"
                    src={require("assets/imgs/page/login-1.png")}
                    alt=""
                  />
                </div>
                <div className="col-lg-6 col-md-8">
                  <div className="login_wrap widget-taber-content background-white">
                    <div className="padding_eight_all bg-white">
                      <div className="heading_s1">
                        <h3 className="mb-5">Login</h3>
                        <p className="mb-30" style={{ fontSize: 14 }}>
                          Don't have an account?
                          <Link to="/register">Create here</Link>
                        </p>
                      </div>
                      {loginState === "otp" ? (
                        <OTPLogin setLoginState={setLoginState} />
                      ) : loginState === "email" ? (
                        <EmailLogin setLoginState={setLoginState} />
                      ) : null}
                      <Google state="Login" />
                      <Facebook state="Login" />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div id="recaptcha-container"></div>
    </Layout>
  );
};

export default Login;
