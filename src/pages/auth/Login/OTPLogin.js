/* eslint-disable react/prop-types */
import ErrorText from 'components/common/ErrorText'
import { RecaptchaVerifier, signInWithPhoneNumber } from 'firebase/auth'
import { authentication } from 'firebaseconfig'
import { ErrorMessage, Field, Form, Formik } from 'formik'
import React from 'react'
import { useState } from 'react'
import { useDispatch } from 'react-redux'
import { useNavigate } from 'react-router-dom'
// import { useNavigate } from 'react-router-dom'
import { toast } from 'react-toastify'
import { setToken } from 'redux/slices/authSlice'
import getFBError from 'util/firebaseErrors'
import * as Yup from 'yup'

const OTPLogin = ({ setLoginState }) => {
  const [phoneLoginState, setPhoneLoginState] = useState('first-phase')
  const [phoneNumber, setPhoneNumber] = useState(null)
  const navigate = useNavigate()

  const dispatch = useDispatch()

  // const generateRecaptcha = () => {
  //   window.recaptchaVerifier = new RecaptchaVerifier(
  //     'recaptcha-container',
  //     {
  //       size: 'invisible',
  //       // eslint-disable-next-line no-unused-vars
  //       callback: (response) => {
  //         // reCAPTCHA solved, allow signInWithPhoneNumber.
  //         // onSignInSubmit();
  //       },
  //     },
  //     authentication
  //   )
  // }

  const phoneNumberSentOtp = ({ number }, { setSubmitting }) => {
    // generateRecaptcha()
    // let appVerifier = window.recaptchaVerifier
    setSubmitting(true)
    if (!window.recaptchaVerifier) {
      window.recaptchaVerifier = new RecaptchaVerifier(
        'recaptcha-container',
        {
          size: 'invisible',
        },
        authentication
      )
    }
    window.recaptchaVerifier.render()
    signInWithPhoneNumber(
      authentication,
      `+91${number}`,
      window.recaptchaVerifier
    )
      .then((confirmationResult) => {
        // setLoading(false)

        window.confirmationResult = confirmationResult
        setPhoneNumber(number)
        toast.success('OTP Sent')

        setPhoneLoginState('second-phase')
        setSubmitting && setSubmitting(false)
        document.querySelector('.otp-register').value = ''
      })
      .catch((err) => {
        toast.error(getFBError(err.code))
      })
    setSubmitting && setSubmitting(false)
  }

  // Verify otp
  const phoneNumberVerifyOtp = ({ otp }, { setSubmitting }) => {
    if (otp.length === 6) {
      const confirmationResult = window.confirmationResult
      confirmationResult
        .confirm(otp)
        .then(async (result) => {
          // User signed in successfully.
          const { user } = result
          const idTokenResult = await user.getIdTokenResult(true)

          toast.success('Verified Successful')
          dispatch(setToken(idTokenResult.token))

          setSubmitting(false)

          //   Navigate After Register otp to details
          navigate('/user-authdetails-update')

          // ...
        })
        .catch((error) => {
          toast.error(error.message)
          // User couldn't sign in (bad verification code?)
        })
    } else {
      toast.error('OTP Must be 6 characters long')
    }
  }

  return (
    <div>
      {phoneLoginState === 'first-phase' ? (
        <Formik
          initialValues={{
            number: '',
          }}
          onSubmit={phoneNumberSentOtp}
          validationSchema={Yup.object({
            number: Yup.string().required('Phone Number Required'),
          })}
        >
          {(fomrik) => {
            const { touched, errors, isSubmitting } = fomrik

            return (
              <Form>
                <div className="form-group">
                  <label htmlFor="number">Phone Number</label>
                  <Field
                    type="text"
                    name="number"
                    className="form-control"
                    placeholder="Eg: 9876543210"
                    style={{
                      border: `${
                        touched.number && errors.number ? '1px solid red' : ''
                      }`,
                    }}
                  />
                  <ErrorMessage name="number" component={ErrorText} />
                </div>

                <div>
                  {/* <p
                    onClick={() => setLoginState('email')}
                    className="unselect-text link mb-10"
                  >
                    Login with Email
                  </p> */}
                  <button
                    type="submit"
                    className="btn w-100 mb-3"
                    disabled={isSubmitting}
                  >
                    Send OTP
                  </button>
                  <button
                    type="button"
                    onClick={() => setLoginState('email')}
                    className="btn w-100 mb-3"
                    disabled={isSubmitting}
                    style={{ background: '#fdc040' }}
                  >
                    Login with Email
                  </button>
                </div>
              </Form>
            )
          }}
        </Formik>
      ) : phoneLoginState === 'second-phase' ? (
        <Formik
          initialValues={{
            otp: '',
          }}
          onSubmit={phoneNumberVerifyOtp}
          validationSchema={Yup.object({
            otp: Yup.number().required('OTP Required'),
          })}
        >
          {(fomrik) => {
            const { touched, errors, isSubmitting } = fomrik

            return (
              <Form>
                <div className="form-group">
                  <label htmlFor="number">OTP</label>
                  <Field
                    type="text"
                    name="otp"
                    className="form-control otp-register"
                    placeholder="Enter OTP"
                    style={{
                      border: `${
                        touched.number && errors.number ? '1px solid red' : ''
                      }`,
                    }}
                  />
                  <ErrorMessage name="otp" component={ErrorText} />
                </div>
                <p
                  style={{
                    marginBottom: '10px',
                    textDecoration: 'underline',
                    cursor: 'pointer',
                  }}
                  onClick={() =>
                    phoneNumberSentOtp(
                      { number: phoneNumber },
                      { setSubmitting: null }
                    )
                  }
                >
                  Resent OTP?
                </p>

                <div>
                  <button className="btn w-100 mb-3" disabled={isSubmitting}>
                    Verify OTP
                  </button>
                </div>
              </Form>
            )
          }}
        </Formik>
      ) : null}
      <div id="recaptcha-container"></div>
    </div>
  )
}

export default OTPLogin
