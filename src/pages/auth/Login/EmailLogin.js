/* eslint-disable react/prop-types */
import React from 'react'
import { signInWithEmailAndPassword } from 'firebase/auth'
import { ErrorMessage, Field, Form, Formik } from 'formik'
import * as Yup from 'yup'
import { authentication } from 'firebaseconfig'
import { toast } from 'react-toastify'
import { useDispatch } from 'react-redux'
import { setToken } from 'redux/slices/authSlice'
import { useNavigate } from 'react-router-dom'
import getFBError from 'util/firebaseErrors'
import ErrorText from 'components/common/ErrorText'

const EmailLogin = ({ setLoginState }) => {
  const dispatch = useDispatch()
  const navigate = useNavigate()

  const validationSchemaEmailLogin = Yup.object({
    email: Yup.string().email('*Invalid Email').required('* Email Required'),
    password: Yup.string().required('* Password Required'),
  })

  const onSubmitHandlerEmailLogin = async (
    { password, email },
    { setSubmitting }
  ) => {
    // setLoading(true)
    try {
      const result = await signInWithEmailAndPassword(
        authentication,
        email?.trim(),
        password
      )
      // setLoading(false)

      const { user } = result
      const idTokenResult = await user.getIdTokenResult(true)

      toast.success('Login Successful')
      dispatch(setToken(idTokenResult.token))
      navigate('/user-authdetails-update')
    } catch (err) {
      // setLoading(false)
      // alert(err.code)
      toast.error(getFBError(err.code))
      // toast.error(err.message)
    }

    setSubmitting(false)
  }

  return (
    <Formik
      initialValues={{ email: '', password: '' }}
      // onSubmit={onSubmit}
      onSubmit={onSubmitHandlerEmailLogin}
      validationSchema={validationSchemaEmailLogin}
    >
      {(fomrik) => {
        const { touched, errors, isSubmitting } = fomrik

        return (
          <Form autoComplete="off">
            <div className="form-group">
              <label htmlFor="email">Email</label>
              <Field
                autocomplete="new-password"
                type="email"
                name="email"
                className="form-control f-1"
                placeholder="Your Email"
                style={{
                  border: `${
                    touched.email && errors.email ? '1px solid red' : ''
                  }`,
                }}
              />

              <ErrorMessage name="email" component={ErrorText} />
            </div>

            <div className="form-group">
              <label htmlFor="password">Password</label>
              <Field
                autocomplete="new-password"
                type="password"
                name="password"
                className="password-login form-control f2"
                style={{
                  border: `${
                    touched.password && errors.password ? '1px solid red' : ''
                  }`,
                }}
              />
              <ErrorMessage name="password" component={ErrorText} />
            </div>
            <p
              onClick={() => navigate('/forgot-password')}
              // style={{
              //   textDecoration: 'underline',
              //   cursor: 'pointer',
              //   marginBottom: '10px',
              // }}
              className="unselect-text link mb-10"
            >
              Forgot Password?
            </p>
            <div>
              {/* <p
                role="button"
                onClick={() => setLoginState('otp')}
       
                className="unselect-text link mb-10"
              >
                Login with OTP
              </p> */}

              <button
                className="btn w-100 mb-3"
                type="submit"
                disabled={isSubmitting}
              >
                Login
              </button>
              <button
                className="btn w-100 mb-3"
                onClick={() => setLoginState('otp')}
                style={{ background: '#fdc040' }}
                type="button"
                disabled={isSubmitting}
              >
                Login with OTP
              </button>
            </div>
          </Form>
        )
      }}
    </Formik>
  )
}

export default EmailLogin
