/* eslint-disable react/prop-types */
import { FacebookAuthProvider, signInWithPopup } from 'firebase/auth'
import { authentication } from 'firebaseconfig'
import React from 'react'
import { useDispatch } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import { toast } from 'react-toastify'
import { setToken } from 'redux/slices/authSlice'
import getFBError from 'util/firebaseErrors'
import facebookIcon from 'assets/imgs/theme/icons/facebook-logo.png'

const Facebook = ({ state }) => {
  const dispatch = useDispatch()
  const navigate = useNavigate()

  const facebookLogin = async () => {
    const provider = new FacebookAuthProvider()

    try {
      const result = await signInWithPopup(authentication, provider)
      const { user } = result
      console.log(result, 'show-me-data')
      const idTokenResult = await user.getIdTokenResult(true)

      toast.success('Login Successful')
      dispatch(setToken(idTokenResult.token))

      navigate('/user-authdetails-update')
    } catch (err) {
      console.log(err, 'fb-error')
      toast.error(getFBError(err.code))
    }

    // signInWithPopup(auth, provider)
    //   .then((res) => {
    //     console.log(res)
    //   })
    //   .catch((err) => {
    //     toast.error(err.message)
    //   })
  }

  return (
    <button
      type="button"
      style={{ background: '#1877F2' }}
      className="btn w-100 mb-3 d-flex align-items-center justify-content-center"
      onClick={facebookLogin}
    >
      <img src={facebookIcon} alt="facebook-icon" height="25" width="25" />{' '}
      &nbsp; {state} with Facebook
    </button>
  )
}

export default Facebook
