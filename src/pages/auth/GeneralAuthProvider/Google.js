/* eslint-disable react/prop-types */
import { GoogleAuthProvider, signInWithPopup } from 'firebase/auth'
import { authentication } from 'firebaseconfig'
import React from 'react'
import { useDispatch } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import { toast } from 'react-toastify'
import { setToken } from 'redux/slices/authSlice'
import getFBError from 'util/firebaseErrors'
import googleIcon from 'assets/imgs/theme/icons/logo-google.png'

const Google = ({ state }) => {
  const googleAuthProvider = new GoogleAuthProvider()
  const dispatch = useDispatch()
  const navigate = useNavigate()

  const googleLogin = async () => {
    try {
      googleAuthProvider.addScope('email')
      const result = await signInWithPopup(authentication, googleAuthProvider)
      googleAuthProvider.addScope('email')
      const { user } = result
      const idTokenResult = await user.getIdTokenResult(true)
      toast.success('Login Successful')
      dispatch(setToken(idTokenResult.token))

      navigate('/user-authdetails-update')
    } catch (err) {
      toast.error(getFBError(err.code))
    }
  }

  return (
    <button
      type="button"
      className="btn w-100 mb-3 d-flex align-items-center justify-content-center"
      style={{ background: '#4285f4' }}
      onClick={googleLogin}
    >
      <img src={googleIcon} alt="google-icon" height="25" width="25" /> &nbsp;
      {state} with Google
    </button>
  )
}

export default Google
