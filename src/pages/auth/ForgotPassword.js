/* eslint-disable react/no-unescaped-entities */
import React, { useEffect } from "react";
import Layout from "components/layout/Layout";
import { useNavigate } from "react-router-dom";

import { ErrorMessage, Field, Form, Formik } from "formik";
import ErrorText from "components/common/ErrorText";
import * as Yup from "yup";
import { useSelector } from "react-redux";

import scrollToTop from "util/scrollToTop";

import { authentication } from "firebaseconfig";
import { sendPasswordResetEmail } from "firebase/auth";
import { toast } from "react-toastify";
import getFBError from "util/firebaseErrors";
// import { Button } from 'react-bootstrap'
// import { scrollToTop } from '../../utils/scroll'

const Login = () => {
  const navigate = useNavigate();
  const { authorized } = useSelector((state) => state.auth);

  const initialValue = {
    email: "",
  };

  const validationSchema = Yup.object({
    email: Yup.string().required("Email Required"),
  });

  const onSubmitHandler = async ({ email }, { setSubmitting }) => {
    // setLoading(true)

    const config = {
      url: "https://ecommercelivefrontend.riolabz.com/login",
      handleCodeInApp: true,
    };
    try {
      await sendPasswordResetEmail(authentication, email, config);
      //   setLoading(false)
      toast.success("Check Your Email For Password Reset Link");
    } catch (err) {
      //   setLoading(false)
      toast.error(getFBError(err.code));
    }
    setSubmitting(false);
  };

  useEffect(() => {
    scrollToTop();
    if (authorized) {
      navigate("/");
    }
  }, [authorized, navigate]);

  return (
    <>
      <Layout parent="Home" sub="Pages" subChild="Login & Register">
        <div className="page-content pt-50 pb-50">
          <div className="container">
            <div className="row">
              <div className="col-xl-8 col-lg-10 col-md-12 m-auto">
                <div className="row">
                  <div className="col-lg-6 pr-30 d-none d-lg-block">
                    <img
                      className="border-radius-15"
                      src={require("assets/imgs/page/login-1.png")}
                      alt=""
                    />
                  </div>
                  <div className="col-lg-6 col-md-8">
                    <div className="login_wrap widget-taber-content background-white">
                      <div className="padding_eight_all bg-white">
                        <div className="heading_s1">
                          <h3 className="mb-5">Forgot Password</h3>
                        </div>

                        <Formik
                          initialValues={initialValue}
                          onSubmit={onSubmitHandler}
                          validationSchema={validationSchema}
                        >
                          {(fomrik) => {
                            const { touched, errors } = fomrik;

                            return (
                              <Form>
                                <div className="form-group">
                                  <label htmlFor="email">Email</label>
                                  <Field
                                    type="email"
                                    name="email"
                                    className="form-control"
                                    placeholder="Enter Your Email"
                                    style={{
                                      border: `${
                                        touched.number && errors.number
                                          ? "1px solid red"
                                          : ""
                                      }`,
                                    }}
                                  />
                                  <ErrorMessage
                                    name="email"
                                    component={ErrorText}
                                  />
                                </div>
                                <div>
                                  <button className="btn w-100 mb-3">
                                    Sent Verification
                                  </button>
                                </div>
                              </Form>
                            );
                          }}
                        </Formik>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div id="recaptcha-container"></div>
      </Layout>
    </>
  );
};

export default Login;
