/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import RadioButton from 'components/common/RadioButton'
import React, { useState } from 'react'
import { useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import { toast } from 'react-toastify'
import orderService from 'service/order'

const PaymentSection = ({
  cartTotal,
  availablePaymentMethods,
  paymentId,
  eventType,
  // orderId,
  // amount,
  razorpayItems,
  setPaymentLoading,
  paymentLoading,
}) => {
  const navigate = useNavigate()
  const [selectedPaymentOption, setSelectedPaymentOption] = useState('Online')
  // const paymentStatus = [
  //   { value: 'Online', name: 'payment', defaultChecked: true },
  //   { value: 'Cod', name: 'payment' },
  // ]

  // Razor pay script load
  const loadScript = (src) => {
    return new Promise((resolve) => {
      const script = document.createElement('script')
      script.src = src
      script.onload = () => {
        resolve(true)
      }

      script.onerror = () => {
        resolve(false)
      }
      document.body.appendChild(script)
    })
  }

  const displayRazorPay = async (amount, razorpayOrderId) => {
    const res = await loadScript('https://checkout.razorpay.com/v1/checkout.js')

    if (!res) {
      toast.error('Razorpay SDK Failed to load')
      return
    }

    const __DEV__ = document.domain === 'localhost'

    // const data = await fetch('http://localhost:8000/razorpay', {
    //   method: 'POST',
    // }).then((t) => t.json())

    // console.log(data, 'showw-razor')

    const options = {
      key: __DEV__ ? 'rzp_test_gXg0smnbVr4ukX' : 'rzp_test_gXg0smnbVr4ukX', // Enter the Key ID generated from the Dashboard
      currency: 'INR',
      amount: amount,
      order_id: razorpayOrderId,
      name: 'zapkart',
      // receipt: paymentId,
      description: 'Thank you for Purchase',
      image: 'https://example.com/your_logo',
      // order_id: 'order_9A33XWu170gUtm', //This is a sample Order ID. Pass the `id` obtained in the response of Step 1
      handler: function (response) {
        // if (response.razorpay_payment_id) {
        //   toast.success('Your order is placed successfully')
        //   // navigate('/')
        // }
        // console.log(response.razorpay_payment_id)
        // console.log(response.razorpay_order_id)
        // console.log(response.razorpay_signature)
      },
      // prefill: {
      //   name: 'Gaurav Kumar',
      //   email: 'gaurav.kumar@example.com',
      //   contact: '9999999999',
      // },
      // notes: {
      //   address: 'Razorpay Corporate Office',
      // },
      // theme: {
      //   color: '#3399cc',
      // },
    }

    var paymentObject = new window.Razorpay(options)

    paymentObject.open()
  }

  const onPayHandler = async () => {
    setPaymentLoading(true)
    const paymentRes = await orderService.orderPaymentMethod({
      paymentId,
      paymentType: selectedPaymentOption,
    })

    if (paymentRes) {
      toast.success('Your Payment Method is Proccessing Please wait...')
      // displayRazorPay()
      // if (selectedPaymentOption === 'Online' && razorpayItems?.amount) {
      //   displayRazorPay()
      // }
    }
    // if (selectedPaymentOption === 'Online') {
    //   displayRazorPay()
    // } else {
    //   toast.success('Your order is placed successfully')
    //   navigate('/')
    // }
  }

  useEffect(() => {
    if (razorpayItems?.amount) {
      displayRazorPay(razorpayItems?.amount, razorpayItems?.razorPayOrderId)
    }
  }, [razorpayItems])

  return (
    <div>
      <div className="px-2">
        <h5>Payment</h5>
        {availablePaymentMethods?.length > 0 && (
          <RadioButton
            filters={availablePaymentMethods}
            handleRadioButton={(e) => {
              setSelectedPaymentOption(e.target.value)
            }}
          />
        )}

        <button
          disabled={paymentLoading}
          className="btn btn-sm mt-4"
          onClick={onPayHandler}
        >
          Pay Now
        </button>
      </div>
    </div>
  )
}

export default PaymentSection
