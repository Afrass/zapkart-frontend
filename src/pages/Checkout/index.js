/* eslint-disable no-unused-vars */
import React, { useEffect, useState } from 'react'

import Layout from 'components/layout/Layout'
// import { Link } from 'react-router-dom'
// import RadioButton from 'components/common/RadioButton'
import { getCartSlice } from 'redux/slices/cartSlice'
import { useDispatch, useSelector } from 'react-redux'
// import CartTotalSection from 'pages/Cart/CartTotalSection'
import AddressSection from './AddressSection'
import { Button, notification, Steps, Upload } from 'antd'
import useUpload from 'hooks/useUpload'
import authService from 'service/auth'
import { multipleImageUpload } from 'util/imageUploader'
import { toast } from 'react-toastify'
import PrescriptionSection from './PrescriptionSection'
import CheckoutSection from './CheckoutSection'
import scrollToTop from 'util/scrollToTop'
import { Navigate, useNavigate } from 'react-router-dom'
import orderService from 'service/order'
import PaymentSection from './PaymentSection'
import { EventSourcePolyfill } from 'event-source-polyfill'

const Checkout = () => {
  const { authorized, user } = useSelector((state) => state.auth)

  const { Step } = Steps

  const dispatch = useDispatch()
  const [current, setCurrent] = useState(0)
  const { cart, totalCartItems, cartTotal } = useSelector((state) => state.cart)
  const [steps, setSteps] = useState([])
  const [selectedShippingAddressId, setSelectedShippingAddressId] =
    useState(null)
  const [selectedBillingAddressId, setSelectedBillingAddressId] = useState(null)

  // const [prescriptions, setPrescriptions] = useState([])
  const [selectedPrescriptions, setSelectedPrescriptions] = useState([])
  const [availablePaymentMethods, setAvailablePaymentMethods] = useState([])

  // Checkout events
  const [isCheckoutEvenStarted, setIsCheckoutEventStarted] = useState(false)
  const [requestId, setRequestId] = useState(null)
  const [paymentId, setPaymentId] = useState(null)
  const [eventData, setEventData] = useState(null)
  const [eventType, setEventType] = useState(null)
  const [razorpayItems, setRazorpayItems] = useState({})
  const [paymentLoading, setPaymentLoading] = useState(false)
  const [couponCode, setCouponCode] = useState('')

  const navigate = useNavigate()

  const onPlaceOrderHandler = async () => {
    setPaymentLoading(true)
    if (selectedShippingAddressId === null) {
      notification.error({
        message: 'Error',
        description: 'Please select shipping address',
      })
    } else if (selectedBillingAddressId === null) {
      notification.error({
        message: 'Error',
        description: 'Please select billing address',
      })
    } else {
      const sendingData = {
        items: cart?.map((cur) => {
          return { id: cur.product.id, quantity: cur.quantity }
        }),
        fromCart: true,
        shippingAddressId: selectedShippingAddressId,
        billingAddressId: selectedBillingAddressId,
        prescriptions: selectedPrescriptions ? selectedPrescriptions : [],
      }
      if (couponCode) {
        sendingData.couponCode = couponCode
      }

      const createOrder = await orderService.createOrder(sendingData)

      if (createOrder) {
        setIsCheckoutEventStarted(true)
        setRequestId(createOrder.data.requestId)
        // next()
        toast.success('Please Wait Order Is Being Proccessing...')
        // displayRazorPay()
      } else {
        setPaymentLoading(false)
      }

      // displayRazorPay()
    }
  }

  let avilableSteps = [
    {
      title: 'Address',
      content: (
        <AddressSection
          setSelectedShippingAddressId={setSelectedShippingAddressId}
          selectedShippingAddressId={selectedShippingAddressId}
          setSelectedBillingAddressId={setSelectedBillingAddressId}
          selectedBillingAddressId={selectedBillingAddressId}
        />
      ),
    },
    {
      title: 'Prescriptions',
      content: (
        <PrescriptionSection
          setSelectedPrescriptions={setSelectedPrescriptions}
          selectedPrescriptions={selectedPrescriptions}
        />
      ),
    },
    {
      title: 'Checkout',
      content: (
        <CheckoutSection
          cart={cart}
          cartTotal={cartTotal}
          onPlaceOrderHandler={onPlaceOrderHandler}
          paymentLoading={paymentLoading}
          setCouponCode={setCouponCode}
          couponCode={couponCode}
        />
      ),
    },
    {
      title: 'Payments',
      content: (
        <PaymentSection
          cart={cart}
          cartTotal={cartTotal}
          availablePaymentMethods={availablePaymentMethods}
          paymentId={paymentId}
          paymentLoading={paymentLoading}
          setPaymentLoading={setPaymentLoading}
          razorpayItems={razorpayItems}
        />
      ),
    },
  ]

  useEffect(() => {
    dispatch(getCartSlice())
  }, [])

  const next = () => {
    setCurrent(current + 1)
  }

  const prev = () => {
    setCurrent(current - 1)
  }

  useEffect(() => {
    scrollToTop()
  }, [current])

  // Showing hiding or showing prescription steps based on response data
  useEffect(() => {
    if (cart?.length > 0) {
      const isPrescriptionAvailble = cart.find(
        (item) => item.product.prescriptionRequired
      )

      if (isPrescriptionAvailble) {
        setSteps(avilableSteps)
      } else {
        avilableSteps.splice(1, 1)
        setSteps(avilableSteps)
      }
    }
  }, [
    cart,
    selectedShippingAddressId,
    selectedBillingAddressId,
    selectedPrescriptions,
    current,
    paymentId,
    availablePaymentMethods,
    razorpayItems,
    paymentLoading,
    eventType,
    couponCode,
  ])

  // Checkout Events
  useEffect(() => {
    if (isCheckoutEvenStarted && requestId) {
      const baseurl = process.env.REACT_APP_BASE_URL
      if (user) {
        let eventSourcePaymentRequired = new EventSourcePolyfill(
          `${baseurl}/api/v1/events/order/Payment_Required`,
          {
            headers: {
              Authorization: 'Bearer ' + window.localStorage.getItem('token'),
              Connection: 'Keep-Alive',
              'Keep-Alive': { timeout: 6000, max: 1000 },
            },
            heartbeatTimeout: 600 * 1000,
          }
        )

        eventSourcePaymentRequired.onmessage = (e) => {
          console.log(JSON.parse(e.data), 'Payment_Required => SUCCESS')
          setEventData(JSON.parse(e.data))

          const data = JSON.parse(e.data)

          if (requestId === data.requestId) {
            setEventType('Payment_Required')

            setPaymentId(data.paymentId)
            setCurrent(current + 1)

            const formattedAvailablePaymentMethods =
              data.availablePaymentMethods?.map((curMethod, i) => {
                return {
                  label: curMethod === 'Cod' ? 'Cash On Delivery' : curMethod,
                  value: curMethod,
                  name: 'payment',
                  defaultChecked: curMethod === 'Online',
                }
              })

            setAvailablePaymentMethods(formattedAvailablePaymentMethods)
            setPaymentLoading(false)
          } else {
            toast.error('Request Id is not valid')
            navigate('/')
            setPaymentLoading(false)
          }

          // setdata(JSON.parse(e.data))
          // seteventype('payment')
          eventSourcePaymentRequired.close()
        }
        eventSourcePaymentRequired.onerror = (e) => {
          eventSourcePaymentRequired.close()
          console.log('PAYMENT_REQUIRED', e)
        }

        // ORDER_FALIED
        let eventSourceOrderFailed = new EventSourcePolyfill(
          `${baseurl}/api/v1/events/order/Order_Failed`,
          {
            headers: {
              Authorization: 'Bearer ' + window.localStorage.getItem('token'),
              Connection: 'Keep-Alive',
              'Keep-Alive': { timeout: 6000, max: 1000 },
            },
            heartbeatTimeout: 600 * 1000,
          }
        )

        eventSourceOrderFailed.onmessage = (e) => {
          console.log(JSON.parse(e.data), 'Order_Failed => SUCCESS')

          setEventData(JSON.parse(e.data))
          setEventType('Order_Failed')

          toast.error('Order Failed')
          setPaymentLoading(false)

          // setdata(JSON.parse(e.data))
          // seteventype('payment')
          eventSourceOrderFailed.close()
        }
        eventSourceOrderFailed.onerror = (e) => {
          eventSourceOrderFailed.close()
          console.log('ORDER_FAILED', e)
          setPaymentLoading(false)
        }

        // ORDER_PROCESSED
        // let eventSourceOrderProcessed = new EventSourcePolyfill(
        //   `${baseurl}/api/v1/events/order/Order_Processed`,
        //   {
        //     headers: {
        //       Authorization: 'Bearer ' + window.localStorage.getItem('token'),
        //       Connection: 'Keep-Alive',
        //       'Keep-Alive': { timeout: 6000, max: 1000 },
        //     },
        //     heartbeatTimeout: 600 * 1000,
        //   }
        // )

        // eventSourceOrderProcessed.onmessage = (e) => {
        //   console.log(JSON.parse(e.data), 'Order_Proccessed => SUCCESS')
        //   const data = JSON.parse(e.data)
        //   setEventData(data)
        //   setEventType('Order_Proccessed')

        //   // setdata(JSON.parse(e.data))
        //   // seteventype('payment')
        //   eventSourceOrderProcessed.close()
        // }
        // eventSourceOrderProcessed.onerror = (e) => {
        //   eventSourceOrderProcessed.close()
        //   console.log('ORDER_Proccessed => FAILURE', e)
        // }

        // ORDER_CONFIRMED
        let eventSourceOrderConfirmed = new EventSourcePolyfill(
          `${baseurl}/api/v1/events/order/Order_Confirmed`,
          {
            headers: {
              Authorization: 'Bearer ' + window.localStorage.getItem('token'),
              Connection: 'Keep-Alive',
              'Keep-Alive': { timeout: 6000, max: 1000 },
            },
            heartbeatTimeout: 600 * 1000,
          }
        )

        eventSourceOrderConfirmed.onmessage = (e) => {
          console.log(JSON.parse(e.data), 'Order_Confirmed => SUCCESS')
          const data = JSON.parse(e.data)
          setEventType('Order_Confirmed')
          setPaymentLoading(false)
          toast.success('Order Placed Successfully')
          navigate(`/order-success/${data.orderId}`)

          setTimeout(() => {
            navigate('/')
          }, 5000)
          // setdata(JSON.parse(e.data))
          // seteventype('payment')
          eventSourceOrderConfirmed.close()
        }
        eventSourceOrderConfirmed.onerror = (e) => {
          eventSourceOrderConfirmed.close()
          console.log('ORDER_Confirmed => FAILURE', e)
          setPaymentLoading(false)
          // setTimeout(() => {
          //   navigate('/')
          // }, 5000)
        }

        // PAYMENT_SUCCESS
        // let eventSourcePaymentSuccess = new EventSourcePolyfill(
        //   `${baseurl}/api/v1/events/order/Payment_Success`,
        //   {
        //     headers: {
        //       Authorization: 'Bearer ' + window.localStorage.getItem('token'),
        //       Connection: 'Keep-Alive',
        //       'Keep-Alive': { timeout: 6000, max: 1000 },
        //     },
        //     heartbeatTimeout: 600 * 1000,
        //   }
        // )

        // eventSourcePaymentSuccess.onmessage = (e) => {
        //   console.log(JSON.parse(e.data), 'PAYMENT_SUCCESS => SUCCESS')
        //   setEventData(JSON.parse(e.data))
        //   setEventType('PAYMENT_SUCCESS')

        //   // setdata(JSON.parse(e.data))
        //   // seteventype('payment')
        //   eventSourcePaymentSuccess.close()
        // }
        // eventSourcePaymentSuccess.onerror = (e) => {
        //   eventSourcePaymentSuccess.close()
        //   console.log('PAYMENT_SUCCESS => FAILURE', e)
        // }

        // PAYMENT_FAILED
        // let eventSourcePaymentFailed = new EventSourcePolyfill(
        //   `${baseurl}/api/v1/events/order/Payment_Failed`,
        //   {
        //     headers: {
        //       Authorization: 'Bearer ' + window.localStorage.getItem('token'),
        //       Connection: 'Keep-Alive',
        //       'Keep-Alive': { timeout: 6000, max: 1000 },
        //     },
        //     heartbeatTimeout: 600 * 1000,
        //   }
        // )

        // eventSourcePaymentFailed.onmessage = (e) => {
        //   console.log(JSON.parse(e.data), 'PAYMENT_FAILED => SUCCESS')

        //   setEventData(JSON.parse(e.data))
        //   setEventType('PAYMENT_FAILED')
        //   // setdata(JSON.parse(e.data))
        //   // seteventype('payment')
        //   eventSourcePaymentFailed.close()
        // }
        // eventSourcePaymentFailed.onerror = (e) => {
        //   eventSourcePaymentFailed.close()
        //   console.log('PAYMENT_FAILED => FAILURE', e)
        // }

        // Razorpay_Payment_Created
        let eventSourceRazorpayPaymentCreated = new EventSourcePolyfill(
          `${baseurl}/api/v1/events/order/Razorpay_Payment_Created`,
          {
            headers: {
              Authorization: 'Bearer ' + window.localStorage.getItem('token'),
              Connection: 'Keep-Alive',
              'Keep-Alive': { timeout: 6000, max: 1000 },
            },
            heartbeatTimeout: 600 * 1000,
          }
        )

        eventSourceRazorpayPaymentCreated.onmessage = (e) => {
          console.log(JSON.parse(e.data), 'Razorpay_Payment_Created => SUCCESS')

          setEventData(JSON.parse(e.data))

          setRazorpayItems(JSON.parse(e.data))

          setEventType('Razorpay_Payment_Created')
          // setdata(JSON.parse(e.data))
          // seteventype('payment')
          eventSourceRazorpayPaymentCreated.close()
        }
        eventSourceRazorpayPaymentCreated.onerror = (e) => {
          eventSourceRazorpayPaymentCreated.close()
          setPaymentLoading(false)

          console.log('Razorpay_Payment_Created => FAILURE', e)
        }
      }
    }

    // return () => {
    //   eventSourcePaymentRequired.close()
    //   eventSourceOrderConfirmed.close()
    //   eventSourceRazorpayPaymentCreated.close()
    // }
  }, [isCheckoutEvenStarted, requestId])

  return (
    <Layout parent="Home" sub="Checkout">
      <section className="mt-50 mb-50">
        <div className="container">
          <h1 className="heading-2 mb-10">Checkout</h1>
          <h6 className="text-body">
            There are <strong className="text-brand">{totalCartItems}</strong>{' '}
            products in your cart
          </h6>
          <div className="row">
            <div className="col-md-8 offset-md-2">
              <Steps current={current} className="mt-30 mb-30">
                {steps.map((item) => (
                  <Step key={item.title} title={item.title} />
                ))}
              </Steps>
              <div className="steps-content">{steps[current]?.content}</div>

              <div className="d-flex justify-content-between mt-60">
                {current > 0 &&
                  current !== steps.length - 1 &&
                  current !== steps.length - 2 && (
                    <button
                      style={{ margin: '0 8px' }}
                      className="btn btn-small"
                      onClick={() => prev()}
                    >
                      Previous
                    </button>
                  )}

                {current < steps.length - 2 && (
                  <button
                    disabled={
                      (current === 1 && selectedPrescriptions?.length === 0) ||
                      (current === 0 &&
                        !selectedShippingAddressId &&
                        !selectedBillingAddressId)
                    }
                    className="btn btn-small"
                    onClick={() => next()}
                  >
                    Next
                  </button>

                  // <Button type="primary" onClick={() => next()}>

                  // </Button>
                )}
                {/* {current === steps.length - 1 && (
                  <button
                    className="btn btn-small"
                    onClick={() => toast.success('process success')}
                  >
                    Done
                  </button>
                )} */}
              </div>
            </div>
          </div>
        </div>
      </section>
    </Layout>
  )
}

// ;<Layout parent="Home" sub="Shop" subChild="Cart">
//   <section className="mt-50 mb-50">
//     <div className="container">
//       <h1 className="heading-2 mb-10">Checkout</h1>
//       <h6 className="text-body">
//         There are <span className="text-brand">{totalCartItems}</span> products
//         in your cart
//       </h6>
//       <div className="row">
//         <div className="col-lg-6 mb-40">
//           <div className="row">
//             <div className="col-md-12 px-4 mt-50">
//               <h4 className="text-brand mb-4">Please Select Address</h4>
//               <AddressSection
//                 setSelectedShippingAddressId={setSelectedShippingAddressId}
//                 selectedShippingAddressId={selectedShippingAddressId}
//               />
//             </div>

//             <div className="col-md-12">
//               <div className="px-4 mt-50">
//                 <h4 className="text-brand mb-3">Prescription</h4>
//                 <div className="d-flex align-items-center mb-10">
//                   <Upload listType="picture-card" {...propsPrescriptions}>
//                     <img
//                       src={prescriptionIcon}
//                       alt="prescription-add"
//                       height={80}
//                       width={80}
//                     />
//                   </Upload>
//                 </div>
//               </div>
//             </div>
//           </div>
//         </div>
//         <div className="col-lg-6">
//           <div className="shadow-sm p-3 mb-5 bg-body rounded">
//             <div className="table-responsive table-no-border">
//               {cart.length <= 0 && 'No Products'}
//               <table
//                 className={cart.length > 0 ? 'table table-wishlist' : 'd-none'}
//               >
//                 <thead>
//                   <tr className="main-heading">
//                     <th className="custome-checkbox start" colSpan="2">
//                       Your Order
//                     </th>
//                     <th scope="col">Unit Price</th>
//                   </tr>
//                 </thead>
//                 <tbody>
//                   {cart.map((item) => (
//                     <tr key={item.id}>
//                       <td className="image product-thumbnail">
//                         <img src={item.product.images[0]} alt="" />
//                       </td>

//                       <td className="product-des product-name">
//                         <h6 className="product-name">
//                           <Link to="/products">{item.product.name}</Link>
//                         </h6>
//                         <div className="product-rate-cover">
//                           <div className="product-rate d-inline-block">
//                             <div
//                               className="product-rating"
//                               style={{
//                                 width: '90%',
//                               }}
//                             ></div>
//                           </div>
//                           <span className="font-small ml-5 text-muted">
//                             {' '}
//                             (4.0)
//                           </span>
//                           <span className="font-small ml-50 text-muted">
//                             X {item.quantity}
//                           </span>
//                         </div>
//                       </td>
//                       <td className="price" data-title="Price">
//                         <h4 className="text-brand">₹{item.product.price}</h4>
//                       </td>
//                     </tr>
//                   ))}
//                 </tbody>
//               </table>
//             </div>
//             <div className="mb-20">
//               {cart?.length > 0 ? (
//                 <CartTotalSection cartTotal={cartTotal} isCtaShown={false} />
//               ) : (
//                 <h4>No Cart Items Found</h4>
//               )}
//             </div>
//             <div className="px-2">
//               <h5>Payment</h5>
//               <RadioButton
//                 filters={paymentStatus}
//                 handleRadioButton={(e) => {
//                   console.log(e.target.value, 'event')
//                 }}
//               />

//               <a
//                 href="#/"
//                 className="btn btn-sm mt-4"
//                 onClick={onPlaceOrderHandler}
//               >
//                 Place Order
//               </a>
//             </div>
//           </div>
//         </div>
//       </div>
//     </div>
//   </section>
// </Layout>

export default Checkout

{
  /* <div className="row">
<div className="col-lg-6 mb-40">
  <div className="row">
    <div className="col-md-12 px-4 mt-50">
      <h4 className="text-brand mb-4">Please Select Address</h4>
      <AddressSection
        setSelectedShippingAddressId={setSelectedShippingAddressId}
        selectedShippingAddressId={selectedShippingAddressId}
      />
    </div>

    <div className="col-md-12">
      <div className="px-4 mt-50">
        <h4 className="text-brand mb-3">Prescription</h4>
        <div className="d-flex align-items-center mb-10">
          <Upload listType="picture-card" {...propsPrescriptions}>
            <img
              src={prescriptionIcon}
              alt="prescription-add"
              height={80}
              width={80}
            />
          </Upload>
        </div>
      </div>
    </div>
  </div>
</div>
<div className="col-lg-6">
  <div className="shadow-sm p-3 mb-5 bg-body rounded">
    <div className="table-responsive table-no-border">
      {cart.length <= 0 && 'No Products'}
      <table
        className={
          cart.length > 0 ? 'table table-wishlist' : 'd-none'
        }
      >
        <thead>
          <tr className="main-heading">
            <th className="custome-checkbox start" colSpan="2">
              Your Order
            </th>
            <th scope="col">Unit Price</th>
          </tr>
        </thead>
        <tbody>
          {cart.map((item) => (
            <tr key={item.id}>
              <td className="image product-thumbnail">
                <img src={item.product.images[0]} alt="" />
              </td>

              <td className="product-des product-name">
                <h6 className="product-name">
                  <Link to="/products">{item.product.name}</Link>
                </h6>
                <div className="product-rate-cover">
                  <div className="product-rate d-inline-block">
                    <div
                      className="product-rating"
                      style={{
                        width: '90%',
                      }}
                    ></div>
                  </div>
                  <span className="font-small ml-5 text-muted">
                    {' '}
                    (4.0)
                  </span>
                  <span className="font-small ml-50 text-muted">
                    X {item.quantity}
                  </span>
                </div>
              </td>
              <td className="price" data-title="Price">
                <h4 className="text-brand">
                  ₹{item.product.price}
                </h4>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
    <div className="mb-20">
      {cart?.length > 0 ? (
        <CartTotalSection
          cartTotal={cartTotal}
          isCtaShown={false}
        />
      ) : (
        <h4>No Cart Items Found</h4>
      )}
    </div>
    <div className="px-2">
      <h5>Payment</h5>
      <RadioButton
        filters={paymentStatus}
        handleRadioButton={(e) => {
          console.log(e.target.value, 'event')
        }}
      />

      <a
        href="#/"
        className="btn btn-sm mt-4"
        onClick={onPlaceOrderHandler}
      >
        Place Order
      </a>
    </div>
  </div>
</div>
</div> */
}

{
  /* <div>
                <a href="#/" className="btn btn-sm">
                  Edit
                </a>
              </div> */
}

{
  /* <img src={require('assets/imgs/page/file.png')} alt="" />
                <div className="ml-40">
                  <h6 className="product-name">Dr. Jhon</h6>
                  <span className="font-small text-muted">18-2-2022</span>
                </div> */
}
