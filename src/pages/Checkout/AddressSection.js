/* eslint-disable react/prop-types */
import React from 'react'
import { useEffect } from 'react'
import { useState } from 'react'
// import {
//   addUserAddressApi,
//   getUserAddressApi,
//   editUserAddressApi,
//   deleteUserAddressApi,
// } from 'api/user'

import authService from 'service/auth'

import { ErrorMessage, Field, Form, Formik } from 'formik'
import ErrorText from 'components/common/ErrorText'
import * as Yup from 'yup'
import { toast } from 'react-toastify'
import { useRef } from 'react'
import scrollToTop from 'util/scrollToTop'
import { useDispatch, useSelector } from 'react-redux'
import { setUser } from 'redux/slices/authSlice'
import { Col, Row } from 'antd'
import constantsService from 'service/constants'
// import { scrollToTop, scrollToBottom } from '../../utils/scroll'

const AddressSection = ({
  setSelectedShippingAddressId,
  selectedShippingAddressId,
  setSelectedBillingAddressId,
  selectedBillingAddressId,
}) => {
  const { user } = useSelector((state) => state.auth)

  const dispatch = useDispatch()

  const defaultInitialValues = {
    line1: '',
    city: '',
    state: '',
    country: '',
    phone: '',
    zipcode: '',
  }

  const [isAddressFormOpen, setIsAddressFormOpen] = useState(false)
  const [selectedAddress, setSelectedAddress] = useState(null)

  const [isBillingAddressSameAsShipping, setIsBillingAddressSameAsShipping] =
    useState(true)

  const [states, setStates] = useState([])

  // const [addressDetails, setAddressDetails] = useState([])
  const [initialValues, setInitialValues] = useState(defaultInitialValues)
  const formikRef = useRef()

  const validationSchema = Yup.object({
    line1: Yup.string().required(' Required'),
    city: Yup.string().required(' Required'),
    state: Yup.string().required(' Required'),
    phone: Yup.string().required(' Required'),
    zipcode: Yup.string().required(' Required'),
  })

  const onSubmitHandler = async (
    { line1, city, state, phone, zipcode },
    { setSubmitting }
  ) => {
    let sendingValues = {
      line1,
      city,
      state,
      phone,
      zipcode,
      country: 'India',
    }
    // Create
    if (!selectedAddress?.id) {
      const res = await authService.createUserAddress(sendingValues)
      if (res) {
        toast.success('Address Added Success')
        getAddress()
        scrollToTop()
        setInitialValues(defaultInitialValues)
        setIsAddressFormOpen(false)
      }
    } else {
      const res = await authService.updateUserAddress(
        selectedAddress.id,
        sendingValues
      )
      if (res) {
        toast.success('Edit Shipping Address Successfully')
        getAddress()
        scrollToTop()
        setInitialValues(defaultInitialValues)
        setIsAddressFormOpen(false)
      }
      // } catch (err) {
      //   toast.error(err.response.data.error)
      // }
    }

    setSubmitting(false)
  }

  const deleteAddressHandler = async (id) => {
    const res = await authService.deleteUserAddress(id)
    if (res) {
      toast.success('Address Deleted Success')
      getAddress()

      setSelectedAddress(null)
      setInitialValues(defaultInitialValues)
    }
  }

  const getConstants = async () => {
    const consts = await constantsService.getConstants()

    setStates(Object.values(consts.GENERAL.STATES.INDIA))
  }

  useEffect(() => {
    getAddress()
    getConstants()
  }, [])

  useEffect(() => {
    if (selectedAddress) {
      setInitialValues(selectedAddress)
    }
  }, [selectedAddress])

  const getAddress = async () => {
    const { data } = await authService.getUserProfile()
    if (data) {
      if (data.address[0]?.id) {
        if (selectedShippingAddressId === null) {
          setSelectedShippingAddressId(data.address[0].id)
        }

        if (selectedBillingAddressId === null) {
          setSelectedBillingAddressId(data.address[0].id)
        }
        // data.address?.map((address) => {
        //   if (address.id !== selectedShippingAddressId) {
        //     setSelectedShippingAddressId(data.address[0].id)
        //   }
        //   if (address.id !== selectedBillingAddressId) {
        //     setSelectedBillingAddressId(data.address[0].id)
        //   }
        // })
        dispatch(setUser({ ...user, address: data.address }))
      } else {
        setSelectedShippingAddressId(null)
        setSelectedBillingAddressId(null)
        setIsBillingAddressSameAsShipping(false)
      }
    }
  }

  // Check if isBillingAddressSameAsShipping is true
  useEffect(() => {
    if (isBillingAddressSameAsShipping) {
      setSelectedBillingAddressId(selectedShippingAddressId)
    }
  }, [isBillingAddressSameAsShipping])

  // If already checked isBillingAddressSameAsShipping is true then set selectedBillingAddressId to selectedShippingAddressId
  useEffect(() => {
    if (isBillingAddressSameAsShipping) {
      setSelectedBillingAddressId(selectedShippingAddressId)
    }
  }, [selectedShippingAddressId])

  return (
    <div>
      <div>
        <h4 className="text-brand mb-4">Please Select Address</h4>

        <div className="mb-10">
          {user.address?.length > 0 ? (
            <Row gutter={16}>
              {user.address.map((address, i) => (
                <Col sm={24} xs={24} md={8} key={i}>
                  <div
                    className={
                      selectedShippingAddressId === address.id
                        ? 'card card-active p-10 mb-10'
                        : 'card p-10 mb-10'
                    }
                    onClick={() => setSelectedShippingAddressId(address.id)}
                    role="button"
                  >
                    <address>
                      {address.line1}
                      <br />
                      {address.zipcode}, {address.city}, {address.state}
                      <br />
                      {address.phone}
                    </address>
                    <div className="d-flex">
                      <a
                        href="#/"
                        className="btn-small"
                        onClick={() => {
                          setIsAddressFormOpen(true)
                          setSelectedAddress(address)
                        }}
                      >
                        Edit
                      </a>
                      <a
                        href="#/"
                        className="btn-small ml-20"
                        style={{ color: 'red' }}
                        onClick={() => deleteAddressHandler(address.id)}
                      >
                        Delete
                      </a>
                    </div>
                    {selectedShippingAddressId === address.id && (
                      <div className="card-active-dot"></div>
                    )}
                  </div>
                </Col>
              ))}
            </Row>
          ) : (
            <p>No Address Found</p>
          )}
        </div>
      </div>

      {/* Billing Address */}

      <div className="mt-40">
        <h4 className="text-brand mb-4">Billing Address</h4>
        <div className="custome-checkbox">
          <input
            className="form-check-input"
            type="checkbox"
            name="checkbox"
            id="exampleCheckbox1"
            checked={isBillingAddressSameAsShipping}
            onChange={(e) => {
              setIsBillingAddressSameAsShipping(e.target.checked)
              if (e.target.checked) {
                setSelectedBillingAddressId(selectedShippingAddressId)
              }
            }}
          />
          <label className="form-check-label" htmlFor="exampleCheckbox1">
            Same as Shipping Address
          </label>
        </div>

        {!isBillingAddressSameAsShipping && (
          <>
            <div className="mb-10">
              {user.address?.length > 0 ? (
                <Row gutter={16}>
                  {user.address.map((address, i) => (
                    <Col key={i}>
                      <div
                        className={
                          selectedBillingAddressId === address.id
                            ? 'card card-active p-10 mb-10'
                            : 'card p-10 mb-10'
                        }
                        onClick={() => setSelectedBillingAddressId(address.id)}
                        role="button"
                      >
                        <address>
                          {address.line1}
                          <br />
                          {address.zipcode}, {address.city}, {address.state}
                          <br />
                          {address.phone}
                        </address>
                        <div className="d-flex">
                          <a
                            href="#/"
                            className="btn-small"
                            onClick={() => {
                              setIsAddressFormOpen(true)
                              setSelectedAddress(address)
                            }}
                          >
                            Edit
                          </a>
                          <a
                            href="#/"
                            className="btn-small ml-20"
                            style={{ color: 'red' }}
                            onClick={() => deleteAddressHandler(address.id)}
                          >
                            Delete
                          </a>
                        </div>
                        {selectedBillingAddressId === address.id && (
                          <div className="card-active-dot"></div>
                        )}
                      </div>
                    </Col>
                  ))}
                </Row>
              ) : (
                <p>No Address Found</p>
              )}
            </div>
          </>
        )}
      </div>
      <div className="add-new-address mt-30">
        <button
          className="btn btn-small"
          onClick={() => {
            setIsAddressFormOpen(true)
            setSelectedAddress(null)
            setInitialValues(defaultInitialValues)
          }}
        >
          <span className="fi-rs-plus"></span> Add New Address
        </button>
      </div>

      {isAddressFormOpen && (
        <div className="address-card-wrapper mt-30">
          <h3 className="inner-page-heading">Address</h3>
          <div className="card-form-wrapper mt-20">
            <Formik
              initialValues={initialValues}
              onSubmit={onSubmitHandler}
              validationSchema={validationSchema}
              enableReinitialize
              innerRef={formikRef}
            >
              {(fomrik) => {
                const { touched, errors } = fomrik

                return (
                  <Form>
                    <div className="row">
                      <div className="col-md-6">
                        <div className="form-group">
                          <label htmlFor="fullName">Line 1</label>
                          <Field
                            type="text"
                            name="line1"
                            className="form-control"
                            placeholder="Line 1"
                            style={{
                              border: `${
                                touched.line1 && errors.line1
                                  ? '1px solid red'
                                  : ''
                              }`,
                            }}
                          />
                          <ErrorMessage name="line1" component={ErrorText} />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="form-group">
                          <label htmlFor="city">City</label>
                          <Field
                            type="text"
                            name="city"
                            className="form-control"
                            placeholder="city"
                            style={{
                              border: `${
                                touched.city && errors.city
                                  ? '1px solid red'
                                  : ''
                              }`,
                            }}
                          />
                          <ErrorMessage name="city" component={ErrorText} />
                        </div>
                      </div>
                    </div>

                    <div className="row">
                      <div className="col-md-6">
                        <div className="form-group">
                          <label htmlFor="state">State</label>
                          <Field
                            as="select"
                            name="state"
                            className="form-control"
                            placeholder="state"
                            style={{
                              border: `${
                                touched.state && errors.state
                                  ? '1px solid red'
                                  : ''
                              }`,
                            }}
                          >
                            {states?.map((state) => (
                              <option key={state} value={state}>
                                {state}
                              </option>
                            ))}
                          </Field>
                          <ErrorMessage name="state" component={ErrorText} />
                        </div>
                      </div>
                      <div className="col-md-6">
                        <div className="form-group">
                          <label htmlFor="">Phone</label>
                          <Field
                            type="text"
                            name="phone"
                            className="form-control"
                            placeholder="Eg: +919988776655"
                            style={{
                              border: `${
                                touched.phone && errors.phone
                                  ? '1px solid red'
                                  : ''
                              }`,
                            }}
                          />
                          <ErrorMessage name="phone" component={ErrorText} />
                        </div>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-md-6">
                        <div className="form-group">
                          <label htmlFor="zipcode">Zipcode</label>
                          <Field
                            type="text"
                            name="zipcode"
                            className="form-control"
                            placeholder="zipcode"
                            style={{
                              border: `${
                                touched.zipcode && errors.zipcode
                                  ? '1px solid red'
                                  : ''
                              }`,
                            }}
                          />
                          <ErrorMessage name="zipcode" component={ErrorText} />
                        </div>
                      </div>
                      <div className="col-md-6"></div>
                    </div>

                    <div className="form-btns-wrapper">
                      <a
                        href="#/"
                        className="btn-small mr-20"
                        style={{ color: 'red' }}
                        onClick={() => {
                          setIsAddressFormOpen(false)
                          setInitialValues(defaultInitialValues)
                        }}
                      >
                        Cancel
                      </a>
                      <button className="btn">Save</button>
                    </div>
                  </Form>
                )
              }}
            </Formik>
          </div>
        </div>
      )}
    </div>
  )
}

export default AddressSection
