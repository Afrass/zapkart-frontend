/* eslint-disable react/prop-types */
import CartTotalSection from 'pages/Cart/CartTotalSection'
import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Link } from 'react-router-dom'
import { toast } from 'react-toastify'
import { setCartTotal } from 'redux/slices/cartSlice'
// import { getSettings } from 'redux/slices/settingSlice'
import orderService from 'service/order'

// import RadioButton from 'components/common/RadioButton'

const CheckoutSection = ({
  cart,
  paymentLoading,
  onPlaceOrderHandler,
  setCouponCode,
  couponCode,
}) => {
  const { cartTotal, totalMrp } = useSelector((state) => state.cart)
  const [couponApplied, setCouponApplied] = useState(false)
  const [couponDiscount, setCouponDiscount] = useState(null)

  // const { settings } = useSelector((state) => state.setting)
  // const [deliveryCharge, setDeliveryCharge] = useState(0)
  const dispatch = useDispatch()

  const onCouponSubmit = async (e) => {
    e.preventDefault()
    const couponData = await orderService.applyCoupon(couponCode)

    if (couponData) {
      dispatch(setCartTotal(cartTotal - couponData.discount))

      toast.success('Coupon applied successfully')
      setCouponApplied(true)
      setCouponDiscount(couponData.discount)
    }
  }

  const clearCoupon = () => {
    dispatch(setCartTotal(cartTotal + couponDiscount))
    setCouponApplied(false)
    setCouponDiscount(null)
    setCouponCode('')
  }

  // const getDeliveryCharge = (deliveryCharges, cartTotal) => {
  //   const charges = []
  //   deliveryCharges?.forEach((cur) => {
  //     if (cur.startAmount <= cartTotal) {
  //       charges.push(cur?.charge)
  //     }
  //   })
  //   if (charges?.length > 0) {
  //     return charges[charges?.length - 1]
  //   } else {
  //     return 'Free Shipping'
  //   }
  // }

  // useEffect(() => {
  //   const deliveryCharge = getDeliveryCharge(
  //     settings.deliveryCharges,
  //     cartTotal
  //   )
  //   setDeliveryCharge(deliveryCharge)
  // }, [settings, cartTotal])

  // useEffect(() => {
  //   dispatch(getSettings())
  // }, [])

  return (
    <div>
      <div className="shadow-sm p-3 mb-5 bg-body rounded">
        <div className="table-responsive table-no-border">
          {cart.length <= 0 && 'No Products'}
          <table
            className={cart.length > 0 ? 'table table-wishlist' : 'd-none'}
          >
            <thead>
              <tr className="main-heading">
                <th className="custome-checkbox start" colSpan="2">
                  Your Order
                </th>
                <th scope="col">Unit Price</th>
              </tr>
            </thead>
            <tbody>
              {cart.map((item) => (
                <tr key={item.id}>
                  <td className="image product-thumbnail">
                    <img src={item?.product?.images[0]} alt="" />
                  </td>

                  <td className="product-des product-name">
                    <h6 className="product-name">
                      <Link
                        to={`/product/${item.product.productTemplateId}/${item.product.id}`}
                      >
                        {item.product.name}
                      </Link>{' '}
                      <span className="font-small ml-50 text-muted">
                        X {item.quantity}
                      </span>
                    </h6>
                    {/* <div className="product-rate-cover"> */}
                    {/* <div className="product-rate d-inline-block">
                        <div
                          className="product-rating"
                          style={{
                            width: '90%',
                          }}
                        ></div>
                      </div> */}
                    {/* <span className="font-small ml-5 text-muted"> (4.0)</span> */}
                    {/* <span className="font-small ml-50 text-muted">
                      X {item.quantity}
                    </span> */}
                    {/* </div> */}
                  </td>
                  <td className="price" data-title="Price">
                    <span
                      style={{
                        color: 'rgba(0, 0, 0, 0.45)',
                        textDecoration: 'line-through',
                      }}
                    >
                      ₹{item.product.mrpPrice}/-
                    </span>
                    <h4 className="text-brand">₹{item.product.price}/-</h4>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
        <div className="mb-20">
          {cart?.length > 0 ? (
            <CartTotalSection
              cartTotal={cartTotal}
              isCtaShown={false}
              couponDiscount={couponDiscount}
              couponCode={couponCode}
              totalMrp={totalMrp}
              // deliveryCharge={deliveryCharge}
            />
          ) : (
            <h4>No Cart Items Found</h4>
          )}
        </div>

        <div className="mb-30 mt-50">
          <div className="heading_s1 mb-2">
            <h4>Apply Coupon</h4>
          </div>
          {couponApplied ? (
            <div className="coupon-success-cont d-flex align-items-center">
              <span className="success-text">
                Coupon {couponCode} Applied Successfully
              </span>
              <span
                className="fi-rs-cross-circle close-btn-coupon"
                onClick={clearCoupon}
              />
            </div>
          ) : (
            <div className="total-amount">
              <div className="left">
                <div className="coupon">
                  <form onSubmit={onCouponSubmit} target="_blank">
                    <div className="form-row row justify-content-center">
                      <div className="form-group col-lg-6">
                        <input
                          className="font-medium"
                          name="Coupon"
                          placeholder="Enter Your Coupon"
                          style={{ height: '42px' }}
                          onChange={(e) => setCouponCode(e.target.value)}
                          value={couponCode}
                        />
                      </div>
                      <div className="form-group col-lg-6">
                        <button className="btn  btn-sm">
                          <i className="fi-rs-label mr-10"></i>
                          Apply
                        </button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          )}
        </div>
        {/* <div className="heading_s1 mb-3">
          <h4>Delivery Charge: ₹{deliveryCharge}</h4>
        </div> */}

        <div className="px-2">
          {/* <h5>Payment</h5>
          <RadioButton
            filters={paymentStatus}
            handleRadioButton={(e) => {
              console.log(e.target.value, 'event')
            }}
          /> */}

          <button
            href="#/"
            className="btn btn-sm mt-4"
            onClick={onPlaceOrderHandler}
            disabled={paymentLoading}
          >
            Place Order
          </button>
        </div>
      </div>
    </div>
  )
}

export default CheckoutSection
