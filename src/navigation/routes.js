/* eslint-disable react/prop-types */
import React, { Suspense } from "react";

import {
  unstable_HistoryRouter as HistoryRouter,
  Routes,
  Route,
  Navigate,
} from "react-router-dom";

import PrivateRoute from "./PrivateRoute";
import Preloader from "../components/common/Preloader";
import { history } from "redux/store";

const routes = [
  // Home
  {
    path: "/",
    component: React.lazy(() => import("../pages/Home")),
    exact: true,
    authorize: false,
  },
  {
    path: "/categories",
    component: React.lazy(() => import("../pages/Categories")),
    exact: true,
    authorize: false,
  },
  {
    path: "/brands",
    component: React.lazy(() => import("../pages/Brands")),
    exact: true,
    authorize: false,
  },

  // Contact
  {
    path: "/contact",
    component: React.lazy(() => import("../pages/Contact")),
    exact: true,
    authorize: false,
  },

  // About
  // {
  //   path: '/about',
  //   component: React.lazy(() => import('../pages/About')),
  //   exact: true,
  //   authorize: false,
  // },

  // Wishlist
  {
    path: "/wishlist",
    component: React.lazy(() => import("../pages/Wishlist")),
    exact: true,
    authorize: true,
  },

  // Cart
  {
    path: "/cart",
    component: React.lazy(() => import("../pages/Cart")),
    exact: true,
    authorize: false,
  },

  // Orders
  // {
  //   path: '/orders',
  //   component: React.lazy(() => import('../pages/Orders')),
  //   exact: true,
  //   authorize: false,
  // },

  // Checkout
  {
    path: "/checkout",
    component: React.lazy(() => import("../pages/Checkout")),
    exact: true,
    authorize: false,
  },

  // Account
  {
    path: "/account",
    component: React.lazy(() => import("../pages/Account")),
    exact: true,
    authorize: true,
  },
  // Products
  // {
  //   path: '/products/category/:id',
  //   component: React.lazy(() => import('../pages/Products')),
  //   exact: true,
  //   authorize: false,
  // },
  {
    path: "/products",
    component: React.lazy(() => import("../pages/Products")),
    exact: true,
    authorize: false,
  },
  {
    path: "/products/medicine",
    component: React.lazy(() => import("../pages/MedicineProducts")),
    exact: true,
    authorize: false,
  },

  // {
  //   path: '/products/search',
  //   component: React.lazy(() => import('../pages/Products')),
  //   exact: true,
  //   authorize: false,
  // },

  {
    path: "/product/:productTemplateId/:id",
    component: React.lazy(() => import("../pages/ProductDetails")),
    exact: true,
    authorize: false,
  },

  {
    path: "/product/:productTemplateId/:id/medicine",
    component: React.lazy(() => import("../pages/MedicineProductDetails")),
    exact: true,
    authorize: false,
  },

  // 404
  {
    path: "/404",
    component: React.lazy(() => import("../pages/404")),
    exact: true,
    authorize: false,
  },

  // Auth
  {
    path: "/login",
    component: React.lazy(() => import("../pages/auth/Login")),
    exact: true,
    authorize: false,
  },
  {
    path: "/register",
    component: React.lazy(() => import("../pages/auth/Register/index")),
    exact: true,
    authorize: false,
  },
  {
    path: "/forgot-password",
    component: React.lazy(() => import("../pages/auth/ForgotPassword")),
    exact: true,
    authorize: false,
  },
  {
    path: "/register/complete",
    component: React.lazy(() =>
      import("../pages/auth/Register/RegisterComplete")
    ),
    exact: true,
    authorize: false,
  },
  {
    path: "/user-authdetails-update",
    component: React.lazy(() => import("../pages/auth/AuthDetails")),
    exact: true,
    authorize: true,
  },
  {
    path: "/information/:id",
    component: React.lazy(() => import("../pages/Information")),
    exact: true,
    authorize: false,
  },

  {
    path: "/order-success/:orderId",
    component: React.lazy(() => import("../pages/OrderSuccess")),
    exact: true,
    authorize: true,
  },
];

const Router = () => {
  return (
    <>
      <HistoryRouter history={history}>
        <Routes>
          {/* <Route path="/" exact>
                <Redirect to="/home" />
              </Route> */}
          {routes.map((route) => {
            const { component: Component } = route;
            if (route.authorize)
              return (
                <Route
                  path={route.path}
                  key={route.key}
                  exact={route.exact}
                  element={
                    <Suspense fallback={<Preloader />}>
                      <PrivateRoute component={route.component} {...route} />
                    </Suspense>
                  }
                />
              );
            return (
              <Route
                path={route?.path}
                element={
                  <Suspense fallback={<Preloader />}>
                    <Component />
                  </Suspense>
                }
                key={route?.path}
                exact={route?.exact}
              />
            );
          })}
          <Route path="*" element={<Navigate to="/404" />} />

          {/* <Navigate from="*" to="/home" /> */}
        </Routes>

        {/* <Route component={NotFoundPage} /> */}
      </HistoryRouter>
    </>
  );
};

export default Router;
