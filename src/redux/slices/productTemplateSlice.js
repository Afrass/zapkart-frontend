/* eslint-disable no-loop-func */
// Add Up Redux Slices Here
// create redux slice function as reducer

// Every Page has their own redux slice function
import { createSlice } from '@reduxjs/toolkit'
import productTemplateService from 'service/productTemplateService'

export const productTemplateSlice = createSlice({
  name: 'productTemplate',
  initialState: {
    // This is entire product template data
    productsTemplate: [],
    // This is defaults products for show case
    defaultProducts: [],
    // Single selected current product template
    productTemplate: {},

    // This is another products from selected variants
    otherProductsFromVariant: [],

    // This is selected product from product template
    product: {},

    filters: [],

    totalProductTemplateCount: 0,

    loading: false,

    searchSuggestions: [],
  },
  reducers: {
    setProductsTemplate: (state, action) => {
      state.productsTemplate = action.payload
    },
    setProductTemplate: (state, action) => {
      state.productTemplate = action.payload
    },
    setProduct: (state, action) => {
      state.product = action.payload
    },
    setSearchSuggestions: (state, action) => {
      state.searchSuggestions = action.payload
    },

    setDefaultProducts: (state, action) => {
      state.defaultProducts = action.payload
    },

    setOtherProductsFromVariant: (state, action) => {
      state.otherProductsFromVariant = action.payload
    },

    setFilters: (state, action) => {
      state.filters = action.payload
    },
    setProductTemplateTotalCount: (state, action) => {
      state.totalProductTemplateCount = action.payload
    },

    setResetProductsDetails: (state) => {
      state.product = {}
      state.otherProductsFromVariant = []
      state.productTemplate = {}
    },

    // Loading handler
    setLoading: (state, action) => {
      state.loading = action.payload
    },
  },
})

export const {
  setProductsTemplate,
  setProduct,
  setDefaultProducts,
  setOtherProductsFromVariant,
  setLoading,
  setFilters,
  setProductTemplate,
  setSearchSuggestions,
  setProductTemplateTotalCount,
  setResetProductsDetails,
} = productTemplateSlice.actions

export const getProductsTemplateBySearch =
  (searchQuery) => async (dispatch) => {
    const data = await productTemplateService.getProductsTemplate(
      '',
      searchQuery
    )
    if (data.data) {
      const searchSuggestions = data.data.map((cur) => {
        return {
          text: cur?.name,
          image: cur?.images?.length > 0 ? cur?.images[0] : null,
          category: cur?.category?.name ? cur?.category?.name : null,
        }
      })

      dispatch(setSearchSuggestions(searchSuggestions))
    }
  }

export const getProductsTemplate =
  (paginationQuery = '', filterQuery = '') =>
  async (dispatch) => {
    dispatch(setLoading(true))
    const data = await productTemplateService.getProductsTemplate(
      paginationQuery,
      filterQuery
    )
    if (data) {
      //   for (let i = 0; i < data.length; i++) {
      //     // if (data[i].listingType === 'ProductTemplates') {
      const defaultProduct = []
      console.log(data.total, 'pslkwl')
      dispatch(setProductTemplateTotalCount(data.total))
      //     //   data[i].listingItems.map((item) => {
      //     //     if (item?.products?.length > 0) {
      //     //       item?.products?.forEach((prod) => {
      //     //         prod.default && defaultProduct.push(prod)
      //     //       })
      //     //     } else {
      //     //       defaultProduct.push(item)
      //     //     }
      //     //   })
      //     //   data[i].defaultProduct = defaultProduct
      //     // }
      //     data[i].products.forEach((item) => {
      //       if (item?.length > 0) {
      //         item.default && defaultProduct.push(item)
      //       } else {
      //         defaultProduct.push(data[i])
      //       }
      //     })
      //     data[i].defaultProduct = defaultProduct
      //   }
      data.data.forEach((item) => {
        if (item.products.length > 0) {
          item.products.forEach((prod) => {
            prod.default &&
              defaultProduct.push({
                ...item,
                ...prod,
                images: prod?.images?.length > 0 ? prod?.images : item?.images,
              })
          })
        } else {
          defaultProduct.push(item)
        }
      })

      dispatch(setProductsTemplate(data))
      dispatch(setDefaultProducts(defaultProduct))
    }
    dispatch(setLoading(false))
  }

export const getProductfromProductTemplate =
  (productTemplateId, productId) => async (dispatch) => {
    dispatch(setLoading(true))
    const data = await productTemplateService.getProductTemplateById(
      productTemplateId
    )
    console.log('show-mwrr', data)

    if (data) {
      const productTemplate = data
      dispatch(setProductTemplate(productTemplate))

      if (productId) {
        const product = productTemplate.products.find(
          (item) => item.id === productId
        )
        dispatch(setProduct(product))

        // We need other related products from variant
        const getOtherProductsFromVariant = productTemplate.products.filter(
          (item) => item?.variant?.id === product?.variant?.id
        )

        const removeCurrentProduct = getOtherProductsFromVariant.filter(
          (item) => item?.id !== product?.id
        )

        dispatch(setOtherProductsFromVariant(removeCurrentProduct))
      }
      dispatch(setLoading(false))

      if (productTemplate?.productType === 'Medicine') {
        const loadedSubs = await productTemplateService.getProductTemplateSubs(
          productTemplateId
        )

        if (loadedSubs?.length > 0) {
          dispatch(
            setProductTemplate({ ...productTemplate, substitutes: loadedSubs })
          )
        }
      }
    }
    dispatch(setLoading(false))
  }

export const onProductVariantChange =
  (variantId) => async (dispatch, getState) => {
    const { productTemplate } = getState().productTemplate

    if (productTemplate) {
      const selectedVariantBasedProduct = productTemplate.products.find(
        (item) => item.variant.id === variantId
      )
      if (selectedVariantBasedProduct) {
        console.log(selectedVariantBasedProduct, 'selectedVariantBasedProduct')
        dispatch(setProduct(selectedVariantBasedProduct))

        // We need other related products from variant
        const getOtherProductsFromVariant = productTemplate.products.filter(
          (item) => item.variant.id === selectedVariantBasedProduct.variant.id
        )

        const removeCurrentProduct = getOtherProductsFromVariant.filter(
          (item) => item.id !== selectedVariantBasedProduct.id
        )

        dispatch(setOtherProductsFromVariant(removeCurrentProduct))
      } else {
        dispatch(setProduct(productTemplate))
        dispatch(setOtherProductsFromVariant([]))
      }
    }
  }

// export const getProductTemplate = (id) => async (dispatch) => {
//   dispatch(setLoading(true))
//   const data = await productTemplateService.getProductTemplateById(id)

//   data.defaultProduct = []

//   if (data.products?.length > 0) {
//     data.products.forEach((item) => {
//       item.default && data.defaultProduct.push(item)
//     })
//   } else {
//     data.defaultProduct = data
//   }

//   if (data) {
//     dispatch(setProductTemplate(data))
//   }
//   dispatch(setLoading(false))
// }

export default productTemplateSlice.reducer
