// Add Up Redux Slices Here
// create redux slice function as reducer

// Every Page has their own redux slice function
import { createSlice } from '@reduxjs/toolkit'
import categoryService from 'service/category'

// import { notification } from 'antd';
// import history from 'utils/history';
// TODO -> Handle api errors

export const categorySlice = createSlice({
  name: 'category',
  initialState: {
    categories: [],
    category: {},
    loading: false,
  },
  reducers: {
    setCategories: (state, action) => {
      state.categories = action.payload
    },
    setCategory: (state, action) => {
      state.category = action.payload
    },

    // Loading handler
    setLoading: (state, action) => {
      state.loading = action.payload
    },
  },
})

export const { setCategories, setCategory, setLoading } = categorySlice.actions

export const getCategories = () => async (dispatch) => {
  dispatch(setLoading(true))
  const data = await categoryService.getCategories()
  if (data) {
    dispatch(setCategories(data))
  }
  dispatch(setLoading(false))
}

export const getCategory = (id) => async (dispatch) => {
  dispatch(setLoading(true))
  const data = await categoryService.getCategoryById(id)
  if (data) {
    dispatch(setCategory(data))
  }
  dispatch(setLoading(false))
}

export default categorySlice.reducer
