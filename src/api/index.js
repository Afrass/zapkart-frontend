import React from "react";
import axios from "axios";
import { errorValidator } from "util/errorValidator";
import { toast } from "react-toastify";

// Please look service folder we are using in this instance there

const VerificationToast = () => (
  <div>
    <div className="toast-header">
      <p className="mr-auto">
        Your Are Not Verified <br />
        <b>
          <a href="/user-authdetails-update">
            Click Here To Complete Your Verification
          </a>
        </b>
      </p>
    </div>
  </div>
);

const instance = axios.create({
  baseURL: process.env.REACT_APP_BASE_URL,
});

// runs on each request.
instance.interceptors.request.use(
  async (config) => {
    const token = localStorage.getItem("token");
    config.headers = {
      Authorization: `Bearer ${token}`,
      deviceToken: window.localStorage.getItem("deviceToken"),
    };

    return config;
  },
  (error) => {
    console.log(error, "fromapi");
    errorValidator(error.response.data);
    Promise.reject(error);
  }
);

let alreadyShownError = false;

instance.interceptors.response.use(
  (response) => {
    // if (response.config.parse) {
    //     //perform the manipulation here and change the response object
    // }
    return response;
  },
  (error) => {
    if (error.response.status === 401) {
      console.log("show-errerer");
      // if (store.getState().auth.authorized) {
      if (
        window.location.pathname !== "/login" &&
        window.location.pathname !== "/user-authdetails-update"
      ) {
        // window.location.href = '/user-authdetails-update'
        if (!alreadyShownError) {
          toast.warning(VerificationToast);
        }

        alreadyShownError = true;
      }

      if (alreadyShownError) {
        setTimeout(() => {
          alreadyShownError = false;
        }, 3000);
      }
      // } else {
      //   // store.dispatch(setLogout())
      // }
      // window.location.href = '/user-authdetails-update'
    }

    errorValidator(error.response.data);
    return Promise.reject(error);
  }
);

export default instance;
