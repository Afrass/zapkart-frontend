// No Need to look these files checkout services folder now

import instance from '..'

export const getProducts = () => instance.get('/api/user/signup')

export const addToCartApi = (data) =>
  instance.post('/api/backend/v1/cart/addToCart', data)

export const removeFromCartApi = (data) =>
  instance.post('/api/backend/v1/cart/removeFromCart', data)

export const getItemsApi = () => instance.get('/api/backend/v1/cart')

// wishlists

export const addToWishlistApi = (data) =>
  instance.post('/api/backend/v1/users/addToWishlist', data)

export const removeFromWishlistApi = (data) =>
  instance.post('/api/backend/v1/cart/removeFromCart', data)

export const getWishlistApi = () =>
  instance.get('/api/backend/v1/users/wishlist')

// Orders
export const getOrdersApi = () => instance.get('/api/backend/v1/order')
export const createOrderApi = (data) =>
  instance.post('/api/backend/v1/order/create', data)

// Checkout
export const checkoutApi = (data) =>
  instance.post('/api/backend/v1/cart/checkout', data)

// Payment Method
export const getPaymentMethodApi = (data) =>
  instance.get('/api/backend/v1/paymentmethod', data)
