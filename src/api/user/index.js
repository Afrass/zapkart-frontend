// No Need to look these files checkout services folder now

import instance from '..'

// We only use these 3 endpoints for a purpose
export const getUserProfileApi = () => instance.get('/api/v1/customers')
export const createUserApi = (data) =>
  instance.post('/api/v1/customers/register', data)
export const updateUserApi = (data) => instance.put('/api/v1/customers', data)
